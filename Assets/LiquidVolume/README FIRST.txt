************************************
*           LIQUID VOLUME          *
* (C) Copyright 2016-2017 Kronnect * 
*            README FILE           *
************************************


How to use this asset
---------------------

Thanks for purchasing LIQUID VOLUME!

Important: please read the brief quick start guide located in LiquidVolume/Documentation.


Support
-------

* Email support: contact@kronnect.me
* Website-Forum Support: http://kronnect.me
* Twitter: @KronnectGames


Other Cool Assets!
------------------

Check our other assets on the Asset Store publisher page:
https://www.assetstore.unity3d.com/en/#!/search/page=1/sortby=popularity/query=publisher:15018



Version history
---------------

Version 3.1 Current Release
- Added Turbulence Speed parameter
- Added Emission Color and Brightness parameters
- API: Added liquidSurfaceYPosition to get the world space vertical position (Y-axis) of liquid surface
- API: Added GetSpillPoint to get the world space position of the point where the liquid starts pouring over the flask when it's rotated
- Added Parent Aware option to force clamp liquid with irregular topology to parent geometry
- [Fix] Fixed Depth Aware option with Single Pass Stereo Rendering

Version 3.0.1 2017-FEB-27
- New demo scene: NonCapped
- New prefab: mug/cup of coffee!

Version 3.0 2017-FEB-14
- New flask type! Irregular. Better adaptation to non-primitive type flasks.
- Improved liquid level calculation when flask is rotated and upperLimit < 1
- [Fix] Fixed depth not being calculated correctly when objects cross the container and depth aware is enabled

Version 2.3 2017-FEB-06
- Added frecuency parameter to allow shorter waves when model scale is greater than one
- [Fix] Fixed rotation issue with react to forces enabled
- [Fix] Shadow went unsynced with liquid

Version 2.2 2017-FEB-07
- Added "React to Forces" feature (new demo scene "Forces")

Version 2.1.1 2017-JAN-03
- Improved default with no flask style to avoid false flask reflections

Version 2.1 2016-DEC-20
- New styles: Default No Flask, Reflections

Version 2.0 2016-DEC-8
- New Reflections style
- VR: Single Pass Stereo Rendering support

Version 1.3 2016-DEC-01
- New Reflections style
- Compatibility with Unity 5.5
- Fixed Center Pivot option so it saves modified changes to model prefabs

Version 1.2 2016-NOV-17
- New "Bake Current Transform" and "Center Pivot" options
- New render queue setting in inspector
- New dither shadow option in inspector

Version 1.1 2016-OCT-26
- New "Ignore Gravity" option to allow liquid rotate with the flask

Version 1.0 2016-SEP-2016 Initial Release







