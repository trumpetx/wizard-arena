using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using VRTK;

namespace UNetVRTK
{
    [RequireComponent(typeof(VRTK_InteractableObject)), RequireComponent(typeof(NetworkTransform))]
    public class NetworkInteractableObject : NetworkBehaviour
    {
        private bool isKinematicOnStart;
        private bool isUseGravityOnStart;

        [HideInInspector]
        [SyncVar(hook = "OnIsGrabbedUpdate")]
        public bool isGrabbed;

        // Transient variables used when authority is not received and we need to visually put the object back (it never moved on the server)
        private Vector3 grabPosition;
        private Quaternion grabRotation;

        private VRTK_InteractableObject interactibleObject;
        private NetworkIdentity networkIdentity;
        private NetworkTransform networkTransform;
        public NetworkIdentity[] additionalItemsToOwn;

        public override void OnStartClient()
        {
            base.OnStartClient();
            OnIsGrabbedUpdate(isGrabbed);
        }

        void Awake()
        {
            interactibleObject = GetComponent<VRTK_InteractableObject>();
            networkIdentity = GetComponent<NetworkIdentity>();
            networkTransform = GetComponent<NetworkTransform>();
            isKinematicOnStart = interactibleObject.isKinematic;
            isUseGravityOnStart = interactibleObject.useGravity;
            if (!networkIdentity.localPlayerAuthority)
            {
                Debug.LogError("UNetVRTK.NetworkInteractableObject requires that the related Network Identity has Local Player Authority.");
            }
        }
        
        public void OnEnable()
        {
            // Only enable this behavior if we're in networked 'mode'
            if (VRTKNetworkBridge.localPlayerVRTKBridge == null)
            {
                enabled = false;
                return;
            }
            interactibleObject.InteractableObjectGrabbed += HandleGrab;
            interactibleObject.InteractableObjectUngrabbed += HandleUngrab;
            interactibleObject.InteractableObjectUsed += HandleUse;
        }

        public void OnDisable()
        {
            interactibleObject.InteractableObjectGrabbed -= HandleGrab;
            interactibleObject.InteractableObjectUngrabbed -= HandleUngrab;
            interactibleObject.InteractableObjectUsed -= HandleUse;
        }

        private void HandleUse(object sender, InteractableObjectEventArgs e)
        {
            bool hadAuthority = hasAuthority;
            if (!hadAuthority)
            {
                GetAuthority();
            }
            StartCoroutine(AsyncUse(sender, DateTime.Now.AddMilliseconds(500), hadAuthority));
        }

        private IEnumerator AsyncUse(object sender, DateTime until, bool hadAuthority)
        {
            yield return new WaitUntil(() => hasAuthority || DateTime.Now > until);
            if (hasAuthority)
            {
                Debug.Log("HasAuthority to use");
                if (!hadAuthority)
                {
                    RelinquishAuthority();
                }
            }
        }

        private void GetAuthority()
        {
            // We can't request authority directly because we need authority to send a [Command]
            VRTKNetworkBridge.localPlayerVRTKBridge.CmdAssignAuthority(networkIdentity);
            foreach (NetworkIdentity identity in additionalItemsToOwn)
            {
                VRTKNetworkBridge.localPlayerVRTKBridge.CmdAssignAuthority(identity);
            }
        }

        private void HandleGrab(object sender, InteractableObjectEventArgs e)
        {
            grabPosition = transform.position;
            grabRotation = transform.rotation;

            GetAuthority();
            // This needs to be async becuase hasAuthority has not yet been set from the network
            StartCoroutine(AsyncHandleGrab(sender, DateTime.Now.AddMilliseconds(500)));
        }

        private IEnumerator AsyncHandleGrab(object sender, DateTime until)
        {
            yield return new WaitUntil(() => hasAuthority || DateTime.Now > until);
            if (hasAuthority)
            {
                CmdGrabChange(true);
            }
            else
            {
                ((VRTK_InteractableObject)sender).ForceStopInteracting();
                transform.position = grabPosition;
                transform.rotation = grabRotation;
            }
        }

        private void HandleUngrab(object sender, InteractableObjectEventArgs e)
        {
            if (hasAuthority)
            {
                // "drop" before we relinquish authority
                CmdGrabChange(false);
                RelinquishAuthority();
            }
        }

        private void RelinquishAuthority()
        {
            VRTKNetworkBridge.localPlayerVRTKBridge.CmdRemoveAuthority(networkIdentity);
            foreach (NetworkIdentity identity in additionalItemsToOwn)
            {
                VRTKNetworkBridge.localPlayerVRTKBridge.CmdRemoveAuthority(identity);
            }
        }

        /// <summary>
        /// Tell all other clients that this object is now "grabbed" (to set kinematic) or "ungrabbed" (to remove kinematic)
        /// </summary>
        [Command]
        private void CmdGrabChange(bool isGrabbed)
        {
            this.isGrabbed = isGrabbed;
        }

        /// <summary>
        /// When an object is grabbed by VRTK, it child's the object so movement is nice and smooth. 
        /// When the network assigns authority and it begins to move, gravity messes up with the prediction 
        /// becuase the other clients are unaware of the object/controller relationship.  Making the object 
        /// kinematic solves this problem.
        /// 
        /// Place any other "OnGrab/OnUngrab" code here
        /// </summary>
        /// <param name="isGrabbed">isGrabbed = kinematic</param>
        public void OnIsGrabbedUpdate(bool isGrabbed)
        {
            if (networkTransform.transformSyncMode == NetworkTransform.TransformSyncMode.SyncRigidbody3D)
            {
                interactibleObject.useGravity = isGrabbed ? false : isUseGravityOnStart;
            }
            else
            {
                interactibleObject.isKinematic = isGrabbed ? true : isKinematicOnStart;
            }
        }
    }
}
