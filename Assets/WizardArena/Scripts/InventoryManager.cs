using System;
using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
    public class InventoryManager
    {
        public readonly Dictionary<int, Spawnable> allItems = new Dictionary<int, Spawnable>();
        public readonly List<int> auctionItmes = new List<int>();
        private readonly Dictionary<int, int> spawned = new Dictionary<int, int>();
        private readonly Dictionary<int, int> inventory = new Dictionary<int, int>();

        public static InventoryManager instance { get; private set; }
        private PlayerSave playerSave;
        
        public InventoryManager(PlayerSaveManager playerSaveManager)
        {
            if (instance == null)
            {
                instance = this;
                playerSave = playerSave = playerSaveManager.playerSave;
                object[] l = Resources.LoadAll("Prefabs/Spawnable");
                foreach(object o in l)
                {
                    if(o is GameObject)
                    {
                        Spawnable s = ((GameObject)o).GetComponent<Spawnable>();
                        if (s != null)
                        {
                            allItems[s.itemId] = s;
                            if (playerSave.inventory.ContainsKey(s.itemId)){
                                inventory[s.itemId] = playerSave.inventory[s.itemId];
                            }
                            if(s.price > 0)
                            {
                                auctionItmes.Add(s.itemId);
                            }
                        }
                    }
                }
            }
        }

        public void CleanupScene()
        {
            foreach (int sId in spawned.Keys)
            {
                bool firstOne;
                AddToInventory(sId, spawned[sId], out firstOne);
            }
            spawned.Clear();
        }

        public bool HasItem(int sId)
        {
            return inventory.ContainsKey(sId) || spawned.ContainsKey(sId);
        }

        public void AddToInventory(int sId, int quantity, out bool firstOne)
        {
            if (!inventory.ContainsKey(sId))
            {
                inventory.Add(sId, 0);
            }
            firstOne = inventory[sId] == 0;
            int qty = Mathf.Max(inventory[sId] + quantity, 0);
            inventory[sId] = qty;
            playerSave.inventory[sId] = qty;
        }

        public bool SpawnFromInventory(int sId, out bool lastOne)
        {
            lastOne = false;
            if (inventory.ContainsKey(sId) && inventory[sId] > 0)
            {
                if (!spawned.ContainsKey(sId))
                {
                    spawned[sId] = 0;
                }
                spawned[sId] = spawned[sId] + 1;
                int qty = inventory[sId] - 1;
                inventory[sId] = qty;
                playerSave.inventory[sId] = qty;
                lastOne = qty == 0;
                return true;
            }
            return false;
        }

        public void RemoveSpawned(int sId)
        {
            if (!spawned.ContainsKey(sId))
            {
                spawned[sId] = 0;
            }
            spawned[sId] = Mathf.Max(spawned[sId] - 1, 0);
        }

        public void ForEachInventoryItem(Action<Spawnable, int> action)
        {
            foreach (var kvp in inventory)
            {
                action.Invoke(allItems[kvp.Key], kvp.Value);
            }
        }
    }
}