using PowerUI;
using System.Collections;
using UnityEngine;
using VRTK;
using Zenject;

namespace WizardArena
{
    public class SpeechBubble
    {
        private AsyncProcessor asyncProcessor;
        private FlatWorldUI speechUI = null;
        private Transform playerTransform;
        private GameObject playerGO;
        private Vector3 displayPosition;
        private Vector3 facePosition;
        private string text;
        private int count;

        [Inject]
        public SpeechBubble(AsyncProcessor asyncProcessor)
        {
            this.asyncProcessor = asyncProcessor;
        }

        // Use this for initialization
        public void ShowSpeech(string text, int seconds, Vector3 displayPosition, Vector3 facePosition)
        {
            this.text = text;
            count = seconds;
            this.displayPosition = displayPosition;
            this.facePosition = facePosition;

            if (null != speechUI)
            {
                speechUI.Destroy();
            }
            speechUI = new FlatWorldUI("speech", 1028, 800);
            // move guinode (#powerui gameobject) to face position, so things are rendered relative to player/face position
            UI.GUINode.transform.position = this.facePosition;
            speechUI.transform.localScale = new Vector3(
                speechUI.transform.localScale.x / 3.5f,// 0.002f,
                speechUI.transform.localScale.y / 3.5f,// 0.0015f,
                speechUI.transform.localScale.z / 2);
            speechUI.document.LoadHtml("<html><head><style type='text/css'>#title { font-size: 75pt; } body { color: black; font-family: 'Milonga', cursive; font-size: 50pt; } #background-box { background: url('parchment.png') no-repeat; width: 1028px; height: 768px; } #content-box{ width:750px; height: 500px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center; } </style></head><body><div id='background-box'><div id='content-box'>" +
                "<span id='speech' style='color: black;'></span>" +
                "</div></div></body></html>");

            speechUI.transform.position = this.displayPosition;
            speechUI.transform.rotation = Quaternion.LookRotation(speechUI.transform.position - this.facePosition);


            asyncProcessor.StartCoroutine(Speech());
        }

        public void ShowSpeech(string text, Vector3 dp, Vector3 fp)
        {
            ShowSpeech(text, 9999, dp, fp);
        }

        public void ShowSpeech(string text, Vector3 dp)
        {
            asyncProcessor.StartCoroutine(PrepSpeech(text, displayPosition));

        }

        private IEnumerator PrepSpeech(string text, Vector3 dp)
        {
            while (VRTK_DeviceFinder.HeadsetTransform() == null)
            {
                yield return new WaitForSeconds(.1f);
            }
            ShowSpeech(text, dp, VRTK_DeviceFinder.HeadsetTransform().position);
        }

        public void ShowSpeech(string text, int seconds, Vector3 dp)
        {
            ShowSpeech(text, seconds, dp, VRTK_DeviceFinder.HeadsetTransform().position);
        }

        private IEnumerator Speech()
        {
            // update transform position and rotation
            Update();  
            while (speechUI != null)
            {
                speechUI.document.getElementById("speech").innerHTML = text;
                --count;
                if (count == 0)
                {
                    speechUI.Destroy();
                    speechUI = null;
                }
                yield return new WaitForSeconds(1f);
            }
        }

        public void Update()
        {
            speechUI.transform.position = this.displayPosition;
            speechUI.transform.rotation = Quaternion.LookRotation(speechUI.transform.position - this.facePosition);
        }
    }
}
