namespace WizardArena
{
    using System.Collections.Generic;
    using System.Linq;

    public static class WizardConstants
    {
        public const int ARCANE = 0;
        public const int EARTH = 1;
        public const int FIRE = 2;
        public const int FROST = 3;
        public const int LIFE = 4;
        public const int LIGHTNING = 5;
        public const int LIGHT = 6;
        public const int SHADOW = 7;
        public const int STORM = 8;
        public const int WATER = 9;
        public const int MELEE = 10;
        public const int RANGED = 11;

        public static Dictionary<int, IElement> ELEMENTS = new Dictionary<int, IElement>();
        public static Dictionary<string, int> NAMES_ELEMENT = new Dictionary<string, int>();

        public static HashSet<int> ARCANE_ELEMENTS = new HashSet<int>(new int[3] { ARCANE, STORM, LIGHTNING });
        public static HashSet<int> ELEMENTAL_ELEMENTS = new HashSet<int>(new int[4] { FIRE, EARTH, FROST, WATER });
        public static HashSet<int> HOLY_ELEMENTS = new HashSet<int>(new int[3] { LIGHT, SHADOW, LIFE });
        public static HashSet<int> NON_MAGICAL = new HashSet<int>(new int[2] { MELEE, RANGED });

        static WizardConstants()
        {
            ELEMENTS.Add(ARCANE, new Arcane());
            ELEMENTS.Add(EARTH, new Earth());
            ELEMENTS.Add(FIRE, new Fire());
            ELEMENTS.Add(FROST, new Frost());
            ELEMENTS.Add(LIFE, new Life());
            ELEMENTS.Add(LIGHTNING, new Lightning());
            ELEMENTS.Add(LIGHT, new Light());
            ELEMENTS.Add(SHADOW, new Shadow());
            ELEMENTS.Add(STORM, new Storm());
            ELEMENTS.Add(WATER, new Water());
            ELEMENTS.Add(MELEE, new Melee());
            ELEMENTS.Add(RANGED, new Ranged());
            NAMES_ELEMENT = ELEMENTS.ToDictionary(k => k.Value.GetName().ToLower(), v => v.Key);
        }
    }
}