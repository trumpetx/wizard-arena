using UnityEngine;
using VRTK;
using TLGFPowerBooks;
using System.Collections.Generic;

namespace WizardArena
{
    public class BookSelect : MonoBehaviour
    {
        private const float BOOK_FLY_SECONDS = 1f;
        private static readonly Vector3 deskPosition = new Vector3(.9f, .7f, 0);
        private static readonly Quaternion deskRotation = Quaternion.Euler(0, 90, 0);

        private VRTK_InteractableObject interactibleObject;
        private List<BookSelect> otherBooks;
        private PBook book;

        private Vector3 endPosition, homePosition, startPosition;
        private Quaternion endRotation, homeRotation, startRotation;
        private Rigidbody rigidBody;
        public GameObject leftPage, rightPage;
        private int numPages;
        private float elapsedTime = 0;
        private bool moving = false;

        private void OnEnable()
        {
            if (otherBooks == null)
            {
                otherBooks = new List<BookSelect>(FindObjectsOfType<BookSelect>());
                otherBooks.Remove(this);
            }

            rigidBody = rigidBody?? GetComponentInChildren<Rigidbody>();
            rigidBody.isKinematic = true;
            interactibleObject = interactibleObject ?? GetComponentInChildren<VRTK_InteractableObject>();
            interactibleObject.InteractableObjectUsed += OnInteractableObjectUsed;
            
            endPosition = startPosition = homePosition = transform.position;
            endRotation = startRotation = homeRotation = transform.rotation;

            book = book ?? GetComponent<PBook>();
            numPages = book.contentContainer.childCount;
            
            leftPage.GetComponent<VRTK_InteractableObject>().InteractableObjectUsed += TurnPagePrev;
            rightPage.GetComponent<VRTK_InteractableObject>().InteractableObjectUsed += TurnPageNext;
        }

        private void OnDisable()
        {
            interactibleObject.InteractableObjectUsed -= OnInteractableObjectUsed;
            leftPage.GetComponent<VRTK_InteractableObject>().InteractableObjectUsed -= TurnPagePrev;
            rightPage.GetComponent<VRTK_InteractableObject>().InteractableObjectUsed -= TurnPageNext;
        }

        private void Update()
        {
            if (moving)
            {
                if (endPosition != null && transform.position != endPosition)
                {
                    transform.SetPositionAndRotation(
                        Vector3.Slerp(startPosition, endPosition, (elapsedTime / BOOK_FLY_SECONDS) / 2),
                        Quaternion.Slerp(startRotation, endRotation, (elapsedTime / BOOK_FLY_SECONDS) / 2));
                    elapsedTime += Time.deltaTime;
                    book.CloseBook();
                }
                else
                {
                    elapsedTime = 0;
                    moving = false;
                }
            }
            else if (transform.position == deskPosition)
            {
                book.OpenBook();
            }
        }

        public void OnInteractableObjectUsed(object sender, InteractableObjectEventArgs args)
        {
            otherBooks.ForEach(b => b.GoHome());

            if (transform.position == homePosition)
            {
                startPosition = transform.position;
                startRotation = transform.rotation;
                endPosition = deskPosition;
                endRotation = deskRotation;
                moving = true;
            }
        }

        public void GoHome()
        {
            if (transform.position != homePosition)
            {
                elapsedTime = 0;
                book.JumpToFirstPage(true);
                book.CloseBook();
                startPosition = transform.position;
                startRotation = transform.rotation;
                endPosition = homePosition;
                endRotation = homeRotation;
                moving = true;
            }
        }
        
        public void TurnPagePrev(object sender, InteractableObjectEventArgs args)
        {
            if (book.GetCurrentPageIndex() == 1)
            {
                GoHome();
            }
            else
            {
                book.PrevPage();
            }
        }

        public void TurnPageNext(object sender, InteractableObjectEventArgs args)
        {
            if (book.GetCurrentPageIndex() >= numPages)
            {
                GoHome();
            }
            else
            {
                book.NextPage();
            }
        }
    }
}