using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace WizardArena
{
    public class FacePlayerRotation : MonoBehaviour
    {
        private NpcCharacter attacker;
        private Quaternion originalRotation;
        private Vector3 originalLocalPosition;

        void Start()
        {
            if (!attacker && !(attacker = GetComponentInChildren<NpcCharacter>()))
            {
                throw new UnityException("NpcCharacter is required in a child of FacePlayerRotation");
            }
        }

        void Update()
        {
            if (attacker.pew.target)
            {
                Vector3 targetPostition = new Vector3(attacker.pew.target.position.x,
                                                       this.transform.position.y,
                                                       attacker.pew.target.position.z);
                transform.LookAt(targetPostition);
            }
        }
    }
}