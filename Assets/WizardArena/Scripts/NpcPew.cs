using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    public class NpcPew : NetworkBehaviour
    {
        public const int SHOOT_TINY = 0;
        public const int SHOOT_SMALL = 1;
        public const int SHOOT_NORMAL = 2;
        public const int SHOOT_LARGE = 3;
        public const int SHOOT_MEGA = 4;

        public delegate void OnShootHandler(int level);
        public event OnShootHandler OnShoot;
        public delegate void OnCleaveHandler();
        public event OnCleaveHandler OnCleave;
        public delegate void OnWallHandler();
        public event OnWallHandler OnWall;
        public delegate void OnShieldHandler();
        public event OnShieldHandler OnShield;
        
        public float speed = 50;

        public GameObject[] largeProjectiles;
        public GameObject[] projectiles;
        public GameObject[] smallProjectiles;
        public GameObject[] shields;

        public Transform shootReferencePosition { get; private set; }
        public Transform faceReferencePosition { get; private set; }
        public Transform target { get; set; }
        public int element { get; set; }
        private HashSet<GameObject> activeShields = new HashSet<GameObject>();

        private void Awake()
        {
            element = -1;
        }

        private void Start()
        {
            if (element == -1)
            {
                while(element == -1 || !WizardConstants.ELEMENTS[element].IsMagic())
                {
                    element = Random.Range(0, WizardConstants.ELEMENTS.Count);
                }
            }

            if(faceReferencePosition == null)
            {
                faceReferencePosition = transform;
            }
        }
        public void DoShield(float seconds)
        {
            DoShield(element, seconds);
        }

        public void DoShield(int element, float seconds)
        {
            OnShield?.Invoke();
            //create shield, add to shield set
            GameObject currentShield = Instantiate(shields[element], faceReferencePosition.position, faceReferencePosition.rotation);
            lock (activeShields)
            {
                activeShields.Add(currentShield);
            }
            StartCoroutine(DestroyShield(seconds, currentShield));
        }

        private IEnumerator DestroyShield(float seconds, GameObject shield)
        {
            yield return new WaitForSeconds(seconds);
            lock (activeShields)
            {
                if (activeShields.Remove(shield))
                {
                    Destroy(shield);
                }
            }
        }

        public void ClearShields()
        {
            // destroy all shields still active
            lock (activeShields)
            {
                foreach (GameObject g in activeShields)
                {
                    Destroy(g);
                }
                activeShields.Clear();
            }
        }
        
        public void DoMegaShoot(float damage)
        {
            GameObject o = largeProjectiles[element];
            Shoot(o, damage);
            OnShoot?.Invoke(SHOOT_MEGA);
        }

        public void DoShoot(float damage)
        {
            GameObject o = projectiles[element];
            Shoot(o, damage);
            OnShoot?.Invoke(SHOOT_NORMAL);
        }
        
        private void Shoot(GameObject elementProjectile, float dmg)
        {
            Vector3 v = faceReferencePosition.position + (faceReferencePosition.forward * 4) + faceReferencePosition.up;
            Vector3 projectileVector = target.position - v;
            GameObject projectile = Instantiate(elementProjectile, v, Quaternion.identity);
            projectile.GetComponent<Rigidbody>().AddForce(projectileVector * speed);
            ChangesHealth d = projectile.GetComponent<ChangesHealth>();
            if (!d)
            {
                d = projectile.AddComponent<ChangesHealth>();
            }
            d.healthChange = -1 * dmg;
            Destroy(projectile, 5f);
        }

        private Vector3 FaceForward()
        {
            Vector3 v = faceReferencePosition.position + (faceReferencePosition.forward * 2);
            v.y = transform.position.y; // Set the y position to the current "floor"
            return v;
        }

        private GameObject InFront(GameObject[] arr)
        {
            return Instantiate(arr[element], FaceForward(), faceReferencePosition.rotation);
        }
        
        public void SetFaceReferencePosition(Transform t)
        {
            faceReferencePosition = t;
        }

        public void SetShootReferencePosition(Transform t)
        {
            shootReferencePosition = t;
        }

        public Transform GetFaceReferencePosition()
        {
            return faceReferencePosition;
        }

        public Transform GetShootReferencePosition()
        {
            return shootReferencePosition;
        }
    }
}