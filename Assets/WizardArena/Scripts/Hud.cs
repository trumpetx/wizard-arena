using UnityEngine;

namespace WizardArena
{
    public class Hud : MonoBehaviour
    {
        public MeshRenderer healthBar;
        public MeshRenderer manaBar;

        private Transform originalParent;
        private float initialY = 0f;

        private float lastHealth;
        private float lastMana;

        private void Awake()
        {
            healthBar.material.color = Color.red;
            manaBar.material.color = Color.blue;
            initialY = healthBar.transform.localScale.y; // all status bars need to be the same size (or they will be!)
        }

        public void RemoveHand()
        {
            if (originalParent != null && originalParent.gameObject.activeInHierarchy)
            {
                transform.SetParent(originalParent);
            }
            gameObject.SetActive(false);
        }

        public void SetHand(Transform o)
        {
            gameObject.SetActive(true);
            if (originalParent == null)
            {
                originalParent = transform.parent;
            }
            transform.SetParent(o);
            transform.localRotation = Quaternion.identity;
            transform.localPosition = Vector3.zero;
        }

        public void SetMana(float mana, float totalMana)
        {
            if (isActiveAndEnabled && mana != lastMana)
            {
                RenderCapsulePercent(manaBar, mana / totalMana);
                lastMana = mana;
            }
        }

        public void SetHealth(float health, float totalHealth)
        {
            if (isActiveAndEnabled && health != lastHealth)
            {
                RenderCapsulePercent(healthBar, health / totalHealth);
                lastHealth = health;
            }
        }

        private void RenderCapsulePercent(MeshRenderer bar, float percent)
        {
            Vector3 barVector = bar.transform.localScale;
            barVector.y = initialY * percent;
            bar.transform.localScale = barVector;
        }
    }
}
