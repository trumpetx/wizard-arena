using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using VRTK;

namespace WizardArena {
    [RequireComponent(typeof(NetworkPew), typeof(NetworkHitWithProjectile))]
    public class NetworkWizardController : VRTKNetworkBehavior
    {
        [SyncVar] [HideInInspector] public float playerMana;
        [SyncVar] [HideInInspector] public float playerHealth;
        [SyncVar] [HideInInspector] public float startingMana;
        [SyncVar] [HideInInspector] public float startingHealth;

        [SyncVar] [HideInInspector] public bool leftHanded;

        [SyncVar(hook = "OnLayerChange")] [HideInInspector] public int assignedLayer;

        public Action<NetworkWizardController> onDisconnectHandler { get; set; }
        public GameObject healthBar;
        private GameObject healthBarSpawn { get; set; }
        public Transform spawnPoint { get; set; }
        public Transform head;
        public Transform playerBody;
        public Transform leftHand;
        public Transform rightHand;
        public Transform feet;
        private VRTK_BasicTeleport vrtkTeleport;
        private VRTK_Pointer vrtkPointer;
        private VRTK_BezierPointerRenderer vrtkPointerRenderer;
        private VRTK_PolicyList vrtkPolicyList;
        private VRTK_PlayAreaCursor vrtkPlayAreaCursor;
        private EnergyBar energyBar;
        public NetworkPew pew { get; private set; }
        public PlayerLogic player { get; private set; }
        public IGestureRecognizer gestureRecognizer { get; private set; }
        public NetworkHitWithProjectile networkHitWithProjectile { get; private set; }
        private WizardArenaVariables wizardArenaVariables;
        private NetworkPotionConsumer potionConsumer;
        private CountdownTimer countdownTimer;
        private IEnumerator regenCoroutine;
        private MenuManager menuManager;
        public int[] availableElements { get; set; }
        
        public static NetworkWizardController localPlayer;

        internal override void Awake()
        {
            countdownTimer = CountdownTimer.instance;
            wizardArenaVariables = WizardArenaVariables.instance;
            base.Awake();
        }

        private void OnDisable()
        {
            if (player != null && isServer)
            {
                player.OnDeath -= DropOut;
            }
            if(gestureRecognizer != null)
            {
                gestureRecognizer.Disable();
            }
            if(energyBar != null)
            {
                Destroy(energyBar.gameObject);
            }
            if (networkHitWithProjectile != null)
            {
                networkHitWithProjectile.OnHit -= OnHit;
            }
            if (regenCoroutine != null)
            {
                StopCoroutine(regenCoroutine);
                regenCoroutine = null;
            }
            if(vrtkTeleport != null)
            {
                vrtkTeleport.Teleported -= OnTeleport;
            }
            if(vrtkPointer != null)
            {
                vrtkPointer.DestinationMarkerSet -= HandleSummon;
            }
            if(potionConsumer != null)
            {
                potionConsumer.OnConsume -= OnPotionConsume;
            }
            if (isLocalPlayer && OptionsMenuManager.instance != null)
            {
                OptionsMenuManager.instance.OnHandednessChange -= OnHandednessChange;
            }
        }

        public override void OnStartLocalPlayer()
        {
            localPlayer = this;
        }

        private IEnumerator Init()
        {
            yield return new WaitUntil(() => localPlayer != null);
            lock (this)
            {
                PlayerLogic player = this.player == null 
                    ? new PlayerLogic(wizardArenaVariables) 
                    : this.player;

                if (isServer)
                {
                    playerHealth = player.GetCurrentHealth();
                    playerMana = player.GetCurrentMana();
                    player.OnDeath += DropOut;
                    networkHitWithProjectile.OnHit += OnHit;
                    regenCoroutine = Regen();
                    StartCoroutine(regenCoroutine);
                    potionConsumer = potionConsumer ?? GetComponentInChildren<NetworkPotionConsumer>();
                    potionConsumer.OnConsume += OnPotionConsume;
                }

                if (!isLocalPlayer)
                {
                    // Set enemy body parts
                    head.GetComponent<MeshRenderer>().material.color = Color.black;
                    playerBody.GetComponent<MeshRenderer>().enabled = false;
                    leftHand.GetComponent<MeshRenderer>().material.color = Color.red;
                    rightHand.GetComponent<MeshRenderer>().material.color = Color.blue;
                    feet.GetComponent<MeshRenderer>().material.color = Color.gray;

                    // Set enemy bar
                    healthBarSpawn = Instantiate(healthBar, playerBody.position, Quaternion.identity);
                    energyBar = healthBarSpawn.GetComponentInChildren<EnergyBar>();
                }

                if (isLocalPlayer)
                {
                    foreach (MeshRenderer r in GetComponentsInChildren<MeshRenderer>())
                    {
                        r.enabled = false;
                    }
                    // Move the player to their starting location
                    VRTK_DeviceFinder.PlayAreaTransform().SetPositionAndRotation(transform.position, transform.rotation);

                    // Set player's layer (Controls bounding box)
                    vrtkTeleport = vrtkTeleport ?? sdkManager.GetComponentInChildren<VRTK_BasicTeleport>();
                    vrtkPolicyList = vrtkPolicyList ?? vrtkTeleport.GetComponent<VRTK_PolicyList>();
                    if (vrtkPointer != null)
                    {
                        vrtkPointer.enabled = false;
                        vrtkPointer = null;
                    }
                    if (vrtkPointerRenderer != null)
                    {
                        vrtkPointerRenderer.enabled = false;
                        vrtkPointerRenderer = null;
                    }

                    menuManager = menuManager ?? GetComponent<MenuManager>();
                    OnHandednessChange(PlayerSaveManager.instance.playerSave.leftHanded);
                    OptionsMenuManager.instance.OnHandednessChange += OnHandednessChange;
                    CmdSetPlayerVars(new short[] { }, 0, .25f, .25f, .25f, .25f);

                    vrtkPointerRenderer.enabled = false;
                    vrtkPointer.enabled = false;
                    vrtkTeleport.Teleported += OnTeleport;

                    Util.AssignLayer(transform, assignedLayer);
                }

                Debug.Log("Init complete: local=" + isLocalPlayer);
                this.player = player; // allow Update() to process
            }
        }

        [Command]
        private void CmdSendPrefs(bool leftHanded)
        {
            this.leftHanded = leftHanded;
        }

        [Command]
        private void CmdSetPlayerVars(short[] talentChoices, int level, float intelligenceMod, float wisdomMod, float luckMod, float staminaMod)
        {
            StartCoroutine(SetPlayerVars(talentChoices, level, intelligenceMod, wisdomMod, luckMod, staminaMod));
        }

        private IEnumerator SetPlayerVars(short[] talentChoices, int level, float intelligenceMod, float wisdomMod, float luckMod, float staminaMod)
        {
            yield return new WaitUntil(() => player != null);
            player.SetPlayerVars(talentChoices, level, intelligenceMod, wisdomMod, luckMod, staminaMod);
        }

        public void OnHandednessChange(bool leftHanded)
        {
            if (gestureRecognizer != null)
            {
                gestureRecognizer.Disable();
            }
            gestureRecognizer = GestureRecognizerFactory.instance.GetGestureRecognizer(OnGestureDetected, leftHanded, sdkManager.loadedSetup);
            menuManager.SetupMenus(pew.element, pew.CmdSetElement);
            if(vrtkPointer != null)
            {
                vrtkPointer.DestinationMarkerSet -= HandleSummon;
            }
            vrtkPointer = OffhandScriptController().GetComponent<VRTK_Pointer>();
            vrtkPointerRenderer = OffhandScriptController().GetComponent<VRTK_BezierPointerRenderer>();
            vrtkPlayAreaCursor = OffhandController().GetComponent<VRTK_PlayAreaCursor>();
            vrtkPointer.DestinationMarkerSet += HandleSummon;
            CmdSendPrefs(leftHanded);
        }

        public void OnPotionConsume(DrinkablePotion potion)
        {
            player.ConsumePotion(potion);
            Destroy(potion.gameObject);
        }

        public void OnLayerChange(int layer)
        {
            Debug.Log("Layer Changed to: " + layer);
            Util.AssignLayer(transform, layer);
        }

        private void DoneWithPointer()
        {
            vrtkPointer.pointerRenderer = vrtkPointerRenderer;
            vrtkPointerRenderer.customCursor = null;
            vrtkPointerRenderer.Toggle(false, false);
            vrtkPointerRenderer.enabled = false;
            vrtkPolicyList.operation = VRTK_PolicyList.OperationTypes.Ignore;
            vrtkPolicyList.identifiers.Clear();
            vrtkPointer.Toggle(false);
            vrtkPointer.enableTeleport = false;
            vrtkPointer.enabled = false;
            vrtkPointerRenderer.playareaCursor = null;
            vrtkPlayAreaCursor.enabled = false;
        }

        private void OnTeleport(object sender, DestinationMarkerEventArgs e)
        {
            DoneWithPointer();
        }

        private void HandleSummon(object sender, DestinationMarkerEventArgs e)
        {
            if (e.enableTeleport)
            {
                return; // we're teleporting, not summoning
            }
            pew.DoSummon(e.destinationPosition, e.destinationRotation ?? Quaternion.identity, player.CalcSummonDmg(pew.element), player.CalcSummonSeconds(pew.element));
            DoneWithPointer();
        }

        private void OnEnable()
        {
            pew = pew ?? GetComponent<NetworkPew>();
            networkHitWithProjectile = networkHitWithProjectile ?? GetComponent<NetworkHitWithProjectile>();
            if (sdkManager.loadedSetup == null)
            {
                return;
            }
            StartCoroutine(Init());
        }

        private IEnumerator Regen()
        {
            while (true)
            {
                // More players = slower ticks (same per/s regen)
                int waitSeconds = Math.Max(Network.connections.Length, 1);
                yield return new WaitForSeconds(waitSeconds);
                if (player != null && pew != null)
                {
                    player.RegenHealthAndMana(waitSeconds, pew.element);
                }
            }
        }

        private void DropOut()
        {
            TargetRpcDropOut(connectionToClient);
        }

        [TargetRpc]
        private void TargetRpcDropOut(NetworkConnection target)
        {
            if (countdownTimer != null)
            {
                countdownTimer.StartCountdown(5, Color.red, () =>
                {
                    target.Disconnect();
                });
            }
        }

        private void OnHit(float healthChange, int element)
        {
            if (player.CalcAndApplyHealthChange(pew.element, healthChange, element))
            {
                TargetRpcPulse(connectionToClient);
            }
        }

        [TargetRpc]
        public void TargetRpcPulse(NetworkConnection target)
        {
            Haptics.Pulse();
        }

        public void OnGestureDetected(Gesture gesture)
        {
            switch (gesture)
            {
                case Gesture.TELEPORT:
                    if (player.GetCurrentMana() >= 20)
                    {
                        CmdPlayerChangeMana(-20);
                        DoneWithPointer();
                        bool isVsMode = false;
                        if (isVsMode)
                        {
                            vrtkPolicyList.operation = VRTK_PolicyList.OperationTypes.Include;
                            vrtkPolicyList.checkType = VRTK_PolicyList.CheckTypes.Layer;
                            vrtkPolicyList.identifiers = new List<string>(new string[] { LayerMask.LayerToName(gameObject.layer) });
                        }
                        else
                        {
                            // TODO: add "out of bounds" layer
                            vrtkPolicyList.operation = VRTK_PolicyList.OperationTypes.Ignore;
                            vrtkPolicyList.checkType = VRTK_PolicyList.CheckTypes.Layer;
                            vrtkPolicyList.identifiers = new List<string>(new string[] {  });
                        }
                        vrtkPlayAreaCursor.enabled = true;
                        vrtkPlayAreaCursor.targetListPolicy = vrtkPolicyList;
                        vrtkPointerRenderer.playareaCursor = vrtkPlayAreaCursor;
                        vrtkPointer.enableTeleport = true;
                        vrtkPointerRenderer.enabled = true;
                        vrtkPointer.enabled = true;
                        return;
                    }
                    break;
                case Gesture.SUMMON:
                    if (player.GetCurrentMana() >= 30)
                    {
                        CmdPlayerChangeMana(-30);
                        DoneWithPointer();
                        NetworkSummonable summonable = pew.summonThings[pew.element].GetComponent<NetworkSummonable>();
                        vrtkPointerRenderer.customCursor = summonable?.preview;
                        vrtkPointerRenderer.enabled = true;
                        vrtkPointer.enabled = true;
                        return;
                    }
                    break;
                default:
                    CmdDoGesture((int)gesture);
                    break;
            }
        }

        // TODO: change to server authority
        [Command]
        public void CmdPlayerChangeMana(float mana)
        {
            player.ModifyMana(mana);
        }

        [Command]
        public void CmdDoGesture(int g)
        {
            Gesture gesture = (Gesture) g;
            int element = pew.element;
            switch (gesture)
            {
                case Gesture.SHOOT:
                    if (player.ModifyMana(-1 * 20))
                    {
                        pew.DoShoot(player.CalcShootDmg(element));
                    }
                    break;
                case Gesture.PUSH:
                    if (player.ModifyMana(-1 * 30))
                    {
                        pew.DoMegaShoot(player.CalcPushDmg(element));
                    }
                    break;
                case Gesture.WALL:
                    if (player.ModifyMana(-1 * 15))
                    {
                        pew.DoWall(player.CalcWallSeconds(element), player.CalcWallDamage(element));
                    }
                    break;
                case Gesture.CLEAVE:
                    if (player.ModifyMana(-1 * 5))
                    {
                        pew.DoCleave(player.CalcCleaveDmg(element));
                    }
                    break;
                case Gesture.SHIELD:
                    if (player.ModifyMana(-1 * 25))
                    {
                        float shiledSeconds = player.CalcShieldSeconds(element);
                        player.SetShielded(DateTime.Now.AddSeconds(shiledSeconds), element);
                        pew.DoShield(shiledSeconds);
                    }
                    break;
                case Gesture.BEAM:
                    if (player.ModifyMana(-1 * 20))
                    {
                        pew.DoBeam(player.CalcBeamDmg(element), player.CalcBeamSeconds(element));
                    }
                    break;
            }
        }

        // Shared with LobbyWizard
        public static void SetLocalPlayerTransformPositions(Transform head, Transform rightHand, Transform leftHand, Transform playerBody, Transform feet)
        {
            if (VRTK_DeviceFinder.GetControllerRightHand() != null
                && VRTK_DeviceFinder.GetControllerLeftHand() != null
                && VRTK_DeviceFinder.HeadsetTransform() != null)
            {
                head.SetPositionAndRotation(VRTK_DeviceFinder.HeadsetTransform().position, VRTK_DeviceFinder.HeadsetTransform().rotation);
                rightHand.SetPositionAndRotation(VRTK_DeviceFinder.GetControllerRightHand().transform.position, VRTK_DeviceFinder.GetControllerRightHand().transform.rotation);
                leftHand.SetPositionAndRotation(VRTK_DeviceFinder.GetControllerLeftHand().transform.position, VRTK_DeviceFinder.GetControllerLeftHand().transform.rotation);
                // rotate only around y axis
                playerBody.SetPositionAndRotation(VRTK_DeviceFinder.HeadsetTransform().position + new Vector3(0, -1, 0)
                                                , new Quaternion(0
                                                , VRTK_DeviceFinder.HeadsetTransform().rotation.y
                                                , 0
                                                , VRTK_DeviceFinder.HeadsetTransform().rotation.w
                                                ));

                Vector3 footPos = new Vector3(
                    VRTK_DeviceFinder.HeadsetTransform().position.x,
                    VRTK_DeviceFinder.PlayAreaTransform().position.y + .05f,
                    VRTK_DeviceFinder.HeadsetTransform().position.z);
                Vector3 footLook = VRTK_DeviceFinder.HeadsetTransform().position + VRTK_DeviceFinder.HeadsetTransform().forward * 2 - footPos;
                footLook.y = 0;
                feet.SetPositionAndRotation(footPos, Quaternion.LookRotation(footLook));
            }
        }

        private void Update()
        {
            if(player == null)
            {
                return;
            }
            if (isServer)
            {
                // Server players sync vars are controlled by game logic
                if (playerHealth != player.GetCurrentHealth())
                {
                    playerHealth = player.GetCurrentHealth();
                }
                if (playerMana != player.GetCurrentMana())
                {
                    playerMana = player.GetCurrentMana();
                }
            }
            else
            {
                player.SetHealthAndMana(playerHealth, playerMana);
            }

            if (isLocalPlayer)
            {
                SetLocalPlayerTransformPositions(head, rightHand, leftHand, playerBody, feet);
                transform.position = playerBody.position;
            }

            if (healthBarSpawn != null && VRTK_DeviceFinder.HeadsetCamera() != null)
            {
                healthBarSpawn.transform.SetPositionAndRotation(
                    new Vector3(playerBody.transform.position.x, 2.2f, playerBody.transform.position.z),
                    Quaternion.LookRotation(healthBarSpawn.transform.position - VRTK_DeviceFinder.HeadsetCamera().position)
                    );
                energyBar.SetValueCurrent((int)player.GetCurrentHealth());
                energyBar.SetValueMax((int)player.GetStartingHealth());
            }
        }

        public override void OnNetworkDestroy()
        {
            onDisconnectHandler?.Invoke(this);
            base.OnNetworkDestroy();
        }
    }
}