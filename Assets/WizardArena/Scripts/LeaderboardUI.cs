using PowerUI;
using System.Collections;
using UnityEngine;

namespace WizardArena
{
    [RequireComponent(typeof(WorldUIHelper))]
    public class LeaderboardUI : MonoBehaviour
    {
        private GameLifecycle gameLifecycle;
        private WorldUIHelper worldUIHelper;
        private Coroutine downloadLeaderboardsCoroutine;

        private void OnEnable()
        {
            gameLifecycle = gameLifecycle ?? GameLifecycle.instance;
            downloadLeaderboardsCoroutine = StartCoroutine(DownloadLeaderboards());
        }

        private void OnDisable()
        {
            if(downloadLeaderboardsCoroutine != null)
            {
                StopCoroutine(downloadLeaderboardsCoroutine);
                downloadLeaderboardsCoroutine = null;
            }
        }

        private IEnumerator DownloadLeaderboards()
        {
            yield return new WaitUntil(() => gameLifecycle.isDoneInitializing);
            if (gameLifecycle.leaderboardController == null)
            {
                downloadLeaderboardsCoroutine = null;
                gameObject.SetActive(false);
                yield return null;
            }
            worldUIHelper = GetComponent<WorldUIHelper>();
            bool init = gameLifecycle.leaderboardController.DownloadLeaderboards((string leaderboard, string name, int score) => {
                AddLeader(name, "" + score);
            });
            worldUIHelper.enabled = init;
        }

        private void AddLeader(string name, string score)
        {
            Dom.Element tr = worldUIHelper.WorldUI.document.getElementById(name);
            if (tr == null)
            {
                tr = worldUIHelper.WorldUI.document.createElement("tr");
                tr.id = name;
                worldUIHelper.WorldUI.document.getElementById("leaderboard").appendChild(tr);
                Debug.Log("Adding new leaderboard member: " + name);
            }
            else
            {
                Debug.Log("Updating existing leaderboard member: " + name);
            }
            foreach (Dom.Node n in tr.children)
            {
                n.remove();
            }
            Dom.Element td1 = tr.document.createElement("td");
            td1.className = "player";
            td1.appendTextContent(name);
            Dom.Element td2 = tr.document.createElement("td");
            td2.className = "score";
            td2.appendTextContent(score);
            tr.appendChild(td1);
            tr.appendChild(td2);
        }
    }
}