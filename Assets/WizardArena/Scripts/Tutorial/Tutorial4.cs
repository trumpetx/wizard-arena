using System;
using System.Collections.Generic;
using Edwon.VR;
using Edwon.VR.Gesture;
using UnityEngine;

namespace WizardArena
{
    public class Tutorial4 : BaseTutorial
    {
        private bool startShielding = false;
        private bool startAttacking = false;
        private DateTime timeToNextShield;
        private DateTime attackUntil;
        private bool attackAfterShield = false;
        private string lastGestureDetected;

        protected override string Name()
        {
            return "Let's move on to your defense.";
        }

        protected override void Setup()
        {
            CommonSetup(100f, new int[] { WizardConstants.ARCANE, WizardConstants.FIRE });

            GestureRecognizer.GestureDetectedEvent += OnGestureDetected;

            List<Objective> o = new List<Objective>();
            o.Add(new Objective("<b>" + Name() + "</b>", objective => DateTime.Now > objective.active.AddSeconds(3)));
            o.Add(new Objective(
                "You can cast shields on yourself.", objective => DateTime.Now > objective.active.AddSeconds(3)));
            o.Add(new Objective("To cast a shield, hold both cast buttons and cast downward," +
                "\n releasing the buttons at the end.", objective => DateTime.Now > objective.active.AddSeconds(5)));
            o.Add(new Objective("I will demonstrate." +
                "\n Try casting it yourself!", objective => lastGestureDetected == "Shield"));
            o.Add(new Objective("!", objective => DateTime.Now > objective.active.AddSeconds(12)));
            o.Add(new Objective("That didn't seem very sportsmanlike..." +
                "\n Teach him a lesson!", objective => DateTime.Now > objective.active.AddSeconds(4)));
            objectiveIt = o.GetEnumerator();
        }

        public override void DoTutorial()
		{
			if (objectiveIt.Current == null || objectiveIt.Current.condition (objectiveIt.Current)) {
				if (objectiveIt.MoveNext ()) {
                    // tutorial cues
                    if(objectiveIt.Current.text.StartsWith("I"))
                    {
                        // friendly wizard starts showing off technique
                        startShielding = true;
                    }
                    if (objectiveIt.Current.text.StartsWith("!"))
                    {
                        // npc attacks with player's shield type
                        npc.pew.element = player.pew.element;

                        // friendly wizard stops while the enemy starts his barrage
                        startShielding = false;
                        startAttacking = true;
                        attackUntil = DateTime.Now.AddSeconds(10);
                    }

                    objectiveIt.Current.active = DateTime.Now;

                    SpeechBubbleManager.instance.AddSpeechbubble(
                        friendlyMouthTransform,
                        objectiveIt.Current.text,
                        SpeechBubbleManager.SpeechbubbleType.NORMAL,
                        99f,
                        Color.white,
                        Vector3.zero
                    );
                }
			}

            // Enemy wizard behavior
            bool healthChanged = lasthealth != npc.character.GetCurrentHealth();
            lasthealth = npc.character.GetCurrentHealth();
            if (healthChanged && npc.character.GetCurrentHealth() < 50f)
            {
                npc.CmdDoShield(20);
                attackAfterShield = true;
            }
            else if (healthChanged && npc.character.GetCurrentHealth() < 100f || startAttacking)
            {
                npc.CmdDoShoot(shootDmg);
            }
            if(startAttacking && DateTime.Now > attackUntil)
            {
                startAttacking = false;
            }
            if (attackAfterShield && npc.animator.GetCurrentAnimatorStateInfo(0).IsName("standing_idle"))
            {
                attackAfterShield = false;
                npc.CmdDoMegaShoot(megaShootDmg);
            }

            //friendly shield demonstration
            if (startShielding && DateTime.Now > timeToNextShield)
            {
                friendly.GetComponentInChildren<NpcCharacter>().CmdDoShield(3);
                timeToNextShield = DateTime.Now.AddSeconds(4);
            }
        }

        override public void Complete()
        {
            base.Complete();
            GestureRecognizer.GestureDetectedEvent -= OnGestureDetected;

        }

        private void OnGestureDetected(string gestureName, double confidence, Handedness hand, bool isDouble)
        {
            if (gestureName.StartsWith("Right--") || gestureName.StartsWith("Left--"))
            {
                return;
            }
            lastGestureDetected = gestureName;
        }
    }
}