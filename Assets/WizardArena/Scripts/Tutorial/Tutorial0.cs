using System;
using System.Collections.Generic;
using UnityEngine;
using Edwon.VR.Gesture;
using Edwon.VR;

namespace WizardArena
{
    public class Tutorial0 : BaseTutorial
    {
        private DateTime timeToNextLoop;
        private string lastGestureDetected;

        override protected string Name()
        {
            return "First, casting Spells.";
        }
        override protected void Setup()
        {
            CommonSetup(10f, new int[] { WizardConstants.ARCANE, WizardConstants.ARCANE });

			GestureRecognizer.GestureDetectedEvent += OnGestureDetected;
            
			List<Objective> o = new List<Objective>();
            o.Add(new Objective("<b> Welcome to the combat tutorial! </b>" +
                "\n\n Let's walk through the basics of battle.", objective => DateTime.Now > objective.active.AddSeconds(5)));
            o.Add (new Objective("<b>" + Name () + "</b>", objective => DateTime.Now > objective.active.AddSeconds(3)));
            o.Add(new Objective("Each spell has its own unique gesture.", objective => DateTime.Now > objective.active.AddSeconds(3)));
            o.Add(new Objective("To cast a spell, start by pressing the cast button," +
                "\n performing the spell's gesture," +
				"\n and releasing the button when the gesture is complete. ", objective => DateTime.Now > objective.active.AddSeconds(10)));
			o.Add (new Objective("Let's start with a simple attack." +
                "\n\n I'll go step by step, while demonstrating the gesture.", objective => DateTime.Now > objective.active.AddSeconds(5), true));
			o.Add (new Objective("First, bring back your casting hand.", objective => DateTime.Now > objective.active.AddSeconds(3)));
			o.Add (new Objective("Press the casting button, and arc your hand forward" +
                "\n in the direction you want to cast.", objective => DateTime.Now > objective.active.AddSeconds(4)));
			o.Add (new Objective("When you're aimed at your target, release the button to fire.", objective => DateTime.Now > objective.active.AddSeconds(4)));
			o.Add (new Objective("Try it out!", objective => lastGestureDetected == "Shoot"));
			objectiveIt = o.GetEnumerator ();
        }

		override public void Complete(){
			base.Complete ();
			GestureRecognizer.GestureDetectedEvent -= OnGestureDetected;

		}

		private void OnGestureDetected(string gestureName, double confidence, Handedness hand, bool isDouble)
		{
			if (gestureName.StartsWith ("Right--") || gestureName.StartsWith ("Left--")) {
				return;
			}
			lastGestureDetected = gestureName;
		}

        override public void DoTutorial()
		{
			if (objectiveIt.Current == null || objectiveIt.Current.condition (objectiveIt.Current)) {
				if (objectiveIt.MoveNext ())
                {
                    lastGestureDetected = null;
					objectiveIt.Current.active = DateTime.Now;
					
                    SpeechBubbleManager.instance.AddSpeechbubble(
                        friendlyMouthTransform,
                        objectiveIt.Current.text,
                        SpeechBubbleManager.SpeechbubbleType.NORMAL,
                        99f,
                        Color.white,
                        Vector3.zero
                        );
				}
                else
                {
                    npc.character.Kill();
				}
			}
            if (objectiveIt.Current.loopingAnimation && DateTime.Now > timeToNextLoop)
            {
                friendly.GetComponentInChildren<Animator>().Play("Standing_1H_Magic_Attack_01");
                timeToNextLoop = DateTime.Now.AddSeconds(3);
            }
        }
    }
}