using System;
using UnityEngine;
namespace WizardArena
{
    public class TutorialHint : MonoBehaviour
    {
        private TextMesh textMesh;
        private MeshRenderer meshRenderer;
        private DateTime until;

        public void Awake()
        {
            textMesh = GetComponent<TextMesh>();
            meshRenderer = GetComponent<MeshRenderer>();
            textMesh.richText = true;
            meshRenderer.enabled = false;
        }

        private void Update()
        {
            if (meshRenderer.enabled && DateTime.Now > until)
            {
                HideHint();
            }
        }

        public void ShowHint(string hint)
        {
            ShowHint(hint, 5);
        }
        public void ShowHint(string hint, int seconds)
        {
            textMesh.text = hint;
            meshRenderer.enabled = true;
            until = DateTime.Now.AddSeconds(seconds);
        }

        public void HideHint()
        {
            textMesh.text = "";
            meshRenderer.enabled = false;
        }
    }
}