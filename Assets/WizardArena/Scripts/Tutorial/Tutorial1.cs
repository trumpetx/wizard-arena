using System.Collections.Generic;
using System;
using UnityEngine;

namespace WizardArena
{
    public class Tutorial1 : BaseTutorial
    {
        private DateTime timeToNextLoop;

        override protected string Name()
        {
            return "Well done!";
        }
        override protected void Setup()
        {
            CommonSetup(21f, new int[] { WizardConstants.ARCANE, WizardConstants.ARCANE });

			List<Objective> o = new List<Objective>();
			o.Add (new Objective("<b>" + Name() + "</b>", objective => DateTime.Now > objective.active.AddSeconds(3)));
			o.Add (new Objective("Let's practice a little.", objective => DateTime.Now > objective.active.AddSeconds(2)));
            o.Add(new Objective("I will continue to " +
                "\n demonstrate the correct form for you.", objective => DateTime.Now > objective.active.AddSeconds(4), true));
            o.Add (new Objective("Try defeating that evil wizard over there!", objective => DateTime.Now > objective.active.AddSeconds(3)));
            
            objectiveIt = o.GetEnumerator ();
        }
        void SetUpTutorial_1()
		{
			CommonSetup (21f, new int[] { WizardConstants.ARCANE, WizardConstants.ARCANE });
		}
		override public void DoTutorial()
		{
			if (objectiveIt.Current == null || objectiveIt.Current.condition (objectiveIt.Current)) {
				if (objectiveIt.MoveNext ())
                {
                    objectiveIt.Current.active = DateTime.Now;
                    SpeechBubbleManager.instance.AddSpeechbubble(
                        friendlyMouthTransform,
                        objectiveIt.Current.text,
                        SpeechBubbleManager.SpeechbubbleType.NORMAL,
                        99f,
                        Color.white,
                        Vector3.zero
                    );
                }
			}
            if (objectiveIt.Current.loopingAnimation && DateTime.Now > timeToNextLoop)
            {
                friendly.GetComponentInChildren<Animator>().Play("Standing_1H_Magic_Attack_01");
                timeToNextLoop = DateTime.Now.AddSeconds(3);
            }
        }
    }
}