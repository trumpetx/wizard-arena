using System;
using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
    public class Tutorial3 : BaseTutorial
    {
        private bool attackAfterShield = false;

        protected override string Name()
        {
            return "Now, for enemy offense!";
        }

        protected override void Setup()
        {
            CommonSetup(100f, new int[] { WizardConstants.ARCANE, WizardConstants.FIRE });

			List<Objective> o = new List<Objective>();
			o.Add (new Objective("<b>" + Name() + "</b>", objective => DateTime.Now > objective.active.AddSeconds(3)));
			o.Add (new Objective("Not all wizards are pacifists;" +
				"\n try defeating him once more!", objective => DateTime.Now > objective.active.AddSeconds(3)));
			objectiveIt = o.GetEnumerator ();
        }

        public override void DoTutorial()
        {
            if (objectiveIt.Current == null || objectiveIt.Current.condition(objectiveIt.Current))
            {
                if (objectiveIt.MoveNext())
                {
                    objectiveIt.Current.active = DateTime.Now;

                    SpeechBubbleManager.instance.AddSpeechbubble(
                        friendlyMouthTransform,
                        objectiveIt.Current.text,
                        SpeechBubbleManager.SpeechbubbleType.NORMAL,
                        99f,
                        Color.white,
                        Vector3.zero
                    );
                }
            }

            bool healthChanged = lasthealth != npc.character.GetCurrentHealth();
            lasthealth = npc.character.GetCurrentHealth();
            if (healthChanged && npc.character.GetCurrentHealth() < 50f)
            {
                npc.CmdDoShield(20);
                attackAfterShield = true;
            }
            else if (healthChanged && npc.character.GetCurrentHealth() < 100f)
            {
                npc.CmdDoShoot(shootDmg);
            }
            if (attackAfterShield && npc.animator.GetCurrentAnimatorStateInfo(0).IsName("standing_idle"))
            {
                attackAfterShield = false;
                npc.CmdDoMegaShoot(megaShootDmg);
            }
        }
    }
}