using UnityEngine;
using Edwon.VR.Gesture;
using System;
using System.Collections.Generic;


namespace WizardArena
{
    public abstract class BaseTutorial : ScriptableObject, ITutorial
    {
        public event EndTutorialHandler OnEndTutorial;

        // Created by the tutorial
        private GameObject spawn;
        protected NpcCharacter npc;

        // Scene objects and settings set by the tutorial manager
        protected GameObject enemy;
        protected GameObject friendly;
        protected Transform friendlyMouthTransform;
        protected NetworkWizardController player;
        protected CountdownTimer countdown;
        protected TutorialHint tutorialHint;
        protected int overrideDeathTime;
        protected Vector3 spawnLocation;
		protected IEnumerator<Objective> objectiveIt;

        protected int megaShootDmg = 29;
        protected int shootDmg = 20;
        protected float lasthealth;
        private int[] originalElements;

        public void SetupTutorial(GameObject friendly, Transform friendlyMouthTransform, GameObject enemy, NetworkWizardController player, CountdownTimer countdown, TutorialHint tutorialHint, int overrideDeathTime, Vector3 spawnLocation, EndTutorialHandler OnEndTutorial)
        {
            // This should be a constructor :/
            this.enemy = enemy;
            this.friendly = friendly;
            this.friendlyMouthTransform = friendlyMouthTransform;
            this.player = player;
            this.countdown = countdown;
            this.tutorialHint = tutorialHint;
            this.overrideDeathTime = overrideDeathTime;
            this.spawnLocation = spawnLocation;
            this.OnEndTutorial += OnEndTutorial;

			spawn = Instantiate (this.enemy, this.spawnLocation, Quaternion.identity);
			npc = spawn.GetComponentInChildren<NpcCharacter> ();
			npc.pew.SetFaceReferencePosition(spawn.transform);
			npc.character.OnDeath += Complete;
			npc.deathDestroyTime = overrideDeathTime;
            Setup();
        }

        protected void CommonSetup(float npcHealth, int[] playerElements)
        {
            player.player.ResetHealthAndMana();
            npc.character = new PlayerLogic(npcHealth, 0);
            originalElements = player.availableElements;
            if (playerElements != null)
            {
                player.availableElements = playerElements;
            }
            npc.OnShielded += HintOnShielded;
            GestureRecognizer.GestureRejectedEvent += HintOnGestureRejected;
        }

        private void HintOnShielded(int element)
        {
            tutorialHint.ShowHint("<b>Our enemy has been shielded by " + WizardConstants.ELEMENTS[element].GetName() + "!</b>" +
                "\n\nUse a different element to attack" +
                "\nby using the D-Pad on your left hand.");
        }

        private void HintOnGestureRejected(string error, string gestureName = null, double confidence = 0)
        {
            if (gestureName != null)
            {
                int idx = gestureName.LastIndexOf("--");
                if (idx > 0)
                {
                    gestureName = gestureName.Substring(idx + 2);
                }
                tutorialHint.ShowHint("It seems like you were trying to do" +
                    "\na <i>" + gestureName + "</i> spell.", 2);
            }
        }

        protected abstract string Name();
        protected abstract void Setup();
		public abstract void DoTutorial ();

        public virtual void Complete()
        {
            //npc.floatingCombatText.Money(npc.transform, 1);
            player.availableElements = originalElements;
            player.OnHandednessChange(PlayerSaveManager.instance.playerSave.leftHanded);
            if (npc != null)
                npc.OnShielded -= HintOnShielded;
            GestureRecognizer.GestureRejectedEvent -= HintOnGestureRejected;
            OnEndTutorial?.Invoke();
        }
    }

	public class Objective
	{
        public bool loopingAnimation { get; private set; }

		public string text { get; private set; }

		public Func<Objective, bool> condition { get; private set; }

		public DateTime active { get; set; }

        public Objective(string text, Func<Objective, bool> condition, bool looping)
        {
            this.text = text;
            this.condition = condition;
            this.loopingAnimation = looping;
        }

        public Objective(string text, Func<Objective, bool> condition) : this(text, condition, false) { }
    }
}