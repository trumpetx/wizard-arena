using UnityEngine;
namespace WizardArena
{
    public delegate void EndTutorialHandler();

    public interface ITutorial
    {
        void SetupTutorial(GameObject friendly, Transform friendlyMouthTransform,  GameObject enemy, NetworkWizardController player, CountdownTimer countdown, TutorialHint hint, int overrideDeathTime, Vector3 enemySpawnLocation, EndTutorialHandler onEndTutorialCallback);
        void DoTutorial();
    }
}