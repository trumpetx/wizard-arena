using System;
using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
    public class Tutorial2 : BaseTutorial
    {
        protected override string Name()
        {
            return "Okay, let's move on to enemy defenses.";
        }
        
        protected override void Setup()
        {
            CommonSetup(40f, new int[] { WizardConstants.ARCANE, WizardConstants.FIRE });

			List<Objective> o = new List<Objective>();
			o.Add (new Objective("<b>" + Name() + "</b>", objective => DateTime.Now > objective.active.AddSeconds(3)));
			o.Add (new Objective(
				"If your opponent knows how to defend themselves," +
				"\n you'll need to adapt!", objective => DateTime.Now > objective.active.AddSeconds(4)));
			o.Add (new Objective("Try defeating him again, and observe.", objective => DateTime.Now > objective.active.AddSeconds(2)));
			objectiveIt = o.GetEnumerator ();

            //friendly.GetComponentInChildren<Animator>().Play("Standing_Turn_Left_90");
        }

        public override void DoTutorial()
        {
            if (objectiveIt.Current == null || objectiveIt.Current.condition(objectiveIt.Current))
            {
                if (objectiveIt.MoveNext())
                {
                    objectiveIt.Current.active = DateTime.Now;
                    SpeechBubbleManager.instance.AddSpeechbubble(
                        friendlyMouthTransform,
                        objectiveIt.Current.text,
                        SpeechBubbleManager.SpeechbubbleType.NORMAL,
                        99f,
                        Color.white,
                        Vector3.zero
                    );
                }
            }

            bool healthChanged = lasthealth != npc.character.GetCurrentHealth();
            lasthealth = npc.character.GetCurrentHealth();
            if (healthChanged)
            {
                npc.CmdDoShield(20);
            }
        }
    }
}