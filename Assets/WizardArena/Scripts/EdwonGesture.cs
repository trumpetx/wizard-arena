using Edwon.VR;
using Edwon.VR.Gesture;
using UnityEngine;
using System;

namespace WizardArena
{
    public class EdwonGesture : IGestureRecognizer
    {
        private VRGestureRig rig { get; set; }
        private Action<Gesture> gestureCallback;

        public EdwonGesture(Transform rigParent, bool leftHanded, Action<Gesture> gestureCallback)
        {
            rig = rigParent.GetComponentInChildren<VRGestureRig>();
            if (rig == null)
            {
                Debug.LogError("No Edwon Gesture Rig detected");
            }
            else
            {
                rig.mainHand = leftHanded ? Handedness.Left : Handedness.Right;
                rig.Reset();
                rig.BeginDetect();
            }
            this.gestureCallback = gestureCallback;
            GestureRecognizer.GestureDetectedEvent += EdwonGestureDetected;
        }


        public void Disable()
        {
            if (rig != null)
            {
                rig.uiState = VRGestureUIState.Idle;
            }
            GestureRecognizer.GestureDetectedEvent -= EdwonGestureDetected;
        }

        private void EdwonGestureDetected(string gestureName, double confidence, Handedness hand, bool isDouble)
        {
            if (gestureName.StartsWith("Right--") || gestureName.StartsWith("Left--"))
            {
                return;
            }
            string confidenceString = confidence.ToString().Substring(0, 4);
            Debug.Log("detected gesture: " + gestureName + " with confidence: " + confidenceString);
            try
            {
                gestureCallback((Gesture) Enum.Parse(typeof(Gesture), gestureName.ToUpper()));
            }
            catch (ArgumentException e)
            {
                Debug.LogError(e);
            }
        }
    }
}