using UnityEngine;
using Zenject;

namespace WizardArena
{
    public class AsyncProcessor : MonoBehaviour
    {
        // Purposely left empty
    }

    public class UpdateHook : MonoBehaviour
    {
        public delegate void OnUpdateHandler();
        public event OnUpdateHandler OnUpdate;

        private void Update()
        {
            if(OnUpdate != null)
            {
                OnUpdate();
            }
        }
    }

    /// <summary>
    /// Provides global services
    /// </summary>
    public class GlobalContext : MonoInstaller<GlobalContext>
    {
        public override void InstallBindings()
        {
            // Global Singletons 
            Container.Bind<AsyncProcessor>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<UpdateHook>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<GameLifecycle>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<PlayerSaveManager>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            Container.Bind<NetworkManagerCustom>().FromComponentInNewPrefabResource("Prefabs/NetworkLobbyManager").AsSingle().NonLazy();
            
            Container.Bind<WizardArenaVariables>().AsSingle().NonLazy();
            Container.Bind<InventoryManager>().AsSingle().NonLazy();
            Container.Bind<CountdownTimer>().AsSingle().NonLazy();
            Container.Bind<GestureRecognizerFactory>().AsSingle().NonLazy();
        }
    }
}
