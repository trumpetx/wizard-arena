using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    [RequireComponent(typeof(Collider))]
    public class NetworkChildHitWithProjectile : NetworkBehaviour
    {
        public NetworkHitWithProjectile topLevelHitWithProjectile;

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject != topLevelHitWithProjectile.gameObject)
            {
                topLevelHitWithProjectile.OnCollisionEnter(other);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject != topLevelHitWithProjectile.gameObject)
            {
                topLevelHitWithProjectile.OnTriggerEnter(other);
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject != topLevelHitWithProjectile.gameObject)
            {
                topLevelHitWithProjectile.OnTriggerStay(other);
            }
        }
    }
}
