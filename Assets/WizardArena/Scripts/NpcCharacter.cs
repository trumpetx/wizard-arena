using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

namespace WizardArena
{
    public class NpcCharacter : NetworkBehaviour
    {
        public const float SHIELD_ANIM_TRIGGER_TIME = 1.12f;
        public const float SHOOT_ANIM_TRIGGER_TIME = .48f;
        public const float MEGA_SHOOT_ANIM_TRIGGER_TIME = 1.18f;

        public delegate void OnShieldedHandler(int element);
        public event OnShieldedHandler OnShielded;

        public float deathDestroyTime = 30f;
        public PlayerLogic character { get; set; }
        public NetworkHitWithProjectile hitWithProjectile { get; set; }
        public NpcPew pew { get; set; }
        public int lastHitWithElement { get; set; }
        public NetworkWizardController targetWizard { get; private set; }
        public Transform target { get { return targetWizard?.playerBody; } }

        // local player
        public Animator animator { get; private set; }
        private EnergyBar energyBar;
        public FloatingCombatText floatingCombatText; // optional floating combat text

        [SyncVar] public float characterHealth;
        [SyncVar] public float characterMana;
        [SyncVar] public float characterTotalHealth;
        [SyncVar] public float characterTotalMana;

        private void OnEnable()
        {
            character = new PlayerLogic(null);
            lastHitWithElement = -1;
            animator = GetComponent<Animator>();
            // "face" wrappers use a parent object, some npcs don't
            energyBar = energyBar?? (transform.parent ?? transform).GetComponentInChildren<EnergyBar>();
            floatingCombatText = floatingCombatText ?? (transform.parent ?? transform).GetComponentInChildren<FloatingCombatText>();

            // only used by server
            if (isServer)
            {
                pew = pew ?? GetComponent<NpcPew>();
                hitWithProjectile = hitWithProjectile ?? GetComponent<NetworkHitWithProjectile>();
                character.OnDeath += OnDeath;
                hitWithProjectile.OnHit += OnHit;
                FindTarget();
            }
        }

        private void FindTarget()
        {
            List<NetworkWizardController> wizards = ((NetworkManagerCustom)NetworkManager.singleton).wizards.Where(w => w.player.GetCurrentHealth() > 0).ToList();
            if (wizards.Count == 0)
            {
                if (targetWizard != null)
                {
                    targetWizard.player.OnDeath -= FindTarget;
                }
                targetWizard = null;
                character.Kill();
            }
            else
            {
                targetWizard = wizards[UnityEngine.Random.Range(0, wizards.Count)];
                targetWizard.player.OnDeath += FindTarget;
            }
        }

        private void OnDisable()
        {
            if (character != null)
            {
                character.OnDeath -= OnDeath;
            }
            if (hitWithProjectile != null)
            {
                hitWithProjectile.OnHit -= OnHit;
            }
            if (targetWizard != null)
            {
                targetWizard.player.OnDeath -= FindTarget;
            }
        }

        [Server]
        public void CmdDoShoot(float damage)
        {
            RpcDoShoot();
            StartCoroutine(WaitToShoot(damage));
        }

        [ClientRpc]
        public void RpcDoShoot()
        {
            animator.SetTrigger("1HAttack");
        }

        private IEnumerator WaitToShoot(float damage)
        {
            yield return new WaitForSeconds(SHOOT_ANIM_TRIGGER_TIME);
            pew.DoShoot(damage);
        }

        [Server]
        public void CmdDoMegaShoot(float damage)
        {
            RpcDoMegaShoot();
            StartCoroutine(WaitToMegaShoot(damage));
        }

        [ClientRpc]
        public void RpcDoMegaShoot()
        {
            animator.SetTrigger("2HAttack");
        }

        private IEnumerator WaitToMegaShoot(float damage)
        {
            yield return new WaitForSeconds(MEGA_SHOOT_ANIM_TRIGGER_TIME);
            pew.DoMegaShoot(damage);
        }
        
        [Server]
        public void CmdDoShield(int seconds)
        {
            if (pew != null)
            {
                RpcDoShield();
                // delay shield creation until shield animation reaches a certain predetermined point
                StartCoroutine(WaitToShield(seconds, lastHitWithElement));
            }
        }

        [ClientRpc]
        public void RpcDoShield()
        {
            animator.SetTrigger("shield");
        }

        private IEnumerator WaitToShield(int seconds, int elementToUse)
        {
            yield return new WaitForSeconds(SHIELD_ANIM_TRIGGER_TIME);
            pew.DoShield(elementToUse, seconds);
            character.SetShielded(DateTime.Now.AddSeconds(seconds), elementToUse);
        }
        
        [Server]
        private void OnHit(float healthChange, int element)
        {
            if (character.ShieldedBy().Contains(element))
            {
                Debug.Log("NPC shielded a hit!");
                OnShielded?.Invoke(element);
            }
            else
            {
                lastHitWithElement = element;
                character.ModifyHealth(healthChange);
                RpcFloatingCombatText(healthChange);
            }
        }

        [ClientRpc]
        private void RpcFloatingCombatText(float healthChange)
        {
            FacePlayerRotation p = gameObject.GetComponentInParent<FacePlayerRotation>();
            floatingCombatText?.HealthChange(p == null ? transform : p.transform, healthChange, false);
        }

        private void Update()
        {
            if (isServer && character != null)
            {
                // Server players sync vars are controlled by game logic
                if (characterHealth != character.GetCurrentHealth())
                {
                    characterHealth = character.GetCurrentHealth();
                }
                if (characterMana != character.GetCurrentMana())
                {
                    characterMana = character.GetCurrentMana();
                }
                if (characterTotalHealth != character.GetStartingHealth())
                {
                    characterTotalHealth = character.GetStartingHealth();
                }
                if (characterTotalMana != character.GetStartingMana())
                {
                    characterTotalMana = character.GetStartingMana();
                }
            }
            if (energyBar != null)
            {
                energyBar.SetValueMax((int)characterTotalHealth);
                energyBar.SetValueCurrent((int)characterHealth);
            }
        }

        [Server]
        private void OnDeath()
        {
            RpcOnDeath();
            pew?.ClearShields();
            FacePlayerRotation toplevel = GetComponentInParent<FacePlayerRotation>();
            Destroy(toplevel == null ? gameObject : toplevel.gameObject, deathDestroyTime);
        }

        [ClientRpc]
        private void RpcOnDeath()
        {
            animator.SetBool("isDead", true);
        }
    }
}