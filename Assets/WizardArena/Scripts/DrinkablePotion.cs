using UnityEngine;

namespace WizardArena
{
    [RequireComponent(typeof(Spawnable))]
    public class DrinkablePotion : MonoBehaviour
    {
        public float healthIncrease;
        public float manaIncrease;
        public bool invisibility;

        public Spawnable spawnable { get; private set; }

        private void Start()
        {
            spawnable = GetComponent<Spawnable>();
        }
    }
}