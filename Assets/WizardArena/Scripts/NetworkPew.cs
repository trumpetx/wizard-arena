using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    [RequireComponent(typeof(NetworkWizardController))]
    public class NetworkPew : NetworkBehaviour
    {
        public int enemyMask { get; set; }
        public const float MULTIPLAYER_MODIFIER = .5f;
        private const float v = 20f;
        public GameObject[] projectiles;
        public GameObject[] bigProjectiles;
        public GameObject[] shields;
        public GameObject[] beams;
        public GameObject[] cleaves;
        public GameObject[] walls;
        public GameObject[] summonThings;

        [SyncVar] [HideInInspector] public int element;

        private NetworkWizardController networkWizardController;
        private NetworkManagerCustom networkManager;

        private void Start()
        {
            networkManager = FindObjectOfType<NetworkManagerCustom>();
            networkWizardController = GetComponent<NetworkWizardController>();
            foreach(GameObject o in cleaves)
            {
                Networkize(o);
                if (o.GetComponent<ChangesHealth>() == null)
                {
                    o.AddComponent<ChangesHealth>();
                }
            }
            foreach(GameObject o in summonThings)
            {
                if(o != null)
                    ClientScene.RegisterPrefab(o);
            }
            foreach (GameObject o in projectiles)
            {
                Networkize(o);
                if (o.GetComponent<ChangesHealth>() == null)
                {
                    o.AddComponent<ChangesHealth>();
                }
            }
            foreach (GameObject o in bigProjectiles)
            {
                Networkize(o);
                if (o.GetComponent<ChangesHealth>() == null)
                {
                    o.AddComponent<ChangesHealth>();
                }
            }
            foreach (GameObject o in beams)
            {
                NetworkTransform t = Networkize(o);
                if (o.GetComponent<ChangesHealthOverTime>() == null)
                {
                    o.AddComponent<ChangesHealthOverTime>();
                }
            }
            foreach (GameObject o in walls)
            {
                Networkize(o);
                if (o.GetComponent<ChangesHealthOverTime>() == null)
                {
                    o.AddComponent<ChangesHealthOverTime>();
                }
            }
            foreach (GameObject o in shields)
            {
                Networkize(o);
            }
        }

        private NetworkTransform Networkize(GameObject o)
        {
            NetworkIdentity i = o.GetComponent<NetworkIdentity>();
            NetworkTransform t = o.GetComponent<NetworkTransform>();
            if (t == null)
            {
                t = o.AddComponent<NetworkTransform>();
            }
            if (i == null)
            {
                i = o.AddComponent<NetworkIdentity>();
            }
            i.localPlayerAuthority = false;
            i.serverOnly = false;
            t.sendInterval = 0;
            ClientScene.RegisterPrefab(o);
            return t;
        }
        

        [Command]
        public void CmdSetElement(int element)
        {
            this.element = element;
        }
        
        public void DoSummon(Vector3 position, Quaternion rotation, float damage, float seconds)
        {
            CmdSummon(position, rotation, damage, seconds);
        }

        [Command]
        public void CmdSummon(Vector3 position, Quaternion rotation, float damage, float seconds)
        {
            GameObject summon = Instantiate(summonThings[element], position, networkWizardController.feet.rotation);
            ChangesHealth doesDamage = summon.GetComponent<ChangesHealth>();
            if(doesDamage != null)
            {
                doesDamage.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
                doesDamage.element = element;
            }
            else
            {
                ChangesHealthOverTime dot = summon.GetComponent<ChangesHealthOverTime>();
                if(dot != null)
                {
                    dot.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
                    dot.element = element;
                    dot.perSeconds = .33f;
                }
            }
            Util.AssignLayer(summon.transform, gameObject.layer);
            NetworkServer.Spawn(summon);
            Destroy(summon, seconds);
        }

        [Server]
        public void DoShoot( float damage )
        {
            GameObject projectile = Instantiate(projectiles[element], GetShootReference().position + GetShootReference().forward, GetShootReference().rotation);
            ChangesHealth d = projectile.GetComponent<ChangesHealth>();
            d.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
            d.element = element;
            projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * v;
            projectile.layer = gameObject.layer;
            NetworkServer.Spawn(projectile);
            Destroy(projectile, 5f);
        }

        [Server]
        public void DoMegaShoot(float damage)
        {
            GameObject projectile = Instantiate(bigProjectiles[element], GetShootReference().position + GetShootReference().forward, GetShootReference().rotation);
            ChangesHealth d = projectile.GetComponent<ChangesHealth>();
            d.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
            d.element = element;
            projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * v;
            projectile.layer = gameObject.layer;
            NetworkServer.Spawn(projectile);
            Destroy(projectile, 5f);
        }

        [Server]
        public void DoWall(float seconds, float damage)
        {
            Vector3 spawnPos = GetFootReference().position + GetFootReference().forward * 2;
            GameObject wall = Instantiate(walls[element], spawnPos, networkWizardController.feet.rotation);
            ChangesHealthOverTime dot = wall.GetComponent<ChangesHealthOverTime>();
            dot.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
            dot.element = element;
            dot.perSeconds = .33f;
            wall.layer = gameObject.layer;
            NetworkServer.Spawn(wall);
            Destroy(wall, seconds);
        }

        [Server]
        public void DoCleave(float damage)
        {
            Transform reference = GetFaceReference();
            GameObject cleave = Instantiate(cleaves[element], reference.position, reference.rotation);
            ChangesHealth d = cleave.GetComponent<ChangesHealth>();
            d.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
            d.element = element;
            cleave.layer = gameObject.layer;
            NetworkServer.Spawn(cleave);
            Destroy(cleave, 2f);

            RaycastHit[] hits = Physics.CapsuleCastAll(
                reference.position + new Vector3(0, -.3f, 0),
                reference.position + new Vector3(0, .6f, 0),
                .3f,
                reference.forward,
                100f,
                enemyMask,
                QueryTriggerInteraction.Ignore);
            Debug.DrawRay(reference.position + new Vector3(0, -.3f, 0), reference.forward, Color.green, 10f);
            Debug.DrawRay(reference.position + new Vector3(0, .6f, 0), reference.forward, Color.green, 10f);
            HashSet<NetworkHitWithProjectile> uniqueColliders = new HashSet<NetworkHitWithProjectile>();
            foreach (RaycastHit hit in hits)
            {
                NetworkChildHitWithProjectile child = hit.collider.GetComponent<NetworkChildHitWithProjectile>();
                if (child != null)
                {
                    uniqueColliders.Add(child.topLevelHitWithProjectile);
                }
                else
                {
                    NetworkHitWithProjectile parent = hit.collider.GetComponent<NetworkHitWithProjectile>();
                    if (parent != null)
                    {
                        uniqueColliders.Add(parent);
                    }
                }
            }
            foreach (NetworkHitWithProjectile hit in uniqueColliders)
            {
                hit.DoHealthChange(d, cleave);
            }
            Debug.Log(hits.Length + " cleave hits with mask " + enemyMask);
            Debug.Log(uniqueColliders.Count + " unique cleave hits");
        }

        [Server]
        public void DoShield(float seconds)
        {
            GameObject shield = Instantiate(shields[element], GetFaceReference().position, Quaternion.identity);
            shield.layer = gameObject.layer;
            NetworkServer.Spawn(shield);
            Destroy(shield, seconds);
        }

        [Server]
        public void DoBeam(float damage, float seconds)
        {
            GameObject beam = Instantiate(beams[element], GetShootReference().position + GetShootReference().forward, GetShootReference().rotation);
            ChangesHealthOverTime dot = beam.GetComponent<ChangesHealthOverTime>();
            dot.healthChange = -1 * damage * MULTIPLAYER_MODIFIER;
            dot.element = element;
            dot.perSeconds = .33f;
            beam.layer = gameObject.layer;
            beam.transform.parent = GetShootReference();
            NetworkServer.Spawn(beam);
            RpcSyncBeamToShootReference(beam);
            Destroy(beam, seconds);
        }

        [ClientRpc]
        public void RpcSyncBeamToShootReference(GameObject go)
        {
            Transform parent = GetShootReference();
            go.transform.parent = parent;
            go.transform.rotation = parent.rotation;
        }

        public Transform GetFootReference()
        {
            return networkWizardController.feet;
        }

        public Transform GetFaceReference()
        {
            return networkWizardController.head.transform;
        }

        public Transform GetShootReference()
        {
            return networkWizardController.leftHanded ? networkWizardController.leftHand.transform : networkWizardController.rightHand.transform;
        }
    }
}