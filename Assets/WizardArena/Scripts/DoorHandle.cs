using UnityEngine;
using VRTK;

namespace WizardArena
{
    [RequireComponent(typeof(MeshRenderer))]
    public class DoorHandle : MonoBehaviour
    {
        private VRTK_InteractableObject interactibleObject;
        private MeshRenderer meshRenderer;
        private Color color;

        private void OnEnable()
        {
            meshRenderer = GetComponent<MeshRenderer>();
            interactibleObject = GetComponentInChildren<VRTK_InteractableObject>();
            color = meshRenderer.material.color;
            meshRenderer.material.color = Color.white;
            interactibleObject.InteractableObjectUsed += OnInteractableObjectUsed;
        }

        private void OnDisable()
        {
            interactibleObject.InteractableObjectUsed -= OnInteractableObjectUsed;
        }
        
        public void OnInteractableObjectUsed(object sender, InteractableObjectEventArgs args)
        {
            FindObjectOfType<NetworkSceneSwitcher>().CmdChangeScene(NetworkSceneSwitcher.PRACTICE_ROOM);
        }
    }
}