using UnityEngine;

namespace WizardArena
{
    public class ChangesHealthOverTime : MonoBehaviour
    {
        public float timeSinceLastChange { get; set; }
        public int element { get; set; }
        public float healthChange { get; set; }
        public float perSeconds = 1f;

        private void OnEnable()
        {
            element = -1;
            healthChange = 0;
            timeSinceLastChange = 0;
        }
    }
}