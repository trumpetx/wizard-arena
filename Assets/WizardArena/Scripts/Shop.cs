using PowerUI;
using UnityEngine;

namespace WizardArena
{
    public class Shop : MonoBehaviour
    {
        private static AudioClip purchaseSound;
        private MenuManager player;
        private AudioSource audioSource;
        private HtmlDocument document;
        private PlayerSaveManager playerSaveManager;
        private InventoryManager inventoryManager;

        private int displayedCurrency = -1;
        

        private void Update()
        {
            if(playerSaveManager != null && displayedCurrency != playerSaveManager.playerSave.currency)
            {
                displayedCurrency = playerSaveManager.playerSave.currency;
                document.getElementById("playerBank").innerHTML = displayedCurrency + " &#x0110;";
            }
        }

        private void Start()
        {
            purchaseSound = Resources.Load<AudioClip>("Sound/Coins_Several_04");
            inventoryManager = InventoryManager.instance;
            playerSaveManager = PlayerSaveManager.instance;
            audioSource = GetComponent<AudioSource>();
            player = FindObjectOfType<MenuManager>();
            document = GetComponent<WorldUIHelper>().document;
            Dom.Node shop = document.getById("shop");
            Dom.Element tr = document.createElement("tr");
            tr.innerHTML = "<th class='iconHeader'>Item</th><th class='descriptionHeader'>Description</th><th class='priceHeader'>Price</th>";
            document.getById("shopHeader").appendChild(tr);
            for (int i=0; i < inventoryManager.auctionItmes.Count; i++)
            {
                int sId = inventoryManager.auctionItmes[i];
                Spawnable s = inventoryManager.allItems[sId];
                if(s.isUnique && inventoryManager.HasItem(sId))
                {
                    // If the player has the item in their inventory or spawned already, don't display this item for sale
                    continue;
                }
                tr = document.createElement("tr");
                tr.id = "item_" + i;
                tr.innerHTML = "<td valign='top'><div class='descrtitle'>" + s.title + "<div><div class='description'>" + s.description + "</div></td><td class='price'>" + s.price + " &#x0110;</td>";
                HtmlTableCellElement td1 = (HtmlTableCellElement) document.createElement("td");
                td1.className = "icon";
                HtmlImageElement img = (HtmlImageElement) document.createElement("img");
                img.image = s.icon.texture;
                img.width = img.height = "100";
                td1.appendChild(img);
                td1.vAlign = "middle";
                string _trId = tr.id;
                tr.addEventListener("mouseup", delegate (MouseEvent e) {
                    if (playerSaveManager.playerSave.currency >= s.price)
                    {
                        player.AddToInventory(s.itemId);
                        playerSaveManager.playerSave.currency -= s.price;
                        audioSource.PlayOneShot(purchaseSound, .75f);
                        if (s.isUnique)
                        {
                            document.getElementById(_trId).remove();
                        }
                        playerSaveManager.SaveAsync();
                    }
                    else
                    {
                        Haptics.Pulse();
                    }
                });
                tr.prependChild(td1);
                shop.appendChild(tr);
            }
        }
    }
}
