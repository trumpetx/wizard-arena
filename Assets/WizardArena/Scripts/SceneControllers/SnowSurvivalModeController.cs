using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
	public class SnowSurvivalModeController : SurvivalModeController {

        public static Vector3 position = new Vector3(-551.5f, 43, -250);
        public static Quaternion rotation = Quaternion.Euler(0, -114, 0);

		public override IEnumerator<SurvivalLocation> InitSurvivalLocations(){
			List<SurvivalLocation> l = new List<SurvivalLocation> ();
			l.Add(new SurvivalLocation(GameObject.FindGameObjectsWithTag("SpawnPoints"),
                position, rotation));
			return l.GetEnumerator();
		}

        protected override string MapName()
        {
            return AbstractLeaderboardController.MAGIC_DIAL;
        }
    }
}