using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
	public class MoleMountainSurvivalModeController : SurvivalModeController {

        public static Vector3 position = new Vector3(-39.55f, 93.881f, -39.32f);
        public static Quaternion rotation = Quaternion.Euler(0, 111.054f, 0);

        public override IEnumerator<SurvivalLocation> InitSurvivalLocations(){
			List<SurvivalLocation> l = new List<SurvivalLocation> ();
			l.Add(new SurvivalLocation(GameObject.FindGameObjectsWithTag("SpawnPoints"),
                position, rotation));
			return l.GetEnumerator();
		}

        protected override string MapName()
        {
            return AbstractLeaderboardController.MOLE_MOUNTAIN;
        }
    }
}