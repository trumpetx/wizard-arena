using System.Collections;
using UnityEngine;
using Zenject;

namespace WizardArena
{
    public class TutorialController : MonoBehaviour
    {
        public int secondsBetweenTutorials = 5;
        public Transform enemyTransform;// = new Vector3(0f, 0.14f, 21f);
        public Transform friendlyTransform;
        public Transform friendlyMouthTransform;
        public GameObject enemy;
        public GameObject friendly;
        private GameObject friendlySpawn;
        private NetworkWizardController player;
        private CountdownTimer countdownTimer;
        private TutorialHint tutorialHint;
        private IEnumerator tutorialIt;

        [Inject]
        public void Init(CountdownTimer countdownTimer)
        {
            this.countdownTimer = countdownTimer;
        }

        private void Start()
        {
            player = NetworkWizardController.localPlayer;
            tutorialHint = GetComponentInChildren<TutorialHint>();
            tutorialIt = new ITutorial[]
            {
            ScriptableObject.CreateInstance("Tutorial0") as ITutorial,
            ScriptableObject.CreateInstance("Tutorial1") as ITutorial,
            ScriptableObject.CreateInstance("Tutorial2") as ITutorial,
            ScriptableObject.CreateInstance("Tutorial3") as ITutorial,
            ScriptableObject.CreateInstance("Tutorial4") as ITutorial,
            }.GetEnumerator();
            NextLevel();

            // passed position and rotation separately. If Transform is passed, it is assigned as parent
            friendlySpawn = Instantiate(friendly, friendlyTransform.position, friendlyTransform.rotation);
        }

        void NextLevel()
        {
            if (!tutorialIt.MoveNext())
            {
                // Cheer sound
                FindObjectOfType<NetworkSceneSwitcher>().CmdChangeScene(NetworkSceneSwitcher.PRACTICE_ROOM);
                return;
            }

            countdownTimer.StartCountdown(secondsBetweenTutorials, Color.black, () =>
            {
                // Inject known elements to save lookup
                ((ITutorial)tutorialIt.Current).SetupTutorial(friendlySpawn, friendlyMouthTransform, enemy, player, countdownTimer, tutorialHint, secondsBetweenTutorials - 1, enemyTransform.position, () =>
                {
                    CancelInvoke("Run");
                    NextLevel();
                });
                InvokeRepeating("Run", 1f, 1f);
            });
        }

        void Run()
        {
            ((ITutorial)tutorialIt.Current).DoTutorial();
        }
    }
}