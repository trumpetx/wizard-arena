using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
	public class ForestSurvivalModeController : SurvivalModeController {

        // Starting position and rotation, updated for each location in scene
        public static Vector3 position = new Vector3(-261.5f, 109.5f, 578.5f);
        public static Quaternion rotation = Quaternion.Euler(0, -101.5f, 0);

        public override IEnumerator<SurvivalLocation> InitSurvivalLocations(){
			List<SurvivalLocation> l = new List<SurvivalLocation> ();
			l.Add(new SurvivalLocation(GameObject.FindGameObjectsWithTag("FortSpawnPoints"),
                position, rotation));

            position = new Vector3(-210f, 214.5f, 334.8f);
            rotation = Quaternion.Euler(0, -249f, 0);
            l.Add(new SurvivalLocation(GameObject.FindGameObjectsWithTag("GateSpawnPoints"),
                position, rotation));

            position = new Vector3(-236f, 214.5f, 333.3f);
            rotation = Quaternion.Euler(0, -114.5f, 0);
            l.Add(new SurvivalLocation(GameObject.FindGameObjectsWithTag("CastleSpawnPoints"),
                position, rotation));
			return l.GetEnumerator();
		}

        protected override string MapName()
        {
            return AbstractLeaderboardController.CLIFF_CASTLE;
        }
    }
}