using UnityEngine;
using Zenject;
using System.Collections.Generic;
using System.Linq;
using VRTK;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace WizardArena
{
	public class SurvivalLocation
	{
		public Transform[] spawnPoints { get; set;}
		public Vector3 playerPos { get; set;}
		public Quaternion playerRot { get; set;}

		public SurvivalLocation(GameObject[] spawnPoints, Vector3 playerPos, Quaternion playerRot)
		{
			this.spawnPoints = new List<GameObject>(spawnPoints).Select(sp => sp.transform).ToArray();
			this.playerPos = playerPos;
			this.playerRot = playerRot;
		}
	}

    [RequireComponent(typeof(NetworkIdentity))]
    public abstract class SurvivalModeController : VRTKNetworkBehavior
    {
        public AudioClip clip;
        public GameObject[] enemiesToSpawn;
		private IEnumerator<SurvivalLocation> survivalLocations;

        private int npcSpawnCt = 0;
        private int npcDeathCt = 0;
        private int wave = 0;
        private float chanceOfSpawnPerSecond = WizardArenaVariables.defaultChanceOfSpawnPerSecond;
        private float chanceOfDoubleSpawn = WizardArenaVariables.defaultChanceOfDoubleSpawn;
        private float chanceOfSpawnPerSecondMuliplier = WizardArenaVariables.defaultChanceOfSpawnPerSecondMuliplier;
        private float chanceOfDoubleSpawnMuliplier = WizardArenaVariables.defaultChanceOfDoubleSpawnMuliplier;
        private int spawnCountMultiplier = WizardArenaVariables.defaultSpawnCountMultiplier;
        private int baseSpawnCount = WizardArenaVariables.defaultBaseSpawnCount;

        private int kills;

        private NetworkManagerCustom networkManager;
        private CountdownTimer countdownTimer;
        private ILeaderboardController leaderboardController;
        private Coroutine spawnRoutine;

		public virtual IEnumerator<SurvivalLocation> InitSurvivalLocations(){
			return new List<SurvivalLocation> (
                new SurvivalLocation[] {
                    new SurvivalLocation(GameObject.FindGameObjectsWithTag("SpawnPoints"), Vector3.zero, Quaternion.identity)
                }).GetEnumerator();
		}

        protected abstract string MapName();

        private void Start()
        {
            foreach(GameObject go in enemiesToSpawn)
            {
                ClientScene.RegisterPrefab(go);
            }
        }

        private void OnEnable()
        {
            networkManager = ((NetworkManagerCustom)NetworkManager.singleton);
            WizardArenaVariables wizardArenaVariables = WizardArenaVariables.instance;
            chanceOfSpawnPerSecondMuliplier = wizardArenaVariables.chanceOfSpawnPerSecondMuliplier;
            chanceOfDoubleSpawnMuliplier = wizardArenaVariables.chanceOfDoubleSpawnMuliplier;
            spawnCountMultiplier = wizardArenaVariables.spawnCountMultiplier;
            baseSpawnCount = wizardArenaVariables.baseSpawnCount;
            chanceOfSpawnPerSecond = wizardArenaVariables.chanceOfSpawnPerSecond;
            chanceOfDoubleSpawn = wizardArenaVariables.chanceOfDoubleSpawn;
            
            countdownTimer = CountdownTimer.instance;
            leaderboardController = GameLifecycle.instance.leaderboardController;

            if (isServer)
            {
                survivalLocations = InitSurvivalLocations();
                if (survivalLocations == null)
                {

                    throw new UnityException("'Survival Mode' requires spawn points");
                }
                networkManager.OnRegisterWizard += InitWizard;
                if (networkManager.ready)
                {
                    networkManager.wizards.ForEach(w => InitWizard(w));
                }
                StartCoroutine(WaitForPlayersAndStart());
            }
        }

        private IEnumerator WaitForPlayersAndStart()
        {
            yield return new WaitUntil(() => networkManager.ready);
            NextRound();
            spawnRoutine = StartCoroutine(Spawn());
        }

        public void InitWizard(NetworkWizardController wizard)
        {
            wizard.player.OnDeath += NextRound;
        }
        private void OnDisable()
        {
            if(spawnRoutine != null)
            {
                StopCoroutine(spawnRoutine);
                spawnRoutine = null;
            }
            if(networkManager != null)
            {
                networkManager.OnRegisterWizard -= InitWizard;
            }
        }

        [Server]
        private void OnNpcDeath()
        {
            kills++;
            if (0 == --npcDeathCt)
            {
                RpcNewWave(++wave);
            }
        }

        private bool AllWizardsDead()
        {
            bool allWizardsDead = ((NetworkManagerCustom)NetworkManager.singleton).wizards.TrueForAll(w => w.player.GetCurrentHealth() <= 0);
            return allWizardsDead;
        }

        [Server]
		public void NextRound()
		{
            if (!AllWizardsDead())
            {
                return;
            }
			if (!survivalLocations.MoveNext()) {
				int score = wave * 10 + kills;
                RpcEndGame(score);
                StartCoroutine(GoToPracticeRoom());
            } 
			else 
			{
                RpcSetPlayerPosition(survivalLocations.Current.playerPos, survivalLocations.Current.playerRot);
				RpcNewWave(++wave);
			}
		}

        private IEnumerator GoToPracticeRoom()
        {
            yield return new WaitForSeconds(4);
            FindObjectOfType<NetworkSceneSwitcher>().CmdChangeScene(NetworkSceneSwitcher.PRACTICE_ROOM);
        }

        [ClientRpc]
        private void RpcEndGame(int score)
        {
            countdownTimer.StartCountdown("Defeat! Wave: " + wave, 4, Color.red, () => { });
            if (leaderboardController != null)
            {
                leaderboardController.UpdateScore(MapName(), score);
            }
        }

        [ClientRpc]
        private void RpcSetPlayerPosition(Vector3 pos, Quaternion rot)
        {
            VRTK_DeviceFinder.PlayAreaTransform().SetPositionAndRotation(pos, rot);
        }

        [ClientRpc]
        private void RpcNewWave(int wave)
        {
            this.wave = wave;
            countdownTimer.StartCountdown("Wave " + wave, 10, Color.black, () =>
            {
                if (isServer)
                {
                    npcSpawnCt = baseSpawnCount + wave * spawnCountMultiplier;
                    chanceOfSpawnPerSecond *= chanceOfSpawnPerSecondMuliplier;
                    chanceOfDoubleSpawn *= chanceOfDoubleSpawnMuliplier;
                    npcDeathCt = npcSpawnCt;
                    Debug.Log("Wave " + wave + "\nnpcSpawnCt: " + npcSpawnCt + "\nchanceOfSpawnPerSecond: " + chanceOfSpawnPerSecond);
                    ((NetworkManagerCustom)NetworkManager.singleton).wizards.ForEach(w => w.player.ResetHealthAndMana());
                }
            });
        }

        private IEnumerator Spawn()
        {
            while (isActiveAndEnabled)
            {
                yield return new WaitForSeconds(1);
                DoSpawn(chanceOfSpawnPerSecond);
            }
        }

        private void DoSpawn(float chanceOfSpawn)
        {
            bool enemiesLeftToSpawn = npcSpawnCt > 0;
            bool noEnemiesUp = npcSpawnCt == npcDeathCt;
            bool randomChance = UnityEngine.Random.Range(0f, 1f) < chanceOfSpawn;
			if (enemiesLeftToSpawn && (noEnemiesUp || randomChance) && survivalLocations.Current.spawnPoints.Length > 0 && enemiesToSpawn.Length > 0 && !AllWizardsDead())
            {
				Transform spawnLocation = survivalLocations.Current.spawnPoints[UnityEngine.Random.Range(0, survivalLocations.Current.spawnPoints.Length)];
                GameObject enemy = enemiesToSpawn[UnityEngine.Random.Range(0, enemiesToSpawn.Length)];
                GameObject instantiatedEnemy = Instantiate(enemy, spawnLocation.position, spawnLocation.rotation);
                instantiatedEnemy.SetActive(false);
                NpcCharacter npc = instantiatedEnemy.GetComponent<NpcCharacter>();
                npc.character.OnDeath += OnNpcDeath;
                NetworkServer.Spawn(npc.gameObject);
                instantiatedEnemy.SetActive(true);
                npcSpawnCt--;
                DoSpawn(chanceOfDoubleSpawn);
            }
        }
    }
}