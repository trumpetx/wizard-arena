﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using VRTK;

namespace WizardArena
{
    [RequireComponent(typeof(NetworkIdentity))]
    public class LobbyWizard : NetworkLobbyPlayer
    {
        public static LobbyWizard localPlayer;
        public Transform head;
        public Transform playerBody;
        public Transform leftHand;
        public Transform rightHand;
        public Transform feet;

        // Client side
        private MenuManager menuManager;

        // Server side
        private MultiplayerHostPage multiplayerHostPage;
        private OptionsMenuManager optionsMenuManager;
        [SyncVar] [HideInInspector] public string playerName;
        [SyncVar] [HideInInspector] public int hmd;
        public static readonly IDictionary<string, NetworkIdentity> otherPlayers = new Dictionary<string, NetworkIdentity>();

        private void Start()
        {
            transform.parent = NetworkManager.singleton.transform;
            if (isServer)
            {
                optionsMenuManager = optionsMenuManager ?? OptionsMenuManager.instance;
                multiplayerHostPage = multiplayerHostPage ?? optionsMenuManager.multiplayerHostPage;
            }
            if (isLocalPlayer)
            {
                VRTK_DeviceFinder.PlayAreaTransform().SetPositionAndRotation(transform.position, transform.rotation);
                menuManager = menuManager ?? GetComponent<MenuManager>();
                menuManager.SetupMenusOutOfCombat();
                localPlayer = this;
                optionsMenuManager.ServerOnPlayerJoinOrLeave();
                foreach (MeshRenderer r in GetComponentsInChildren<MeshRenderer>())
                {
                    r.enabled = false;
                }
            }
            DontDestroyOnLoad(gameObject);
        }

        public override void OnClientEnterLobby()
        {
            base.OnClientEnterLobby();
            StartCoroutine(SetPlayerVars());
        }

        private IEnumerator SetPlayerVars()
        {
            yield return new WaitUntil(() => VRTK_SDKManager.instance.loadedSetup != null);
            CmdSetPlayerVars(GameLifecycle.instance.username, (int)VRTK_DeviceFinder.GetHeadsetType(), isServer);
        }

        private void OnPlayerDisconnected(NetworkPlayer player)
        {
            if (isServer)
            {
                otherPlayers.Remove(playerName);
                optionsMenuManager.ServerOnPlayerJoinOrLeave();
            }
        }

        [Command]
        public void CmdSetPlayerVars(string playerName, int hmd, bool isHost)
        {
            this.playerName = playerName;
            this.hmd = hmd;
            if (!isHost)
            {
                otherPlayers[playerName] = GetComponent<NetworkIdentity>();
                optionsMenuManager.ServerOnPlayerJoinOrLeave();
                optionsMenuManager.ShowMenu(true, optionsMenuManager.multiplayerHostPage.gameObject);
            }
        }

        public void Update()
        {
            if (isLocalPlayer)
            {
                NetworkWizardController.SetLocalPlayerTransformPositions(head, rightHand, leftHand, playerBody, feet);
                transform.position = playerBody.position;
            }
        }
    }
}