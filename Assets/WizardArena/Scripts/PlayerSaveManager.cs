using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WizardArena
{
    public class PlayerSave
    {
        public static readonly List<int> DEMO_ELEMENTS = new List<int>(new int[] { WizardConstants.ARCANE, WizardConstants.EARTH, WizardConstants.FIRE, WizardConstants.LIGHT });
        public const int GENDER_MALE = 0;
        public const int GENDER_FEMALE = 1;

        public List<int> selectedElements;
        public List<int> availableElements;
        public int currency = 100;
        public bool leftHanded = false;
        public Dictionary<int, int> inventory = new Dictionary<int, int>();
        public int maxElements = 3;
        public float menuDistance = .7f;
        public int menuCurve = 140;
        public int gender = GENDER_MALE;
        public int skinTone = 1;
        public float volume = .5f;
        public int graphicsLevel = 3;
        public float experiencePctEarned = 0;
        public int level = 1;
        public int lastPlatform = GameLifecycle.NOPLATFORM;

        public PlayerSave()
        {
            availableElements = DEMO_ELEMENTS;
            selectedElements = new List<int>(new int[] { WizardConstants.ARCANE });
        }
    }

    public class PlayerSaveManager : MonoBehaviour
    {
        public static PlayerSaveManager instance;
        private byte[] pw = new byte[] { 0x27, 0x1e, 0xfc, 0x55, 0x77, 0xf1, 0xe9, 0x27, 0x52, 0xac, 0x27, 0xa3, 0x8b, 0xf8, 0xc6, 0xea, 0x79, 0x59, 0x09, 0x8a, 0xee, 0xfb, 0x8e, 0x4c, 0x7c, 0x13, 0x48, 0x6b, 0x41, 0x16, 0xb4, 0xdb };

        private PlayerSave _playerSave;
        public PlayerSave playerSave
        {
            get { _playerSave = _playerSave ?? Load(); return _playerSave; }
            private set { _playerSave = value; }
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;

                if (Application.isPlaying)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }
        private void OnEnable()
        {
            SceneManager.activeSceneChanged += AutoSave;
        }

        private void OnDisable()
        {
            SceneManager.activeSceneChanged -= AutoSave;
        }

        private void OnApplicationQuit()
        {
            Save(playerSave);
        }

        private void AutoSave(Scene arg0, Scene arg1)
        {
            SaveAsync();
        }

        public void SaveAsync()
        {
            StartCoroutine(DoSaveAsync(playerSave));
        }

        private IEnumerator DoSaveAsync(PlayerSave playerSave)
        {
            Save();
            yield return null;
        }

        public void Save()
        {
            Save(playerSave);
        }

        private PlayerSave Save(PlayerSave saveGame)
        {
            lock (this)
            {
                try
                {
                    PlayerPrefsX.SetBool("leftHanded", saveGame.leftHanded);
                    PlayerPrefs.SetInt("gender", saveGame.gender);
                    PlayerPrefs.SetInt("skinTone", saveGame.skinTone);
                    PlayerPrefs.SetInt("graphicsLevel", saveGame.graphicsLevel);
                    PlayerPrefs.SetInt("menuCurve", saveGame.menuCurve);
                    PlayerPrefs.SetInt("lastPlatform", saveGame.lastPlatform);
                    PlayerPrefs.SetFloat("menuDistance", saveGame.menuDistance);
                    PlayerPrefs.SetFloat("volume", saveGame.volume);
                    PlayerPrefsX.SetIntArray("selectedElements", saveGame.selectedElements.ToArray());
                    using (ES2Writer writer = ES2Writer.Create("shader64.dll?encrypt=true&password=" + Convert.ToBase64String(pw).Replace("=", "").Replace("&", "")))
                    {
                        writer.Write(saveGame.currency, "currency");
                        writer.Write(saveGame.availableElements, "availableElements");
                        writer.Write(saveGame.inventory, "inventory");
                        writer.Write(saveGame.maxElements, "maxElements");
                        writer.Write(saveGame.experiencePctEarned, "experiencePctEarned");
                        writer.Write(saveGame.level, "level");
                        writer.Save();
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("Error writing save file.");
                    return saveGame;
                }
            }
            Debug.Log("Player Saved");
            return saveGame;
        }

        public PlayerSave Load()
        {
            Debug.Log("Loading Save");
            PlayerSave s = new PlayerSave();
            lock (this)
            {
                try
                {
                    s.leftHanded = PlayerPrefsX.GetBool("leftHanded", false);
                    s.gender = PlayerPrefs.GetInt("gender", PlayerSave.GENDER_MALE);
                    s.graphicsLevel = PlayerPrefs.GetInt("graphicsLevel", 2);
                    s.skinTone = PlayerPrefs.GetInt("skinTone", 1);
                    s.menuCurve = PlayerPrefs.GetInt("menuCurve", 140);
                    s.lastPlatform = PlayerPrefs.GetInt("lastPlatform", GameLifecycle.NOPLATFORM);
                    s.menuDistance = PlayerPrefs.GetFloat("menuDistance", 1.2f);
                    s.volume = PlayerPrefs.GetFloat("volume", .5f);
                    s.selectedElements = new List<int>(PlayerPrefsX.GetIntArray("selectedElements", WizardConstants.ARCANE, 1));
                    using (ES2Reader reader = ES2Reader.Create("shader64.dll?encrypt=true&password=" + Convert.ToBase64String(pw).Replace("=", "").Replace("&", "")))
                    {
                        s.currency = SafeRead(reader, "currency", 0);
                        s.availableElements = reader.ReadList<int>("availableElements");
                        if(s.availableElements.Count == 0)
                        {
                            s.availableElements = PlayerSave.DEMO_ELEMENTS;
                        }
                        s.inventory = reader.ReadDictionary<int, int>("inventory");
                        s.maxElements = SafeRead(reader, "maxElements", 3);
                        s.level = SafeRead(reader, "level", 1);
                        s.experiencePctEarned = SafeRead(reader, "experiencePctEarned", 0f);
                        return s;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("First time running the application");
                    Save(s);
                }
            }
            return s;
        }

        private T SafeRead<T>(ES2Reader file, string tag, T defaultValue)
        {

            if (file.TagExists(tag))
            {
                return file.Read<T>(tag);
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
