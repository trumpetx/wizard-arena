using System;
using System.Collections;
using UnityEngine;
using VRTK;

namespace WizardArena
{
    public class TowerLandscapeIntro : MonoBehaviour
    {
        public static Vector3 position = new Vector3(360.8f, 152.11f, -167.39f);
        public static Vector3 endPosition = new Vector3(160.8f, 52.11f, -167.39f);
        public static Quaternion rotation = Quaternion.Euler(1.2f, -44.8f, -2.4f);

        private static float speed = 10f;

        public Transform friendlyWizardMouth;

        public GameObject fallParticles;
        public GameObject landParticles;

        private void Awake()
        {
            VRTK_SDKManager.instance.AddBehaviourToToggleOnLoadedSetupChange(this);
        }

        private void OnEnable()
        {
            Transform playArea = VRTK_DeviceFinder.PlayAreaTransform();
            GameObject _fallParticles = Instantiate(fallParticles, playArea);
            _fallParticles.transform.rotation = Quaternion.LookRotation(playArea.position - endPosition);
            _fallParticles.transform.localPosition = playArea.transform.InverseTransformPoint((endPosition - playArea.position).normalized * 50f + playArea.position);
            StartCoroutine(MoveOverSeconds(playArea, endPosition, 5, () => {
                GameObject _landPartciles = Instantiate(landParticles, endPosition, Quaternion.identity);
                Destroy(_landPartciles, 2f);
                Destroy(_fallParticles);
                SpeechBubbleManager.instance.AddSpeechbubble(
                    friendlyWizardMouth,
                    "Well met, Wizard!\n" +
                    "I'm glad you made it in one piece.\n" +
                    "You should head inside and get some rest.",
                    SpeechBubbleManager.SpeechbubbleType.NORMAL,
                    9999f,
                    Color.white,
                    Vector3.zero
                    );
            }));
        }

        public IEnumerator MoveOverSeconds(Transform t, Vector3 end, float seconds, Action onEnd = null)
        {
            float elapsedTime = 0;
            Vector3 startingPos = t.position;
            while (elapsedTime < seconds)
            {
                t.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            t.position = end;

            if(onEnd != null)
            {
                onEnd.Invoke();
            }
        }

        private void OnDestroy()
        {
            VRTK_SDKManager.instance.RemoveBehaviourToToggleOnLoadedSetupChange(this);
        }
    }
}