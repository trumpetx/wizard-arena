using UnityEngine;

namespace WizardArena
{
    public class FloatingCombatText : MonoBehaviour
    {
        public readonly Color damageColor = Color.red;
        public readonly Color healColor = Color.green;
        public readonly Color moneyColor = Util.ColorFromHex("eead0e");
        public float critScale = 2.0f;
        public int fontSize = 90;
        public float fontScale = 0.05f;

        private TextMesh textMesh;
        private MeshRenderer meshRenderer;
        private Animator animator;

        private void Awake()
        {
            textMesh = GetComponent<TextMesh>();
            meshRenderer = GetComponent<MeshRenderer>();
            animator = GetComponent<Animator>();
        }

        public void FloatingText(Transform place, string msg, bool bold, Color color)
        {
            var go = new GameObject("Floating Combat Text Holder");
            // flip our text around ("backwards", as to be legible looking "AT" place, not "FROM")
            Quaternion flip = new Quaternion(0, 180f, 0, 0);
            go.transform.SetPositionAndRotation(place.transform.position + (place.up * 2.1f), place.rotation * flip);
            
            GameObject o = Instantiate(gameObject, go.transform);

            FloatingCombatText c = o.GetComponent<FloatingCombatText>();
            c.textMesh.color = color;
            c.textMesh.richText = true;
            c.textMesh.text = bold ? "<b>" + msg + "</b>" : msg;
            Destroy(o, 1f);
            Destroy(go, 1f); 
        }

        public void Damage(Transform parent, float amt, bool crit)
        {
            FloatingText(parent, "-" + (Mathf.Abs(amt)), crit, damageColor);
        }

        public void Heal(Transform parent, float amt, bool crit)
        {
            FloatingText(parent, "+" + (Mathf.Abs(amt)), crit, healColor);
        }

        public void Money(Transform parent, int amt)
        {
            FloatingText(parent, "+" + (Mathf.Abs(amt)), false, moneyColor);
        }

        public void HealthChange(Transform parent, float healthChange, bool crit)
        {
            if (healthChange < 0)
            {
                Damage(parent, healthChange, crit);
            }
            else
            {
                Heal(parent, healthChange, crit);
            }
        }
    }
}