using PowerUI;
using System.Collections;
using UnityEngine;
using Zenject;

namespace WizardArena
{
    public class CountdownTimer
    {
        private VRTK.VRTK_SDKManager vrtkManager;
        private AsyncProcessor asyncProcessor;
        private WorldUI countdownUI = null;
        private int count;

        public delegate void EndCountdownHandler();
        public event EndCountdownHandler OnEndCountdown;
        
        public static CountdownTimer instance { get; private set; }

        [Inject]
        public CountdownTimer(AsyncProcessor asyncProcessor)
        {
            this.asyncProcessor = asyncProcessor;
            if (instance == null)
            {
                instance = this;
            }
        }
        
        public void StartCountdown(int seconds, Color color, EndCountdownHandler OnEndCountdown)
        {
            StartCountdown(null, seconds, color, OnEndCountdown);
        }

        public void StartCountdown(string title, int seconds, Color color, EndCountdownHandler OnEndCountdown)
        {
            if (null != countdownUI)
            {
                countdownUI.Destroy();
            }
            asyncProcessor.StartCoroutine(PrepCountdown(title, seconds, color));
            this.OnEndCountdown = OnEndCountdown;
        }

        public void StartCountdown(int seconds, Color color)
        {
            StartCountdown(seconds, color, null);
        }

        public void StartCountdown(int seconds)
        {
            StartCountdown(seconds, Color.black, null);
        }

        private IEnumerator PrepCountdown(string title, int seconds, Color color)
        {
            while (VRTK.VRTK_DeviceFinder.HeadsetTransform() == null)
            {
                yield return new WaitForSeconds(.1f);
            }
            if(countdownUI != null)
            {
                asyncProcessor.StopCoroutine("Countdown");
                countdownUI.Destroy();
            }
            countdownUI = new FlatWorldUI("countdown", 1028, 800);
            countdownUI.transform.localScale = new Vector3(countdownUI.transform.localScale.x / 2, countdownUI.transform.localScale.y / 2, countdownUI.transform.localScale.z / 2);
            // Load from template?
            countdownUI.document.LoadHtml("<html><head><style type='text/css'>#title { font-size: 75pt; } body { color: black; font-family: 'Milonga', cursive; font-size: 370pt; } #background-box { background: url('parchment.png') no-repeat; width: 1028px; height: 768px; } #content-box{ width:750px; height: 500px; margin-left: auto; margin-right: auto; margin-top: 100px; text-align: center; } </style></head><body><div id='background-box'><div id='content-box'>" +
                (title == null ? "" : "<span id='title'>" + title + "</span><br/>") +
                "<span id='countdown' style='color: " + color.ToCss() + ";'></span></div></div></body></html>");
            Vector3 playerPosition = VRTK.VRTK_DeviceFinder.HeadsetTransform().localPosition;
            countdownUI.transform.localPosition = playerPosition + Vector3.forward * 7 + Vector3.up * 2;
            countdownUI.transform.rotation = Quaternion.LookRotation(countdownUI.transform.position - playerPosition);

            count = seconds;
            asyncProcessor.StartCoroutine(Countdown(countdownUI.document.getElementById("countdown")));
        }

        private IEnumerator Countdown(Dom.Element div)
        {
            while (count > 0)
            {
                div.innerHTML = "" + (--count);
                yield return new WaitForSeconds(1f);
            }
            countdownUI.Destroy();
            countdownUI = null;
            if (OnEndCountdown != null)
            {
                OnEndCountdown();
                OnEndCountdown = null;
            }
        }
    }
}