
using System;

namespace WizardArena
{
    public enum Gesture { SHOOT, BEAM, SHIELD, PUSH, WALL, CLEAVE, TELEPORT, SUMMON }

    public interface IGestureRecognizer
    {
        void Disable();
    }
}
