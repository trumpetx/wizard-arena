using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    [RequireComponent(typeof(NetworkHitWithProjectile), typeof(Animator), typeof(NpcCharacter))]
    public class SurvivalModeEnemyMovement : NetworkBehaviour
    {
        const string WALKING = "Walking";
        const string ATTACKING = "Attacking";
        const string DEAD = "Dead";

        public float dps = 10f;
        private NpcCharacter npc;
        private Animator animator;
        private UnityEngine.AI.NavMeshAgent nav;
        private NetworkHitWithProjectile hitWithProjectile;
        [HideInInspector] [SyncVar] public bool isColliding;

        private void OnEnable()
        {
            nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
            animator = GetComponent<Animator>();
            npc = GetComponent<NpcCharacter>();
            npc.character.OnDeath += OnMyDeath;

            if (isServer)
            {
                hitWithProjectile = GetComponent<NetworkHitWithProjectile>();
                hitWithProjectile.OnHit += OnHit;
                foreach (Attack0Behavior b in animator.GetBehaviours<Attack0Behavior>())
                {
                    b.OnAttack += DoHit;
                }
            }
        }

        private void DoHit()
        {
            npc.targetWizard?.networkHitWithProjectile.DoHealthChange(-1 * dps, gameObject.name.Contains("Archer") ? WizardConstants.RANGED : WizardConstants.MELEE);
        }

        private void OnDisable()
        {
            if (hitWithProjectile != null)
            {
                hitWithProjectile.OnHit -= OnHit;
            }
            if (npc?.character != null)
            {
                npc.character.OnDeath -= OnMyDeath;
            }
        }

        [Server]
        void OnTriggerStay(Collider collider)
        {
            // point forward vector at player for raycast
            transform.LookAt(npc.target.transform, transform.up);
            RaycastHit hit;
            if (null != collider.gameObject.GetComponentInParent<NetworkWizardController>())
            {
                isColliding = Physics.Raycast(transform.position, transform.forward, out hit);
            }
        }

        [Server]
        private void OnTriggerExit(Collider collider)
        {
            NetworkWizardController wizard = collider.gameObject.GetComponentInParent<NetworkWizardController>();
            if (wizard != null && wizard == npc.target)
            {
                isColliding = false;
            }
        }

        private void OnHit(float dmg, int element)
        {
            // TODO: make an enemy "type" instead of hard coding this
            if (element == WizardConstants.FIRE)
            {
                dmg *= 1.5f;
            }
            npc.character.ModifyHealth(-1 * dmg);
        }

        private void OnMyDeath()
        {
            if (nav != null)
            {
                nav.enabled = false;
            }
            animator.SetTrigger(DEAD);
            Destroy(gameObject, 5f);
        }

        private void Update()
        {
            if(npc.character == null || npc.target == null)
            {
                return;
            }
            if (npc.character.GetCurrentHealth() > 0 && isColliding)
            {
                if (nav != null)
                    nav.enabled = false;
                animator.SetBool(ATTACKING, true);
                // HACK - trying to make the animation stand still better
            }
            else if (npc.character.GetCurrentHealth() > 0 && transform.position != npc.target.transform.position)
            {
                animator.SetBool(WALKING, true);
                if (nav != null)
                {
                    nav.enabled = true;
                    if (npc.targetWizard != null && !nav.SetDestination(npc.target.transform.position))
                    {
                        Debug.Log("Skelly got stuck @ " + transform.position + ", destroying...");
                        npc.character.Kill();
                    }
                }
            }
            else if (npc.character.GetCurrentHealth() > 0 && npc.targetWizard.player.GetCurrentHealth() < 0)
            {
                if (nav != null)
                    nav.enabled = false;
                animator.SetBool(WALKING, false);
                animator.SetBool(ATTACKING, false);
            }
        }
    }
}
