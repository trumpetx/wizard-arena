using CurvedUI;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using VRTK;

namespace WizardArena
{
    public class OptionsMenuManager : VRTKMonoBehavior
    {
        public static OptionsMenuManager instance { get; set; }
        private GameLifecycle gameLifecycle;
        public Canvas canvas;
        private CurvedUISettings settings;
        public Button settingsButton;
        public Button characterButton;
        public Button talentsButton;
        public Button multiplayerButton;
        public Button quitButton;
        public Button exitButton;
        private Button[] buttons;

        public SettingsPage settingsPage;
        public CharacterPage characterPage;
        public TalentsPage talentsPage;
        public MultiplayerHostPage multiplayerHostPage;
        public MultiplayerClientPage multiplayerClientPage;

        public GameObject activePage; // Set to BlankPage in Editor
        private GameObject hand;
        private VRTK_ControllerEvents triggerDetector;

        public delegate void OnHandednessChangeHandler(bool leftHanded);
        public event OnHandednessChangeHandler OnHandednessChange;
        private Coroutine setupCoroutine;

        internal override void Awake()
        {
            base.Awake();
            canvas.gameObject.SetActive(false);
            instance = this;
        }

        internal override void OnDestroy()
        {
            instance = null;
            base.OnDestroy();
        }

        private void OnEnable()
        {
            gameLifecycle = gameLifecycle ?? GameLifecycle.instance;
            sdkManager.scriptAliasLeftController.GetComponent<VRTK_ControllerEvents>().ButtonTwoPressed += ShowMenu;
            sdkManager.scriptAliasRightController.GetComponent<VRTK_ControllerEvents>().ButtonTwoPressed += ShowMenu;
            settings = settings ?? canvas.GetComponent<CurvedUISettings>();

            buttons = new Button[] { characterButton, settingsButton, talentsButton, multiplayerButton, quitButton, exitButton };
            characterButton.onClick.AddListener(() => SwitchPage(characterPage.gameObject));
            settingsButton.onClick.AddListener(() => SwitchPage(settingsPage.gameObject));
            talentsButton.onClick.AddListener(() => SwitchPage(talentsPage.gameObject));
            exitButton.onClick.AddListener(() =>
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            });

            settingsPage.menuDistance.value = Mathf.Max(playerSave.menuDistance, .25f);
            settingsPage.leftHanded.isOn = playerSave.leftHanded;
            settingsPage.volume.value = playerSave.volume;
            settingsPage.male.isOn = playerSave.gender == PlayerSave.GENDER_MALE;
            settingsPage.female.isOn = playerSave.gender == PlayerSave.GENDER_FEMALE;
            settingsPage.graphicsQuality.value = playerSave.graphicsLevel;
            settingsPage.skinTone.value = playerSave.skinTone;
            settingsPage.menuCurve.value = playerSave.menuCurve;
            OnEquip();
            setupCoroutine = StartCoroutine(SetupMultiplayerButtons());
        }

        private IEnumerator SetupMultiplayerButtons()
        {
            yield return new WaitUntil(() => LobbyWizard.localPlayer != null);
            multiplayerButton.onClick.AddListener(() => {
                if (LobbyWizard.localPlayer.isServer)
                {
                    SwitchPage(multiplayerHostPage.gameObject);
                    // Add Host Migrate Button
                }
                else
                {
                    SwitchPage(multiplayerClientPage.gameObject);
                    quitButton.gameObject.SetActive(true);
                }
            });
            if (!LobbyWizard.localPlayer.isServer)
            {
                quitButton.onClick.AddListener(() => LobbyWizard.localPlayer.GetComponent<NetworkIdentity>().connectionToServer.Disconnect());
            }
            quitButton.gameObject.SetActive(false);
            setupCoroutine = null;
        }

        public void ServerOnPlayerJoinOrLeave()
        {
            if (LobbyWizard.localPlayer.isServer)
            {
                multiplayerHostPage.RemovePlayersFromList();
                multiplayerHostPage.AddHostToList();
                foreach (string playerName in LobbyWizard.otherPlayers.Keys)
                {
                    multiplayerHostPage.AddPlayerToList(playerName, () => LobbyWizard.otherPlayers[playerName].connectionToClient.Disconnect());
                }
            }
        }

        private void SwitchPage(GameObject go)
        {
            quitButton.gameObject.SetActive(false);
            activePage.SetActive(false);
            go.SetActive(true);
            settings.SetAllChildrenDirty();
            activePage = go;
        }

        private void OnEquip()
        {
            // calculate the new values for stats from inventory
            characterPage.SetIntellegence(.25f);
            characterPage.SetWisdom(.25f);
            characterPage.SetStamina(.25f);
            characterPage.SetLuck(.25f);
        }

        private void Update()
        {
            if (hand != null)
            {
                CurvedUIInputModule.CustomControllerRay = new Ray(hand.transform.position, hand.transform.forward);
                CurvedUIInputModule.CustromControllerButtonDown = triggerDetector.IsButtonPressed(VRTK_ControllerEvents.ButtonAlias.TriggerPress);

                characterPage.SetLevel(playerSave.level);
                characterPage.SetCurrency(playerSave.currency);
                characterPage.SetExperience(playerSave.experiencePctEarned);
            }
        }

        private void OnDisable()
        {
            sdkManager.scriptAliasLeftController.GetComponent<VRTK_ControllerEvents>().ButtonTwoPressed -= ShowMenu;
            sdkManager.scriptAliasRightController.GetComponent<VRTK_ControllerEvents>().ButtonTwoPressed -= ShowMenu;
            if (buttons != null)
            {
                foreach (Button b in buttons)
                {
                    b.onClick.RemoveAllListeners();
                }
            }
            if(setupCoroutine != null)
            {
                StopCoroutine(setupCoroutine);
            }
        }

        public void OnMenuDistanceChange(float value)
        {
            if (canvas.isActiveAndEnabled)
            {
                playerSave.menuDistance = Mathf.Min(Mathf.Max(value, .5f), 2);
                canvas.transform.position = (canvas.transform.position - VRTK_DeviceFinder.HeadsetTransform().position).normalized * playerSave.menuDistance + VRTK_DeviceFinder.HeadsetTransform().position;
            }
        }

        public void OnMenuCurveChange(float value)
        {
            if (canvas.isActiveAndEnabled)
            {
                playerSave.menuCurve = (int)Mathf.Min(Mathf.Max(value, 0), 180);
                settings.Angle = playerSave.menuCurve;
            }
        }

        public void OnSkinToneChange(float value)
        {
            if (canvas.isActiveAndEnabled)
            {
                playerSave.skinTone = (int)Mathf.Min(Mathf.Max(value, 1), 3);
                foreach (HandManager m in HandManager.instances)
                {
                    m.SetSkinTone(playerSave.skinTone);
                }
            }
        }

        public void OnVolumeChange(float value)
        {
            if (canvas.isActiveAndEnabled)
            {
                playerSave.volume = Mathf.Min(Mathf.Max(value, 0), 1);
                AudioListener.volume = playerSave.volume;
            }
        }

        public void OnGraphicsQualityChange(float value)
        {
            if (canvas.isActiveAndEnabled)
            {
                playerSave.graphicsLevel = (int)Mathf.Min(Mathf.Max(value, 1), 3);
                gameLifecycle.SetGraphicsLevel(playerSave.graphicsLevel);
            }
        }

        private void SetGenderPref(int gender)
        {
            playerSave.gender = gender;
            foreach (HandManager m in HandManager.instances)
            {
                m.SetGender(playerSave.gender);
            }
        }

        public void SetFemale(bool value)
        {
            if (canvas.isActiveAndEnabled && value)
            {
                SetGenderPref(PlayerSave.GENDER_FEMALE);
            }
        }

        public void SetMale(bool value)
        {
            if (canvas.isActiveAndEnabled && value)
            {
                SetGenderPref(PlayerSave.GENDER_MALE);
            }
        }

        public void OnLefthandednessChange(bool value)
        {
            if (canvas.isActiveAndEnabled)
            {
                playerSave.leftHanded = value;
                ResetHandedness();
                if(OnHandednessChange != null)
                {
                    OnHandednessChange(value);
                }
            }
        }

        private void ResetHandedness()
        {
            hand = playerSave.leftHanded ? VRTK_DeviceFinder.GetControllerLeftHand(true) : VRTK_DeviceFinder.GetControllerRightHand(true);
            GameObject scriptAlias = VRTK_DeviceFinder.GetScriptAliasController(hand);
            triggerDetector = scriptAlias.GetComponent<VRTK_ControllerEvents>();
        }

        public void ShowMenu(bool show, GameObject page)
        {
            canvas.gameObject.SetActive(show);

            if (show)
            {
                if (activePage != page)
                {
                    SwitchPage(page);
                }
                characterPage.SetName(gameLifecycle.username);
                float distance = Mathf.Max(playerSave.menuDistance, .5f);
                ResetHandedness();
                Vector3 h = VRTK_DeviceFinder.HeadsetTransform().position;
                Vector3 pos = h + VRTK_DeviceFinder.HeadsetTransform().forward * distance;
                canvas.transform.position = (new Vector3(pos.x, h.y, pos.z) - h).normalized * distance + h;
                canvas.transform.rotation = Quaternion.LookRotation(canvas.transform.position - h);
                OnMenuCurveChange(playerSave.menuCurve);
            }
            else
            {
                hand = null;
                PlayerSaveManager.instance.Save();
            }
        }
        
        private void ShowMenu(object sender, ControllerInteractionEventArgs e)
        {
            ShowMenu(!canvas.isActiveAndEnabled, activePage);
        }
    }
}