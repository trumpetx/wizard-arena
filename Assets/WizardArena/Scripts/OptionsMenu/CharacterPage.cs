using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WizardArena
{
    public class CharacterPage : MonoBehaviour
    {
        public Text playerName;
        public Text playerLevel;
        public Text currency;
        public RectTransform intelligence;
        public RectTransform wisdom;
        public RectTransform luck;
        public RectTransform stamina;
        public RectTransform experience;

        public void SetCurrency(int c)
        {
            c = Mathf.Max(c, 0);
            currency.text = "" + c;
        }

        public void SetName(string n)
        {
            if(n == null || n.Trim() == "")
            {
                n = "Wizard";
            }
            playerName.text = "" + n;
        }

        public void SetLevel(int c)
        {
            c = Mathf.Max(c, 1);
            playerLevel.text = "Level " + c;
        }

        public void SetIntellegence(float pct)
        {
            SetPct(intelligence, pct);
        }

        public void SetWisdom(float pct)
        {
            SetPct(wisdom, pct);
        }

        public void SetLuck(float pct)
        {
            SetPct(luck, pct);
        }

        public void SetStamina(float pct)
        {
            SetPct(stamina, pct);
        }

        public void SetExperience(float pct)
        {
            SetPct(experience, pct);
        }

        private void SetPct(RectTransform bar, float pct)
        {
            pct = Mathf.Max(0, Mathf.Min(1f, pct));
            RectTransform parent = bar.parent.GetComponent<RectTransform>();
            bar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, parent.rect.width * pct);
        }
    }
}