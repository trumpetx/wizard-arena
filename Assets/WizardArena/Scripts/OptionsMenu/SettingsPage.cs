using UnityEngine;
using UnityEngine.UI;

namespace WizardArena
{
    public class SettingsPage : MonoBehaviour
    {
        public Slider graphicsQuality;
        public Slider menuCurve;
        public Slider menuDistance;
        public Slider volume;
        public Slider skinTone;
        public Toggle leftHanded;
        public Toggle male;
        public Toggle female;
    }
}