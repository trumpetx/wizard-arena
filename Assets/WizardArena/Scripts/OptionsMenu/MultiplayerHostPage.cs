﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace WizardArena
{
    public class MultiplayerHostPage : MonoBehaviour
    {
        public GameObject menuMultiplayerRowPrefab;
        public VerticalLayoutGroup multiplayerRows;

        public void AddPlayerToList(string playerName, Action onKickCallback)
        {
            GameObject row = Instantiate(menuMultiplayerRowPrefab, multiplayerRows.gameObject.transform);
            row.GetComponentInChildren<Button>().onClick.AddListener(() => onKickCallback.Invoke());
            row.GetComponentInChildren<Text>().text = playerName;
            row.transform.parent = multiplayerRows.transform;
        }

        public void AddHostToList()
        {
            GameObject row = Instantiate(menuMultiplayerRowPrefab, multiplayerRows.gameObject.transform);
            row.GetComponentInChildren<Button>().gameObject.SetActive(false);
            row.GetComponentInChildren<Text>().text = GameLifecycle.instance.username;
            row.transform.parent = multiplayerRows.transform;
        }

        public void RemovePlayersFromList()
        {
            foreach(Text t in multiplayerRows.GetComponentsInChildren<Text>(true))
            {
                Destroy(t.transform.parent.gameObject);
            }
        }

        // Lobby View
        // Quit

    }
}