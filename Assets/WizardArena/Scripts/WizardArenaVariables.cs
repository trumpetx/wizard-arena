using UnityEngine;

namespace WizardArena
{
    public class WizardArenaVariables
    {
        public const float MANA_REGEN = 4f;
        public const float HEALTH_REGEN = .5f;
        public const float SHIELD_SECONDS = 30f;
        public const float BEAM_SECONDS = 5f;
        public const float SUMMON_SECONDS = 15f;
        public const float WALL_SECONDS = 10f;
        public const float CLEAVE_DMG = 10f;
        public const float SHOOT_DMG = 20f;
        public const float MEGA_SHOOT_DMG = 30f;
        public const float BEAM_DMG = 2f;
        public const float SUMMON_DMG = 2f;
        public const float WALL_DMG = 5f;

        public const float defaultChanceOfSpawnPerSecondMuliplier = 1.1f;
        public const float defaultChanceOfDoubleSpawnMuliplier = 1.1f;
        public const int defaultSpawnCountMultiplier = 2;
        public const int defaultBaseSpawnCount = 3;
        public const float defaultChanceOfSpawnPerSecond = .08f;
        public const float defaultChanceOfDoubleSpawn = .2f;

        public float manaRegen  { get; private set; }
        public float healthRegen  { get; private set; }
        public float shieldSeconds { get; private set; }
        public float beamSeconds { get; private set; }
        public float summonSeconds { get; private set; }
        public float wallSeconds  { get; private set; }
        public float cleaveDmg  { get; private set; }
        public float shootDmg { get; private set; }
        public float megaShootDmg  { get; private set; }
        public float beamDmg { get; private set; }
        public float summonDmg { get; private set; }
        public float wallDmg { get; private set; }

        public float chanceOfSpawnPerSecondMuliplier { get; private set; }
        public float chanceOfDoubleSpawnMuliplier  { get; private set; }
        public int spawnCountMultiplier  { get; private set; }
        public int baseSpawnCount  { get; private set; }
        public float chanceOfSpawnPerSecond  { get; private set; }
        public float chanceOfDoubleSpawn  { get; private set; }

        public static WizardArenaVariables instance { get; private set; }

        public WizardArenaVariables()
        {
            // Multiple instantiations of this class will not overwrite the original static 'player' instance (set in GlobalContext)
            if (instance == null)
            {
                instance = this;
            }
            RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler(OnRemoteUpdate);
            chanceOfSpawnPerSecondMuliplier = defaultChanceOfSpawnPerSecondMuliplier;
            chanceOfDoubleSpawnMuliplier = defaultChanceOfDoubleSpawnMuliplier;
            spawnCountMultiplier = defaultSpawnCountMultiplier;
            baseSpawnCount  = defaultBaseSpawnCount;
            chanceOfSpawnPerSecond = defaultChanceOfSpawnPerSecond;
            chanceOfDoubleSpawn = defaultChanceOfDoubleSpawn;
            manaRegen = MANA_REGEN;
            healthRegen = HEALTH_REGEN;
            shieldSeconds = SHIELD_SECONDS;
            beamSeconds = BEAM_SECONDS;
            summonSeconds = SUMMON_SECONDS;
            wallSeconds = WALL_SECONDS;
            cleaveDmg = CLEAVE_DMG;
            shootDmg = SHOOT_DMG;
            megaShootDmg = MEGA_SHOOT_DMG;
            beamDmg = BEAM_DMG;
            summonDmg = SUMMON_DMG;
            wallDmg = WALL_DMG;
        }

        public void OnRemoteUpdate()
        {
            chanceOfSpawnPerSecondMuliplier = RemoteSettings.GetFloat("chanceOfSpawnPerSecondMuliplier", defaultChanceOfSpawnPerSecondMuliplier);
            chanceOfDoubleSpawnMuliplier = RemoteSettings.GetFloat("chanceOfDoubleSpawnMuliplier", defaultChanceOfDoubleSpawnMuliplier);
            spawnCountMultiplier = RemoteSettings.GetInt("spawnCountMultiplier", defaultSpawnCountMultiplier);
            baseSpawnCount = RemoteSettings.GetInt("baseSpawnCount", defaultBaseSpawnCount);
            chanceOfSpawnPerSecond = RemoteSettings.GetFloat("chanceOfSpawnPerSecond", defaultChanceOfSpawnPerSecond);
            chanceOfDoubleSpawn = RemoteSettings.GetFloat("chanceOfDoubleSpawn", defaultChanceOfDoubleSpawn);

            manaRegen = RemoteSettings.GetFloat("manaRegen", MANA_REGEN);
            healthRegen = RemoteSettings.GetFloat("healthRegen", HEALTH_REGEN);
            shieldSeconds = RemoteSettings.GetFloat("shieldSeconds", SHIELD_SECONDS);
            beamSeconds = RemoteSettings.GetFloat("shieldSeconds", BEAM_SECONDS);
            beamSeconds = RemoteSettings.GetFloat("shieldSeconds", BEAM_SECONDS);
            wallSeconds = RemoteSettings.GetFloat("wallSeconds", WALL_SECONDS);
            cleaveDmg = RemoteSettings.GetFloat("cleaveDmg", CLEAVE_DMG);
            shootDmg = RemoteSettings.GetFloat("shootDmg", SHOOT_DMG);
            megaShootDmg = RemoteSettings.GetFloat("megaShootDmg", MEGA_SHOOT_DMG);
            beamDmg = RemoteSettings.GetFloat("beamDmg", BEAM_DMG);
            summonDmg = RemoteSettings.GetFloat("summonDmg", SUMMON_DMG);
            wallDmg = RemoteSettings.GetFloat("wallDmg", WALL_DMG);

            Debug.Log("Updated Remote Varaibles");
        }
    }
}