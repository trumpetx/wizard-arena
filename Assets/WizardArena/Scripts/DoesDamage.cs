using UnityEngine;

namespace WizardArena
{
    public class ChangesHealth : MonoBehaviour
    {
        public bool healthChangeApplied { get; set; }
        public int element { get; set; }
        public float healthChange { get; set; }

        private void OnEnable()
        {
            element = -1;
            healthChange = 0;
            healthChangeApplied = false;
        }
    }
}