using UnityEngine;
using System;
using System.Text;

namespace WizardArena
{
    public class Util
    {
        public static void AssignLayer(Transform root, int layer)
        {
            root.gameObject.layer = layer;
            foreach (Transform child in root)
                AssignLayer(child, layer);
        }

        public static Color ColorFromHex(string hex)
        {
            if (hex.StartsWith("#"))
            {
                hex = hex.Substring(1);
            }

            if (hex.Length == 6)
            {
                return new Color(Convert.ToInt32(hex.Substring(0, 2), 16), Convert.ToInt32(hex.Substring(2, 2), 16), Convert.ToInt32(hex.Substring(4, 2), 16));
            }
            else if(hex.Length == 3)
            {
                return new Color(Convert.ToInt32(hex.Substring(0, 1), 16), Convert.ToInt32(hex.Substring(1, 1), 16), Convert.ToInt32(hex.Substring(2, 1), 16));
            }

            throw new Exception("Invalid HEX color: " + hex);
        }

        public static int ParsePewObject(string objectName, out string descriptor)
        {
            StringBuilder sb = new StringBuilder();
            string element = null;
            for (int i = 0; i < objectName.Length; i++)
            {
                char c = objectName[i];
                if (!char.IsLetter(c))
                {
                    break;
                }
                if (char.IsUpper(c) && i != 0 && element == null)
                {
                    element = sb.ToString();
                    sb = new StringBuilder();
                }
                sb.Append(c);
            }
            descriptor = sb.ToString();
            return WizardConstants.NAMES_ELEMENT[element.ToLower()];
        }
    }
}
