using System;
using UnityEngine;


/// <summary>A laser pointer which fires an input ray from e.g. a 3D hand.</summary>
public class LaserPointer : PowerUI.InputPointer
{

    public Transform Transform;
    /// <summary>True if UI's move around.</summary>
    public bool MovingUIs;


    /// <summary>Setup a laser pointer from the given transform (e.g. a hand object).</summary>
    public LaserPointer(Transform transform) : this(transform, true) { }

    /// <summary>Setup a laser pointer from the given transform (e.g. a hand object).</summary>
    /// <param name='movingUIs'>Set this to true if your WorldUI's move around.
    /// Setting it to false is a small performance saving. If you're not sure, just use true.</param>
    public LaserPointer(Transform transform, bool movingUIs)
    {
        Transform = transform;
        MovingUIs = movingUIs;

        // It won't interact with the main UI so just put it offscreen to skip checking entirely:
        ScreenX = ScreenY = -1000f;

    }

    public override bool Raycast(out RaycastHit hit, Camera cam, Vector2 screenPoint)
    {

        // Fire off your ray in whatever your pointers direction is; we'll use transform.forward here.
        // ('forward' is +ve z):
        Ray ray = new Ray(Transform.position, Transform.forward);

        return Physics.Raycast(ray, out hit);
    }

    public override bool Relocate(out Vector2 delta)
    {

        // We can just ignore delta (it's for the main UI):
        delta = Vector2.zero;

        if (MovingUIs)
        {
            // Always recompute.
            return true;
        }

        // Transform moved?
        if (Transform != null && Transform.hasChanged)
        {
            Transform.hasChanged = false;
            return true;
        }

        // Don't bother recalculating - it's not moved.
        // (Always return true if your UI's are moving instead).
        return false;
    }

}