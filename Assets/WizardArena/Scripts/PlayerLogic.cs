
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WizardArena
{
    public class PlayerLogic
    {
        private const float BASE_STAT = 100;
        private readonly Dictionary<int, DateTime> shieldedBy = new Dictionary<int, DateTime>();
        private float currentHealth = BASE_STAT;
        private float startingHealth = BASE_STAT;
        private float currentMana;
        private float startingMana;
        private bool hasDied;
        public delegate void DeathEventHandler();
        public event DeathEventHandler OnDeath;
        private WizardArenaVariables wizardArenaVariables;
        private PlayerTalents playerTalents;
        private int level;
        private float stamina;
        private float wisdom;
        private float intelligence;
        private float luck;

        public PlayerLogic(WizardArenaVariables vars)
        {
            wizardArenaVariables = vars;
            SetPlayerVars(new short[] { }, 0, .25f, .25f, .25f, .25f);
        }

        public PlayerLogic(float health, float mana)
        {
            startingHealth = currentHealth = health;
            startingMana = currentMana = mana;
        }

        public void SetPlayerVars(short[] talentChoices, int level, float intelligenceMod, float wisdomMod, float luckMod, float staminaMod)
        {
            this.level = level;
            // Players have a 0 sum equipment allotment, if clients cheat - send them back to "base" levels
            if (!ValidateModifiers(intelligenceMod, wisdomMod, luckMod, staminaMod))
            {
                staminaMod = .25f;
                luckMod = .25f;
                wisdomMod = .25f;
                intelligenceMod = .25f;
            }
            playerTalents = new PlayerTalents(talentChoices, level);
            stamina = BASE_STAT * staminaMod * playerTalents.staminaModifier;
            luck = BASE_STAT * luckMod * playerTalents.luckModifier;
            wisdom = BASE_STAT * wisdomMod * playerTalents.wisdomModifier;
            intelligence = BASE_STAT * intelligenceMod * playerTalents.intelligenceModifier;

            currentHealth = startingHealth = BASE_STAT + (int) stamina;
            currentMana = startingMana = BASE_STAT + (int) wisdom;
        }

        private bool ValidateModifiers(float intelligenceMod, float wisdomMod, float luckMod, float staminaMod)
        {
            return intelligenceMod + wisdomMod + luckMod + staminaMod < 1.01f;
        }

        public List<int> ShieldedBy()
        {
            return shieldedBy.Where(kvp => kvp.Value > DateTime.Now).Select(kvp2 => kvp2.Key).ToList();
        }

        public void SetShielded(DateTime until, int byElement)
        {
            if (!shieldedBy.ContainsKey(byElement))
            {
                shieldedBy.Add(byElement, until);
            }
            shieldedBy[byElement] = until;
        }

        public bool ModifyHealth(float amt)
        {
            if (currentHealth >= startingHealth && amt > 0)
            {
                return false;
            }
            SetHealthAndMana(currentHealth + amt, currentMana);
            if(amt < 0) Debug.Log("Health Loss: " + amt);
            return true;
        }

        public bool ModifyMana(float amt)
        {
            if (currentMana >= startingMana && amt > 0)
            {
                return false;
            }
            float newAmt = currentMana + amt;
            if (0 > newAmt)
            {
                return false;
            }
            SetHealthAndMana(currentHealth, newAmt);
            return true;
        }

        private void CheckDeath()
        {
            if (currentHealth <= 0f)
            {
                if (OnDeath != null && !hasDied)
                {
                    OnDeath();
                }
                hasDied = true;
            }
        }

        public void Kill()
        {
            SetHealthAndMana(-1f, currentMana);
        }

        public void SetHealthAndMana(float health, float mana)
        {
            hasDied &= health < 0;
            currentHealth = health;
            if (currentHealth > startingHealth)
            {
                currentHealth = startingHealth;
            }

            currentMana = mana;
            if (currentMana > startingMana)
            {
                currentMana = startingMana;
            }
            CheckDeath();
        }

        public void ResetHealthAndMana()
        {
            SetHealthAndMana(startingHealth, startingMana);
        }

        public float GetCurrentMana()
        {
            return currentMana;
        }

        public float GetCurrentHealth()
        {
            return currentHealth;
        }

        public float GetStartingMana()
        {
            return startingMana;
        }

        public float GetStartingHealth()
        {
            return startingHealth;
        }

        public void RegenHealthAndMana(float atRate, int e)
        {
            // Don't regen when dead
            if (currentHealth > 0)
            {
                IElement element = WizardConstants.ELEMENTS[e];
                float healthModifier = element.ModifyHealthRegen(playerTalents.healthRegenModifier);
                float manaModifier = element.ModifyManaRegen(playerTalents.manaRegenModifier) * wisdom / 100f;

                float health = wizardArenaVariables.healthRegen * healthModifier * atRate;
                float mana = wizardArenaVariables.manaRegen * manaModifier * atRate;
                ModifyHealth(health);
                ModifyMana(mana);
            }
        }

        public void ConsumePotion(DrinkablePotion potion)
        {
            ModifyHealth(potion.healthIncrease * playerTalents.potionModifier);
            ModifyMana(potion.manaIncrease * playerTalents.potionModifier);
        }

        public float CalcShootDmg(int e)
        {
            float shootModifier = WizardConstants.ELEMENTS[e].ModifyShoot(baseElementalModifier(e, playerTalents.shootModifier));
            return wizardArenaVariables.shootDmg * shootModifier;
        }

        public float CalcPushDmg(int e)
        {
            float shootModifier = WizardConstants.ELEMENTS[e].ModifyShoot(baseElementalModifier(e, playerTalents.shootModifier));
            return wizardArenaVariables.megaShootDmg * shootModifier;
        }

        public float CalcCleaveDmg(int e)
        {
            float cleaveModifier = WizardConstants.ELEMENTS[e].ModifyCleave(baseElementalModifier(e, playerTalents.cleaveModifier));
            return wizardArenaVariables.cleaveDmg * cleaveModifier;
        }

        public float CalcWallSeconds(int e)
        {
            float wallModifier = WizardConstants.ELEMENTS[e].ModifyWall(baseElementalModifier(e, playerTalents.wallModifier));
            return wizardArenaVariables.wallSeconds * wallModifier;
        }

        public float CalcShieldSeconds(int e)
        {
            float shieldModifier = WizardConstants.ELEMENTS[e].ModifyShield(baseElementalModifier(e, playerTalents.shieldTimeModififer));
            return wizardArenaVariables.shieldSeconds * shieldModifier;
        }

        public float CalcShieldDmgReductionPct(int el, int incomingElement)
        {
            float shieldModifier = playerTalents.shieldModifier;
            ShieldedBy().ForEach(e => {
                shieldModifier = WizardConstants.ELEMENTS[e].ModifyIncomingShieldDamage(shieldModifier, el);
            });
            return shieldModifier;
        }

        public float CalcBeamSeconds(int e)
        {
            float beamTimeModifier = WizardConstants.ELEMENTS[e].ModifyBeamSeconds(baseElementalModifier(e, playerTalents.beamTimeModifier));
            return wizardArenaVariables.beamSeconds * beamTimeModifier;
        }

        public float CalcBeamDmg(int e)
        {
            float beamModifier = WizardConstants.ELEMENTS[e].ModifyBeam(baseElementalModifier(e, playerTalents.beamModifier));
            return wizardArenaVariables.beamDmg * beamModifier;
        }

        public float CalcWallDamage(int e)
        {
            float wallDamageModifier = WizardConstants.ELEMENTS[e].ModifyWallDamage(baseElementalModifier(e, playerTalents.wallDamageModifier));
            return wizardArenaVariables.wallDmg * wallDamageModifier;
        }

        public float CalcSummonSeconds(int e)
        {
            float summonTimeModifier = WizardConstants.ELEMENTS[e].ModifySummonSeconds(baseElementalModifier(e, playerTalents.summonTimeModifier));
            return wizardArenaVariables.summonSeconds * summonTimeModifier;
        }

        public float CalcSummonDmg(int e)
        {
            float summonModifier = WizardConstants.ELEMENTS[e].ModifySummon(baseElementalModifier(e, playerTalents.summonModifier));
            return wizardArenaVariables.summonDmg * summonModifier;
        }

        public bool CalcAndApplyHealthChange(int e, float damage, int incomingElement)
        {
            float shieldModifier = CalcShieldDmgReductionPct(e, incomingElement);
            float finalDamage = damage - damage * shieldModifier;

            if (finalDamage >= 0)
            {
                finalDamage *= playerTalents.defenseModifier;
            }

            if (finalDamage > 0f)
            {
                return ModifyHealth(-1 * finalDamage);
            }
            return false;
        }

        private float baseElementalModifier(int e, float playerTalentsModifier)
        {
            return (WizardConstants.ELEMENTAL_ELEMENTS.Contains(e) && playerTalents.elementalist)
                ? playerTalentsModifier * 1.05f
                : playerTalentsModifier;
        }
    }
    
    public class PlayerTalents
    {
        private static readonly TalentTier[] talentTiers = new TalentTier[6];
        static PlayerTalents()
        {
            talentTiers[0] = new TalentTier(
                // Summoner - 15% longer summoning
                p => { p.shootModifier = p.summonTimeModifier * 1.15f; },
                // Wizard - 10% projectile increase
                p => { p.shootModifier = p.shootModifier * 1.1f; },
                // Cleric - 10% heal
                p => { p.healModifier = p.healModifier * 1.1f; });
            talentTiers[1] = new TalentTier(
                // Thick Skin - 10% lower damage
                p => { p.defenseModifier = p.defenseModifier * .9f; },
                // Fountain of Mana - 5% mana regen
                p => { p.manaRegenModifier = p.manaRegenModifier * 1.05f; },
                // Magic Hands
                p => { p.magicHands = true; });
            talentTiers[2] = new TalentTier(
                // Takes mana to gain mana
                p => { p.takesManaToGainMana = true; },
                // Rebirth
                p => { p.rebirth = true; },
                // One more for the road
                p => { p.oneMoreForTheRoad = true; });
            talentTiers[3] = new TalentTier(
                // Alchemist - 50% potions
                p => { p.potionModifier = p.potionModifier * 1.5f; },
                // Arcanist - 5% int
                p => { p.intelligenceModifier = p.intelligenceModifier * 1.05f; },
                // Elementalist - 5% stronger elemental spells
                p => { p.elementalist = true; });
            talentTiers[4] = new TalentTier(
                p => { },
                p => { },
                p => { });
            talentTiers[5] = new TalentTier(
                p => { },
                p => { },
                p => { });
        }

        public float shieldModifier = 0; 
        public float shieldTimeModififer = 1;
        public float wallModifier = 1;
        public float cleaveModifier = 1;
        public float shootModifier = 1;
        public float healthRegenModifier = 1;
        public float manaRegenModifier = 1;
        public float beamTimeModifier = 1;
        public float beamModifier = 1;
        public float summonTimeModifier = 1;
        public float summonModifier = 1;
        public float wallDamageModifier = 1;
        public float potionModifier = 1;

        // Stat modifiers
        public float intelligenceModifier = 1;
        public float wisdomModifier = 1;
        public float staminaModifier = 1;
        public float luckModifier = 1;

        // Meta-modifiers
        // incoming negative HP changes
        public float defenseModifier = 1;

        // incoming/external postive HP changes
        public float healModifier = 1; // TODO: apply

        // Flag talents "have it or don't"
        public bool magicHands; // TODO: apply
        public bool oneMoreForTheRoad; // TODO: apply
        public bool rebirth; // TODO: apply
        public bool takesManaToGainMana; // TODO: apply
        public bool elementalist;

        public PlayerTalents(short[] talents, int level)
        {
            // One talent choice every  5 levels
            for (int i = 0; i < level/5 && i < talents.Length; i++)
            {
                talentTiers[i].applyChoice(this, talents[i]);
            }
        }
    }

    public class TalentTier
    {
        private readonly Action<PlayerTalents> one;
        private readonly Action<PlayerTalents> two;
        private readonly Action<PlayerTalents> three;
        public TalentTier(Action<PlayerTalents> one, Action<PlayerTalents> two, Action<PlayerTalents> three)
        {
            this.one = one;
            this.two = two;
            this.three = three;
        }

        public void applyChoice(PlayerTalents t, short choice)
        {
            if(choice < 0)
            {
                one.Invoke(t);
            }
            else if(choice == 0)
            {
                two.Invoke(t);
            }
            else if (choice > 0)
            {
                three.Invoke(t);
            }
        }
    }
}
