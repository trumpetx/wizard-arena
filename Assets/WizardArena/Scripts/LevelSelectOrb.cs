using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using VRTK;

namespace WizardArena
{
    [RequireComponent(typeof(MeshRenderer))]
    public class LevelSelectOrb : NetworkBehaviour
    {
        private const float seconds = 1f;
        private VRTK_InteractableObject interactibleObject;
        private MeshRenderer meshRenderer;
        private Color color;
        private float alpha;
        private NetworkSceneSwitcher teleport;
        private Vector3 _endPosition;
        public Vector3 endPosition {
            get { return _endPosition; }
            set {
                _endPosition = value;
                distance = Vector3.Distance(endPosition, transform.position);
            }
        }
        public float distance;
        private Vector3 startingPosition { get; set; }
        private Quaternion startingRotation { get; set; }
        private string _levelName;
        public string levelName
        {
            get { return _levelName; }
            set
            {
                _levelName = value;
                meshRenderer.material = materials.Find(m => m.name == _levelName);
            }
        }
        private float elapsedTime = 0;
        public Action onUse { get; set; }
        private bool pullback = true;
        public Material material { get; set; }
        public List<Material> materials = new List<Material>();
        private Rigidbody rigidBody;
        private IEnumerator beginComeback;

        private void OnEnable()
        {
            rigidBody = GetComponent<Rigidbody>();
            meshRenderer = GetComponent<MeshRenderer>();
            interactibleObject = GetComponent<VRTK_InteractableObject>();
            color = meshRenderer.material.color;
            interactibleObject.InteractableObjectUsed += OnInteractableObjectUsed;
            interactibleObject.InteractableObjectGrabbed += OnInteractableObjectGrabbed;
            interactibleObject.InteractableObjectUngrabbed += OnInteractableObjectUngrabbed;
            startingRotation = transform.rotation;
            startingPosition = transform.position;
        }

        private void OnInteractableObjectGrabbed(object sender, InteractableObjectEventArgs args)
        {
            pullback = false;
        }

        private void OnInteractableObjectUngrabbed(object sender, InteractableObjectEventArgs args)
        {
            if(beginComeback != null)
            {
                StopCoroutine(beginComeback);
            }
            beginComeback = BeginComeback();
            StartCoroutine(beginComeback);
        }

        private IEnumerator BeginComeback()
        {
            yield return new WaitForSeconds(1.5f);
            if (!interactibleObject.IsGrabbed())
            {
                startingPosition = transform.position;
                startingRotation = transform.rotation;
                pullback = true;
            }
        }

        private void OnDisable()
        {
            interactibleObject.InteractableObjectUsed -= OnInteractableObjectUsed;
        }

        private void Update()
        {
            rigidBody.isKinematic = interactibleObject.IsGrabbed() || pullback;
            if (pullback && endPosition != null && transform.position != endPosition)
            {
                transform.rotation = Quaternion.Lerp(startingRotation, Quaternion.identity, (elapsedTime / seconds));
                transform.position = Vector3.Lerp(startingPosition, endPosition, (elapsedTime / seconds));
                elapsedTime += Time.deltaTime;
            }
            else if(pullback)
            {
                transform.Rotate(Vector3.up * -.1f);
                elapsedTime = 0;
            }
        }

        public void OnInteractableObjectUsed(object sender, InteractableObjectEventArgs args)
        {
            if(onUse != null)
            {
                onUse.Invoke();
            }
        }
    }
}