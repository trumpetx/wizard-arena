using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;

namespace WizardArena
{
    public class AudioPerScene : VRTKNetworkBehavior
    {
        public AudioClip darkAmulet;
        public AudioClip practiceRoom;
        public AudioClip wizardDuel;

        private void OnEnable()
        {
            AudioListener.volume = PlayerSaveManager.instance.playerSave.volume;
            AudioSource audioSource = VRTK_DeviceFinder.HeadsetCamera().GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = VRTK_DeviceFinder.HeadsetCamera().gameObject.AddComponent<AudioSource>();
            }
            AudioClip clip = null;
            float volume = .1f;

            switch (SceneManager.GetActiveScene().name)
            {
                case NetworkSceneSwitcher.LAVA_LANDSCAPE:
                    //v = new Vector3(-34.817f, 200.19f, 40.98f);
                    //r = Quaternion.Euler(0, 140f, 0);
                    clip = practiceRoom;
                    break;
                case NetworkSceneSwitcher.BANDIT_CAMP:
                    //v = new Vector3(374.36f, 18.10f, 248.59f);
                    //r = Quaternion.identity;
                    clip = darkAmulet;
                    break;
                case NetworkSceneSwitcher.MOUNTAIN_MAP:
                    clip = darkAmulet;
                    break;
                case NetworkSceneSwitcher.PRACTICE_ROOM:
                    //v = Vector3.zero;
                    //r = Quaternion.identity;
                    clip = practiceRoom;
                    break;
                case NetworkSceneSwitcher.FOREST_LANDSCAPE:
                    //v = ForestSurvivalModeController.position;
                    //r = ForestSurvivalModeController.rotation;
                    clip = wizardDuel;
                    break;
                case NetworkSceneSwitcher.SNOW_LANDSCAPE:
                    //v = SnowSurvivalModeController.position;
                    //r = SnowSurvivalModeController.rotation;
                    clip = wizardDuel;
                    break;
                case NetworkSceneSwitcher.TOWER_LANDSCAPE:
                    //v = TowerLandscapeIntro.position;
                    //r = TowerLandscapeIntro.rotation;
                    clip = wizardDuel;
                    break;
            }
            if (audioSource != null && clip != null)
            {
                audioSource.clip = clip;
                audioSource.loop = true;
                audioSource.volume = volume;
                audioSource.Play();
            }
            else if (audioSource != null)
            {
                audioSource.Stop();
            }
        }
    }
}
