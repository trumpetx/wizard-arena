using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;

namespace WizardArena 
{
    public class NetworkManagerCustom : NetworkLobbyManager
    {
        private HashSet<Transform> takenPositions = new HashSet<Transform>();
        public List<NetworkWizardController> wizards = new List<NetworkWizardController>();
        public bool ready = false;
        public delegate void RegisterWizardHandler(NetworkWizardController wizard);
        public event RegisterWizardHandler OnRegisterWizard;

        private void Start()
        {
            StartHost();
        }

        public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
        {
            Debug.Log("Creating Lobby Player for playerControllerId " + playerControllerId);
            Transform start = GetStartPosition();
            GameObject lobbyPlayer = Instantiate(lobbyPlayerPrefab.gameObject, start.position, start.rotation);
            lobbyPlayer.transform.parent = transform;
            return lobbyPlayer;
        }

        public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
        {
            Debug.Log("Creating Game Player for playerControllerId " + playerControllerId);
            Transform startPosition = startPositions.First(p => !takenPositions.Contains(p));
            if (startPosition == null)
            {
                throw new UnityException("No start positions are available.");
            }
            takenPositions.Add(startPosition);
            GameObject player = Instantiate(gamePlayerPrefab, startPosition.position, startPosition.rotation);
            NetworkWizardController controller = player.GetComponent<NetworkWizardController>();
            controller.spawnPoint = startPosition;
            controller.onDisconnectHandler = HandlePlayerDisconnected;

            // For vs. mode
            if (false)
            {
                NetworkPew pew = player.GetComponent<NetworkPew>();
                controller.assignedLayer = startPosition.gameObject.layer;
                int enemyMask = 0;
                foreach (var p in startPositions)
                {
                    int layer = p.gameObject.layer;
                    if (layer != controller.gameObject.layer)
                    {
                        enemyMask |= 1 << layer;
                    }
                }
                pew.enemyMask = enemyMask;
            }
            StartCoroutine(AsyncRegisterWizard(controller));
            return player;
        }

        private IEnumerator AsyncRegisterWizard(NetworkWizardController controller)
        {
            yield return new WaitUntil(() => controller.player != null && ready);
            OnRegisterWizard?.Invoke(controller);
            wizards.Add(controller);
        }

        private void HandlePlayerDisconnected(NetworkWizardController controller)
        {
            takenPositions.Remove(controller.spawnPoint);
            wizards.Remove(controller);
        }

        public override void OnLobbyServerSceneChanged(string sceneName)
        {
            Debug.Log("Scene Change complete for " + sceneName);
            ready = true;
        }

        public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
        {
            return true;
        }

        /*

        public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
        {
            Debug.Log("OnLobbyServerCreateLobbyPlayer");
            GameObject lobbyPlayer = Instantiate(lobbyPlayerPrefab.gameObject, Vector3.zero, Quaternion.identity);
            lobbyPlayer.transform.parent = transform;
            return lobbyPlayer;
        }

        public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
        {
            return Instantiate(gamePlayerPrefab, Vector3.zero, Quaternion.identity);
        }
        private OptionsMenuManager optionsMenuManager;

        private void Start()
        {
            optionsMenuManager = OptionsMenuManager.instance;
            StartHost();
        }

        //
        // Summary:
        //     ///
        //     Called on the client when adding a player to the lobby fails.
        //     ///
        public override void OnLobbyClientAddPlayerFailed()
        {
            Debug.Log("OnLobbyClientAddPlayerFailed");
        }
        //
        // Summary:
        //     ///
        //     This is called on the client when it connects to server.
        //     ///
        //
        // Parameters:
        //   conn:
        //     The connection that connected.
        public override void OnLobbyClientConnect(NetworkConnection conn)
        {
            Debug.Log("OnLobbyClientConnect");
        }
        //
        // Summary:
        //     ///
        //     This is called on the client when disconnected from a server.
        //     ///
        //
        // Parameters:
        //   conn:
        //     The connection that disconnected.
        public override void OnLobbyClientDisconnect(NetworkConnection conn)
        {
            Debug.Log("OnLobbyClientDisconnect");
        }
        //
        // Summary:
        //     ///
        //     This is a hook to allow custom behaviour when the game client enters the lobby.
        //     ///
        public override void OnLobbyClientEnter()
        {
            Debug.Log("OnLobbyClientEnter");
        }
        //
        // Summary:
        //     ///
        //     This is a hook to allow custom behaviour when the game client exits the lobby.
        //     ///
        public override void OnLobbyClientExit()
        {
            base.OnLo
            Debug.Log("OnLobbyClientExit");
        }
        //
        // Summary:
        //     ///
        //     This is called on the client when the client is finished loading a new networked
        //     scene.
        //     ///
        //
        // Parameters:
        //   conn:
        public override void OnLobbyClientSceneChanged(NetworkConnection conn)
        {
            Debug.Log("OnLobbyClientSceneChanged");
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when a new client connects to the server.
        //     ///
        //
        // Parameters:
        //   conn:
        //     The new connection.
        public override void OnLobbyServerConnect(NetworkConnection conn)
        {
            Debug.Log("OnLobbyServerConnect");
        }
        //
        // Summary:
        //     ///
        //     This allows customization of the creation of the GamePlayer object on the server.
        //     ///
        //
        // Parameters:
        //   conn:
        //     The connection the player object is for.
        //
        //   playerControllerId:
        //     The controllerId of the player on the connnection.
        //
        // Returns:
        //     ///
        //     A new GamePlayer object.
        //     ///
        public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
        {
            Debug.Log("OnLobbyServerCreateGamePlayer: playerControllerId=" + playerControllerId);
            return base.OnLobbyServerCreateGamePlayer(conn, playerControllerId);
        }
        //
        // Summary:
        //     ///
        //     This allows customization of the creation of the lobby-player object on the server.
        //     ///
        //
        // Parameters:
        //   conn:
        //     The connection the player object is for.
        //
        //   playerControllerId:
        //     The controllerId of the player.
        //
        // Returns:
        //     ///
        //     The new lobby-player object.
        //     ///
        public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
        {
            Debug.Log("OnLobbyServerCreateLobbyPlayer");
            GameObject lobbyPlayer = base.OnLobbyServerCreateLobbyPlayer(conn, playerControllerId);

            return lobbyPlayer;
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when a client disconnects.
        //     ///
        //
        // Parameters:
        //   conn:
        //     The connection that disconnected.
        public override void OnLobbyServerDisconnect(NetworkConnection conn)
        {
            Debug.Log("OnLobbyServerDisconnect");
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when a player is removed.
        //     ///
        //
        // Parameters:
        //   conn:
        //
        //   playerControllerId:
        public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
        {
            Debug.Log("OnLobbyServerPlayerRemoved: playerControllerId="+ playerControllerId);
            optionsMenuManager.multiplayerHostPage.RemovePlayerFromList(null);
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when all the players in the lobby are ready.
        //     ///
        public override void OnLobbyServerPlayersReady()
        {
            Debug.Log("OnLobbyServerPlayersReady");
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when a networked scene finishes loading.
        //     ///
        //
        // Parameters:
        //   sceneName:
        //     Name of the new scene.
        public override void OnLobbyServerSceneChanged(string sceneName)
        {
            Debug.Log("OnLobbyServerSceneChanged: sceneName=" + sceneName);
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when it is told that a client has finished switching
        //     from the lobby scene to a game player scene.
        //     ///
        //
        // Parameters:
        //   lobbyPlayer:
        //     The lobby player object.
        //
        //   gamePlayer:
        //     The game player object.
        //
        // Returns:
        //     ///
        //     False to not allow this player to replace the lobby player.
        //     ///
        public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
        {
            Debug.Log("OnLobbyServerSceneLoadedForPlayer");
            return base.OnLobbyServerSceneLoadedForPlayer(lobbyPlayer, gamePlayer);
        }
        //
        // Summary:
        //     ///
        //     This is called on the client when a client is started.
        //     ///
        //
        // Parameters:
        //   lobbyClient:
        public override void OnLobbyStartClient(NetworkClient lobbyClient)
        {
            Debug.Log("OnLobbyStartClient");
        }
        //
        // Summary:
        //     ///
        //     This is called on the host when a host is started.
        //     ///
        public override void OnLobbyStartHost()
        {
            Debug.Log("OnLobbyStartHost");
        }
        //
        // Summary:
        //     ///
        //     This is called on the server when the server is started - including when a host
        //     is started.
        //     ///
        public override void OnLobbyStartServer()
        {
            Debug.Log("OnLobbyStartServer");
        }
        //
        // Summary:
        //     ///
        //     This is called on the client when the client stops.
        //     ///
        public override void OnLobbyStopClient()
        {
            Debug.Log("OnLobbyStopClient");
        }

        //
        // Summary:
        //     ///
        //     This is called on the host when the host is stopped.
        //     ///
        public override void OnLobbyStopHost()
        {
            Debug.Log("OnLobbyStopHost");
        } 
         */

    }
}