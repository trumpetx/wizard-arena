using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    public class NetworkPotionConsumer : NetworkBehaviour
    {
        public delegate void OnDrinkHandler(DrinkablePotion potion);
        public event OnDrinkHandler OnConsume;
        public AudioClip drinkSound;
        private AudioSource audioSource;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.volume = .5f;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (OnConsume != null)
            {
                DrinkablePotion potion = other.GetComponent<DrinkablePotion>();
                if (potion != null)
                {
                    audioSource.PlayOneShot(drinkSound);
                    OnConsume(potion);
                }
            }
        }
    }
}
