using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    public class EasyWizard : VRTKNetworkBehavior
    {
        public GameObject enemy;
        private GameObject spawn;
        private NpcCharacter npc { get; set; }
        private NetworkWizardController player { get; set; }
        private CountdownTimer countdown;
        private NetworkSceneSwitcher teleport;
        private float difficultyChange = 1f;
        private int difficultyLevel = 1;

        private void OnEnable()
        {
            countdown = CountdownTimer.instance;
            player = FindObjectOfType<NetworkWizardController>();
            player.player.OnDeath += OnPlayerDeath;
            NewWizard();
        }
        private void NewWizard()
        {
            countdown.StartCountdown(5, Color.black, Spawn);
            InvokeRepeating("Shoot", (5 + Random.value * 10), 3 * difficultyChange);
            difficultyChange *= .93f;
            Debug.Log("Difficulty is now : " + (++difficultyLevel));
        }

        private void Spawn()
        {
            spawn = Instantiate(enemy, new Vector3(373, 18, 265.9f), Quaternion.identity);
            if (!(npc = spawn.GetComponentInChildren<NpcCharacter>()))
            {
                throw new UnityException("Expected to spawn a NpcCharacter");
            }
            npc.pew.SetFaceReferencePosition(spawn.transform);
            npc.character.OnDeath += OnNpcDeath;
        }

        private void Shoot()
        {
            if (npc && Random.value > .5f)
            {
                npc.pew.element = player.availableElements[Random.Range(0, player.availableElements.Length)];
                npc.CmdDoShoot(10f);
            }
        }

        private void OnDisable()
        {
            if (npc)
            {
                npc.character.OnDeath -= OnNpcDeath;
            }
            if (spawn)
            {
                DestroyImmediate(spawn);
            }
            if (player)
            {
                player.player.OnDeath -= OnPlayerDeath;
            }
        }

        private void OnNpcDeath()
        {
            if (npc)
            {
                npc.character.OnDeath -= OnNpcDeath;
                npc = null;
            }
            NewWizard();
        }

        private void OnPlayerDeath()
        {
            countdown.StartCountdown(5, Color.red, () => FindObjectOfType<NetworkSceneSwitcher>().CmdChangeScene(NetworkSceneSwitcher.PRACTICE_ROOM));
        }
    }
}