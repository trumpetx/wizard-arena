﻿using UnityEngine;
using VRTK;

namespace WizardArena
{
    public class DynamicPointer : VRTKNetworkBehavior
    {
        private int curvedUiLayerMask;
        private int powerUiLayerMask;
        private bool pointerOn;
        private Transform lastMain;
        private LaserPointer powerUiPointer;

        private void OnEnable()
        {
            curvedUiLayerMask |= 1 << 13; // CurvedUI
            powerUiLayerMask |= 1 << 14;  // InputUI
        }

        private void OnDisable()
        {
            curvedUiLayerMask = 0;
            powerUiLayerMask = 0;
            VRTK_InteractUse vrtkInteractUse = MainHandScriptController().GetComponent<VRTK_InteractUse>();
            if (vrtkInteractUse != null)
            {
                vrtkInteractUse.UseButtonPressed -= MouseClick;
                vrtkInteractUse.UseButtonReleased -= MouseUnclick;
            }
            if(powerUiPointer != null)
            {
                powerUiPointer.Remove();
                powerUiPointer = null;
            }
        }

        private void MouseClick(object sender, ControllerInteractionEventArgs e)
        {
            if (powerUiPointer != null)
            {
                powerUiPointer.Click(0);
            }
        }

        private void MouseUnclick(object sender, ControllerInteractionEventArgs e)
        {
            if (powerUiPointer != null)
            {
                powerUiPointer.LeftUp();
            }
        }

        private void TogglePowerUiPointer(bool on)
        {
            if (on)
            {
                powerUiPointer = powerUiPointer ?? new LaserPointer(MainHandController());
                powerUiPointer.Add();
                VRTK_InteractUse vrtkInteractUse = powerUiPointer.Transform.GetComponent<VRTK_InteractUse>();
                vrtkInteractUse.UseButtonPressed += MouseClick;
                vrtkInteractUse.UseButtonReleased += MouseUnclick;
            }
            else if (powerUiPointer != null)
            {
                powerUiPointer.Remove();
                VRTK_InteractUse vrtkInteractUse = powerUiPointer.Transform.GetComponent<VRTK_InteractUse>();
                vrtkInteractUse.UseButtonPressed -= MouseClick;
                vrtkInteractUse.UseButtonReleased -= MouseUnclick;
                powerUiPointer = null;
            }
        }

        private void Update()
        {
            if (powerUiLayerMask != 0)
            {
                Transform main = MainHandController();
                // Weird bug where the curved ui input "loses" the pointer - this should reset the state
                pointerOn = pointerOn && main.GetComponent<VRTK_Pointer>().enabled;

                if (pointerOn && main != lastMain)
                {
                    Toggle(pointerOn);
                    TogglePowerUiPointer(false);
                }
                lastMain = main;

                Vector3 origin = main.position;
                Ray ray = new Ray(origin, main.forward);
                if (Physics.Raycast(ray, Mathf.Infinity, curvedUiLayerMask) != pointerOn)
                {
                    pointerOn = !pointerOn;
                }
                else if (Physics.Raycast(ray, Mathf.Infinity, powerUiLayerMask) != (powerUiPointer != null))
                {
                    TogglePowerUiPointer(powerUiPointer == null);
                }
                else
                {
                    // No changes, continue
                    return;
                }
                Toggle(powerUiPointer != null || pointerOn);
            }
        }

        private void Toggle(bool pointerOn)
        {
            GameObject mainHandScript = MainHandScriptController();
            GameObject offHandScript = OffhandScriptController();
            VRTK_StraightPointerRenderer renderer = offHandScript.GetComponent<VRTK_StraightPointerRenderer>();
            VRTK_Pointer pointer = offHandScript.GetComponent<VRTK_Pointer>();
            renderer.enabled = false;
            pointer.enabled = false;

            renderer = mainHandScript.GetComponent<VRTK_StraightPointerRenderer>();
            pointer = mainHandScript.GetComponent<VRTK_Pointer>();
            renderer.enabled = pointerOn;
            pointer.enabled = pointerOn;
            pointer.pointerRenderer = renderer;
        }
    }
}