﻿using UnityEngine;

public class RotateAndBounce : MonoBehaviour
{
    public float rotateFactor = .1f;
    public float bounceHeight = .3f;
    private bool goDown = true;
    private Vector3 upperPosition, lowerPosition;
    
    private void Start ()
    {
        upperPosition = transform.position + new Vector3(0, bounceHeight/2, 0);
        lowerPosition = transform.position - new Vector3(0, bounceHeight/2, 0);
    }
	
	
	private void Update () {
        if (goDown)
        {
            transform.position = Vector3.Slerp(transform.position, lowerPosition, Time.deltaTime * .5f);
        }
        else
        {
            transform.position = Vector3.Slerp(transform.position, upperPosition, Time.deltaTime * .5f);
        }
        if (transform.position.y <= lowerPosition.y + .1f)
        {
            goDown = false;
        }
        else if (transform.position.y >= upperPosition.y - .1f)
        {
            goDown = true;
        }
        
        if (rotateFactor != 0)
        {
            transform.Rotate(Vector3.up * rotateFactor);
        }
    }
}
