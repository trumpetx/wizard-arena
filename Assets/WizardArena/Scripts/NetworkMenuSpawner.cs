using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using VRTK;

namespace WizardArena
{
    public class NetworkMenuSpawner : VRTKNetworkBehavior
    {
        private HashSet<int> registered = new HashSet<int>();

        public void DoSpawn(Spawnable prefab, Vector3 pos, Quaternion rot)
        {
            if (!registered.Contains(prefab.itemId))
            {
                ClientScene.RegisterPrefab(prefab.gameObject);
                registered.Add(prefab.itemId);
            }
            CmdDoSpawn(prefab.gameObject, pos, rot);
        }

        [Command]
        public void CmdDoSpawn(GameObject prefab, Vector3 pos, Quaternion rot)
        {
            GameObject o = Instantiate(prefab, pos, rot);
            NetworkServer.Spawn(o);
            TargetRpcOnSpawned(connectionToClient, o);
        }

        [TargetRpc]
        public void TargetRpcOnSpawned(NetworkConnection target, GameObject spawned)
        {
            if (MainHandController() != null)
            {
                VRTK_InteractGrab grabber = MainHandController().GetComponent<VRTK_InteractGrab>();
                VRTK_InteractTouch toucher = MainHandController().GetComponent<VRTK_InteractTouch>();
                if (grabber != null && toucher != null)
                {
                    toucher.ForceTouch(spawned);
                    grabber.AttemptGrab();
                }
            }
        }
    }
}
