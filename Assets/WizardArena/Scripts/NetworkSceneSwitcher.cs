using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using VRTK;
using System;

namespace WizardArena
{
    public class NetworkSceneSwitcher : VRTKNetworkBehavior
    {
        public const string TOWER_LANDSCAPE = "TowerLandscape";
        public const string PRACTICE_ROOM = "PracticeRoom";
        public const string LAVA_LANDSCAPE = "LavaLandscape";
        public const string BANDIT_CAMP = "BanditCamp";
        public const string MOUNTAIN_MAP = "MountainMap";
		public const string FOREST_LANDSCAPE = "ForestLandscape";
		public const string SNOW_LANDSCAPE = "SnowLandscape";
        public const string MULTIPLAYER = "Multiplayer";
        
        private static Texture splash;
        private static Texture loadingBar;
        private static Texture loadingFill;

        private Coroutine loadSceneCoroutine;
        private object changeSceneLock = new object();
        private NetworkManagerCustom networkManager;

        private void OnEnable()
        {
            networkManager = (NetworkManagerCustom) NetworkManager.singleton;
            if (isLocalPlayer)
            {
                splash = splash ?? Resources.Load("splash") as Texture;
                loadingBar = loadingBar ?? Resources.Load("loading-bar") as Texture;
                loadingFill = loadingFill ?? Resources.Load("loading-fill") as Texture;
                if (splash == null || loadingBar == null || loadingFill == null)
                {
                    Debug.LogError("Splash screen files are not in Resources/");
                }
            }
        }

        [ClientRpc]
        public void RpcClientLoadScene(string sceneName)
        {
            lock (changeSceneLock)
            {
                if (loadSceneCoroutine == null && !isServer)
                {
                    LoadScene(sceneName, () =>
                    {
                        Debug.Log("Client Ready!");
                        ClientScene.Ready(connectionToServer);
                        networkManager.OnLobbyClientSceneChanged(connectionToServer);
                    });
                }
            }
        }

        [Command]
        public void CmdChangeScene(string sceneName)
        {
            if (true)
            {
                if (networkManager.lobbyScene != sceneName)
                {
                    networkManager.playScene = sceneName;
                    networkManager.ServerChangeScene(sceneName);
                }
                else
                {
                    networkManager.ServerReturnToLobby();
                }
                return;
            }
            lock (changeSceneLock)
            {
                if (loadSceneCoroutine == null)
                {
                    networkManager.wizards.Clear();
                    RpcClientLoadScene(sceneName);
                    LoadScene(sceneName, () =>
                    {
                        NetworkServer.SpawnObjects();
                        Debug.Log("Host Ready!");
                        networkManager.OnLobbyServerSceneChanged(sceneName);
                    });
                }
                else
                {
                    Debug.LogError("Unable to switch scenes to " + sceneName + ". An existing scene load is underway.");
                }
            }
        }

        public void LoadScene(string sceneName, Action onSceneLoaded)
        {
            Scene activeScene = SceneManager.GetActiveScene();
            if (VRTK_SDKManager.instance != null && VRTK_SDKManager.instance.loadedSetup != null && VRTK_SDKManager.instance.loadedSetup.headsetSDK.GetType() == typeof(SDK_SteamVRHeadset))
            {
                Debug.Log("Changing Scene (SteamVR) from " + activeScene.name + " to " + sceneName);
                SteamVR_LoadLevel steamLoadLevel = new GameObject("SteamVR_LoadLevel").AddComponent<SteamVR_LoadLevel>();
                GameObject ls = new GameObject("SteamVR_LoadLevel_LoadingScreen");
                GameObject pb = new GameObject("SteamVR_LoadLevel_ProgressBar");
                pb.transform.parent = ls.transform;
                pb.transform.localPosition = new Vector3(pb.transform.localPosition.x, pb.transform.localPosition.y - 2f, pb.transform.localPosition.z);
                steamLoadLevel.loadingScreenTransform = ls.transform;
                steamLoadLevel.loadingScreen = splash;
                steamLoadLevel.loadingScreenDistance = 40f;
                steamLoadLevel.loadingScreenWidthInMeters = 40f;
                steamLoadLevel.loadAsync = true;
                steamLoadLevel.loadAdditive = false;
                steamLoadLevel.progressBarTransform = pb.transform;
                steamLoadLevel.progressBarEmpty = loadingBar;
                steamLoadLevel.progressBarFull = loadingFill;
                steamLoadLevel.progressBarWidthInMeters = 40f;
                steamLoadLevel.levelName = sceneName;
                // This was made public by us, updates to SteamVR will break this
                steamLoadLevel.LoadLevel();
            }
            else
            {
                Debug.Log("Changing Scene from " + activeScene.name + " to " + sceneName);
                SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
            }
            onSceneLoaded.Invoke();
            loadSceneCoroutine = null;
        }
    }
}