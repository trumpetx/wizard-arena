using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;
using System;

namespace WizardArena
{
    public class MenuManager : VRTKNetworkBehavior
    {
        private static Sprite returnIcon;
        
        private InventoryManager inventoryManager;
        private VRTK_RadialMenu elementMenu;
        private VRTK_RadialMenu inventoryMenu;
        private NetworkMenuSpawner menuSpawner;
        
        private void OnEnable()
        {
            menuSpawner = menuSpawner ?? GetComponent<NetworkMenuSpawner>();
            returnIcon = returnIcon ?? Resources.Load<Sprite>("return");
            inventoryManager = inventoryManager ?? InventoryManager.instance;
            foreach (var menu in FindObjectsOfType<VRTK_RadialMenu>())
            {
                menu.enabled = false;
            }
        }
        internal override void OnDestroy()
        {
            inventoryManager.CleanupScene();
            base.OnDestroy();
        }

        public void SetupMenusOutOfCombat()
        {
            SetupMenus(-1, null);
        }

        public void SetupMenus(int currentElement, Action<int> onElementSelect)
        {
            inventoryMenu = MainHandController().GetComponentInChildren<VRTK_RadialMenu>(true);
            inventoryMenu.enabled = true;
            // some weird and obscure bug where it can't find the canvas from inside the class - strictly a workaround
            inventoryMenu.parentCanvas = MainHandController().GetComponentInChildren<Canvas>();
            SetupInventoryMenu();

            elementMenu = OffhandController().GetComponentInChildren<VRTK_RadialMenu>(true);
            elementMenu.enabled = true;
            elementMenu.parentCanvas = OffhandController().GetComponentInChildren<Canvas>();
            SetupElementMenu(currentElement, playerSave.selectedElements, playerSave.availableElements, playerSave.maxElements, onElementSelect);
        }
        
        private void ReturnGrabbed(VRTK_InteractGrab grab)
        {
            if (grab != null && grab.GetGrabbedObject() != null)
            {
                Spawnable currentlyGrabbed = grab.GetGrabbedObject().GetComponent<Spawnable>();
                if (currentlyGrabbed != null)
                {
                    AddToInventory(currentlyGrabbed.itemId);
                    grab.ForceRelease();
                    currentlyGrabbed.enabled = false;
                    Despawn(currentlyGrabbed);
                }
            }
        }

        public void Despawn(Spawnable spawnable)
        {
            inventoryManager.RemoveSpawned(spawnable.itemId);
            Destroy(spawnable.gameObject);
        }

        public void AddToInventory(int sId, int quantity = 1)
        {
            bool firstOne;
            inventoryManager.AddToInventory(sId, quantity, out firstOne);
            if (firstOne)
            {
                SetupInventoryMenu();
            }
        }

        private void SetupElementMenu(int currentElement, List<int> selectedElements, List<int> availableElements, int maxElements, Action<int> onElementSelect)
        {
            bool inCombat = currentElement > -1;
            List<VRTK_RadialMenu.RadialMenuButton> buttons = elementMenu.buttons;
            buttons.Clear();
            elementMenu.hideOnRelease = true;
            elementMenu.generateOnAwake = false;

            if (inCombat)
            {
                // Add just the "selected" buttons
                selectedElements.ForEach(id => {
                    VRTK_RadialMenu.RadialMenuButton button = new VRTK_RadialMenu.RadialMenuButton();
                    button.ButtonIcon = WizardConstants.ELEMENTS[id].GetIcon();
                    button.OnClick.AddListener(() => {
                        onElementSelect?.Invoke(id);
                        for (int k = 0; k < elementMenu.menuButtons.Count; k++)
                        {
                            elementMenu.menuButtons[k].GetComponent<Button>().enabled = selectedElements.IndexOf(id) != k;
                        }
                    });
                    elementMenu.buttons.Add(button);
                });
            }
            else
            {
                availableElements.ConvertAll(i => WizardConstants.ELEMENTS[i]).Where(e => e.IsMagic()).ToList().ForEach(element =>
                {
                    int id = element.GetId();
                    VRTK_RadialMenu.RadialMenuButton button = new VRTK_RadialMenu.RadialMenuButton();
                    button.ButtonIcon = element.GetIcon();
                    button.OnClick.AddListener(() =>
                    {
                        onElementSelect?.Invoke(id);
                        if (selectedElements.Contains(id))
                        {
                            return;
                        }
                        if (selectedElements.Count >= maxElements)
                        {
                            selectedElements.RemoveAt(0);
                        }
                        selectedElements.Add(id);
                        for (int k = 0; k < elementMenu.menuButtons.Count; k++)
                        {
                            elementMenu.menuButtons[k].GetComponent<Button>().enabled = !playerSave.selectedElements.Any(e => availableElements.IndexOf(e) == k);
                        }
                    });
                    elementMenu.buttons.Add(button);
                });
            }
            elementMenu.RegenerateButtons();
            PostRegenerate(elementMenu);
            if (inCombat)
            {
                elementMenu.menuButtons[selectedElements.IndexOf(currentElement)].GetComponent<Button>().enabled = false;
            }
            else
            {
                selectedElements.ForEach(e => elementMenu.menuButtons[availableElements.IndexOf(e)].GetComponent<Button>().enabled = false);
            }
        }

        private void SetupInventoryMenu()
        {
            List<VRTK_RadialMenu.RadialMenuButton> buttons = inventoryMenu.buttons;
            buttons.Clear();
            inventoryMenu.hideOnRelease = true;
            inventoryMenu.generateOnAwake = false;

            inventoryManager.ForEachInventoryItem((Spawnable s, int quantity) =>
            {
                if (quantity > 0)
                {
                    inventoryMenu.buttons.Add(SpawnableMenuItem(s));
                }
            });
            
            VRTK_RadialMenu.RadialMenuButton returnButton = new VRTK_RadialMenu.RadialMenuButton();
            returnButton.ButtonIcon = returnIcon;
            returnButton.OnClick.AddListener(() =>
            {
                ReturnGrabbed(MainHandScriptController().GetComponent<VRTK_InteractGrab>());
            });
            inventoryMenu.buttons.Add(returnButton);

            inventoryMenu.RegenerateButtons();
            inventoryMenu.enabled = buttons.Count != 0;
            if (inventoryMenu.enabled)
            {
                PostRegenerate(inventoryMenu);
            }
        }

        private VRTK_RadialMenu.RadialMenuButton SpawnableMenuItem(Spawnable spawnable)
        {
            VRTK_RadialMenu.RadialMenuButton button = new VRTK_RadialMenu.RadialMenuButton();
            button.ButtonIcon = spawnable.icon;
            VRTK_InteractableObject io = spawnable.GetComponent<VRTK_InteractableObject>();
            button.OnClick.AddListener(() => {
                VRTK_InteractGrab grabber = MainHandScriptController().GetComponent<VRTK_InteractGrab>();
                ReturnGrabbed(grabber);
                bool resetMenu;
                if (inventoryManager.SpawnFromInventory(spawnable.itemId, out resetMenu))
                {
                    // Spawnable prefabs have references to themselves.  Thier spawned clones will retain the same reference to the original prefab
                    menuSpawner.DoSpawn(spawnable, MainHandController().transform.position, Quaternion.identity);
                }
                else
                {
                    Debug.LogError("Unable to spawn item (" + spawnable.name + ") because there are none in the inventory.");
                    resetMenu = true;
                }
                if (resetMenu)
                {
                    SetupInventoryMenu();
                }

            });
            return button;
        }

        private void PostRegenerate(VRTK_RadialMenu m)
        {
            ColorBlock colors = new ColorBlock();
            colors.disabledColor = Color.white;
            colors.normalColor = new Color(0, 0, 0, 0);
            colors.highlightedColor = Color.gray;
            colors.colorMultiplier = 1;
            colors.fadeDuration = .1f;
            colors.pressedColor = Color.black;
            m.menuButtons.ForEach(mb =>
            {
                mb.GetComponent<Button>().colors = colors;
            });
        }
    }
}