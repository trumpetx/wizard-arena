
using System;
using UnityEngine;

namespace WizardArena
{
    public class SimulatorGestureRecognizer : IGestureRecognizer
    {
        private Action<Gesture> gestureHandler;
        private UpdateHook updateHook;
        public SimulatorGestureRecognizer(Action<Gesture> gestureHandler, UpdateHook updateHook)
        {
            this.updateHook = updateHook;
            this.gestureHandler = gestureHandler;
            updateHook.OnUpdate += OnUpdate;
        }

        public void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                gestureHandler.Invoke(Gesture.SHOOT);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                gestureHandler.Invoke(Gesture.CLEAVE);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                gestureHandler.Invoke(Gesture.PUSH);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                gestureHandler.Invoke(Gesture.SHIELD);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                gestureHandler.Invoke(Gesture.BEAM);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                gestureHandler.Invoke(Gesture.SUMMON);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                gestureHandler.Invoke(Gesture.TELEPORT);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                gestureHandler.Invoke(Gesture.WALL);
            }
        }

        public void Disable()
        {
            updateHook.OnUpdate -= OnUpdate;
        }
    }
}