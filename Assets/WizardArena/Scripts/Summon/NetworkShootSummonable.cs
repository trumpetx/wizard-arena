﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    [RequireComponent(typeof(ChangesHealth))]
    public class NetworkShootSummonable : NetworkSummonable
    {
        public GameObject projectilePrefab;
        private Transform target;

        private float shootTimer = 0f;
        private float shootCooldown = 2f; // modify for wisdom
        private const float v = 20f;
        private ChangesHealth doesDamage;

        internal override void OnEnable()
        {
            base.OnEnable();
            doesDamage = doesDamage ?? GetComponent<ChangesHealth>();
            ClientScene.RegisterPrefab(projectilePrefab);
        }

        private void FindTarget()
        {
            List<NetworkWizardController> enemies = new List<NetworkWizardController>(FindObjectsOfType<NetworkWizardController>())
                // Add distance check
                .Where(p => p.assignedLayer != gameObject.layer).ToList();
            target = (enemies.Count > 0) 
                ? enemies.OrderBy(x => Guid.NewGuid()).Take(1).First().transform 
                : null;
        }

        private void Update()
        {
            if (isServer)
            {
                if (shootTimer >= shootCooldown)
                {
                    if(target == null || !target.gameObject.activeSelf)
                    {
                        FindTarget();
                    }

                    if (target != null)
                    {
                        DoShoot();
                    }
                    shootTimer = 0;
                }
                shootTimer += Time.deltaTime;
            }
        }

        private void DoShoot()
        {
            GameObject projectile = Instantiate(projectilePrefab, transform.position + new Vector3(0, 1f, 0), Quaternion.identity);
            projectile.transform.LookAt(target);
            ChangesHealth d = projectile.GetComponent<ChangesHealth>();
            d.healthChange = doesDamage.healthChange;
            d.element = doesDamage.element;
            projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * v;
            Util.AssignLayer(projectile.transform, gameObject.layer);
            NetworkServer.Spawn(projectile);
            Destroy(projectile, 5f);
        }
    }
}