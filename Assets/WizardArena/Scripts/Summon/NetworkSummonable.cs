using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    public class NetworkSummonable : NetworkBehaviour
    {
        public GameObject preview;
        public GameObject summonEffect;

        internal virtual void OnEnable()
        {
            if (summonEffect != null)
            {
                // Create summon effect when created, destroy after x seconds
                Destroy(Instantiate(summonEffect, transform.position, transform.rotation), 3f);
            }
        }
    }
}