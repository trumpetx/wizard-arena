using System;
using VRTK;

namespace WizardArena
{
    public class GestureRecognizerFactory
    {
        public static GestureRecognizerFactory instance { get; private set; }
        private UpdateHook updateHook;

        public GestureRecognizerFactory(UpdateHook updateHook)
        {
            if (instance == null)
            {
                instance = this;
            }
            this.updateHook = updateHook;
        }

        public IGestureRecognizer GetGestureRecognizer(Action<Gesture> gestureHandler, bool leftHanded, VRTK_SDKSetup loadedSetup)
        {
            if (loadedSetup.GetComponentInChildren<SDK_InputSimulator>() != null)
            {
                return new SimulatorGestureRecognizer(gestureHandler, updateHook);
            }
            else
            {
                return new EdwonGesture(loadedSetup.transform, leftHanded, gestureHandler);
            }
        }
    }
}