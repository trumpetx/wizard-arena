using PowerUI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    [RequireComponent(typeof(WorldUIHelper))]
    public class LevelSelect : NetworkBehaviour
    {
        private Vector3 endPosition = new Vector3(.823f, .872f, .453f);
        private WorldUIHelper worldUiHelper;
        private Dictionary<string, Vector3> startingPositions = new Dictionary<string, Vector3>();
        public LevelSelectOrb levelSelectOrbPrefab;
        private LevelSelectOrb orb;
        
        private void Start()
        {
            worldUiHelper = GetComponent<WorldUIHelper>();
            PopulateStartingPositions();
            AddClickHandler("moleMountain", e => CmdSelectLevelOrb(NetworkSceneSwitcher.MOUNTAIN_MAP));
            AddClickHandler("tutorial", e => CmdSelectLevelOrb(NetworkSceneSwitcher.LAVA_LANDSCAPE));
            AddClickHandler("banditCamp", e => CmdSelectLevelOrb(NetworkSceneSwitcher.BANDIT_CAMP));
			AddClickHandler("cliffCastle", e => CmdSelectLevelOrb(NetworkSceneSwitcher.FOREST_LANDSCAPE));
			AddClickHandler("magicDial", e => CmdSelectLevelOrb(NetworkSceneSwitcher.SNOW_LANDSCAPE));
            AddClickHandler("multiplayer", e => CmdSelectLevelOrb(NetworkSceneSwitcher.MULTIPLAYER));

            ClientScene.RegisterPrefab(levelSelectOrbPrefab.gameObject);
        }

        private void AddClickHandler(string id, Action<MouseEvent> method)
        {
            worldUiHelper.document.getElementById(id).addEventListener("click", method);
        }

        [Command]
        private void CmdSelectLevelOrb(string levelName)
        {
            if (orb != null)
            {
                Destroy(orb.gameObject);
            }
            orb = Instantiate(levelSelectOrbPrefab, startingPositions[levelName], Quaternion.identity);
            orb.levelName = levelName;
            orb.endPosition = endPosition;
            NetworkServer.Spawn(orb.gameObject);
            RpcSetOrbVars(levelName, orb.gameObject);
        }

        [ClientRpc]
        private void RpcSetOrbVars(string levelName, GameObject o)
        {
            LevelSelectOrb orb = o.GetComponent<LevelSelectOrb>();
            orb.levelName = levelName;
            GameObject localPlayer = LobbyWizard.localPlayer == null ? NetworkWizardController.localPlayer.gameObject : LobbyWizard.localPlayer.gameObject;
            orb.onUse = () => localPlayer.GetComponent<NetworkSceneSwitcher>().CmdChangeScene(levelName);
        }

        private void OnDestroy()
        {
            Destroy(orb);
        }

        // find transform positions in scene corresponding to each level
        private void PopulateStartingPositions()
        {
            startingPositions.Add(NetworkSceneSwitcher.LAVA_LANDSCAPE,
                new Vector3(-.3f, 1.55f, 3.048f));

            startingPositions.Add(NetworkSceneSwitcher.MOUNTAIN_MAP,
                new Vector3(.5f, 1.55f, 3.048f));

            startingPositions.Add(NetworkSceneSwitcher.BANDIT_CAMP,
                new Vector3(1.3f, 1.55f, 3.048f));

            startingPositions.Add(NetworkSceneSwitcher.FOREST_LANDSCAPE,
                new Vector3(-.3f, 1f, 3.048f));

            startingPositions.Add(NetworkSceneSwitcher.SNOW_LANDSCAPE,
                new Vector3(.5f, 1f, 3.048f));

            startingPositions.Add(NetworkSceneSwitcher.MULTIPLAYER,
                new Vector3(1.3f, 1f, 3.048f));
        }
    }
}
