using UnityEngine;

namespace WizardArena
{
    public class Life : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load < Sprite >("life_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.LIFE;
        }

        override public string GetName()
        {
            return "Life";
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.HOLY_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }
    }
}