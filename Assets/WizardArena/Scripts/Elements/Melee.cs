
using UnityEngine;

namespace WizardArena
{
    public class Melee : AbstractElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("128/sword");
            return icon;
        }

        public override int GetId()
        {
            return WizardConstants.MELEE;
        }

        public override bool IsMagic()
        {
            return false;
        }

        public override string GetName()
        {
            return "Melee";
        }
    }

}