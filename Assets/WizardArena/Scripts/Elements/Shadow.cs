using UnityEngine;

namespace WizardArena
{
    public class Shadow : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("shadow_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.SHADOW;
        }

        override public string GetName()
        {
            return "Shadow";
        }

        override public float ModifyHealthRegen(float existingModifier)
        {
            return existingModifier + .5f;
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.HOLY_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }
    }
}