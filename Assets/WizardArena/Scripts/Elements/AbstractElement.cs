using UnityEngine;

namespace WizardArena
{
    public abstract class AbstractElement : IElement
    {
        public abstract int GetId();
        public abstract string GetName();
        public abstract Sprite GetIcon();
        public virtual float ModifyCleave(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyHealthRegen(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return existingModifier;
        }
        public virtual float ModifyManaRegen(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyShield(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyShoot(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyWall(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyBeamSeconds(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyBeam(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifyWallDamage(float existingModifier)
        {
            return  existingModifier;
        }
        public virtual float ModifySummonSeconds(float existingModifier)
        {
            return existingModifier;
        }
        public virtual float ModifySummon(float existingModifier)
        {
            return existingModifier;
        }
        public virtual bool IsMagic()
        {
            return true;
        }

        override public bool Equals(object obj)
        {
            AbstractElement e = obj as AbstractElement;
            return e != null && GetId() == e.GetId();
        }

        override public int GetHashCode()
        {
            return GetId();
        }
    }

}