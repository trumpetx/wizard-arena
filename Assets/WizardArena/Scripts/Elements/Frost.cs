using UnityEngine;

namespace WizardArena
{
    public class Frost : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("ice_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.FROST;
        }

        override public string GetName()
        {
            return "Frost";
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.ELEMENTAL_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }

        override public float ModifyShoot(float existingModifier)
        {
            return existingModifier + .5f;
        }
    }
}