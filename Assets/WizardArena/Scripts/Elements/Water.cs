using UnityEngine;

namespace WizardArena
{
    public class Water : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("water_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.WATER;
        }

        override public string GetName()
        {
            return "Water";
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.ELEMENTAL_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }

        override public float ModifyShoot(float existingModifier)
        {
            return existingModifier + .5f;
        }
    }
}