using UnityEngine;

namespace WizardArena
{
    public interface IElement
    {
        int GetId();
        string GetName();
        Sprite GetIcon();
        float ModifyShoot(float existingModifier);
        float ModifyShield(float existingModifier);
        float ModifyCleave(float existingModifier);
        float ModifyWall(float existingModifier);
        float ModifyIncomingShieldDamage(float existingModifier, int fromElement);
        float ModifyManaRegen(float existingModifier);
        float ModifyHealthRegen(float existingModifier);
        float ModifyBeamSeconds(float existingModifier);
        float ModifyBeam(float existingModifier);
        float ModifySummonSeconds(float existingModifier);
        float ModifySummon(float existingModifier);
        float ModifyWallDamage(float existingModifier);
        bool IsMagic();
    }
}