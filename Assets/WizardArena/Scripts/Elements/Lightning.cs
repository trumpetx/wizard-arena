using UnityEngine;

namespace WizardArena
{
    public class Lightning :AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("storm_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.LIGHTNING;
        }

        override public string GetName()
        {
            return "Lightning";
        }

        override public float ModifyManaRegen(float existingModifier)
        {
            return existingModifier + 1f;
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.ARCANE_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }
    }
}