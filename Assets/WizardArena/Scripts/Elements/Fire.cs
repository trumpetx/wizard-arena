using UnityEngine;

namespace WizardArena
{
    public class Fire : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("fire_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.FIRE;
        }

        override public string GetName()
        {
            return "Fire";
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.ELEMENTAL_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }

        override public float ModifyShoot(float existingModifier)
        {
            return existingModifier + .5f;
        }
    }
}