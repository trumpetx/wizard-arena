
using UnityEngine;

namespace WizardArena
{
    public class Ranged : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("128/bow");
            return icon;
        }

        public override int GetId()
        {
            return WizardConstants.RANGED;
        }

        public override bool IsMagic()
        {
            return false;
        }

        public override string GetName()
        {
            return "Ranged";
        }
    }
}