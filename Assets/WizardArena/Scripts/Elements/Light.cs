using UnityEngine;

namespace WizardArena
{
    public class Light : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("light_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.LIGHT;
        }

        override public string GetName()
        {
            return "Light";
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.HOLY_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }

        override public float ModifySummon(float existingModifier)
        {
            return -1 * existingModifier;
        }
    }
}