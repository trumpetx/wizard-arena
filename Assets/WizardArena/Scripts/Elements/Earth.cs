using UnityEngine;

namespace WizardArena
{
    public class Earth : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("earth_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.EARTH;
        }

        override public string GetName()
        {
            return "Earth";
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.ELEMENTAL_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }

        override public float ModifyShoot(float existingModifier)
        {
            return existingModifier + .5f;
        }
    }
}