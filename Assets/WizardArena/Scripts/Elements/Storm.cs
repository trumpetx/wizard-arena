using UnityEngine;

namespace WizardArena
{
    public class Storm : AbstractElement, IElement
    {
        private Sprite icon;

        public override Sprite GetIcon()
        {
            icon = icon ?? Resources.Load<Sprite>("wind_icon");
            return icon;
        }

        override public int GetId()
        {
            return WizardConstants.STORM;
        }

        override public string GetName()
        {
            return "Wind";
        }
        override public float ModifyManaRegen(float existingModifier)
        {
            return existingModifier + 1f;
        }

        override public float ModifyIncomingShieldDamage(float existingModifier, int fromElement)
        {
            return WizardConstants.ARCANE_ELEMENTS.Contains(fromElement) ? 1f : existingModifier;
        }
    }
}