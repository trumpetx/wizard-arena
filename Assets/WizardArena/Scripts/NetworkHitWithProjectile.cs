using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    public class NetworkHitWithProjectile : NetworkBehaviour
    {
        public delegate void OnHitHandler(float healthChange, int element);
        public event OnHitHandler OnHit;

        private void OnEnable()
        {
            if (!GetComponent<Collider>() && !GetComponentInChildren<NetworkChildHitWithProjectile>())
            {
                throw new UnityException("NetworkHitWithProjectile requires a related Collider or NetworkChildHitWithProjectile w/ Collider");
            }
        }

        public void DoHealthChange(ChangesHealth changesHealth, GameObject sourceObject)
        {
            if (!isServer)
            {
                return;
            }
            if (changesHealth != null && changesHealth.gameObject.layer != gameObject.layer && changesHealth.healthChange != 0)
            {
                int element;
                if (changesHealth.element < 0)
                {
                    string descriptor;
                    element = Util.ParsePewObject(sourceObject.name, out descriptor);
                }
                else
                {
                    element = changesHealth.element;
                }

                DoHealthChange(changesHealth.healthChange, element);
            }
        }

        public void DoHealthChange(float healthChange, int element)
        {
            OnHit?.Invoke(healthChange, element);
        }

        public void OnTriggerEnter(Collider collider)
        {
            if (!isServer)
            {
                return;
            }
            ChangesHealth changesHealth = collider.gameObject.GetComponent<ChangesHealth>();
            if (changesHealth != null)
            {
                DoHealthChange(changesHealth, collider.gameObject);
            }
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (!isServer)
            {
                return;
            }
            OnTriggerEnter(collision.collider);
        }

        public void OnTriggerStay(Collider collider)
        {
            if (!isServer)
            {
                return;
            }
            ChangesHealthOverTime changesHealthOverTime = collider.GetComponentInParent<ChangesHealthOverTime>();
            if (changesHealthOverTime != null && changesHealthOverTime.gameObject.layer != gameObject.layer)
            {
                changesHealthOverTime.timeSinceLastChange += Time.deltaTime;
                if (changesHealthOverTime.timeSinceLastChange > changesHealthOverTime.perSeconds)
                {
                    changesHealthOverTime.timeSinceLastChange = 0;
                    int element;
                    if (changesHealthOverTime.element < 0)
                    {
                        string descriptor;
                        element = Util.ParsePewObject(collider.gameObject.name, out descriptor);
                    }
                    else
                    {
                        element = changesHealthOverTime.element;
                    }
                    DoHealthChange(changesHealthOverTime.healthChange, element);
                }
            }
        }
    }
}