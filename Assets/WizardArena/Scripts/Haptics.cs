using UnityEngine;
using VRTK;

namespace WizardArena
{
    public class Haptics
    {
        public static void Pulse()
        {
            Pulse(VRTK_DeviceFinder.GetControllerRightHand(), VRTK_DeviceFinder.GetControllerRightHand());
        }

        public static void Pulse(params GameObject[] controllers)
        {
            foreach (GameObject controller in controllers)
            {
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(controller), 2000);
            }
        }

        public static void Pulse(params Transform[] controllers)
        {
            foreach (Transform controller in controllers)
            {
                VRTK_ControllerHaptics.TriggerHapticPulse(VRTK_ControllerReference.GetControllerReference(controller.gameObject), 2000);
            }
        }
    }
}