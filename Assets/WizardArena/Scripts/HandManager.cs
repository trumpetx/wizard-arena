using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace WizardArena
{
    public class HandManager : VRTKNetworkBehavior
    {
        public static HashSet<HandManager> instances = new HashSet<HandManager>();
        private VRTK_InteractGrab interactGrab;
        private Animator animator;
        public GameObject femaleHand;
        public GameObject maleHand;
        public Material femaleColor01;
        public Material femaleColor02;
        public Material femaleColor03;
        public Material maleColor01;
        public Material maleColor02;
        public Material maleColor03;

        private void OnEnable()
        {
            interactGrab = interactGrab ?? GetComponentInParent<VRTK_InteractGrab>();
            interactGrab.GrabButtonPressed += Grab;
            interactGrab.GrabButtonReleased += UnGrab;
            SetGender(playerSave.gender);
            SetSkinTone(playerSave.skinTone);
            instances.Add(this);
        }

        private void OnDisable()
        {
            if (interactGrab != null)
            {
                interactGrab.GrabButtonPressed -= Grab;
                interactGrab.GrabButtonReleased -= UnGrab;
            }
            instances.Remove(this);
        }

        private void Grab(object sender, ControllerInteractionEventArgs e)
        {
            animator.SetBool("Hold", true);
        }

        private void UnGrab(object sender, ControllerInteractionEventArgs e)
        {
            animator.SetBool("Hold", false);
        }

        public void SetSkinTone(int tone)
        {
            switch (tone)
            {
                case 3:
                    femaleHand.GetComponentInChildren<SkinnedMeshRenderer>().material = femaleColor03;
                    maleHand.GetComponentInChildren<SkinnedMeshRenderer>().material = maleColor03;
                    break;
                case 2:
                    femaleHand.GetComponentInChildren<SkinnedMeshRenderer>().material = femaleColor02;
                    maleHand.GetComponentInChildren<SkinnedMeshRenderer>().material = maleColor02;
                    break;
                case 1:
                default:
                    femaleHand.GetComponentInChildren<SkinnedMeshRenderer>().material = femaleColor01;
                    maleHand.GetComponentInChildren<SkinnedMeshRenderer>().material = maleColor01;
                    break;
            }
        }

        public void SetGender(int gender)
        {
            bool female = gender == PlayerSave.GENDER_FEMALE;
            femaleHand.gameObject.SetActive(female);
            maleHand.gameObject.SetActive(!female);
            animator = GetComponentInChildren<Animator>(false);
        }
    }
}