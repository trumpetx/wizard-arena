using UnityEngine;

namespace WizardArena
{
    public class Attack0Behavior : StateMachineBehaviour
    {
        private const string ATTACK_IDX = "AttackIndex";
        public delegate void OnAttackHandler();
        public event OnAttackHandler OnAttack;

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            bool isArcher = animator.gameObject.name.Contains("Archer");
            // for archers: we just looped around from 3 (fire)
            bool archerAttack = isArcher && animator.GetInteger(ATTACK_IDX) == 0;
            if (OnAttack != null && (archerAttack || !isArcher))
            {
                OnAttack();
            }
        }

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // This lets us apply this behavior to all states and just  start "tracking" it when we enter attack.
            if (stateInfo.IsName("Attacking 0"))
            {
                animator.SetInteger(ATTACK_IDX, 0);
            }
            int attackIndex = animator.GetInteger(ATTACK_IDX);
            // Debug.Log(animator.gameObject.name + "/" + attackIndex);

            if (animator.gameObject.name.Contains("Archer"))
            {
                // Archers = arrow, aim, fire, repeat
                attackIndex = (attackIndex > 1) ? 0 : attackIndex + 1;
            }
            else
            {
                while (attackIndex == animator.GetInteger(ATTACK_IDX))
                {
                    attackIndex = Random.Range(0, 3);
                }
            }
            animator.SetInteger(ATTACK_IDX, attackIndex);
        }
    }
}