using UnityEngine;
using UnityEngine.Networking;

namespace WizardArena
{
    [RequireComponent(typeof(NetworkTransform))]
    public class Spawnable : NetworkBehaviour
    {
        public int itemId;
        public Sprite icon;
        public bool isUnique;
        public int price;
        public string title;
        public string description;
    }
}
