﻿using System;

namespace WizardArena
{
    public interface ILeaderboardController
    {   
        bool DownloadLeaderboards(Action<string, string, int> callback);
        void UpdateScore(string leaderboardName, int score);
    }
}