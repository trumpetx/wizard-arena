using System;
using Facepunch.Steamworks;
using UnityEngine;
using UnityEngine.VR;
using Zenject;

namespace WizardArena
{
    public class GameLifecycle : MonoBehaviour
    {
        public const int OCULUS = 1;
        public const int STEAM = 2;
        public const int NOPLATFORM = 0;
        public static GameLifecycle instance { get; private set; }
        private const uint APP_ID = 632490;
        public Client client { get; private set; }
        public string username { get; private set; }
        public int platform { get; private set; }
        public ILeaderboardController leaderboardController { get; private set; }
        public bool isDoneInitializing = false;

        private PlayerSaveManager playerSaveManager;

        [Inject]
        public void Inject(PlayerSaveManager playerSaveManager)
        {
            this.playerSaveManager = playerSaveManager;
        }

        private void Init()
        {
            username = "Wizard";
            Debug.Log("Starting Game Lifecycle");
            SetGraphicsLevel(playerSaveManager.playerSave.graphicsLevel);
            if (playerSaveManager.playerSave.lastPlatform == OCULUS)
            {
                isDoneInitializing = DoInit(InitOculus) || DoInit(InitSteam);
            }
            else
            {
                isDoneInitializing = DoInit(InitSteam) || DoInit(InitOculus);
            }
            playerSaveManager.playerSave.lastPlatform = platform;
            if (!isDoneInitializing)
            {
                Debug.LogError("Did not initialize to a game service.  Running in standalone mode.");
                isDoneInitializing = true;
            }
        }

        private bool DoInit(Func<bool> action)
        {
            try
            {
                return action.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            return false;
        }

        private bool InitSteam()
        {
            Config.ForUnity(Application.platform.ToString());
            client = new Client(APP_ID);
            if (client.IsValid)
            {
                Debug.Log("SteamId: " + client.SteamId);
                Debug.Log("Username: " + client.Username);
                username = client.Username;
                leaderboardController = new SteamLeaderboardController(this);
                platform = STEAM;
                return true;
            }
            else
            {
                Debug.LogError("Steam is not initialized");
                client.Dispose();
                client = null;
                return false;
            }
        }

        private bool InitOculus()
        {
            Oculus.Platform.Core.Initialize("1799924146701521");
            Oculus.Platform.Entitlements.IsUserEntitledToApplication().OnComplete((Oculus.Platform.Message callback) =>
            {
                if (!callback.IsError)
                {
                    Oculus.Platform.Users.GetLoggedInUser().OnComplete(m =>
                    {
                        if (!m.IsError)
                        {
                            Debug.Log("Oculus ID: " + m.GetUser().OculusID);
                            username = m.GetUser().OculusID;
                            leaderboardController = new OculusLeaderboardController(this);
                            platform = OCULUS;
                        }
                        else
                        {
                            Debug.LogError("Error getting Oculus UserId");
                        }
                    });
                    Oculus.Platform.Request.RunCallbacks();
                }
                else
                {
                    Debug.LogError("Oculus does not authorize this game");
                }
            });
            Oculus.Platform.Request.RunCallbacks();
            return leaderboardController != null;
        }

        public void SetGraphicsLevel(int graphicsLevel)
        {
            float scale = .5f * graphicsLevel;
            UnityEngine.XR.XRSettings.eyeTextureResolutionScale = scale; // .5, 1, or 1.5
            Debug.Log("Render Scale set to: " + scale);
        }

        private void Update()
        {
            if(client != null)
            {
                client.Update();
            }
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                Init();
                if (Application.isPlaying)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void OnApplicationQuit()
        {
            Debug.Log("Disposing Game Lifecycle");
            if (client != null)
            {
                client.Dispose();
                client = null;
            }
        }
    }
}