﻿using System;

namespace WizardArena
{ 
    public abstract class AbstractLeaderboardController : ILeaderboardController
    {
        public const string MOLE_MOUNTAIN = "Mole Mountain";
        public const string CLIFF_CASTLE = "Cliff Castle";
        public const string MAGIC_DIAL = "Magic Dial";
        public static readonly string[] LEADERBOARD_LIST = new string[] { MOLE_MOUNTAIN };

        public abstract bool DownloadLeaderboards(Action<string, string, int> callback);
        public abstract void UpdateScore(string leaderboardName, int score);
    }
}
