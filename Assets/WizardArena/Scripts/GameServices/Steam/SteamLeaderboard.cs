using Facepunch.Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WizardArena
{
    public class SteamLeaderboardController : AbstractLeaderboardController, ILeaderboardController
    {
        public static IDictionary<string, Leaderboard> ALL_LEADERBOARDS = new Dictionary<string, Leaderboard>();
        private GameLifecycle gameLifecycle;
        private Client client;
        
        public SteamLeaderboardController(GameLifecycle gameLifecycle)
        {
            this.gameLifecycle = gameLifecycle;
            client = gameLifecycle.client;
        }

        public override bool DownloadLeaderboards(Action<string, string, int> callback)
        {
            if(client == null || !client.IsValid)
            {
                Debug.LogError("Steam Client is not valid.  Unable to download leaderboards.");
                return false;
            }
            bool anyGoodLeaderboards = false;
            foreach (string leaderboardName in LEADERBOARD_LIST)
            {
                Leaderboard leaderBoard;
                if (ALL_LEADERBOARDS.ContainsKey(leaderboardName))
                {
                    leaderBoard = ALL_LEADERBOARDS[leaderboardName];
                }
                else
                {
                    leaderBoard = client.GetLeaderboard(leaderboardName, Client.LeaderboardSortMethod.Ascending, Client.LeaderboardDisplayType.Numeric);
                    ALL_LEADERBOARDS.Add(leaderboardName, leaderBoard);
                }
                if (leaderBoard != null && !leaderBoard.IsError)
                {
                    gameLifecycle.StartCoroutine(DownloadLeaderboard(leaderBoard, callback));
                    client.Update();
                }
                anyGoodLeaderboards |= leaderBoard != null && !leaderBoard.IsError;
            }
            return anyGoodLeaderboards;
        }

        private IEnumerator DownloadLeaderboard(Leaderboard leaderBoard, Action<string, string, int> callback)
        {
            while (!leaderBoard.IsValid)
            {
                client.Update();
                yield return new WaitForEndOfFrame();
            }
            Debug.Log("Downloading status of leaderboard: " + leaderBoard.Name);
            leaderBoard.FetchScores(Leaderboard.RequestType.Global, 0, 100, results =>
            {
                if (results != null && leaderBoard.Name == MOLE_MOUNTAIN) // TODO: multiple leaderboards on single GUI
                {
                    foreach (Leaderboard.Entry result in results)
                    {
                        callback(leaderBoard.Name, result.Name, result.Score);
                    }
                }
            }, error => {
                Debug.LogError("Error downloading leaderboard: " + leaderBoard.Name + "\n" + error);
            });
        }

        public override void UpdateScore(string leaderboardName, int score)
        {
            if (client == null || !client.IsValid)
            {
                Debug.LogError("Steam Client is not valid.  Unable to download leaderboards.");
                return;
            }
            bool tried = false;
            if (ALL_LEADERBOARDS.ContainsKey(leaderboardName))
            {
                Leaderboard leaderboard = ALL_LEADERBOARDS[leaderboardName];
                tried = leaderboard.AddScore(false, score, null, result => { }, error => {
                    Debug.LogError("Score not updated: " + leaderboard.Name + " => " + error);
                });
            }
            if (!tried)
            {
                Debug.LogError("Leaderboard not valid for update: " + leaderboardName + " => " + score);
            }
        }
    }
}
