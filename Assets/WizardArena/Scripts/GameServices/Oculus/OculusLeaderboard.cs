﻿using Oculus.Platform;
using Oculus.Platform.Models;
using System;
using UnityEngine;

namespace WizardArena
{
    public class OculusLeaderboardController : AbstractLeaderboardController, ILeaderboardController
    {
        private GameLifecycle gameLifecycle;

        public OculusLeaderboardController(GameLifecycle gameLifecycle)
        {
            this.gameLifecycle = gameLifecycle;
        }

        public override bool DownloadLeaderboards(Action<string, string, int> callback)
        {
            Leaderboards.GetEntries(MOLE_MOUNTAIN, 20, LeaderboardFilterType.None, LeaderboardStartAt.Top).OnComplete(m => ProcessCallback(MOLE_MOUNTAIN, m, callback));
            return true;
        }

        private void ProcessCallback(string leaderboard, Message<LeaderboardEntryList> msg, Action<string, string, int> callback)
        {
            if (!msg.IsError)
            {
                foreach (LeaderboardEntry entry in msg.Data)
                {
                    callback.Invoke(leaderboard, entry.User.OculusID, (int) entry.Score);
                }
                
                if (msg.Data.HasNextPage)
                {
                    Leaderboards.GetNextEntries(msg.Data).OnComplete(m => ProcessCallback(leaderboard, m, callback));
                }
            }
            else
            {
                Debug.Log("Error retrieving Oculus Leaderboard: " + leaderboard);
            }
        }
        
        public override void UpdateScore(string leaderboardName, int score)
        {
            Leaderboards.WriteEntry(leaderboardName, score);
        }
    }
}