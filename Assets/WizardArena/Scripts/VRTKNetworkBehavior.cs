using UnityEngine;
using UnityEngine.Networking;
using VRTK;

namespace WizardArena
{
    [RequireComponent(typeof(NetworkIdentity))]
    public abstract class VRTKNetworkBehavior : NetworkBehaviour
    {
        internal VRTK_SDKManager sdkManager;
        internal PlayerSave playerSave;

        internal virtual void Awake()
        {
            sdkManager = sdkManager ?? VRTK_SDKManager.instance;
            playerSave = playerSave ?? PlayerSaveManager.instance.playerSave;
            sdkManager.AddBehaviourToToggleOnLoadedSetupChange(this);
        }

        internal virtual void OnDestroy()
        {
            sdkManager.RemoveBehaviourToToggleOnLoadedSetupChange(this);
        }
        
        internal GameObject MainHandScriptController()
        {
            return playerSave.leftHanded ? sdkManager.scriptAliasLeftController : sdkManager.scriptAliasRightController;
        }

        internal GameObject OffhandScriptController()
        {
            return playerSave.leftHanded ? sdkManager.scriptAliasRightController : sdkManager.scriptAliasLeftController;
        }

        internal Transform MainHandController()
        {
            GameObject o = playerSave.leftHanded ? VRTK_DeviceFinder.GetControllerLeftHand() : VRTK_DeviceFinder.GetControllerRightHand();
            return o?.transform;
        }

        internal Transform OffhandController()
        {
            GameObject o = playerSave.leftHanded ? VRTK_DeviceFinder.GetControllerRightHand() : VRTK_DeviceFinder.GetControllerLeftHand();
            return o?.transform;
        }
    }
}