﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TLGFPowerBooks;

public class ExampleKeyboardController : MonoBehaviour {

	public PBook pBook;


	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.LeftControl)) {
			if (pBook.GetBookState () == PBook.BookState.CLOSED) {
				pBook.OpenBook ();
			}
			if (pBook.GetBookState () == PBook.BookState.OPEN) {
				pBook.CloseBook ();
			}
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			pBook.NextPage ();
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			pBook.PrevPage ();
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			pBook.GoToLastPage (40);
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			pBook.GoToFirstPage (40);
		}
	}
}
