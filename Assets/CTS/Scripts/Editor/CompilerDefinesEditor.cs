using UnityEditor;

namespace CTS
{
    /// <summary>
    /// Injects CTS_PRESENT define into project
    /// </summary>
    [InitializeOnLoad]
    public class CompilerDefinesEditor : Editor
    {
        private static readonly string m_editorSymbol = "CTS_PRESENT";
        static CompilerDefinesEditor()
        {
            var symbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            if (!symbols.Contains(m_editorSymbol))
            {
                symbols += ";" + m_editorSymbol;
                PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, symbols);
            }
        }
    }
}