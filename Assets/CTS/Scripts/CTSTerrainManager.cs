using System;
using System.Collections.Generic;
using UnityEngine;

namespace CTS
{
    /// <summary>
    /// Manages communication between weather, terrain profiles and terrain instances.
    /// </summary>
    public class CTSTerrainManager : CTSSingleton<CTSTerrainManager>
    {
        /// <summary>
        /// The shaders in the scene
        /// </summary>
        private List<CompleteTerrainShader> m_shaderList = new List<CompleteTerrainShader>();

        /// <summary>
        /// The last time the shader list was updated
        /// </summary>
        private DateTime m_lastShaderListUpdate = DateTime.MinValue;

        /// <summary>
        /// The controllers in the scene
        /// </summary>
        private List<CTSWeatherController> m_controllerList = new List<CTSWeatherController>();

        /// <summary>
        /// The last time the controller list was updated
        /// </summary>
        private DateTime m_lastControllerListUpdate = DateTime.MinValue;

        /// <summary>
        /// Make sure its only ever a singleton by stopping direct instantiation
        /// </summary>
        protected CTSTerrainManager()
        {
        }

        /// <summary>
        /// Grab all the shaders in the scene
        /// </summary>
        /// <param name="force">Force an update always</param>
        public void RegisterAllShaders(bool force = false)
        {
            if (Application.isPlaying)
            {
                if (force)
                {
                    m_shaderList.Clear();
                    m_shaderList.AddRange(GameObject.FindObjectsOfType<CompleteTerrainShader>());
                    m_lastShaderListUpdate = DateTime.Now;
                }
                else
                {
                    if (m_shaderList.Count == 0 || (DateTime.Now - m_lastShaderListUpdate).TotalSeconds > 30)
                    {
                        m_shaderList.AddRange(GameObject.FindObjectsOfType<CompleteTerrainShader>());
                        m_lastShaderListUpdate = DateTime.Now;
                    }
                }
            }
            else
            {
                if (force || m_shaderList.Count == 0 || (DateTime.Now - m_lastShaderListUpdate).TotalSeconds > 30)
                {
                    m_shaderList.Clear();
                    m_shaderList.AddRange(GameObject.FindObjectsOfType<CompleteTerrainShader>());
                    m_lastShaderListUpdate = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Grab all the controllers in the scene
        /// </summary>
        /// <param name="force">Force an update always</param>
        public void RegisterAllControllers(bool force = false)
        {
            if (Application.isPlaying)
            {
                if (force)
                {
                    m_controllerList.Clear();
                    m_controllerList.AddRange(GameObject.FindObjectsOfType<CTSWeatherController>());
                    m_lastControllerListUpdate = DateTime.Now;
                }
            }
            else
            {
                if (force || m_controllerList.Count == 0 || (DateTime.Now - m_lastControllerListUpdate).TotalSeconds > 30)
                {
                    m_controllerList.Clear();
                    m_controllerList.AddRange(GameObject.FindObjectsOfType<CTSWeatherController>());
                    m_lastControllerListUpdate = DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Return true if the profile is actively assigned to a terrain
        /// </summary>
        /// <param name="profile">The profile being checked</param>
        /// <returns>True if its been assiged to a terrain</returns>
        public bool ProfileIsActive(CTSProfile profile)
        {
            //Return rubbish if we have no usage
            if (profile == null)
            {
                return false;
            }

            //Make sure shaders are registered
            RegisterAllShaders();

            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null && shader.Profile != null)
                {
                    if (shader.Profile.GetInstanceID() == profile.GetInstanceID())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Broadcast a message to select this profile on all the CTS terrains in the scene
        /// </summary>
        /// <param name="profile">Profile being selected</param>
        public void BroadcastProfileSelect(CTSProfile profile)
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Broadcast the select
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                m_shaderList[idx].ShaderMode = CTSConstants.ShaderMode.DesignTime;
                m_shaderList[idx].Profile = profile;
                m_shaderList[idx].UpdateShader();
            }
            System.GC.Collect();
        }

        /// <summary>
        /// Broadcast a profile update to all the shaders using it in the scene
        /// </summary>
        /// <param name="profile">Profile being updated</param>
        public void BroadcastProfileUpdate(CTSProfile profile)
        {
            //Make sure shaders are registered
            RegisterAllShaders();

            //Also make sure weather is registered
            if (m_controllerList.Count == 0 )

            //Can not do this on a null profile
            if (profile == null)
            {
                Debug.LogWarning("Cannot update shader on null profile.");
                return;
            }

            //Broadcast the update
            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null && shader.Profile != null)
                {
                    if (shader.Profile.GetInstanceID() == profile.GetInstanceID())
                    {
                        shader.UpdateShader();
                    }                    
                }
            }
        }

        /// <summary>
        /// Broadcast a shader setup on the selected profile in the scene, otherwise all
        /// </summary>
        /// <param name="profile">Profile being updated, otherwise all</param>
        public void BroadcastShaderSetup(CTSProfile profile)
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Broadcast the setup
            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null && shader.Profile != null)
                {
                    if (profile == null)
                    {
                        shader.UpdateProfileFromTerrainForced();
                    }
                    else
                    {
                        //Find the first match and update it
                        if (shader.Profile.GetInstanceID() == profile.GetInstanceID())
                        {
                            shader.UpdateProfileFromTerrainForced();
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Broadcast create terrain normals
        /// </summary>
        public void BroadcastCreateGlobalNormals()
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Broadcast the setup
            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null)
                {
                    shader.ConstructTerrainNormals();
                }
            }
        }

        /// <summary>
        /// Broadcast set runtime mode ON
        /// </summary>
        public void PackageForBuild()
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Broadcast the setup
            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null)
                {
                    shader.ShaderMode = CTSConstants.ShaderMode.RunTime;
                }
            }
            System.GC.Collect();
        }

        /// <summary>
        /// Broadcast an albedo texture switch
        /// </summary>
        /// <param name="profile">Selected profile - null means all CTS terrains</param>
        /// <param name="texture">New texture</param>
        /// <param name="textureIdx">Index</param>
        /// <param name="tiling">Tiling</param>
        /// <returns></returns>
        public void BroadcastAlbedoTextureSwitch(CTSProfile profile, Texture2D texture, int textureIdx, float tiling)
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Do the texture switch
            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null && shader.Profile != null)
                {
                    if (profile == null)
                    {
                        shader.ReplaceAlbedoInTerrain(texture, textureIdx, tiling);
                    }
                    else
                    {
                        if (shader.Profile.GetInstanceID() == profile.GetInstanceID())
                        {
                            shader.ReplaceAlbedoInTerrain(texture, textureIdx, tiling);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Broadcast a normal texture switch
        /// </summary>
        /// <param name="profile">Selected profile - null means all CTS terrains</param>
        /// <param name="texture">New texture</param>
        /// <param name="textureIdx">Index</param>
        /// <param name="tiling">Tiling</param>
        public void BroadcastNormalTextureSwitch(CTSProfile profile, Texture2D texture, int textureIdx, float tiling)
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Do the texture switch
            CompleteTerrainShader shader = null;
            for (int idx = 0; idx < m_shaderList.Count; idx++)
            {
                shader = m_shaderList[idx];
                if (shader != null && shader.Profile != null)
                {
                    if (profile == null)
                    {
                        shader.ReplaceNormalInTerrain(texture, textureIdx, tiling);
                    }
                    else
                    {
                        if (shader.Profile.GetInstanceID() == profile.GetInstanceID())
                        {
                            shader.ReplaceNormalInTerrain(texture, textureIdx, tiling);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Broadcast a weather update
        /// </summary>
        /// <param name="manager">The manager with the update</param>
        public void BroadcastWeatherUpdate(CTSWeatherManager manager)
        {
            //Periodically update this list
            RegisterAllControllers();

            //And then broadcast to it
            for (int idx = 0; idx < m_controllerList.Count; idx++)
            {
                m_controllerList[idx].ProcessWeatherUpdate(manager);
            }
        }

        /// <summary>
        /// This will remove world seams from loaded terrains - should only be called once for entire terrrain set
        /// </summary>
        public void RemoveWorldSeams()
        {
            //Make sure shaders are registered
            RegisterAllShaders(true);

            //Broadcast the seam
            if (m_shaderList.Count > 0)
            {
                m_shaderList[0].RemoveWorldSeams();
            }
        }
    }
}