using UnityEngine;

namespace CTS
{
    public class CTSDemoController : MonoBehaviour
    {
        public CTSProfile unityProfile;
        public CTSProfile basicProfile;
        public CTSProfile advancedProfile;
        public CTSProfile tesselatedProfile;

        public void SelectUnity()
        {
            CTSTerrainManager.Instance.BroadcastProfileSelect(unityProfile);
        }

        public void SelectBasic()
        {
            CTSTerrainManager.Instance.BroadcastProfileSelect(basicProfile);
        }

        public void SelectAdvanced()
        {
            CTSTerrainManager.Instance.BroadcastProfileSelect(advancedProfile);
        }

        public void SelectTesselated()
        {
            CTSTerrainManager.Instance.BroadcastProfileSelect(tesselatedProfile);
        }
    }
}
