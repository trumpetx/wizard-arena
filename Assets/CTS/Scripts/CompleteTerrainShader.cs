using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace CTS
{
    /// <summary>
    /// The complete terrain shader editor manager
    /// </summary>
    [RequireComponent(typeof(Terrain))]
    [System.Serializable]
    public class CompleteTerrainShader : MonoBehaviour
    {
        /// <summary>
        /// The path where CTS is located
        /// </summary>
        public string m_ctsDirectory = "Assets/CTS/";

        /// <summary>
        /// The mode the shader is in
        /// </summary>
        public CTSConstants.ShaderMode ShaderMode
        {
            get { return m_shaderMode; }
            set
            {
                if (m_shaderMode != value)
                {
                    m_shaderMode = value;
                    if (m_shaderMode == CTSConstants.ShaderMode.DesignTime)
                    {
                        if (!string.IsNullOrEmpty(m_profilePath))
                        {
                            #if UNITY_EDITOR
                            Profile = AssetDatabase.LoadAssetAtPath<CTSProfile>(m_profilePath);
                            ReplaceTerrainTexturesFromProfile();
                            #endif
                        }
                    }
                    else
                    {
                        ReplaceTerrainTexturesFromProfile();
                        m_profile = null;
                    }
                    SetDirty(this);
                }
            }
        }
        [SerializeField]
        private CTSConstants.ShaderMode m_shaderMode = CTSConstants.ShaderMode.DesignTime;

        /// <summary>
        /// The terrain profile
        /// </summary>
        public CTSProfile Profile
        {
            get { return m_profile; }
            set
            {
                //Update our path
                m_ctsDirectory = GetCTSDirectory();

                //Make sure we have terrain
                if (m_terrain == null)
                {
                    m_terrain = transform.GetComponent<Terrain>();
                }

                //Then make sure we have terrain materials
                if (m_basicMaterial == null)
                {
                    CreateBasicMaterial();
                }

                if (m_advancedMaterial == null)
                {
                    CreateAdvancedMaterial();
                }

                if (m_tesselationMaterial == null)
                {
                    CreateTesselationMaterial();
                }

                if (m_profile == null)
                {
                    m_profilePath = "";
                    m_profile = value;
                    if (m_profile != null)
                    {
                        #if UNITY_EDITOR
                        m_profilePath = AssetDatabase.GetAssetPath(m_profile);
                        #endif
                        if (m_profile.TerrainTextures.Count == 0)
                        {
                            UpdateProfileFromTerrainForced();
                        }
                        else if (TerrainNeedsTextureUpdate())
                        {
                            ReplaceTerrainTexturesFromProfile();
                        }
                    }
                }
                else
                {
                    if (value == null)
                    {
                        m_profile = value;
                        m_profilePath = "";
                    }
                    else
                    {
                        if (m_profile.GetInstanceID() != value.GetInstanceID())
                        {
                            m_profile = value;
                            #if UNITY_EDITOR
                            m_profilePath = AssetDatabase.GetAssetPath(m_profile);
                            #endif
                        }
                        if (m_profile.TerrainTextures.Count == 0)
                        {
                            UpdateProfileFromTerrainForced();
                        }
                        else if (TerrainNeedsTextureUpdate())
                        {
                            ReplaceTerrainTexturesFromProfile();
                        }
                    }
                }
                SetDirty(this);
            }
        }
        [SerializeField]
        private CTSProfile m_profile;
        [SerializeField]
        private string m_profilePath;

        /// <summary>
        /// Global normal map
        /// </summary>
        public Texture2D GlobalNormalMap
        {
            get { return m_globalNormalMap; }
            set
            {
                if (value == null)
                {
                    if (m_globalNormalMap != null)
                    {
                        SetDirty(this, false);
                        m_globalNormalMap = value;
                    }
                }
                else
                {
                    if (m_globalNormalMap == null || m_globalNormalMap.GetInstanceID() != value.GetInstanceID())
                    {
                        SetDirty(this, false);
                        m_globalNormalMap = value;
                    }
                }
            }
        }
        [SerializeField]
        private Texture2D m_globalNormalMap;

        /// <summary>
        /// Basic shader 
        /// </summary>
        public Material BasicMaterial
        {
            set
            {
                m_basicMaterial = value;
                SetDirty(this);
            }
        }
        [SerializeField]
        private Material m_basicMaterial;

        /// <summary>
        /// Create the basic material
        /// </summary>
        private void CreateBasicMaterial()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Cannot create basic material without a terrain!");
                return;
            }

            //Debug.Log("Creating basic material");

            //Find shader and create material
            Shader shader = Shader.Find("CTS Terrain Shader Basic");

            Material material = new Material(shader);
            material.hideFlags = HideFlags.HideInInspector;

            #if UNITY_EDITOR
            Directory.CreateDirectory(m_ctsDirectory + "Terrains/");
            AssetDatabase.CreateAsset(material, string.Format("{0}Terrains/{1}_{2}_Base.mat", m_ctsDirectory, m_terrain.name, Mathf.Abs(m_terrain.GetInstanceID())));
            AssetDatabase.Refresh();
            #endif

            BasicMaterial = material;
        }

        /// <summary>
        /// Advanced shader 
        /// </summary>
        public Material AdvancedMaterial
        {
            set
            {
                m_advancedMaterial = value;
                SetDirty(this);
            }
        }
        [SerializeField]
        private Material m_advancedMaterial;

        /// <summary>
        /// Create the advanced material
        /// </summary>
        private void CreateAdvancedMaterial()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Cannot create advanced material without a terrain!");
                return;
            }

            //Debug.Log("Creating advanced material");

            //Find shader and create material
            Shader shader = Shader.Find("CTS Terrain Shader Advanced");

            Material material = new Material(shader);
            material.hideFlags = HideFlags.HideInInspector;

            #if UNITY_EDITOR
            Directory.CreateDirectory(m_ctsDirectory + "Terrains/");
            AssetDatabase.CreateAsset(material, string.Format("{0}Terrains/{1}_{2}_Advanced.mat", m_ctsDirectory, m_terrain.name, Mathf.Abs(m_terrain.GetInstanceID())));
            AssetDatabase.Refresh();
            #endif

            AdvancedMaterial = material;
        }

        /// <summary>
        /// Tesselation shader
        /// </summary>
        public Material TesselationMaterial
        {
            set
            {
                m_tesselationMaterial = value;
                SetDirty(this);
            }
        }
        [SerializeField]
        private Material m_tesselationMaterial;

        /// <summary>
        /// Create the tesselation material
        /// </summary>
        private void CreateTesselationMaterial()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Cannot create tesselation material without a terrain!");
                return;
            }

            //Debug.Log("Creating advanced tess material");

            //Find shader and create material
            Shader shader = Shader.Find("CTS Terrain Shader Advanced Tess");

            Material material = new Material(shader);
            material.hideFlags = HideFlags.HideInInspector;

            #if UNITY_EDITOR
            Directory.CreateDirectory(m_ctsDirectory + "Terrains/");
            AssetDatabase.CreateAsset(material, string.Format("{0}Terrains/{1}_{2}_Advanced_Tess.mat", m_ctsDirectory, m_terrain.name, Mathf.Abs(m_terrain.GetInstanceID())));
            AssetDatabase.Refresh();
            #endif

            TesselationMaterial = material;
        }

        /// <summary>
        /// Get and set the shader type
        /// </summary>
        public CTSConstants.ShaderType ShaderType
        {
            get { return m_activeShaderType; }
            set
            {
                if (m_material == null || value != m_activeShaderType)
                {
                    switch (value)
                    {
                        case CTSConstants.ShaderType.Unity:
                            ApplyUnityShader();
                            break;
                        case CTSConstants.ShaderType.Basic:
                            ApplyBasicShader();
                            break;
                        case CTSConstants.ShaderType.Advanced:
                            ApplyAdvancedShader();
                            break;
                        case CTSConstants.ShaderType.Tesselation:
                            ApplyTesselationShader();
                            break;
                    }
                    SetDirty(m_terrain);
                    SetDirty(this);
                }
            }
        }
        [SerializeField]
        private CTSConstants.ShaderType m_activeShaderType = CTSConstants.ShaderType.Unity;

        /// <summary>
        /// Current terrain
        /// </summary>
        private Terrain m_terrain;

        /// <summary>
        /// Current material
        /// </summary>
        private Material m_material;

        /// <summary>
        /// Flags used to decipher terrain changes
        /// </summary>
        [Flags]
        internal enum TerrainChangedFlags
        {
            NoChange = 0,
            Heightmap = 1 << 0,
            TreeInstances = 1 << 1,
            DelayedHeightmapUpdate = 1 << 2,
            FlushEverythingImmediately = 1 << 3,
            RemoveDirtyDetailsImmediately = 1 << 4,
            WillBeDestroyed = 1 << 8,
        }

        /// <summary>
        /// Detect terrain changes - use this to request updates if necessary
        /// </summary>
        /// <param name="flags"></param>
        void OnTerrainChanged(int flags)
        {
            //Debug.Log((TerrainChangedFlags)flags);
            if ((TerrainChangedFlags) flags == TerrainChangedFlags.Heightmap ||
                (TerrainChangedFlags) flags == TerrainChangedFlags.DelayedHeightmapUpdate ||
                (TerrainChangedFlags)flags == TerrainChangedFlags.FlushEverythingImmediately)
            {
                //m_shaderNormalsUpdateRequested = true;
            }
        }

        /// <summary>
        /// Awake
        /// </summary>
        void Awake()
        {
            m_terrain = transform.GetComponent<Terrain>();
        }

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            m_terrain = transform.GetComponent<Terrain>();
            CTSTerrainManager.Instance.RegisterAllShaders();
        }

        /// <summary>
        /// Enable
        /// </summary>
        void OnEnable()
        {
            m_terrain = transform.GetComponent<Terrain>();
        }

        /// <summary>
        /// Apply the basic shader
        /// </summary>
        private void ApplyBasicShader()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Unable to locate Terrain, apply unity shader cancelled.");
                return;
            }

            //Create it if it does not exist
            if (m_basicMaterial == null)
            {
                CreateBasicMaterial();
            }

            //Apply it
            m_activeShaderType = CTSConstants.ShaderType.Basic;
            m_terrain.materialType = Terrain.MaterialType.Custom;
            m_terrain.materialTemplate = m_basicMaterial;
            m_material = m_basicMaterial;
            SetDirty(m_terrain, false);
        }

        /// <summary>
        /// Apply the advanced shader
        /// </summary>
        private void ApplyAdvancedShader()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Unable to locate Terrain, apply unity shader cancelled.");
                return;
            }

            //Create it if it does not exist
            if (m_advancedMaterial == null)
            {
                CreateAdvancedMaterial();
            }

            //Apply it
            m_activeShaderType = CTSConstants.ShaderType.Advanced;
            m_terrain.materialType = Terrain.MaterialType.Custom;
            m_terrain.materialTemplate = m_advancedMaterial;
            m_material = m_advancedMaterial;
            SetDirty(m_terrain, false);
        }

        /// <summary>
        /// Apply the tesselation shader
        /// </summary>
        private void ApplyTesselationShader()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Unable to locate Terrain, apply unity shader cancelled.");
                return;
            }

            //Create it if it does not exist
            if (m_tesselationMaterial == null)
            {
                CreateTesselationMaterial();
            }

            //Apply it
            m_activeShaderType = CTSConstants.ShaderType.Tesselation;
            m_terrain.materialType = Terrain.MaterialType.Custom;
            m_terrain.materialTemplate = m_tesselationMaterial;
            m_material = m_tesselationMaterial;
            SetDirty(m_terrain, false);
        }

        /// <summary>
        /// Apply the unity shader
        /// </summary>
        private void ApplyUnityShader()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("Unable to locate Terrain, apply unity shader cancelled.");
                return;
            }

            //Apply it
            m_terrain.basemapDistance = 2000f;
            m_activeShaderType = CTSConstants.ShaderType.Unity;
            m_terrain.materialType = Terrain.MaterialType.BuiltInStandard;
            m_terrain.materialTemplate = null;
            m_material = null;
            SetDirty(m_terrain, false);
        }

        /// <summary>
        /// Returns the first asset that matches the file path and name passed. Will try
        /// full path first, then will try just the file name.
        /// </summary>
        /// <param name="fileNameOrPath">File name as standalone or fully pathed</param>
        /// <returns>Object or null if it was not found</returns>
        public static UnityEngine.Object GetAsset(string fileNameOrPath, Type assetType)
        {
            #if UNITY_EDITOR
            if (!string.IsNullOrEmpty(fileNameOrPath))
            {
                UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath(fileNameOrPath, assetType);
                if (obj != null)
                {
                    return obj;
                }
                else
                {
                    string path = GetAssetPath(Path.GetFileName(fileNameOrPath));
                    if (!string.IsNullOrEmpty(path))
                    {
                        return AssetDatabase.LoadAssetAtPath(path, assetType);
                    }
                }
            }
            #endif
            return null;
        }

        /// <summary>
        /// Get the asset path of the first thing that matches the name
        /// </summary>
        /// <param name="fileName">File name to search for</param>
        /// <returns></returns>
        public static string GetAssetPath(string fileName)
        {
            #if UNITY_EDITOR
            string fName = Path.GetFileNameWithoutExtension(fileName);
            string[] assets = AssetDatabase.FindAssets(fName, null);
            for (int idx = 0; idx < assets.Length; idx++)
            {
                string path = AssetDatabase.GUIDToAssetPath(assets[idx]);
                if (Path.GetFileName(path) == fileName)
                {
                    return path;
                }
            }
            #endif
            return "";
        }

        /// <summary>
        /// Update the shader settings into the terrain
        /// </summary>
        public void UpdateShader()
        {
            //Check for valid profile
            if (m_profile == null)
            {
                Debug.LogError("CTS is missing profile, cannot update.");
                return;
            }

            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("CTS is missing terrain, cannot update.");
                return;
            }

            //Check for valid shader types and material
            if (m_material == null || m_activeShaderType != m_profile.m_shaderType)
            {
                ShaderType = m_profile.m_shaderType;
                if (m_material == null && m_activeShaderType != CTSConstants.ShaderType.Unity)
                {
                    Debug.LogError("CTS is missing material, cannot update.");
                    return;
                }
            }

            //Update basemap distance
            if (m_terrain.basemapDistance != m_profile.m_globalBasemapDistance && m_activeShaderType != CTSConstants.ShaderType.Unity)
            {
                m_terrain.basemapDistance = m_profile.m_globalBasemapDistance;
                SetDirty(m_terrain, false);
            }

            //Exit if unity shader
            if (m_activeShaderType == CTSConstants.ShaderType.Unity)
            {
                return;
            }

            //Set material dirty
            //SetDirty(m_material, false);

            //Global settings
            m_material.SetFloat("_UV_Mix_Power", m_profile.m_globalUvMixPower);
            m_material.SetFloat("_UV_Mix_Start_Distance", m_profile.m_globalUvMixStartDistance + UnityEngine.Random.Range(0.001f, 0.003f));
            m_material.SetFloat("_Global_Normalmap_Power", m_profile.m_globalNormalPower);
            m_material.SetFloat("_Perlin_Normal_Tiling_Close", m_profile.m_globalDetailNormalCloseTiling);
            m_material.SetFloat("_Perlin_Normal_Tiling_Far", m_profile.m_globalDetailNormalFarTiling);
            m_material.SetFloat("_Perlin_Normal_Power", m_profile.m_globalDetailNormalFarPower);
            m_material.SetFloat("_Perlin_Normal_Power_Close", m_profile.m_globalDetailNormalClosePower);
            m_material.SetFloat("_Terrain_Smoothness", m_profile.m_globalTerrainSmoothness);
            m_material.SetFloat("_Terrain_Specular", m_profile.m_globalTerrainSpecular);
            m_material.SetFloat("_TessValue", m_profile.m_globalTesselationPower);
            m_material.SetFloat("_TessMin", m_profile.m_globalTesselationMinDistance);
            m_material.SetFloat("_TessMax", m_profile.m_globalTesselationMaxDistance);
            m_material.SetFloat("_TessPhongStrength", m_profile.m_globalTesselationPhongStrength);
            m_material.SetFloat("_TessDistance", m_profile.m_globalTesselationMaxDistance);
            m_material.SetInt("_Ambient_Occlusion_Type", (int)m_profile.m_globalAOType);

            if (m_profile.m_globalAOType == CTSConstants.AOType.None)
            {
                m_material.DisableKeyword("_Use_AO_ON");
                m_material.DisableKeyword("_USE_AO_TEXTURE_ON");
                m_material.SetInt("_Use_AO", 0);
                m_material.SetInt("_Use_AO_Texture", 0);
                m_material.SetFloat("_Ambient_Occlusion_Power", 0f);
            }
            else if (m_profile.m_globalAOType == CTSConstants.AOType.NormalMapBased)
            {
                m_material.DisableKeyword("_USE_AO_TEXTURE_ON");
                m_material.SetInt("_Use_AO_Texture", 0);
                if (m_profile.m_globalAOPower > 0)
                {
                    m_material.EnableKeyword("_USE_AO_ON");
                    m_material.SetInt("_Use_AO", 1);
                    m_material.SetFloat("_Ambient_Occlusion_Power", m_profile.m_globalAOPower);
                }
                else
                {
                    m_material.DisableKeyword("_USE_AO_ON");
                    m_material.SetInt("_Use_AO", 0);
                    m_material.SetFloat("_Ambient_Occlusion_Power", 0f);
                }
            }
            else
            {
                if (m_profile.m_globalAOPower > 0)
                {
                    m_material.EnableKeyword("_USE_AO_ON");
                    m_material.EnableKeyword("_USE_AO_TEXTURE_ON");
                    m_material.SetInt("_Use_AO", 1);
                    m_material.SetInt("_Use_AO_Texture", 1);
                    m_material.SetFloat("_Ambient_Occlusion_Power", m_profile.m_globalAOPower);
                }
                else
                {
                    m_material.DisableKeyword("_USE_AO_ON");
                    m_material.DisableKeyword("_USE_AO_TEXTURE_ON");
                    m_material.SetInt("_Use_AO", 0);
                    m_material.SetInt("_Use_AO_Texture", 0);
                    m_material.SetFloat("_Ambient_Occlusion_Power", 0f);
                }
            }

//            Debug.Log("Keywords:");
//            for (int i = 0; i < m_material.shaderKeywords.Length; i++)
//            {
//                Debug.Log(string.Format("{0} - {1}", m_material.shaderKeywords[i], m_material.IsKeywordEnabled(m_material.shaderKeywords[i])));
//            }


            //Geological settings
            m_material.SetFloat("_Geological_Map_Offset_Close", m_profile.m_geoMapCloseOffset);
            m_material.SetFloat("_Geological_Map_Close_Power", m_profile.m_geoMapClosePower);
            m_material.SetFloat("_Geological_Tiling_Close", m_profile.m_geoMapTilingClose);
            m_material.SetFloat("_Geological_Map_Offset_Far", m_profile.m_geoMapFarOffset);
            m_material.SetFloat("_Geological_Map_Far_Power", m_profile.m_geoMapFarPower);
            m_material.SetFloat("_Geological_Tiling_Far", m_profile.m_geoMapTilingFar);

            //Snow settings
            m_material.SetFloat("_Snow_Amount", m_profile.m_snowAmount);
            m_material.SetFloat("_Snow_Maximum_Angle", m_profile.m_snowMaxAngle);
            m_material.SetFloat("_Snow_Maximum_Angle_Hardness", m_profile.m_snowMaxAngleHardness);
            m_material.SetFloat("_Snow_Min_Height", m_profile.m_snowMinHeight);
            m_material.SetFloat("_Snow_Min_Height_Blending", m_profile.m_snowMinHeightBlending);
            m_material.SetFloat("_Snow_Noise_Power", m_profile.m_snowNoisePower);
            m_material.SetFloat("_Snow_Noise_Tiling", m_profile.m_snowNoiseTiling);
            m_material.SetFloat("_Snow_Normal_Scale", m_profile.m_snowNormalScale);
            m_material.SetFloat("_Snow_Perlin_Power", m_profile.m_snowDetailPower);
            m_material.SetFloat("_Snow_Tiling", m_profile.m_snowTilingClose);
            m_material.SetFloat("_Snow_Tiling_Far_Multiplier", m_profile.m_snowTilingFar);
            m_material.SetFloat("_Snow_Brightness", m_profile.m_snowBrightness);
            m_material.SetFloat("_Snow_Blend_Normal", m_profile.m_snowBlendNormal);
            m_material.SetFloat("_Snow_Smoothness", m_profile.m_snowSmoothness);
            m_material.SetFloat("_Snow_Specular", m_profile.m_snowSpecular);
            m_material.SetFloat("_Snow_Heightblend_Close", m_profile.m_snowHeightmapBlendClose);
            m_material.SetFloat("_Snow_Heightblend_Far", m_profile.m_snowHeightmapBlendFar);
            m_material.SetFloat("_Snow_Height_Contrast", m_profile.m_snowHeightmapContrast);
            m_material.SetFloat("_Snow_Heightmap_Depth", m_profile.m_snowHeightmapDepth);
            m_material.SetFloat("_Snow_Heightmap_MinHeight", m_profile.m_snowHeightmapMinValue);
            m_material.SetFloat("_Snow_Heightmap_MaxHeight", m_profile.m_snowHeightmapMaxValue);
            m_material.SetFloat("_Snow_Ambient_Occlusion_Power", m_profile.m_snowAOStrength);
            m_material.SetFloat("_Snow_Tesselation_Depth", m_profile.m_snowTesselationDepth);
            m_material.SetVector("_Snow_Color", new Vector4(m_profile.m_snowTint.r * m_profile.m_snowBrightness, m_profile.m_snowTint.g * m_profile.m_snowBrightness, m_profile.m_snowTint.b * m_profile.m_snowBrightness, m_profile.m_snowSmoothness));

            //Texture settings

            //Make sure the textures have been 
            ReAttachTextures();

            //Global normal map
            if (m_globalNormalMap != null)
            {
                m_material.SetTexture("_Global_Normal_Map", m_globalNormalMap);
            }

            //Global geo map
            if (m_profile.GeoAlbedo != null)
            {
                m_material.SetTexture("_Texture_Geological_Map", m_profile.GeoAlbedo);
            }

            //Albedo's
            if (m_profile.AlbedosTextureArray != null)
            {
                m_material.SetTexture("_Texture_Array_Albedo", m_profile.AlbedosTextureArray);
            }

            //Normals
            if (m_profile.NormalsTextureArray != null)
            {
                m_material.SetTexture("_Texture_Array_Normal", m_profile.NormalsTextureArray);
            }

            //AO's
            if (m_profile.AOTextureArray != null)
            {
                if (m_profile.m_globalAOType != CTSConstants.AOType.TextureBased)
                {
                    m_material.SetTexture("_Texture_Array_Emission_AO", null);
                }
                else
                {
                    if (m_profile.m_globalAOPower > 0)
                    {
                        m_material.SetTexture("_Texture_Array_Emission_AO", m_profile.AOTextureArray);
                    }
                    else
                    {
                        m_material.SetTexture("_Texture_Array_Emission_AO", null);
                    }
                }
            }

            //Splats
            int numSplats = m_terrain.terrainData.alphamapTextures.Length;
            if (numSplats > 0)
            {
                m_material.SetTexture("_Texture_Splat_1", m_terrain.terrainData.alphamapTextures[0]);
            }
            else
            {
                m_material.SetTexture("_Texture_Splat_1", null);
            }

            if (numSplats > 1)
            {
                m_material.SetTexture("_Texture_Splat_2", m_terrain.terrainData.alphamapTextures[1]);
            }
            else
            {
                m_material.SetTexture("_Texture_Splat_2", null);
            }

            if (numSplats > 2)
            {
                m_material.SetTexture("_Texture_Splat_3", m_terrain.terrainData.alphamapTextures[2]);
            }
            else
            {
                m_material.SetTexture("_Texture_Splat_3", null);
            }

            if (numSplats > 3)
            {
                m_material.SetTexture("_Texture_Splat_4", m_terrain.terrainData.alphamapTextures[3]);
            }
            else
            {
                m_material.SetTexture("_Texture_Splat_4", null);
            }

            //Push per texture based settings
            CTSTerrainTextureDetails td;
            int actualIdx = 0;
            for (int i = 0; i < m_profile.TerrainTextures.Count; i++)
            {
                td = m_profile.TerrainTextures[i];
                actualIdx = i + 1;
                m_material.SetInt(string.Format("_Texture_{0}_Albedo_Index", actualIdx), td.m_albedoIdx);
                m_material.SetInt(string.Format("_Texture_{0}_Normal_Index", actualIdx), td.m_normalIdx);
                m_material.SetInt(string.Format("_Texture_{0}_Height_Index", actualIdx), td.m_heightIdx);
                m_material.SetInt(string.Format("_Texture_{0}_Emission_AO_Index", actualIdx), td.m_aoIdx);

                m_material.SetFloat(string.Format("_Texture_{0}_Tiling", actualIdx), td.m_albedoTilingClose);
                m_material.SetFloat(string.Format("_Texture_{0}_Far_Multiplier", actualIdx), td.m_albedoTilingFar);
                m_material.SetFloat(string.Format("_Texture_{0}_Perlin_Power", actualIdx), td.m_detailPower);
                m_material.SetFloat(string.Format("_Texture_{0}_Snow_Reduction", actualIdx), td.m_snowReductionPower);
                m_material.SetFloat(string.Format("_Texture_{0}_Geological_Power", actualIdx), td.m_geologicalPower);

                m_material.SetFloat(string.Format("_Texture_{0}_Heightmap_Depth", actualIdx), td.m_heightDepth);
                m_material.SetFloat(string.Format("_Texture_{0}_Height_Contrast", actualIdx), td.m_heightContrast);
                m_material.SetFloat(string.Format("_Texture_{0}_Heightblend_Close", actualIdx), td.m_heightBlendClose);
                m_material.SetFloat(string.Format("_Texture_{0}_Heightblend_Far", actualIdx), td.m_heightBlendFar);
                m_material.SetFloat(string.Format("_Texture_{0}_Tesselation_Depth", actualIdx), td.m_heightTesselationDepth);
                m_material.SetFloat(string.Format("_Texture_{0}_Heightmap_MinHeight", actualIdx), td.m_heightMin);
                m_material.SetFloat(string.Format("_Texture_{0}_Heightmap_MaxHeight", actualIdx), td.m_heightMax);

                m_material.SetFloat(string.Format("_Texture_{0}_Emission_Power", actualIdx), td.m_emissionStrength);
                m_material.SetFloat(string.Format("_Texture_{0}_AO_Power", actualIdx), td.m_aoPower);
                m_material.SetFloat(string.Format("_Texture_{0}_Normal_Power", actualIdx), td.m_normalStrength);
                m_material.SetFloat(string.Format("_Texture_{0}_Triplanar", actualIdx), td.m_triplanar ? 1f : 0f);

                m_material.SetVector(string.Format("_Texture_{0}_Color", actualIdx), new Vector4(td.m_tint.r * td.m_tintBrightness, td.m_tint.g * td.m_tintBrightness, td.m_tint.b * td.m_tintBrightness, td.m_smoothness));
            }

            //And fill out rest as well
            for (int i = m_profile.TerrainTextures.Count; i < 16; i++)
            {
                actualIdx = i + 1;
                m_material.SetInt(string.Format("_Texture_{0}_Albedo_Index", actualIdx), -1);
                m_material.SetInt(string.Format("_Texture_{0}_Normal_Index", actualIdx), -1);
                m_material.SetInt(string.Format("_Texture_{0}_Height_Index", actualIdx), -1);
                m_material.SetInt(string.Format("_Texture_{0}_Emission_Index", actualIdx), -1);
                m_material.SetInt(string.Format("_Texture_{0}_AO_Index", actualIdx), -1);
            }

            //Push other one off texture related settings
            m_material.SetInt("_Texture_Perlin_Normal_Index", m_profile.m_globalDetailNormalMapIdx);
            m_material.SetInt("_Texture_Snow_Index", m_profile.m_snowAlbedoTextureIdx);
            m_material.SetInt("_Texture_Snow_Normal_Index", m_profile.m_snowNormalTextureIdx);
            m_material.SetInt("_Texture_Snow_Height_Index", m_profile.m_snowHeightTextureIdx);
            m_material.SetInt("_Texture_Snow_AO_Index", m_profile.m_snowAOTextureIdx);
            m_material.SetInt("_Texture_Snow_Emission_Index", m_profile.m_snowEmissionTextureIdx);
            m_material.SetInt("_Texture_Snow_Noise_Index", m_profile.m_snowNoiseTextureIdx);

            //Mark the material as dirty
            //SetDirty(m_material, false);
        }

        /// <summary>
        /// Re-attach the textures to the current material if they exist
        /// </summary>
        public void ReAttachTextures()
        {
            //Check we have a valid profile
            if (m_profile == null)
            {
                Debug.LogWarning("Cannot re-attach arrays, missing profile");
                return;
            }

            #if UNITY_EDITOR
            string profilePath = AssetDatabase.GetAssetPath(m_profile);
            profilePath = profilePath.Replace(".asset", "");

            //See if we can re-attach the global normal
            if (m_globalNormalMap == null)
            {
                //Debug.Log("Reattaching global normals");
                string normalsPath = AssetDatabase.GetAssetPath(m_material);
                normalsPath = normalsPath.Replace("_Advanced.mat", "_Normals.png");
                normalsPath = normalsPath.Replace("_Advanced_Tess.mat", "_Normals.png");
                normalsPath = normalsPath.Replace("_Base.mat", "_Normals.png");
                GlobalNormalMap = AssetDatabase.LoadAssetAtPath<Texture2D>(normalsPath);
            }

            //See if we can attach albedos
            if (m_profile.AlbedosTextureArray == null)
            {
                //Debug.Log("Reattaching albedos");
                m_profile.AlbedosTextureArray = AssetDatabase.LoadAssetAtPath<Texture2DArray>(profilePath + "_Albedos.asset");
            }

            //See if we can attach normals
            if (m_profile.NormalsTextureArray == null)
            {
                //Debug.Log("Reattaching normals");
                m_profile.NormalsTextureArray = AssetDatabase.LoadAssetAtPath<Texture2DArray>(profilePath + "_Normals.asset");
            }

            //See if we can attach aos
            if (m_profile.AOTextureArray == null && m_profile.m_globalAOType == CTSConstants.AOType.TextureBased)
            {
                //Debug.Log("Reattaching aos");
                m_profile.AOTextureArray = AssetDatabase.LoadAssetAtPath<Texture2DArray>(profilePath + "_AOs.asset");
            }
            #endif
        }

        /// <summary>
        /// Regenerate the arrays if necessary
        /// </summary>
        public void RegenerateArraysIfNecessary()
        {
            m_profile.RegenerateArraysIfNecessary();
        }

        /// <summary>
        /// Update profile settings from terrain and refresh shader
        /// </summary>
        public void UpdateProfileFromTerrainForced()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogError("CTS is missing terrain, cannot update.");
                return;
            }

            m_profile.UpdateSettingsFromTerrain(m_terrain, true);
            UpdateShader();
        }

        /// <summary>
        /// Check to see if the profile needs a texture update
        /// </summary>
        /// <returns>True if it does, false otherwise</returns>
        public bool ProfileNeedsTextureUpdate()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogWarning("No terrain , unable to check if needs texture update");
                return false;
            }

            if (m_profile == null)
            {
                Debug.LogWarning("No profile, unable to check if needs texture update");
                return false;
            }

            if (m_profile.TerrainTextures.Count == 0)
            {
                return false;
            }

            //Now check the terrain splats against the profile splats
            SplatPrototype[] splats = m_terrain.terrainData.splatPrototypes;
            if (m_profile.TerrainTextures.Count != splats.Length)
            {
                return true;
            }

            //Check each individual texture
            SplatPrototype splatProto;
            CTSTerrainTextureDetails terrainDetail;
            for (int idx = 0; idx < splats.Length; idx++)
            {
                terrainDetail = m_profile.TerrainTextures[idx];
                splatProto = splats[idx];
                if (terrainDetail.Albedo == null)
                {
                    if (splatProto.texture != null)
                    {
                        return true;
                    }
                }
                else
                {
                    if (splatProto.texture == null)
                    {
                        return true;
                    }
                    else
                    {
                        if (terrainDetail.Albedo.GetInstanceID() != splatProto.texture.GetInstanceID())
                        {
                            return true;
                        }
                    }
                }

                if (terrainDetail.Normal == null)
                {
                    if (splatProto.normalMap != null)
                    {
                        return true;
                    }
                }
                else
                {
                    if (splatProto.normalMap == null)
                    {
                        return true;
                    }
                    else
                    {
                        if (terrainDetail.Normal.GetInstanceID() != splatProto.normalMap.GetInstanceID())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Check to see if the terrain needs a texture update
        /// </summary>
        /// <returns></returns>
        public bool TerrainNeedsTextureUpdate()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogWarning("No terrain , unable to check if needs texture update");
                return false;
            }

            if (m_profile == null)
            {
                Debug.LogWarning("No profile, unable to check if needs texture update");
                return false;
            }

            if (m_profile.TerrainTextures.Count == 0)
            {
                return false;
            }

            //Now check the terrain splats against the profile splats
            SplatPrototype[] splats = m_terrain.terrainData.splatPrototypes;
            if (m_profile.TerrainTextures.Count != splats.Length)
            {
                return true;
            }

            //Check each individual texture
            SplatPrototype splatProto;
            CTSTerrainTextureDetails terrainDetail;
            for (int idx = 0; idx < splats.Length; idx++)
            {
                terrainDetail = m_profile.TerrainTextures[idx];
                splatProto = splats[idx];
                if (terrainDetail.Albedo == null)
                {
                    if (splatProto.texture != null)
                    {
                        return true;
                    }
                }
                else
                {
                    if (splatProto.texture == null)
                    {
                        return true;
                    }
                    else
                    {
                        if (terrainDetail.Albedo.GetInstanceID() != splatProto.texture.GetInstanceID())
                        {
                            return true;
                        }
                        if (terrainDetail.m_albedoTilingClose != splatProto.tileSize.x)
                        {
                            return true;
                        }
                    }
                }

                if (terrainDetail.Normal == null)
                {
                    if (splatProto.normalMap != null)
                    {
                        return true;
                    }
                }
                else
                {
                    if (splatProto.normalMap == null)
                    {
                        return true;
                    }
                    else
                    {
                        if (terrainDetail.Normal.GetInstanceID() != splatProto.normalMap.GetInstanceID())
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Replace the existing texture in the terrain with a new one
        /// </summary>
        /// <param name="texture">New texture</param>
        /// <param name="textureIdx">Index of texture</param>
        /// <param name="tiling">Tiling</param>
        public void ReplaceAlbedoInTerrain(Texture2D texture, int textureIdx, float tiling)
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }

            if (m_terrain != null)
            {
                SplatPrototype[] splats = m_terrain.terrainData.splatPrototypes;
                if (textureIdx >= 0 && textureIdx < splats.Length)
                {
                    splats[textureIdx].texture = texture;
                    splats[textureIdx].tileSize = new Vector2(tiling, tiling);
                    m_terrain.terrainData.splatPrototypes = splats;
                    m_terrain.Flush();
                    SetDirty(m_terrain, false);
                }
                else
                {
                    Debug.LogWarning("Invalid texture index in replace albedo");
                }
            }
        }

        /// <summary>
        /// Replace the existing normal texture in the terrain with a new one
        /// </summary>
        /// <param name="texture">New texture</param>
        /// <param name="textureIdx">Index of texture</param>
        /// <param name="tiling">Tiling</param>
        public void ReplaceNormalInTerrain(Texture2D texture, int textureIdx, float tiling)
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }

            if (m_terrain != null)
            {
                SplatPrototype[] splats = m_terrain.terrainData.splatPrototypes;
                if (textureIdx >= 0 && textureIdx < splats.Length)
                {
                    splats[textureIdx].normalMap = texture;
                    splats[textureIdx].tileSize = new Vector2(tiling, tiling);
                    m_terrain.terrainData.splatPrototypes = splats;
                    m_terrain.Flush();
                    SetDirty(m_terrain, false);
                }
                else
                {
                    Debug.LogWarning("Invalid texture index in replace normal!");
                }
            }
        }

        /// <summary>
        /// Construct normal from terrain
        /// </summary>
        public void ConstructTerrainNormals()
        {
            //Make sure we have terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                Debug.LogWarning("Could not make terrain normal, as terrain object not set.");
                return;
            }

            #if UNITY_EDITOR
            EditorUtility.DisplayProgressBar("Baking Textures", "Generating terrain normals : " + m_terrain.name, 0f);
            #endif

            Color nrmColor;
            Vector3 normal;
            int width = m_terrain.terrainData.heightmapWidth * 2;
            int height = m_terrain.terrainData.heightmapHeight * 2;
            #if UNITY_EDITOR
            float dimensions = width * height;
            #endif
            Texture2D nrmTex = new Texture2D(width, height, TextureFormat.RGBA32, false);
            nrmTex.name = m_terrain.name + " Nrm";
            nrmTex.wrapMode = TextureWrapMode.Clamp;
            nrmTex.filterMode = FilterMode.Bilinear;
            nrmTex.anisoLevel = 8;

            for (int x = 0; x < width; x++)
            {
                #if UNITY_EDITOR
                if (x % 250 == 0)
                {
                    EditorUtility.DisplayProgressBar("Baking Textures", "Ingesting global normals : " + m_terrain.name + "..", (float)(x * width) / dimensions);
                }
                #endif

                for (int z = 0; z < height; z++)
                {
                    normal = m_terrain.terrainData.GetInterpolatedNormal((float)x / (float)width, (float)z / (float)height);
                    nrmColor = new Color();
                    nrmColor.r = 0f;
                    nrmColor.g = (normal.y * 2f) - 1f; //0..1
                    nrmColor.b = 0f;
                    nrmColor.a = (normal.x * 2f) - 1f; //0..1  
                    nrmTex.SetPixel(x, z, nrmColor);
                }
            }
            nrmTex.Apply();

            #if UNITY_EDITOR
            EditorUtility.DisplayProgressBar("Baking Textures", "Encoding global normals : " + m_terrain.name + "..", 0f);

            //In case this is exectuted before the materil has been created
            if (m_material == null)
            {
                ShaderType = m_activeShaderType;
            }

            //Save it
            string normalsPath = AssetDatabase.GetAssetPath(m_material);
            if (string.IsNullOrEmpty(normalsPath))
            {
                normalsPath = string.Format("{0}Terrains/{1}", m_ctsDirectory, m_material.name);
                normalsPath = normalsPath.Replace("_Advanced", "_Normals.png");
                normalsPath = normalsPath.Replace("_Advanced_Tess", "_Normals.png");
                normalsPath = normalsPath.Replace("_Base", "_Normals.png");
            }
            else
            {
                normalsPath = normalsPath.Replace("_Advanced.mat", "_Normals.png");
                normalsPath = normalsPath.Replace("_Advanced_Tess.mat", "_Normals.png");
                normalsPath = normalsPath.Replace("_Base.mat", "_Normals.png");
            }

            Directory.CreateDirectory(m_ctsDirectory + "Terrains/");
            byte[] content = nrmTex.EncodeToPNG();
            File.WriteAllBytes(normalsPath, content);
            AssetDatabase.Refresh();

            //Import it back in as a normal map
            var importer = AssetImporter.GetAtPath(normalsPath) as TextureImporter;
            if (importer != null)
            {
                importer.isReadable = true;
                importer.textureType = TextureImporterType.NormalMap;
                importer.textureCompression = TextureImporterCompression.Compressed;
                importer.convertToNormalmap = true;
                importer.heightmapScale = 0.1f;
                importer.anisoLevel = 8;
                importer.filterMode = FilterMode.Bilinear;
                importer.mipmapEnabled = true;
                importer.mipmapFilter = TextureImporterMipFilter.BoxFilter;
                importer.normalmapFilter = TextureImporterNormalFilter.Standard;
                importer.wrapMode = TextureWrapMode.Clamp;
                AssetDatabase.ImportAsset(normalsPath, ImportAssetOptions.ForceUpdate);
                AssetDatabase.Refresh();
            }

            //Load & assign it
            nrmTex = AssetDatabase.LoadAssetAtPath<Texture2D>(normalsPath);
            EditorUtility.ClearProgressBar();
            #endif

            GlobalNormalMap = nrmTex;
        }

        /// <summary>
        /// Replace the terrain textures dependent on the thing being done
        /// </summary>
        public void ReplaceTerrainTexturesFromProfile()
        {
            //Exit if no terrain
            if (m_terrain == null)
            {
                m_terrain = transform.GetComponent<Terrain>();
            }
            if (m_terrain == null)
            {
                return;
            }

            //Exit if no profile
            if (m_profile == null)
            {
                Debug.LogWarning("No profile, unable to replace terrain textures");
                return;
            }

            //Exit if no textures
            if (m_profile.TerrainTextures.Count == 0)
            {
                Debug.LogWarning("No profile textures, unable to replace terrain textures");
                return;
            }

            //Create new splats
            SplatPrototype[] splats = new SplatPrototype[m_profile.TerrainTextures.Count];

            //Put the placeholder textures in if we are runtime and have the texture replacements
            if (m_shaderMode == CTSConstants.ShaderMode.RunTime && 
                splats.Length == m_profile.ReplacementTerrainAlbedos.Count && 
                splats.Length == m_profile.ReplacementTerrainNormals.Count)
            {
                for (int idx = 0; idx < splats.Length; idx++)
                {
                    splats[idx] = new SplatPrototype();
                    splats[idx].texture = m_profile.ReplacementTerrainAlbedos[idx];
                    splats[idx].normalMap = m_profile.ReplacementTerrainNormals[idx];
                    splats[idx].tileSize = new Vector2(m_profile.TerrainTextures[idx].m_albedoTilingClose, m_profile.TerrainTextures[idx].m_albedoTilingClose);
                }
            }
            //Otherwise put the proper textures in
            else
            {
                for (int idx = 0; idx < splats.Length; idx++)
                {
                    splats[idx] = new SplatPrototype();
                    splats[idx].texture = m_profile.TerrainTextures[idx].Albedo;
                    splats[idx].normalMap = m_profile.TerrainTextures[idx].Normal;
                    splats[idx].tileSize = new Vector2(m_profile.TerrainTextures[idx].m_albedoTilingClose, m_profile.TerrainTextures[idx].m_albedoTilingClose);
                }
            }

            //Push splats back to the terrain
            m_terrain.terrainData.splatPrototypes = splats;
            m_terrain.Flush();

            //Mark terrain as dirty
            SetDirty(m_terrain);
        }

        /// <summary>
        /// Return the CTS directory in the project
        /// </summary>
        /// <returns>If in editor it returns the full cts directory, if in build, returns assets directory.</returns>
        public static string GetCTSDirectory()
        {
            #if UNITY_EDITOR
            string[] assets = AssetDatabase.FindAssets("CTS_ReadMe", null);
            for (int idx = 0; idx < assets.Length; idx++)
            {
                string path = AssetDatabase.GUIDToAssetPath(assets[idx]);
                if (Path.GetFileName(path) == "CTS_ReadMe.txt")
                {
                    return Path.GetDirectoryName(path) + "/";
                }
            }
            #endif
            return "Assets/CTS/";
        }

        /// <summary>
        /// Marks the object and its scene if possible as dirty. NOTE: This will only work
        /// in the editor. It compiles out of builds.
        /// </summary>
        /// <param name="obj">Object to mark</param>
        /// <param name="recordUndo">Tell Unity we also want and undo option</param>
        /// <param name="isPlayingAllowed">Allow dirty when application is playing as well</param>
        public static void SetDirty(UnityEngine.Object obj, bool recordUndo = true, bool isPlayingAllowed = true)
        {
            #if UNITY_EDITOR

            //Check to see if we are playing 
            if (!isPlayingAllowed && Application.isPlaying)
            {
                return;
            }

            //Check to see if we are really an object
            if (obj == null)
            {
                Debug.LogWarning("Attempting to set null object dirty");
                return;
            }

            //Undo
            if (recordUndo)
            {
                Undo.RecordObject(obj, "Made changes");
            }

            //Calling this everywhere - because unity doco so obscure
            EditorUtility.SetDirty(obj);

            //Mark the scene as dirty as well for non persisted objects
            #if !(UNITY_5_0 || UNITY_5_1 || UNITY_5_2)
            if (!Application.isPlaying && !EditorUtility.IsPersistent(obj))
                {
                    MonoBehaviour mb = obj as MonoBehaviour;
                    if (mb != null)
                    {
                        EditorSceneManager.MarkSceneDirty(mb.gameObject.scene);
                        return;
                    }
                    GameObject go = obj as GameObject;
                    if (go != null)
                    {
                        EditorSceneManager.MarkSceneDirty(go.scene);
                        return;
                    }
                    EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
                }
            #endif
            #endif
        }

        /// <summary>
        /// Remove the seams from active terrain tiles
        /// </summary>
        public void RemoveWorldSeams()
        {
            Terrain[] terrains = Terrain.activeTerrains;

            //Check to see if we have terrains
            if (terrains.Length == 0)
            {
                return;
            }

            //Get the terrain bounds
            int terrainX;
            int terrainZ;
            float terrainWidth = terrains[0].terrainData.size.x;
            float terrainHeight = terrains[0].terrainData.size.z;
            float minBoundsX = float.MaxValue;
            float maxBoundsX = float.MinValue;
            float minBoundsZ = float.MaxValue;
            float maxBoundsZ = int.MinValue;
            foreach (var terrain in terrains)
            {
                Vector3 position = terrain.GetPosition();

                if (position.x < minBoundsX)
                {
                    minBoundsX = position.x;
                }
                if (position.z < minBoundsZ)
                {
                    minBoundsZ = position.z;
                }
                if (position.x > maxBoundsX)
                {
                    maxBoundsX = position.x;
                }
                if (position.z > maxBoundsZ)
                {
                    maxBoundsZ = position.z;
                }
            }

            //Put the terrains in right places
            int tilesX = (int)((maxBoundsX - minBoundsX) / terrainWidth) + 1;
            int tilesZ = (int)((maxBoundsZ - minBoundsZ) / terrainHeight) + 1;
            Terrain[,] terrainTiles = new Terrain[tilesX, tilesZ];
            foreach (var terrain in terrains)
            {
                Vector3 position = terrain.GetPosition();
                terrainX = tilesX - (int)((maxBoundsX - position.x) / terrainWidth) - 1;
                terrainZ = tilesZ - (int)((maxBoundsZ - position.z) / terrainHeight) - 1;
                //Debug.Log(string.Format("{0},{1} - {2},{3}", position.x, position.z, terrainX, terrainZ));
                terrainTiles[terrainX, terrainZ] = terrain;
            }

            //Now assign neightbors
            for (int tx = 0; tx < tilesX; tx++)
            {
                for (int tz = 0; tz < tilesZ; tz++)
                {
                    Terrain right = null;
                    Terrain left = null;
                    Terrain bottom = null;
                    Terrain top = null;

                    if (tx > 0) left = terrainTiles[(tx - 1), tz];
                    if (tx < tilesX - 1) right = terrainTiles[(tx + 1), tz];
                    if (tz > 0) bottom = terrainTiles[tx, (tz - 1)];
                    if (tz < tilesZ - 1) top = terrainTiles[tx, (tz + 1)];

                    terrainTiles[tx, tz].SetNeighbors(left, top, right, bottom);
                }
            }
        }
    }
}