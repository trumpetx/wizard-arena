using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CTS
{
    /// <summary>
    /// Simple FPS counter from Unity Standard Assets - just drop this on a text component
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class CTSFps : MonoBehaviour
    {
        const string cFormat = "FPS {0}";
        const float cMeasurePeriod = 0.5f;
        private int m_currentFps;
        private int m_fpsAccumulator = 0;
        private float m_fpsNextPeriod = 0;
        private Text m_Text;

        /// <summary>
        /// External access to fps
        /// </summary>
        public int FPS
        {
            get { return m_currentFps; }
        }

        private void Start()
        {
            m_fpsNextPeriod = Time.realtimeSinceStartup + cMeasurePeriod;
            m_Text = GetComponent<Text>();
        }

        private void Update()
        {
            // measure average frames per second
            m_fpsAccumulator++;
            if (Time.realtimeSinceStartup > m_fpsNextPeriod)
            {
                m_currentFps = (int)(m_fpsAccumulator / cMeasurePeriod);
                m_fpsAccumulator = 0;
                m_fpsNextPeriod += cMeasurePeriod;
                m_Text.text = string.Format(cFormat, m_currentFps);
            }
        }
    }

}
