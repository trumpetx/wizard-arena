Welcome to CTS - the Complete Terrain Shader!
================================

The CTS terrain shader is a profile driven shader, which means that you can generate as many profiles as you like, and apply them any time you want. 

CTS profiles keep their settings as you switch between design time and runtime. This allows you to configure your profile exactly the way you want in game, and retained the settings when you exit back to editor mode.

SETUP :

1. Set your lighting to linear / deferred for best visuals.

To set up linear:
File -> Build Settings -> Player Settings
Other Settings -> Colour Space : Linear

To set up deferred:
Edit -> Project Settings -> Graphics
On all three tiers:
  Uncheck "Use Defaults"
  Select Rendering Path : Deferred

2. Create a terrain and add your textures (CTS supports up to 16 textures).

3. Add CTS to your terrain.
Component -> CTS -> Add CTS To Terrain.

4. Create a new CTS profile and apply it to the terrain:
Component -> CTS -> Create And Apply Profile

5. Select your CTS Profile:
It will be in the CTS / Profiles directory. You can apply any profile by selecting it and hitting the 'Apply Profile'. NOTE : Applying a profile will overwrite any textures that were previously stored in the terrain.

6. Edit your CTS Profile:
Select and apply the CTS profile. You can then edit in the inspector and changes will be reflected into the terrain in real time. When the profile turns red then you will need to re-bake your texures for the changes to be applied to your terrain. This only need to be done when you change your textures.
 

RUNTIME / BUILDING :
Bake your profile and then select Package CTS For Build. Save your scene and then make your build. If it fails with a DX9 error, then Apply, Bake and then save your scene and run the build process again. If using the Tesselation shader then make sure that DX11 is enabled in your build API's.

PERFORMANCE :
We have include an FPS script and prefab based on the Unity FPS script in standard assets. When we compared it against NVidia's FPS information it seemed a lot more accurate than what is shown in the Unity editor. Drag this into your scene to get a more realistic view of your runtime frame rate.

WEATHER :
To add weather to your terrain select Component -> CTS -> Add Weather Manager. You can then use this interface to control how your terrain responds to different weather. If you are integrating via script, CTS will signal its presence via the CTS_PRESENT define.

DEMOS : 
You can unpack the demo's in the DEMO directory. To take advantage of the post effects used you will also need to install the Unity Post Processing Stack from https://www.assetstore.unity3d.com/en/#!/content/83912

HELP : 
CTS is self documenting, so to get help on any component, just hit the ? at the top of the component. Additional help and video tutorials can be found at http://www.procedural-worlds.com/cts/.

NOTE : 
Do not delete this readme file. It is used to locate where CTS has been placed in your project, and removing it will break CTS.

ABOUT : 
CTS is proudly bought to you by Bartlomiej Galas (Nature Manufacture) & Adam Goodrich (Procedural Worlds). Many thanks to the team at Amplify, Szymon Wojciak, Pawel Homenko and the CTS beta team for their help in bringing CTS to life!

Enjoy!!