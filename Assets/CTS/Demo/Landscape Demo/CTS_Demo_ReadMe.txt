Welcome to the CTS demos!
====================

To get the best out of the demo's do the following:

1. Set your lighting to Linear / Deferred.

To set up linear:
File -> Build Settings -> Player Settings
Other Settings -> Colour Space : Linear

To set up deferred:
Edit -> Project Settings -> Graphics
On all three tiers:
  Uncheck "Use Defaults"
  Select Rendering Path : Deferred

2. Install the Unity Post Processing Stack from https://www.assetstore.unity3d.com/en/#!/content/83912

3. Add the Post Processing Behaviour component to the Main Camera.

4. Drag the Landscape_ImageEffects profile into the Profile slot on the post processing behaviour.

5. Run the scene and then choose the appropriate profile by clicking the button.

Enjoy!!