Shader "CTS Terrain Shader Advanced Tess"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		[HideInInspector] _DummyTex( "", 2D ) = "white" {}
		[Toggle]_Use_AO_Texture("Use_AO_Texture", Int) = 0
		_Tess_Distance("Tess_Distance", Range( 0 , 100)) = 50
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 15
		_TessMin( "Tess Min Distance", Float ) = 0
		_TessMax( "Tess Max Distance", Float ) = 100
		_TessPhongStrength( "Phong Tess Strength", Range( 0, 1 ) ) = 1
		_Geological_Tiling_Close("Geological_Tiling_Close", Range( 0 , 1000)) = 87
		_Geological_Tiling_Far("Geological_Tiling_Far", Range( 0 , 1000)) = 87
		_Geological_Map_Offset_Far("Geological_Map_Offset _Far", Range( 0 , 1)) = 1
		_Geological_Map_Offset_Close("Geological_Map_Offset _Close", Range( 0 , 1)) = 1
		_Geological_Map_Close_Power("Geological_Map_Close_Power", Range( 0 , 2)) = 0
		_Geological_Map_Far_Power("Geological_Map_Far_Power", Range( 0 , 2)) = 1
		_UV_Mix_Power("UV_Mix_Power", Range( 0.01 , 10)) = 4
		_UV_Mix_Start_Distance("UV_Mix_Start_Distance", Range( 0 , 100000)) = 400
		_Perlin_Normal_Tiling_Close("Perlin_Normal_Tiling_Close", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Tiling_Far("Perlin_Normal_Tiling_Far", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Power("Perlin_Normal_Power", Range( 0 , 2)) = 1
		_Perlin_Normal_Power_Close("Perlin_Normal_Power_Close", Range( 0 , 1)) = 0.5
		_Terrain_Smoothness("Terrain_Smoothness", Range( 0 , 2)) = 1
		_Terrain_Specular("Terrain_Specular", Range( 0 , 3)) = 1
		_Texture_4_AO_Power("Texture_4_AO_Power", Range( 0 , 1)) = 1
		_Texture_15_AO_Power("Texture_15_AO_Power", Range( 0 , 1)) = 1
		_Texture_3_AO_Power("Texture_3_AO_Power", Range( 0 , 1)) = 1
		_Texture_8_AO_Power("Texture_8_AO_Power", Range( 0 , 1)) = 1
		_Texture_6_AO_Power("Texture_6_AO_Power", Range( 0 , 1)) = 1
		_Texture_16_AO_Power("Texture_16_AO_Power", Range( 0 , 1)) = 1
		_Texture_7_AO_Power("Texture_7_AO_Power", Range( 0 , 1)) = 1
		_Texture_5_AO_Power("Texture_5_AO_Power", Range( 0 , 1)) = 1
		_Texture_12_AO_Power("Texture_12_AO_Power", Range( 0 , 1)) = 1
		_Texture_1_AO_Power("Texture_1_AO_Power", Range( 0 , 1)) = 1
		_Texture_11_AO_Power("Texture_11_AO_Power", Range( 0 , 1)) = 1
		_Texture_10_AO_Power("Texture_10_AO_Power", Range( 0 , 1)) = 1
		_Texture_13_AO_Power("Texture_13_AO_Power", Range( 0 , 1)) = 1
		_Texture_14_AO_Power("Texture_14_AO_Power", Range( 0 , 1)) = 1
		_Texture_2_AO_Power("Texture_2_AO_Power", Range( 0 , 1)) = 1
		_Texture_9_AO_Power("Texture_9_AO_Power", Range( 0 , 1)) = 1
		_Global_Normalmap_Power("Global_Normalmap_Power", Range( 0 , 10)) = 0
		_Texture_1_Tiling("Texture_1_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_2_Tiling("Texture_2_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_3_Tiling("Texture_3_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_4_Tiling("Texture_4_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_5_Tiling("Texture_5_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_6_Tiling("Texture_6_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_7_Tiling("Texture_7_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_8_Tiling("Texture_8_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_9_Tiling("Texture_9_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_10_Tiling("Texture_10_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_11_Tiling("Texture_11_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_12_Tiling("Texture_12_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_13_Tiling("Texture_13_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_14_Tiling("Texture_14_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_15_Tiling("Texture_15_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_16_Tiling("Texture_16_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_1_Far_Multiplier("Texture_1_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_2_Far_Multiplier("Texture_2_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_3_Far_Multiplier("Texture_3_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_4_Far_Multiplier("Texture_4_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_5_Far_Multiplier("Texture_5_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_6_Far_Multiplier("Texture_6_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_7_Far_Multiplier("Texture_7_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_8_Far_Multiplier("Texture_8_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_9_Far_Multiplier("Texture_9_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_10_Far_Multiplier("Texture_10_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_11_Far_Multiplier("Texture_11_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_12_Far_Multiplier("Texture_12_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_13_Far_Multiplier("Texture_13_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_14_Far_Multiplier("Texture_14_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_15_Far_Multiplier("Texture_15_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_16_Far_Multiplier("Texture_16_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_1_Perlin_Power("Texture_1_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_2_Perlin_Power("Texture_2_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_Array_Emission_AO("Texture_Array_Emission_AO", 2DArray ) = "" {}
		_Texture_3_Perlin_Power("Texture_3_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_4_Perlin_Power("Texture_4_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_5_Perlin_Power("Texture_5_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_6_Perlin_Power("Texture_6_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_7_Perlin_Power("Texture_7_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_8_Perlin_Power("Texture_8_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_9_Perlin_Power("Texture_9_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_10_Perlin_Power("Texture_10_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_11_Perlin_Power("Texture_11_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_12_Perlin_Power("Texture_12_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_13_Perlin_Power("Texture_13_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_14_Perlin_Power("Texture_14_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_15_Perlin_Power("Texture_15_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_16_Perlin_Power("Texture_16_Perlin_Power", Range( 0 , 2)) = 0
		_Snow_Heightmap_Depth("Snow_Heightmap_Depth", Range( 0 , 10)) = 1
		_Snow_Tesselation_Depth("Snow_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_14_Heightmap_Depth("Texture_14_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_13_Heightmap_Depth("Texture_13_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_11_Heightmap_Depth("Texture_11_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_15_Heightmap_Depth("Texture_15_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_16_Heightmap_Depth("Texture_16_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_12_Heightmap_Depth("Texture_12_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_6_Heightmap_Depth("Texture_6_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_10_Heightmap_Depth("Texture_10_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_9_Heightmap_Depth("Texture_9_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_5_Heightmap_Depth("Texture_5_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_7_Heightmap_Depth("Texture_7_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_3_Heightmap_Depth("Texture_3_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_8_Heightmap_Depth("Texture_8_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_4_Heightmap_Depth("Texture_4_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_2_Heightmap_Depth("Texture_2_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_1_Heightmap_Depth("Texture_1_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_1_Tesselation_Depth("Texture_1_Tesselation_Depth", Range( 0 , 10)) = 1
		_Snow_Height_Contrast("Snow_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_14_Height_Contrast("Texture_14_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_16_Height_Contrast("Texture_16_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_15_Height_Contrast("Texture_15_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_9_Height_Contrast("Texture_9_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_13_Height_Contrast("Texture_13_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_10_Height_Contrast("Texture_10_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_11_Height_Contrast("Texture_11_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_12_Height_Contrast("Texture_12_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_6_Height_Contrast("Texture_6_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_8_Height_Contrast("Texture_8_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_7_Height_Contrast("Texture_7_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_3_Height_Contrast("Texture_3_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_5_Height_Contrast("Texture_5_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_4_Height_Contrast("Texture_4_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_1_Height_Contrast("Texture_1_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_2_Height_Contrast("Texture_2_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_2_Tesselation_Depth("Texture_2_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_3_Tesselation_Depth("Texture_3_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_4_Tesselation_Depth("Texture_4_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_5_Tesselation_Depth("Texture_5_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_6_Tesselation_Depth("Texture_6_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_7_Tesselation_Depth("Texture_7_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_8_Tesselation_Depth("Texture_8_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_9_Tesselation_Depth("Texture_9_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_10_Tesselation_Depth("Texture_10_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_11_Tesselation_Depth("Texture_11_Tesselation_Depth", Range( 0 , 10)) = 0
		_Texture_12_Tesselation_Depth("Texture_12_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_13_Tesselation_Depth("Texture_13_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_14_Tesselation_Depth("Texture_14_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_16_Heightblend_Far("Texture_16_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_15_Heightblend_Far("Texture_15_Heightblend_Far", Range( 1 , 10)) = 5
		_Snow_Heightblend_Far("Snow_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_14_Heightblend_Far("Texture_14_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_12_Heightblend_Far("Texture_12_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_Snow_Noise_Index("Texture_Snow_Noise_Index", Int) = -1
		_Texture_16_Heightblend_Close("Texture_16_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_13_Heightblend_Far("Texture_13_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_15_Heightblend_Close("Texture_15_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_7_Heightblend_Far("Texture_7_Heightblend_Far", Range( 1 , 10)) = 5
		_Snow_Heightblend_Close("Snow_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_14_Heightblend_Close("Texture_14_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_11_Heightblend_Far("Texture_11_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_13_Heightblend_Close("Texture_13_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_10_Heightblend_Far("Texture_10_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_9_Heightblend_Far("Texture_9_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_12_Heightblend_Close("Texture_12_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_8_Heightblend_Far("Texture_8_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_11_Heightblend_Close("Texture_11_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_6_Heightblend_Far("Texture_6_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_10_Heightblend_Close("Texture_10_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_9_Heightblend_Close("Texture_9_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_5_Heightblend_Far("Texture_5_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_8_Heightblend_Close("Texture_8_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_3_Heightblend_Far("Texture_3_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_4_Heightblend_Far("Texture_4_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_7_Heightblend_Close("Texture_7_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_15_Tesselation_Depth("Texture_15_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_6_Heightblend_Close("Texture_6_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_5_Heightblend_Close("Texture_5_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_2_Heightblend_Far("Texture_2_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_4_Heightblend_Close("Texture_4_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_2_Heightmap_MaxHeight("Texture_2_Heightmap_MaxHeight", Range( 0 , 1)) = 0
		_Texture_4_Heightmap_MaxHeight("Texture_4_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_14_Heightmap_MaxHeight("Texture_14_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_13_Heightmap_MaxHeight("Texture_13_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_9_Heightmap_MaxHeight("Texture_9_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_15_Heightmap_MaxHeight("Texture_15_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_16_Heightmap_MaxHeight("Texture_16_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_7_Heightmap_MaxHeight("Texture_7_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_10_Heightmap_MaxHeight("Texture_10_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_11_Heightmap_MaxHeight("Texture_11_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_12_Heightmap_MaxHeight("Texture_12_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_5_Heightmap_MaxHeight("Texture_5_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_6_Heightmap_MaxHeight("Texture_6_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_3_Heightmap_MaxHeight("Texture_3_Heightmap_MaxHeight", Range( 0 , 1)) = 0
		_Texture_8_Heightmap_MaxHeight("Texture_8_Heightmap_MaxHeight", Range( 0 , 2)) = 0
		_Texture_1_Heightmap_MaxHeight("Texture_1_Heightmap_MaxHeight", Range( 0 , 1)) = 0
		_Texture_1_Heightblend_Far("Texture_1_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_16_Tesselation_Depth("Texture_16_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_3_Heightblend_Close("Texture_3_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_Geological_Map("Texture_Geological_Map", 2D) = "white" {}
		_Texture_2_Heightblend_Close("Texture_2_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_1_Heightblend_Close("Texture_1_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_1_Geological_Power("Texture_1_Geological_Power", Range( 0 , 2)) = 1
		_Texture_2_Geological_Power("Texture_2_Geological_Power", Range( 0 , 2)) = 1
		_Texture_3_Geological_Power("Texture_3_Geological_Power", Range( 0 , 2)) = 1
		_Texture_4_Geological_Power("Texture_4_Geological_Power", Range( 0 , 2)) = 1
		_Texture_5_Geological_Power("Texture_5_Geological_Power", Range( 0 , 2)) = 1
		_Texture_6_Geological_Power("Texture_6_Geological_Power", Range( 0 , 2)) = 1
		_Texture_7_Geological_Power("Texture_7_Geological_Power", Range( 0 , 2)) = 1
		_Texture_8_Geological_Power("Texture_8_Geological_Power", Range( 0 , 2)) = 1
		_Texture_9_Geological_Power("Texture_9_Geological_Power", Range( 0 , 2)) = 1
		_Texture_10_Geological_Power("Texture_10_Geological_Power", Range( 0 , 2)) = 1
		_Texture_11_Geological_Power("Texture_11_Geological_Power", Range( 0 , 2)) = 1
		_Texture_12_Geological_Power("Texture_12_Geological_Power", Range( 0 , 2)) = 1
		_Texture_13_Geological_Power("Texture_13_Geological_Power", Range( 0 , 2)) = 1
		_Texture_14_Geological_Power("Texture_14_Geological_Power", Range( 0 , 2)) = 1
		[Toggle]_Use_AO("Use_AO", Int) = 0
		_Texture_15_Geological_Power("Texture_15_Geological_Power", Range( 0 , 2)) = 1
		_Texture_16_Geological_Power("Texture_16_Geological_Power", Range( 0 , 2)) = 1
		_Snow_Specular("Snow_Specular", Range( 0 , 3)) = 1
		_Snow_Normal_Scale("Snow_Normal_Scale", Range( 0 , 5)) = 1
		_Snow_Color("Snow_Color", Vector) = (1,1,1,1)
		_Snow_Blend_Normal("Snow_Blend_Normal", Range( 0 , 1)) = 0.8
		_Snow_Amount("Snow_Amount", Range( 0 , 2)) = 0
		_Snow_Tiling("Snow_Tiling", Range( 0.001 , 20)) = 15
		_Snow_Tiling_Far_Multiplier("Snow_Tiling_Far_Multiplier", Range( 0.001 , 20)) = 1
		_Snow_Perlin_Power("Snow_Perlin_Power", Range( 0 , 2)) = 0
		_Snow_Noise_Power("Snow_Noise_Power", Range( 0 , 1)) = 1
		_Snow_Noise_Tiling("Snow_Noise_Tiling", Range( 0.001 , 1)) = 0.02
		_Snow_Min_Height("Snow_Min_Height", Range( -1000 , 10000)) = -1000
		_Snow_Min_Height_Blending("Snow_Min_Height_Blending", Range( 0 , 500)) = 1
		_Snow_Maximum_Angle("Snow_Maximum_Angle", Range( 0.001 , 180)) = 30
		_Snow_Maximum_Angle_Hardness("Snow_Maximum_Angle_Hardness", Range( 0.001 , 10)) = 1
		_Texture_1_Snow_Reduction("Texture_1_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_2_Snow_Reduction("Texture_2_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_3_Snow_Reduction("Texture_3_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_4_Snow_Reduction("Texture_4_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_5_Snow_Reduction("Texture_5_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_6_Snow_Reduction("Texture_6_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_7_Snow_Reduction("Texture_7_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_8_Snow_Reduction("Texture_8_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_9_Snow_Reduction("Texture_9_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_10_Snow_Reduction("Texture_10_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_11_Snow_Reduction("Texture_11_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_12_Snow_Reduction("Texture_12_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_13_Snow_Reduction("Texture_13_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_14_Snow_Reduction("Texture_14_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_15_Snow_Reduction("Texture_15_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_16_Snow_Reduction("Texture_16_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_Array_Normal("Texture_Array_Normal", 2DArray ) = "" {}
		_Texture_Array_Albedo("Texture_Array_Albedo", 2DArray ) = "" {}
		_Texture_Perlin_Normal_Index("Texture_Perlin_Normal_Index", Int) = -1
		_Snow_Ambient_Occlusion_Power("Snow_Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		_Ambient_Occlusion_Power("Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		_Texture_9_Color("Texture_9_Color", Vector) = (1,1,1,1)
		_Texture_10_Color("Texture_10_Color", Vector) = (1,1,1,1)
		_Texture_13_Color("Texture_13_Color", Vector) = (1,1,1,1)
		_Texture_11_Color("Texture_11_Color", Vector) = (1,1,1,1)
		_Texture_12_Color("Texture_12_Color", Vector) = (1,1,1,1)
		_Texture_15_Color("Texture_15_Color", Vector) = (1,1,1,1)
		_Texture_14_Color("Texture_14_Color", Vector) = (1,1,1,1)
		_Texture_8_Color("Texture_8_Color", Vector) = (1,1,1,1)
		_Texture_16_Color("Texture_16_Color", Vector) = (1,1,1,1)
		_Texture_7_Color("Texture_7_Color", Vector) = (1,1,1,1)
		_Texture_5_Color("Texture_5_Color", Vector) = (1,1,1,1)
		_Texture_6_Color("Texture_6_Color", Vector) = (1,1,1,1)
		_Texture_4_Color("Texture_4_Color", Vector) = (1,1,1,1)
		_Texture_1_Color("Texture_1_Color", Vector) = (1,1,1,1)
		_Texture_3_Color("Texture_3_Color", Vector) = (1,1,1,1)
		_Texture_2_Color("Texture_2_Color", Vector) = (1,1,1,1)
		_Texture_2_Triplanar("Texture_2_Triplanar", Range( 0 , 1)) = 0
		_Texture_16_Triplanar("Texture_16_Triplanar", Range( 0 , 1)) = 0
		_Texture_15_Triplanar("Texture_15_Triplanar", Range( 0 , 1)) = 0
		_Texture_12_Triplanar("Texture_12_Triplanar", Range( 0 , 1)) = 0
		_Texture_14_Triplanar("Texture_14_Triplanar", Range( 0 , 1)) = 0
		_Texture_13_Triplanar("Texture_13_Triplanar", Range( 0 , 1)) = 0
		_Texture_11_Triplanar("Texture_11_Triplanar", Range( 0 , 1)) = 0
		_Texture_9_Triplanar("Texture_9_Triplanar", Range( 0 , 1)) = 0
		_Texture_10_Triplanar("Texture_10_Triplanar", Range( 0 , 1)) = 0
		_Texture_8_Triplanar("Texture_8_Triplanar", Range( 0 , 1)) = 0
		_Texture_7_Triplanar("Texture_7_Triplanar", Range( 0 , 1)) = 0
		_Texture_6_Triplanar("Texture_6_Triplanar", Range( 0 , 1)) = 0
		_Texture_5_Triplanar("Texture_5_Triplanar", Range( 0 , 1)) = 0
		_Texture_4_Triplanar("Texture_4_Triplanar", Range( 0 , 1)) = 0
		_Texture_3_Triplanar("Texture_3_Triplanar", Range( 0 , 1)) = 0
		_Texture_1_Triplanar("Texture_1_Triplanar", Range( 0 , 1)) = 0
		_Texture_1_Normal_Power("Texture_1_Normal_Power", Range( 0 , 2)) = 1
		_Texture_2_Normal_Power("Texture_2_Normal_Power", Range( 0 , 2)) = 1
		_Texture_3_Normal_Power("Texture_3_Normal_Power", Range( 0 , 2)) = 1
		_Texture_4_Normal_Power("Texture_4_Normal_Power", Range( 0 , 2)) = 1
		_Texture_5_Normal_Power("Texture_5_Normal_Power", Range( 0 , 2)) = 1
		_Texture_6_Normal_Power("Texture_6_Normal_Power", Range( 0 , 2)) = 1
		_Texture_7_Normal_Power("Texture_7_Normal_Power", Range( 0 , 2)) = 1
		_Texture_8_Normal_Power("Texture_8_Normal_Power", Range( 0 , 2)) = 1
		_Texture_9_Normal_Power("Texture_9_Normal_Power", Range( 0 , 2)) = 1
		_Texture_10_Normal_Power("Texture_10_Normal_Power", Range( 0 , 2)) = 1
		_Texture_11_Normal_Power("Texture_11_Normal_Power", Range( 0 , 2)) = 1
		_Texture_12_Normal_Power("Texture_12_Normal_Power", Range( 0 , 2)) = 1
		_Texture_13_Normal_Power("Texture_13_Normal_Power", Range( 0 , 2)) = 1
		_Texture_13_Albedo_Index("Texture_13_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_12_Emission_AO_Index("Texture_12_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_12_Albedo_Index("Texture_12_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_12_Normal_Index("Texture_12_Normal_Index", Range( -1 , 100)) = -1
		_Texture_14_Normal_Power("Texture_14_Normal_Power", Range( 0 , 2)) = 1
		_Texture_15_Normal_Power("Texture_15_Normal_Power", Range( 0 , 2)) = 1
		_Texture_16_Normal_Power("Texture_16_Normal_Power", Range( 0 , 2)) = 1
		_Texture_Splat_1("Texture_Splat_1", 2D) = "black" {}
		_Texture_1_Albedo_Index("Texture_1_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_2_Albedo_Index("Texture_2_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_1_Emission_AO_Index("Texture_1_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_2_Emission_AO_Index("Texture_2_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_3_Normal_Index("Texture_3_Normal_Index", Range( -1 , 100)) = -1
		_Texture_11_Emission_AO_Index("Texture_11_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_3_Emission_AO_Index("Texture_3_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_13_Normal_Index("Texture_13_Normal_Index", Range( -1 , 100)) = -1
		_Texture_3_Albedo_Index("Texture_3_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_11_Normal_Index("Texture_11_Normal_Index", Range( -1 , 100)) = -1
		_Texture_10_Emission_AO_Index("Texture_10_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_10_Normal_Index("Texture_10_Normal_Index", Range( -1 , 100)) = -1
		_Texture_9_Emission_AO_Index("Texture_9_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_11_Albedo_Index("Texture_11_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_9_Normal_Index("Texture_9_Normal_Index", Range( -1 , 100)) = -1
		_Texture_10_Albedo_Index("Texture_10_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_9_Albedo_Index("Texture_9_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_4_Normal_Index("Texture_4_Normal_Index", Range( -1 , 100)) = -1
		_Texture_4_Emission_AO_Index("Texture_4_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_4_Albedo_Index("Texture_4_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_5_Normal_Index("Texture_5_Normal_Index", Range( -1 , 100)) = -1
		_Texture_5_Emission_AO_Index("Texture_5_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_6_Albedo_Index("Texture_6_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_5_Albedo_Index("Texture_5_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_6_Normal_Index("Texture_6_Normal_Index", Range( -1 , 100)) = -1
		_Texture_6_Emission_AO_Index("Texture_6_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_7_Normal_Index("Texture_7_Normal_Index", Range( -1 , 100)) = -1
		_Texture_8_Albedo_Index("Texture_8_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_7_Emission_AO_Index("Texture_7_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_8_Normal_Index("Texture_8_Normal_Index", Range( -1 , 100)) = -1
		_Texture_16_Emission_AO_Index("Texture_16_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_8_Emission_AO_Index("Texture_8_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_16_Albedo_Index("Texture_16_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_15_Emission_AO_Index("Texture_15_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_16_Normal_Index("Texture_16_Normal_Index", Range( -1 , 100)) = -1
		_Texture_15_Albedo_Index("Texture_15_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_14_Emission_AO_Index("Texture_14_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_15_Normal_Index("Texture_15_Normal_Index", Range( -1 , 100)) = -1
		_Texture_14_Albedo_Index("Texture_14_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_13_Emission_AO_Index("Texture_13_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_14_Normal_Index("Texture_14_Normal_Index", Range( -1 , 100)) = -1
		_Texture_7_Albedo_Index("Texture_7_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_1_Normal_Index("Texture_1_Normal_Index", Range( -1 , 100)) = -1
		_Texture_2_Normal_Index("Texture_2_Normal_Index", Range( -1 , 100)) = -1
		_Texture_Splat_2("Texture_Splat_2", 2D) = "black" {}
		_Snow_Emission_AO_index("Snow_Emission_AO_index", Range( -1 , 100)) = -1
		_Texture_Snow_Normal_Index("Texture_Snow_Normal_Index", Range( -1 , 100)) = 0
		_Texture_Snow_Index("Texture_Snow_Index", Range( -1 , 100)) = 0
		_Texture_Splat_3("Texture_Splat_3", 2D) = "black" {}
		_Texture_Splat_4("Texture_Splat_4", 2D) = "black" {}
		_Global_Normal_Map("Global_Normal_Map", 2D) = "bump" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+100" }
		Cull Back
		ZTest LEqual
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Tessellation.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#pragma multi_compile __ _USE_AO_TEXTURE_ON
		#pragma multi_compile __ _USE_AO_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float2 uv_DummyTex;
			float3 worldNormal;
			INTERNAL_DATA
		};

		struct appdata
		{
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float4 texcoord : TEXCOORD0;
			float4 texcoord1 : TEXCOORD1;
			float4 texcoord2 : TEXCOORD2;
			float4 texcoord3 : TEXCOORD3;
			fixed4 color : COLOR;
			UNITY_VERTEX_INPUT_INSTANCE_ID
		};

		uniform fixed _Perlin_Normal_Tiling_Close;
		uniform int _Texture_Perlin_Normal_Index;
		uniform fixed _Perlin_Normal_Power_Close;
		uniform fixed _Perlin_Normal_Tiling_Far;
		uniform fixed _Perlin_Normal_Power;
		uniform fixed _UV_Mix_Start_Distance;
		uniform fixed _UV_Mix_Power;
		uniform fixed _Texture_16_Perlin_Power;
		uniform sampler2D _Texture_Splat_4;
		uniform sampler2D _DummyTex;
		uniform fixed _Texture_15_Perlin_Power;
		uniform fixed _Texture_14_Perlin_Power;
		uniform fixed _Texture_13_Perlin_Power;
		uniform fixed _Texture_12_Perlin_Power;
		uniform sampler2D _Texture_Splat_3;
		uniform fixed _Texture_11_Perlin_Power;
		uniform fixed _Texture_10_Perlin_Power;
		uniform fixed _Texture_9_Perlin_Power;
		uniform fixed _Texture_8_Perlin_Power;
		uniform sampler2D _Texture_Splat_2;
		uniform fixed _Texture_7_Perlin_Power;
		uniform fixed _Texture_6_Perlin_Power;
		uniform fixed _Texture_5_Perlin_Power;
		uniform fixed _Texture_1_Perlin_Power;
		uniform sampler2D _Texture_Splat_1;
		uniform fixed _Texture_2_Perlin_Power;
		uniform fixed _Texture_4_Perlin_Power;
		uniform fixed _Texture_3_Perlin_Power;
		uniform fixed _Snow_Perlin_Power;
		uniform fixed _Texture_1_Tesselation_Depth;
		uniform fixed _Texture_1_Normal_Index;
		uniform fixed _Texture_1_Triplanar;
		uniform fixed _Texture_1_Tiling;
		uniform fixed _Texture_1_Far_Multiplier;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Normal );
		uniform fixed _Texture_2_Tesselation_Depth;
		uniform fixed _Texture_2_Normal_Index;
		uniform fixed _Texture_2_Triplanar;
		uniform fixed _Texture_2_Tiling;
		uniform fixed _Texture_2_Far_Multiplier;
		uniform fixed _Texture_3_Tesselation_Depth;
		uniform fixed _Texture_3_Normal_Index;
		uniform fixed _Texture_3_Triplanar;
		uniform fixed _Texture_3_Tiling;
		uniform fixed _Texture_3_Far_Multiplier;
		uniform fixed _Texture_4_Tesselation_Depth;
		uniform fixed _Texture_4_Normal_Index;
		uniform fixed _Texture_4_Triplanar;
		uniform fixed _Texture_4_Tiling;
		uniform fixed _Texture_4_Far_Multiplier;
		uniform fixed _Texture_5_Tesselation_Depth;
		uniform fixed _Texture_5_Normal_Index;
		uniform fixed _Texture_5_Triplanar;
		uniform fixed _Texture_5_Tiling;
		uniform fixed _Texture_5_Far_Multiplier;
		uniform fixed _Texture_6_Tesselation_Depth;
		uniform fixed _Texture_6_Normal_Index;
		uniform fixed _Texture_6_Triplanar;
		uniform fixed _Texture_6_Tiling;
		uniform fixed _Texture_6_Far_Multiplier;
		uniform fixed _Texture_7_Tesselation_Depth;
		uniform fixed _Texture_7_Normal_Index;
		uniform fixed _Texture_7_Triplanar;
		uniform fixed _Texture_7_Tiling;
		uniform fixed _Texture_7_Far_Multiplier;
		uniform fixed _Texture_8_Tesselation_Depth;
		uniform fixed _Texture_8_Normal_Index;
		uniform fixed _Texture_8_Triplanar;
		uniform fixed _Texture_8_Tiling;
		uniform fixed _Texture_8_Far_Multiplier;
		uniform fixed _Texture_9_Tesselation_Depth;
		uniform fixed _Texture_9_Normal_Index;
		uniform fixed _Texture_9_Triplanar;
		uniform fixed _Texture_9_Tiling;
		uniform fixed _Texture_9_Far_Multiplier;
		uniform fixed _Texture_10_Tesselation_Depth;
		uniform fixed _Texture_10_Normal_Index;
		uniform fixed _Texture_10_Triplanar;
		uniform fixed _Texture_10_Tiling;
		uniform fixed _Texture_10_Far_Multiplier;
		uniform fixed _Texture_11_Tesselation_Depth;
		uniform fixed _Texture_11_Normal_Index;
		uniform fixed _Texture_11_Triplanar;
		uniform fixed _Texture_11_Tiling;
		uniform fixed _Texture_11_Far_Multiplier;
		uniform fixed _Texture_12_Tesselation_Depth;
		uniform fixed _Texture_12_Normal_Index;
		uniform fixed _Texture_12_Triplanar;
		uniform fixed _Texture_12_Tiling;
		uniform fixed _Texture_12_Far_Multiplier;
		uniform fixed _Texture_13_Tesselation_Depth;
		uniform fixed _Texture_13_Normal_Index;
		uniform fixed _Texture_13_Triplanar;
		uniform fixed _Texture_13_Tiling;
		uniform fixed _Texture_13_Far_Multiplier;
		uniform fixed _Texture_14_Tesselation_Depth;
		uniform fixed _Texture_14_Normal_Index;
		uniform fixed _Texture_14_Triplanar;
		uniform fixed _Texture_14_Tiling;
		uniform fixed _Texture_14_Far_Multiplier;
		uniform fixed _Texture_15_Tesselation_Depth;
		uniform fixed _Texture_15_Normal_Index;
		uniform fixed _Texture_15_Triplanar;
		uniform fixed _Texture_15_Tiling;
		uniform fixed _Texture_15_Far_Multiplier;
		uniform fixed _Texture_16_Tesselation_Depth;
		uniform fixed _Texture_16_Normal_Index;
		uniform fixed _Texture_16_Triplanar;
		uniform fixed _Texture_16_Tiling;
		uniform fixed _Texture_16_Far_Multiplier;
		uniform fixed _Snow_Emission_AO_index;
		uniform fixed _Snow_Tiling;
		uniform fixed _Texture_Snow_Normal_Index;
		uniform fixed _Snow_Height_Contrast;
		uniform fixed _Snow_Heightmap_Depth;
		uniform fixed _Snow_Tiling_Far_Multiplier;
		uniform fixed _Snow_Amount;
		uniform fixed _Snow_Noise_Tiling;
		uniform int _Texture_Snow_Noise_Index;
		uniform fixed _Snow_Noise_Power;
		uniform fixed _Snow_Maximum_Angle_Hardness;
		uniform fixed _Snow_Maximum_Angle;
		uniform fixed _Snow_Min_Height;
		uniform fixed _Snow_Min_Height_Blending;
		uniform fixed _Texture_16_Snow_Reduction;
		uniform fixed _Texture_15_Snow_Reduction;
		uniform fixed _Texture_13_Snow_Reduction;
		uniform fixed _Texture_12_Snow_Reduction;
		uniform fixed _Texture_11_Snow_Reduction;
		uniform fixed _Texture_9_Snow_Reduction;
		uniform fixed _Texture_8_Snow_Reduction;
		uniform fixed _Texture_7_Snow_Reduction;
		uniform fixed _Texture_5_Snow_Reduction;
		uniform fixed _Texture_1_Snow_Reduction;
		uniform fixed _Texture_2_Snow_Reduction;
		uniform fixed _Texture_3_Snow_Reduction;
		uniform fixed _Texture_4_Snow_Reduction;
		uniform fixed _Texture_6_Snow_Reduction;
		uniform fixed _Texture_10_Snow_Reduction;
		uniform fixed _Texture_14_Snow_Reduction;
		uniform fixed _Snow_Heightblend_Close;
		uniform fixed _Snow_Heightblend_Far;
		uniform fixed _Texture_13_Heightmap_Depth;
		uniform fixed _Texture_13_Height_Contrast;
		uniform fixed _Texture_13_Heightblend_Close;
		uniform fixed _Texture_13_Heightblend_Far;
		uniform fixed _Texture_14_Heightmap_Depth;
		uniform fixed _Texture_14_Height_Contrast;
		uniform fixed _Texture_14_Heightblend_Close;
		uniform fixed _Texture_14_Heightblend_Far;
		uniform fixed _Texture_15_Heightmap_Depth;
		uniform fixed _Texture_15_Height_Contrast;
		uniform fixed _Texture_15_Heightblend_Close;
		uniform fixed _Texture_15_Heightblend_Far;
		uniform fixed _Texture_16_Heightmap_Depth;
		uniform fixed _Texture_16_Height_Contrast;
		uniform fixed _Texture_16_Heightblend_Close;
		uniform fixed _Texture_16_Heightblend_Far;
		uniform fixed _Texture_9_Heightmap_Depth;
		uniform fixed _Texture_9_Height_Contrast;
		uniform fixed _Texture_9_Heightblend_Close;
		uniform fixed _Texture_9_Heightblend_Far;
		uniform fixed _Texture_10_Heightmap_Depth;
		uniform fixed _Texture_10_Height_Contrast;
		uniform fixed _Texture_10_Heightblend_Close;
		uniform fixed _Texture_10_Heightblend_Far;
		uniform fixed _Texture_11_Heightmap_Depth;
		uniform fixed _Texture_11_Height_Contrast;
		uniform fixed _Texture_11_Heightblend_Close;
		uniform fixed _Texture_11_Heightblend_Far;
		uniform fixed _Texture_12_Heightmap_Depth;
		uniform fixed _Texture_12_Height_Contrast;
		uniform fixed _Texture_12_Heightblend_Close;
		uniform fixed _Texture_12_Heightblend_Far;
		uniform fixed _Texture_5_Heightmap_Depth;
		uniform fixed _Texture_5_Height_Contrast;
		uniform fixed _Texture_5_Heightblend_Close;
		uniform fixed _Texture_5_Heightblend_Far;
		uniform fixed _Texture_6_Heightmap_Depth;
		uniform fixed _Texture_6_Height_Contrast;
		uniform fixed _Texture_6_Heightblend_Close;
		uniform fixed _Texture_6_Heightblend_Far;
		uniform fixed _Texture_7_Heightmap_Depth;
		uniform fixed _Texture_7_Height_Contrast;
		uniform fixed _Texture_7_Heightblend_Close;
		uniform fixed _Texture_7_Heightblend_Far;
		uniform fixed _Texture_8_Heightmap_Depth;
		uniform fixed _Texture_8_Height_Contrast;
		uniform fixed _Texture_8_Heightblend_Close;
		uniform fixed _Texture_8_Heightblend_Far;
		uniform fixed _Texture_1_Height_Contrast;
		uniform fixed _Texture_1_Heightmap_Depth;
		uniform fixed _Texture_1_Heightblend_Close;
		uniform fixed _Texture_1_Heightblend_Far;
		uniform fixed _Texture_2_Heightmap_Depth;
		uniform fixed _Texture_2_Height_Contrast;
		uniform fixed _Texture_2_Heightblend_Close;
		uniform fixed _Texture_2_Heightblend_Far;
		uniform fixed _Texture_3_Heightmap_Depth;
		uniform fixed _Texture_3_Height_Contrast;
		uniform fixed _Texture_3_Heightblend_Close;
		uniform fixed _Texture_3_Heightblend_Far;
		uniform fixed _Texture_4_Heightmap_Depth;
		uniform fixed _Texture_4_Height_Contrast;
		uniform fixed _Texture_4_Heightblend_Close;
		uniform fixed _Texture_4_Heightblend_Far;
		uniform fixed _Texture_1_Normal_Power;
		uniform fixed _Texture_2_Normal_Power;
		uniform fixed _Texture_3_Normal_Power;
		uniform fixed _Texture_4_Normal_Power;
		uniform fixed _Texture_5_Normal_Power;
		uniform fixed _Texture_6_Normal_Power;
		uniform fixed _Texture_7_Normal_Power;
		uniform fixed _Texture_8_Normal_Power;
		uniform fixed _Texture_9_Normal_Power;
		uniform fixed _Texture_10_Normal_Power;
		uniform fixed _Texture_11_Normal_Power;
		uniform fixed _Texture_12_Normal_Power;
		uniform fixed _Texture_13_Normal_Power;
		uniform fixed _Texture_14_Normal_Power;
		uniform fixed _Texture_15_Normal_Power;
		uniform fixed _Texture_16_Normal_Power;
		uniform fixed _Snow_Normal_Scale;
		uniform fixed _Snow_Blend_Normal;
		uniform fixed _Global_Normalmap_Power;
		uniform sampler2D _Global_Normal_Map;
		uniform fixed _Texture_1_Albedo_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Albedo );
		uniform fixed4 _Texture_1_Color;
		uniform fixed _Texture_2_Albedo_Index;
		uniform fixed4 _Texture_2_Color;
		uniform fixed _Texture_3_Albedo_Index;
		uniform fixed4 _Texture_3_Color;
		uniform fixed _Texture_4_Albedo_Index;
		uniform fixed4 _Texture_4_Color;
		uniform fixed _Texture_5_Albedo_Index;
		uniform fixed4 _Texture_5_Color;
		uniform fixed _Texture_6_Albedo_Index;
		uniform fixed4 _Texture_6_Color;
		uniform fixed _Texture_7_Albedo_Index;
		uniform fixed4 _Texture_7_Color;
		uniform fixed _Texture_8_Albedo_Index;
		uniform fixed4 _Texture_8_Color;
		uniform fixed _Texture_9_Albedo_Index;
		uniform fixed4 _Texture_9_Color;
		uniform fixed _Texture_10_Albedo_Index;
		uniform fixed4 _Texture_10_Color;
		uniform fixed _Texture_11_Albedo_Index;
		uniform fixed4 _Texture_11_Color;
		uniform fixed _Texture_12_Albedo_Index;
		uniform fixed4 _Texture_12_Color;
		uniform fixed _Texture_13_Albedo_Index;
		uniform fixed4 _Texture_13_Color;
		uniform fixed _Texture_14_Albedo_Index;
		uniform fixed4 _Texture_14_Color;
		uniform fixed _Texture_15_Albedo_Index;
		uniform fixed4 _Texture_15_Color;
		uniform fixed _Texture_16_Albedo_Index;
		uniform fixed4 _Texture_16_Color;
		uniform sampler2D _Texture_Geological_Map;
		uniform fixed _Geological_Map_Offset_Close;
		uniform fixed _Geological_Tiling_Close;
		uniform fixed _Geological_Map_Close_Power;
		uniform fixed _Geological_Tiling_Far;
		uniform fixed _Geological_Map_Offset_Far;
		uniform fixed _Geological_Map_Far_Power;
		uniform fixed _Texture_16_Geological_Power;
		uniform fixed _Texture_15_Geological_Power;
		uniform fixed _Texture_14_Geological_Power;
		uniform fixed _Texture_13_Geological_Power;
		uniform fixed _Texture_12_Geological_Power;
		uniform fixed _Texture_11_Geological_Power;
		uniform fixed _Texture_10_Geological_Power;
		uniform fixed _Texture_9_Geological_Power;
		uniform fixed _Texture_8_Geological_Power;
		uniform fixed _Texture_7_Geological_Power;
		uniform fixed _Texture_6_Geological_Power;
		uniform fixed _Texture_5_Geological_Power;
		uniform fixed _Texture_1_Geological_Power;
		uniform fixed _Texture_2_Geological_Power;
		uniform fixed _Texture_4_Geological_Power;
		uniform fixed _Texture_3_Geological_Power;
		uniform fixed _Texture_Snow_Index;
		uniform fixed4 _Snow_Color;
		uniform fixed _Terrain_Specular;
		uniform fixed _Snow_Specular;
		uniform fixed _Terrain_Smoothness;
		uniform fixed _Texture_1_Emission_AO_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Emission_AO );
		uniform fixed _Texture_1_AO_Power;
		uniform fixed _Texture_2_Emission_AO_Index;
		uniform fixed _Texture_2_AO_Power;
		uniform fixed _Texture_3_Emission_AO_Index;
		uniform fixed _Texture_3_AO_Power;
		uniform fixed _Texture_4_Emission_AO_Index;
		uniform fixed _Texture_4_AO_Power;
		uniform fixed _Texture_5_Emission_AO_Index;
		uniform fixed _Texture_5_AO_Power;
		uniform fixed _Texture_6_Emission_AO_Index;
		uniform fixed _Texture_6_AO_Power;
		uniform fixed _Texture_7_Emission_AO_Index;
		uniform fixed _Texture_7_AO_Power;
		uniform fixed _Texture_8_Emission_AO_Index;
		uniform fixed _Texture_8_AO_Power;
		uniform fixed _Texture_9_Emission_AO_Index;
		uniform fixed _Texture_9_AO_Power;
		uniform fixed _Texture_10_Emission_AO_Index;
		uniform fixed _Texture_10_AO_Power;
		uniform fixed _Texture_11_Emission_AO_Index;
		uniform fixed _Texture_11_AO_Power;
		uniform fixed _Texture_12_Emission_AO_Index;
		uniform fixed _Texture_12_AO_Power;
		uniform fixed _Texture_13_Emission_AO_Index;
		uniform fixed _Texture_13_AO_Power;
		uniform fixed _Texture_14_Emission_AO_Index;
		uniform fixed _Texture_14_AO_Power;
		uniform fixed _Texture_15_Emission_AO_Index;
		uniform fixed _Texture_15_AO_Power;
		uniform fixed _Texture_16_Emission_AO_Index;
		uniform fixed _Texture_16_AO_Power;
		uniform fixed _Snow_Ambient_Occlusion_Power;
		uniform fixed _Ambient_Occlusion_Power;
		uniform fixed _Tess_Distance;
		uniform fixed _Snow_Tesselation_Depth;
		uniform fixed _Texture_1_Heightmap_MaxHeight;
		uniform fixed _Texture_2_Heightmap_MaxHeight;
		uniform fixed _Texture_3_Heightmap_MaxHeight;
		uniform fixed _Texture_4_Heightmap_MaxHeight;
		uniform fixed _Texture_5_Heightmap_MaxHeight;
		uniform fixed _Texture_6_Heightmap_MaxHeight;
		uniform fixed _Texture_7_Heightmap_MaxHeight;
		uniform fixed _Texture_8_Heightmap_MaxHeight;
		uniform fixed _Texture_10_Heightmap_MaxHeight;
		uniform fixed _Texture_9_Heightmap_MaxHeight;
		uniform fixed _Texture_11_Heightmap_MaxHeight;
		uniform fixed _Texture_12_Heightmap_MaxHeight;
		uniform fixed _Texture_13_Heightmap_MaxHeight;
		uniform fixed _Texture_14_Heightmap_MaxHeight;
		uniform fixed _Texture_15_Heightmap_MaxHeight;
		uniform fixed _Texture_16_Heightmap_MaxHeight;
		uniform fixed _TessValue;
		uniform fixed _TessMin;
		uniform fixed _TessMax;
		uniform fixed _TessPhongStrength;

		float4 tessFunction( appdata v0, appdata v1, appdata v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}

		void vertexDataFunc( inout appdata v )
		{
			v.texcoord.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float3 normalWorld = UnityObjectToWorldNormal( v.normal );
			float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex);
			fixed4 tex2DNode4371 = tex2Dlod( _Texture_Splat_4, fixed4( v.texcoord.xy, 0.0 , 0.0 ) );
			fixed4 tex2DNode4370 = tex2Dlod( _Texture_Splat_3, fixed4( v.texcoord.xy, 0.0 , 0.0 ) );
			fixed4 tex2DNode4369 = tex2Dlod( _Texture_Splat_2, fixed4( v.texcoord.xy, 0.0 , 0.0 ) );
			fixed4 tex2DNode4368 = tex2Dlod( _Texture_Splat_1, fixed4( v.texcoord.xy, 0.0 , 0.0 ) );
			fixed3 temp_cast_5 = (_Texture_1_Tesselation_Depth).xxx;
			float2 appendResult1998 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 Top_Bottom = appendResult1998;
			float2 appendResult3284 = float2( ( 1.0 / _Texture_1_Tiling ) , ( 1.0 / _Texture_1_Tiling ) );
			float4 texArray3300 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Normal_Index) , 0 );
			fixed2 temp_cast_6 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5491 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_6 ), _Texture_1_Normal_Index) , 0 );
			fixed UVmixDistance = clamp( pow( ( distance( ase_worldPos , _WorldSpaceCameraPos ) / _UV_Mix_Start_Distance ) , _UV_Mix_Power ) , 0.0 , 1.0 );
			fixed3 BlendComponents = clamp( pow( ( normalWorld * normalWorld ) , 25.0 ) , float3( -1,-1,-1 ) , float3( 1,1,1 ) );
			float2 appendResult879 = float2( ase_worldPos.z , ase_worldPos.y );
			fixed2 Front_Back = appendResult879;
			float4 texArray3299 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult3284 ), _Texture_1_Normal_Index) , 0 );
			float2 appendResult2002 = float2( ase_worldPos.x , ase_worldPos.y );
			fixed2 Left_Right = appendResult2002;
			float4 texArray3301 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult3284 ), _Texture_1_Normal_Index) , 0 );
			float3 weightedBlendVar6391 = BlendComponents;
			float weightedAvg6391 = ( ( weightedBlendVar6391.x*texArray3299.w + weightedBlendVar6391.y*texArray3300.w + weightedBlendVar6391.z*texArray3301.w )/( weightedBlendVar6391.x + weightedBlendVar6391.y + weightedBlendVar6391.z ) );
			fixed2 temp_cast_7 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5486 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3284 ) / temp_cast_7 ), _Texture_1_Normal_Index) , 0 );
			fixed2 temp_cast_8 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5489 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3284 ) / temp_cast_8 ), _Texture_1_Normal_Index) , 0 );
			float3 weightedBlendVar6394 = BlendComponents;
			float weightedAvg6394 = ( ( weightedBlendVar6394.x*texArray5486.w + weightedBlendVar6394.y*texArray5491.w + weightedBlendVar6394.z*texArray5489.w )/( weightedBlendVar6394.x + weightedBlendVar6394.y + weightedBlendVar6394.z ) );
			fixed ifLocalVar6609 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6609 = lerp( texArray3300.w , texArray5491.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6609 = lerp( weightedAvg6391 , weightedAvg6394 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6609 = lerp( texArray3300.w , texArray5491.w , UVmixDistance );
			fixed3 ifLocalVar7727 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar7727 = ifLocalVar6609;
			fixed3 Texture_1_H = ifLocalVar7727;
			fixed3 temp_cast_9 = (_Texture_2_Tesselation_Depth).xxx;
			float2 appendResult3349 = float2( ( 1.0 / _Texture_2_Tiling ) , ( 1.0 / _Texture_2_Tiling ) );
			float4 texArray3350 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Normal_Index) , 0 );
			fixed2 temp_cast_10 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5533 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_10 ), _Texture_2_Normal_Index) , 0 );
			float4 texArray3384 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult3349 ), _Texture_2_Normal_Index) , 0 );
			float4 texArray3351 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult3349 ), _Texture_2_Normal_Index) , 0 );
			float3 weightedBlendVar6399 = BlendComponents;
			float weightedAvg6399 = ( ( weightedBlendVar6399.x*texArray3384.w + weightedBlendVar6399.y*texArray3350.w + weightedBlendVar6399.z*texArray3351.w )/( weightedBlendVar6399.x + weightedBlendVar6399.y + weightedBlendVar6399.z ) );
			fixed2 temp_cast_11 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5530 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3349 ) / temp_cast_11 ), _Texture_2_Normal_Index) , 0 );
			fixed2 temp_cast_12 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5532 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3349 ) / temp_cast_12 ), _Texture_2_Normal_Index) , 0 );
			float3 weightedBlendVar6400 = BlendComponents;
			float weightedAvg6400 = ( ( weightedBlendVar6400.x*texArray5530.w + weightedBlendVar6400.y*texArray5533.w + weightedBlendVar6400.z*texArray5532.w )/( weightedBlendVar6400.x + weightedBlendVar6400.y + weightedBlendVar6400.z ) );
			fixed ifLocalVar6614 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6614 = lerp( texArray3350.w , texArray5533.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6614 = lerp( weightedAvg6399 , weightedAvg6400 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6614 = lerp( texArray3350.w , texArray5533.w , UVmixDistance );
			fixed3 ifLocalVar7731 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar7731 = ifLocalVar6614;
			fixed3 Texture_2_H = ifLocalVar7731;
			fixed3 temp_cast_13 = (_Texture_3_Tesselation_Depth).xxx;
			float2 appendResult3415 = float2( ( 1.0 / _Texture_3_Tiling ) , ( 1.0 / _Texture_3_Tiling ) );
			float4 texArray3416 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Normal_Index) , 0 );
			fixed2 temp_cast_14 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5586 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_14 ), _Texture_3_Normal_Index) , 0 );
			float4 texArray3445 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult3415 ), _Texture_3_Normal_Index) , 0 );
			float4 texArray3417 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult3415 ), _Texture_3_Normal_Index) , 0 );
			float3 weightedBlendVar6406 = BlendComponents;
			float weightedAvg6406 = ( ( weightedBlendVar6406.x*texArray3445.w + weightedBlendVar6406.y*texArray3416.w + weightedBlendVar6406.z*texArray3417.w )/( weightedBlendVar6406.x + weightedBlendVar6406.y + weightedBlendVar6406.z ) );
			fixed2 temp_cast_15 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5560 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3415 ) / temp_cast_15 ), _Texture_3_Normal_Index) , 0 );
			fixed2 temp_cast_16 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5572 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3415 ) / temp_cast_16 ), _Texture_3_Normal_Index) , 0 );
			float3 weightedBlendVar6407 = BlendComponents;
			float weightedAvg6407 = ( ( weightedBlendVar6407.x*texArray5560.w + weightedBlendVar6407.y*texArray5586.w + weightedBlendVar6407.z*texArray5572.w )/( weightedBlendVar6407.x + weightedBlendVar6407.y + weightedBlendVar6407.z ) );
			fixed ifLocalVar6620 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6620 = lerp( texArray3416.w , texArray5586.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6620 = lerp( weightedAvg6406 , weightedAvg6407 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6620 = lerp( texArray3416.w , texArray5586.w , UVmixDistance );
			fixed3 ifLocalVar7735 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Index > -1.0 )
				ifLocalVar7735 = ifLocalVar6620;
			fixed3 Texture_3_H = ifLocalVar7735;
			fixed3 temp_cast_17 = (_Texture_4_Tesselation_Depth).xxx;
			float2 appendResult3482 = float2( ( 1.0 / _Texture_4_Tiling ) , ( 1.0 / _Texture_4_Tiling ) );
			float4 texArray3483 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Normal_Index) , 0 );
			fixed2 temp_cast_18 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5615 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_18 ), _Texture_4_Normal_Index) , 0 );
			float4 texArray3512 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult3482 ), _Texture_4_Normal_Index) , 0 );
			float4 texArray3484 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult3482 ), _Texture_4_Normal_Index) , 0 );
			float3 weightedBlendVar6413 = BlendComponents;
			float weightedAvg6413 = ( ( weightedBlendVar6413.x*texArray3512.w + weightedBlendVar6413.y*texArray3483.w + weightedBlendVar6413.z*texArray3484.w )/( weightedBlendVar6413.x + weightedBlendVar6413.y + weightedBlendVar6413.z ) );
			fixed2 temp_cast_19 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5596 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3482 ) / temp_cast_19 ), _Texture_4_Normal_Index) , 0 );
			fixed2 temp_cast_20 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5604 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3482 ) / temp_cast_20 ), _Texture_4_Normal_Index) , 0 );
			float3 weightedBlendVar6414 = BlendComponents;
			float weightedAvg6414 = ( ( weightedBlendVar6414.x*texArray5596.w + weightedBlendVar6414.y*texArray5615.w + weightedBlendVar6414.z*texArray5604.w )/( weightedBlendVar6414.x + weightedBlendVar6414.y + weightedBlendVar6414.z ) );
			fixed ifLocalVar6626 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6626 = lerp( texArray3483.w , texArray5615.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6626 = lerp( weightedAvg6413 , weightedAvg6414 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6626 = lerp( texArray3483.w , texArray5615.w , UVmixDistance );
			fixed3 ifLocalVar7739 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Index > -1.0 )
				ifLocalVar7739 = ifLocalVar6626;
			fixed3 Texture_4_H = ifLocalVar7739;
			float4 layeredBlendVar6518 = tex2DNode4368;
			float3 layeredBlend6518 = ( lerp( lerp( lerp( lerp( 0.0 , ( temp_cast_5 * Texture_1_H ) , layeredBlendVar6518.x ) , ( temp_cast_9 * Texture_2_H ) , layeredBlendVar6518.y ) , ( temp_cast_13 * Texture_3_H ) , layeredBlendVar6518.z ) , ( temp_cast_17 * Texture_4_H ) , layeredBlendVar6518.w ) );
			fixed3 temp_cast_21 = (_Texture_5_Tesselation_Depth).xxx;
			float2 appendResult4399 = float2( ( 1.0 / _Texture_5_Tiling ) , ( 1.0 / _Texture_5_Tiling ) );
			float4 texArray4424 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Normal_Index) , 0 );
			fixed2 temp_cast_22 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5655 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_22 ), _Texture_5_Normal_Index) , 0 );
			float4 texArray4417 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4399 ), _Texture_5_Normal_Index) , 0 );
			float4 texArray4422 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4399 ), _Texture_5_Normal_Index) , 0 );
			float3 weightedBlendVar6420 = BlendComponents;
			float weightedAvg6420 = ( ( weightedBlendVar6420.x*texArray4417.w + weightedBlendVar6420.y*texArray4424.w + weightedBlendVar6420.z*texArray4422.w )/( weightedBlendVar6420.x + weightedBlendVar6420.y + weightedBlendVar6420.z ) );
			fixed2 temp_cast_23 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5636 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4399 ) / temp_cast_23 ), _Texture_5_Normal_Index) , 0 );
			fixed2 temp_cast_24 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5644 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4399 ) / temp_cast_24 ), _Texture_5_Normal_Index) , 0 );
			float3 weightedBlendVar6421 = BlendComponents;
			float weightedAvg6421 = ( ( weightedBlendVar6421.x*texArray5636.w + weightedBlendVar6421.y*texArray5655.w + weightedBlendVar6421.z*texArray5644.w )/( weightedBlendVar6421.x + weightedBlendVar6421.y + weightedBlendVar6421.z ) );
			fixed ifLocalVar6632 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6632 = lerp( texArray4424.w , texArray5655.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6632 = lerp( weightedAvg6420 , weightedAvg6421 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6632 = lerp( texArray4424.w , texArray5655.w , UVmixDistance );
			fixed3 ifLocalVar7743 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Index > -1.0 )
				ifLocalVar7743 = ifLocalVar6632;
			fixed3 Texture_5_H = ifLocalVar7743;
			fixed3 temp_cast_25 = (_Texture_6_Tesselation_Depth).xxx;
			float2 appendResult4471 = float2( ( 1.0 / _Texture_6_Tiling ) , ( 1.0 / _Texture_6_Tiling ) );
			float4 texArray4493 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Normal_Index) , 0 );
			fixed2 temp_cast_26 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5695 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_26 ), _Texture_6_Normal_Index) , 0 );
			float4 texArray4486 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4471 ), _Texture_6_Normal_Index) , 0 );
			float4 texArray4491 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4471 ), _Texture_6_Normal_Index) , 0 );
			float3 weightedBlendVar6427 = BlendComponents;
			float weightedAvg6427 = ( ( weightedBlendVar6427.x*texArray4486.w + weightedBlendVar6427.y*texArray4493.w + weightedBlendVar6427.z*texArray4491.w )/( weightedBlendVar6427.x + weightedBlendVar6427.y + weightedBlendVar6427.z ) );
			fixed2 temp_cast_27 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5676 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4471 ) / temp_cast_27 ), _Texture_6_Normal_Index) , 0 );
			fixed2 temp_cast_28 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5684 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4471 ) / temp_cast_28 ), _Texture_6_Normal_Index) , 0 );
			float3 weightedBlendVar6428 = BlendComponents;
			float weightedAvg6428 = ( ( weightedBlendVar6428.x*texArray5676.w + weightedBlendVar6428.y*texArray5695.w + weightedBlendVar6428.z*texArray5684.w )/( weightedBlendVar6428.x + weightedBlendVar6428.y + weightedBlendVar6428.z ) );
			fixed ifLocalVar6638 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6638 = lerp( texArray4493.w , texArray5695.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6638 = lerp( weightedAvg6427 , weightedAvg6428 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6638 = lerp( texArray4493.w , texArray5695.w , UVmixDistance );
			fixed3 ifLocalVar7748 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Index > -1.0 )
				ifLocalVar7748 = ifLocalVar6638;
			fixed3 Texture_6_H = ifLocalVar7748;
			fixed3 temp_cast_29 = (_Texture_7_Tesselation_Depth).xxx;
			float2 appendResult4545 = float2( ( 1.0 / _Texture_7_Tiling ) , ( 1.0 / _Texture_7_Tiling ) );
			float4 texArray4567 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Normal_Index) , 0 );
			fixed2 temp_cast_30 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5735 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_30 ), _Texture_7_Normal_Index) , 0 );
			float4 texArray4560 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4545 ), _Texture_7_Normal_Index) , 0 );
			float4 texArray4565 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4545 ), _Texture_7_Normal_Index) , 0 );
			float3 weightedBlendVar6434 = BlendComponents;
			float weightedAvg6434 = ( ( weightedBlendVar6434.x*texArray4560.w + weightedBlendVar6434.y*texArray4567.w + weightedBlendVar6434.z*texArray4565.w )/( weightedBlendVar6434.x + weightedBlendVar6434.y + weightedBlendVar6434.z ) );
			fixed2 temp_cast_31 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5716 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4545 ) / temp_cast_31 ), _Texture_7_Normal_Index) , 0 );
			fixed2 temp_cast_32 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5724 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4545 ) / temp_cast_32 ), _Texture_7_Normal_Index) , 0 );
			float3 weightedBlendVar6435 = BlendComponents;
			float weightedAvg6435 = ( ( weightedBlendVar6435.x*texArray5716.w + weightedBlendVar6435.y*texArray5735.w + weightedBlendVar6435.z*texArray5724.w )/( weightedBlendVar6435.x + weightedBlendVar6435.y + weightedBlendVar6435.z ) );
			fixed ifLocalVar6644 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6644 = lerp( texArray4567.w , texArray5735.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6644 = lerp( weightedAvg6434 , weightedAvg6435 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6644 = lerp( texArray4567.w , texArray5735.w , UVmixDistance );
			fixed3 ifLocalVar7752 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Index > -1.0 )
				ifLocalVar7752 = ifLocalVar6644;
			fixed3 Texture_7_H = ifLocalVar7752;
			fixed3 temp_cast_33 = (_Texture_8_Tesselation_Depth).xxx;
			float2 appendResult4619 = float2( ( 1.0 / _Texture_8_Tiling ) , ( 1.0 / _Texture_8_Tiling ) );
			float4 texArray4641 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Normal_Index) , 0 );
			fixed2 temp_cast_34 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5775 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_34 ), _Texture_8_Normal_Index) , 0 );
			float4 texArray4634 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4619 ), _Texture_8_Normal_Index) , 0 );
			float4 texArray4639 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4619 ), _Texture_8_Normal_Index) , 0 );
			float3 weightedBlendVar6441 = BlendComponents;
			float weightedAvg6441 = ( ( weightedBlendVar6441.x*texArray4634.w + weightedBlendVar6441.y*texArray4641.w + weightedBlendVar6441.z*texArray4639.w )/( weightedBlendVar6441.x + weightedBlendVar6441.y + weightedBlendVar6441.z ) );
			fixed2 temp_cast_35 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5756 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4619 ) / temp_cast_35 ), _Texture_8_Normal_Index) , 0 );
			fixed2 temp_cast_36 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5764 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4619 ) / temp_cast_36 ), _Texture_8_Normal_Index) , 0 );
			float3 weightedBlendVar6442 = BlendComponents;
			float weightedAvg6442 = ( ( weightedBlendVar6442.x*texArray5756.w + weightedBlendVar6442.y*texArray5775.w + weightedBlendVar6442.z*texArray5764.w )/( weightedBlendVar6442.x + weightedBlendVar6442.y + weightedBlendVar6442.z ) );
			fixed ifLocalVar6650 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6650 = lerp( texArray4641.w , texArray5775.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6650 = lerp( weightedAvg6441 , weightedAvg6442 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6650 = lerp( texArray4641.w , texArray5775.w , UVmixDistance );
			fixed3 ifLocalVar7756 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Index > -1.0 )
				ifLocalVar7756 = ifLocalVar6650;
			fixed3 Texture_8_H = ifLocalVar7756;
			float4 layeredBlendVar6525 = tex2DNode4369;
			float3 layeredBlend6525 = ( lerp( lerp( lerp( lerp( layeredBlend6518 , ( temp_cast_21 * Texture_5_H ) , layeredBlendVar6525.x ) , ( temp_cast_25 * Texture_6_H ) , layeredBlendVar6525.y ) , ( temp_cast_29 * Texture_7_H ) , layeredBlendVar6525.z ) , ( temp_cast_33 * Texture_8_H ) , layeredBlendVar6525.w ) );
			fixed4 temp_cast_38 = (_Texture_9_Tesselation_Depth).xxxx;
			float2 appendResult4736 = float2( ( 1.0 / _Texture_9_Tiling ) , ( 1.0 / _Texture_9_Tiling ) );
			float4 texArray4788 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Normal_Index) , 0 );
			fixed2 temp_cast_39 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5811 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_39 ), _Texture_9_Normal_Index) , 0 );
			float4 texArray5285 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4736 ), _Texture_9_Normal_Index) , 0 );
			float4 texArray4783 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4736 ), _Texture_9_Normal_Index) , 0 );
			float3 weightedBlendVar6448 = BlendComponents;
			float weightedAvg6448 = ( ( weightedBlendVar6448.x*texArray5285.w + weightedBlendVar6448.y*texArray4788.w + weightedBlendVar6448.z*texArray4783.w )/( weightedBlendVar6448.x + weightedBlendVar6448.y + weightedBlendVar6448.z ) );
			fixed2 temp_cast_40 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5796 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4736 ) / temp_cast_40 ), _Texture_9_Normal_Index) , 0 );
			fixed2 temp_cast_41 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5806 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4736 ) / temp_cast_41 ), _Texture_9_Normal_Index) , 0 );
			float3 weightedBlendVar6449 = BlendComponents;
			float weightedAvg6449 = ( ( weightedBlendVar6449.x*texArray5796.w + weightedBlendVar6449.y*texArray5811.w + weightedBlendVar6449.z*texArray5806.w )/( weightedBlendVar6449.x + weightedBlendVar6449.y + weightedBlendVar6449.z ) );
			fixed ifLocalVar6668 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6668 = lerp( texArray4788.w , texArray5811.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar6668 = lerp( weightedAvg6448 , weightedAvg6449 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6668 = lerp( texArray4788.w , texArray5811.w , UVmixDistance );
			fixed4 ifLocalVar7721 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar7721 = ifLocalVar6668;
			fixed4 Texture_9_H = ifLocalVar7721;
			fixed4 temp_cast_42 = (_Texture_10_Tesselation_Depth).xxxx;
			float2 appendResult4738 = float2( ( 1.0 / _Texture_10_Tiling ) , ( 1.0 / _Texture_10_Tiling ) );
			float4 texArray4822 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Normal_Index) , 0 );
			fixed2 temp_cast_43 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5851 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_43 ), _Texture_10_Normal_Index) , 0 );
			float4 texArray4798 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4738 ), _Texture_10_Normal_Index) , 0 );
			float4 texArray4791 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4738 ), _Texture_10_Normal_Index) , 0 );
			float3 weightedBlendVar6455 = BlendComponents;
			float weightedAvg6455 = ( ( weightedBlendVar6455.x*texArray4798.w + weightedBlendVar6455.y*texArray4822.w + weightedBlendVar6455.z*texArray4791.w )/( weightedBlendVar6455.x + weightedBlendVar6455.y + weightedBlendVar6455.z ) );
			fixed2 temp_cast_44 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5836 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4738 ) / temp_cast_44 ), _Texture_10_Normal_Index) , 0 );
			fixed2 temp_cast_45 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5846 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4738 ) / temp_cast_45 ), _Texture_10_Normal_Index) , 0 );
			float3 weightedBlendVar6456 = BlendComponents;
			float weightedAvg6456 = ( ( weightedBlendVar6456.x*texArray5836.w + weightedBlendVar6456.y*texArray5851.w + weightedBlendVar6456.z*texArray5846.w )/( weightedBlendVar6456.x + weightedBlendVar6456.y + weightedBlendVar6456.z ) );
			fixed ifLocalVar6662 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6662 = lerp( texArray4822.w , texArray5851.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6662 = lerp( weightedAvg6455 , weightedAvg6456 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6662 = lerp( texArray4822.w , texArray5851.w , UVmixDistance );
			fixed4 ifLocalVar7717 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7717 = ifLocalVar6662;
			fixed4 Texture_10_H = ifLocalVar7717;
			fixed4 temp_cast_46 = (_Texture_11_Tesselation_Depth).xxxx;
			float2 appendResult4741 = float2( ( 1.0 / _Texture_11_Tiling ) , ( 1.0 / _Texture_11_Tiling ) );
			float4 texArray4856 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Normal_Index) , 0 );
			fixed2 temp_cast_47 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5891 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_47 ), _Texture_11_Normal_Index) , 0 );
			float4 texArray4828 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4741 ), _Texture_11_Normal_Index) , 0 );
			float4 texArray4811 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4741 ), _Texture_11_Normal_Index) , 0 );
			float3 weightedBlendVar6462 = BlendComponents;
			float weightedAvg6462 = ( ( weightedBlendVar6462.x*texArray4828.w + weightedBlendVar6462.y*texArray4856.w + weightedBlendVar6462.z*texArray4811.w )/( weightedBlendVar6462.x + weightedBlendVar6462.y + weightedBlendVar6462.z ) );
			fixed2 temp_cast_48 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5876 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4741 ) / temp_cast_48 ), _Texture_11_Normal_Index) , 0 );
			fixed2 temp_cast_49 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5886 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4741 ) / temp_cast_49 ), _Texture_11_Normal_Index) , 0 );
			float3 weightedBlendVar6463 = BlendComponents;
			float weightedAvg6463 = ( ( weightedBlendVar6463.x*texArray5876.w + weightedBlendVar6463.y*texArray5891.w + weightedBlendVar6463.z*texArray5886.w )/( weightedBlendVar6463.x + weightedBlendVar6463.y + weightedBlendVar6463.z ) );
			fixed ifLocalVar6656 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6656 = lerp( texArray4856.w , texArray5891.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6656 = lerp( weightedAvg6462 , weightedAvg6463 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6656 = lerp( texArray4856.w , texArray5891.w , UVmixDistance );
			fixed4 ifLocalVar7715 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Index > -1.0 )
				ifLocalVar7715 = ifLocalVar6656;
			fixed4 Texture_11_H = ifLocalVar7715;
			fixed4 temp_cast_50 = (_Texture_12_Tesselation_Depth).xxxx;
			float2 appendResult4751 = float2( ( 1.0 / _Texture_12_Tiling ) , ( 1.0 / _Texture_12_Tiling ) );
			float4 texArray4870 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Normal_Index) , 0 );
			fixed2 temp_cast_51 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5931 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_51 ), _Texture_12_Normal_Index) , 0 );
			float4 texArray4850 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult4751 ), _Texture_12_Normal_Index) , 0 );
			float4 texArray4852 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult4751 ), _Texture_12_Normal_Index) , 0 );
			float3 weightedBlendVar6469 = BlendComponents;
			float weightedAvg6469 = ( ( weightedBlendVar6469.x*texArray4850.w + weightedBlendVar6469.y*texArray4870.w + weightedBlendVar6469.z*texArray4852.w )/( weightedBlendVar6469.x + weightedBlendVar6469.y + weightedBlendVar6469.z ) );
			fixed2 temp_cast_52 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5916 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4751 ) / temp_cast_52 ), _Texture_12_Normal_Index) , 0 );
			fixed2 temp_cast_53 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5926 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4751 ) / temp_cast_53 ), _Texture_12_Normal_Index) , 0 );
			float3 weightedBlendVar6470 = BlendComponents;
			float weightedAvg6470 = ( ( weightedBlendVar6470.x*texArray5916.w + weightedBlendVar6470.y*texArray5931.w + weightedBlendVar6470.z*texArray5926.w )/( weightedBlendVar6470.x + weightedBlendVar6470.y + weightedBlendVar6470.z ) );
			fixed ifLocalVar6674 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6674 = lerp( texArray4870.w , texArray5931.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6674 = lerp( weightedAvg6469 , weightedAvg6470 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6674 = lerp( texArray4870.w , texArray5931.w , UVmixDistance );
			fixed4 ifLocalVar7708 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Index > -1.0 )
				ifLocalVar7708 = ifLocalVar6674;
			fixed4 Texture_12_H = ifLocalVar7708;
			float4 layeredBlendVar6527 = tex2DNode4370;
			float4 layeredBlend6527 = ( lerp( lerp( lerp( lerp( fixed4( layeredBlend6525 , 0.0 ) , ( temp_cast_38 * Texture_9_H ) , layeredBlendVar6527.x ) , ( temp_cast_42 * Texture_10_H ) , layeredBlendVar6527.y ) , ( temp_cast_46 * Texture_11_H ) , layeredBlendVar6527.z ) , ( temp_cast_50 * Texture_12_H ) , layeredBlendVar6527.w ) );
			fixed4 temp_cast_54 = (_Texture_13_Tesselation_Depth).xxxx;
			float2 appendResult5027 = float2( ( 1.0 / _Texture_13_Tiling ) , ( 1.0 / _Texture_13_Tiling ) );
			float4 texArray5120 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Normal_Index) , 0 );
			fixed2 temp_cast_55 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5971 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_55 ), _Texture_13_Normal_Index) , 0 );
			float4 texArray5127 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult5027 ), _Texture_13_Normal_Index) , 0 );
			float4 texArray5109 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult5027 ), _Texture_13_Normal_Index) , 0 );
			float3 weightedBlendVar6476 = BlendComponents;
			float weightedAvg6476 = ( ( weightedBlendVar6476.x*texArray5127.w + weightedBlendVar6476.y*texArray5120.w + weightedBlendVar6476.z*texArray5109.w )/( weightedBlendVar6476.x + weightedBlendVar6476.y + weightedBlendVar6476.z ) );
			fixed2 temp_cast_56 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5956 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5027 ) / temp_cast_56 ), _Texture_13_Normal_Index) , 0 );
			fixed2 temp_cast_57 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5966 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5027 ) / temp_cast_57 ), _Texture_13_Normal_Index) , 0 );
			float3 weightedBlendVar6477 = BlendComponents;
			float weightedAvg6477 = ( ( weightedBlendVar6477.x*texArray5956.w + weightedBlendVar6477.y*texArray5971.w + weightedBlendVar6477.z*texArray5966.w )/( weightedBlendVar6477.x + weightedBlendVar6477.y + weightedBlendVar6477.z ) );
			fixed ifLocalVar6680 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6680 = lerp( texArray5120.w , texArray5971.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6680 = lerp( weightedAvg6476 , weightedAvg6477 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6680 = lerp( texArray5120.w , texArray5971.w , UVmixDistance );
			fixed4 ifLocalVar7704 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Index > -1.0 )
				ifLocalVar7704 = ifLocalVar6680;
			fixed4 Texture_13_H = ifLocalVar7704;
			fixed4 temp_cast_58 = (_Texture_14_Tesselation_Depth).xxxx;
			float2 appendResult5033 = float2( ( 1.0 / _Texture_14_Tiling ) , ( 1.0 / _Texture_14_Tiling ) );
			float4 texArray5178 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Normal_Index) , 0 );
			fixed2 temp_cast_59 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6011 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_59 ), _Texture_14_Normal_Index) , 0 );
			float4 texArray5017 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult5033 ), _Texture_14_Normal_Index) , 0 );
			float4 texArray5170 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult5033 ), _Texture_14_Normal_Index) , 0 );
			float3 weightedBlendVar6483 = BlendComponents;
			float weightedAvg6483 = ( ( weightedBlendVar6483.x*texArray5017.w + weightedBlendVar6483.y*texArray5178.w + weightedBlendVar6483.z*texArray5170.w )/( weightedBlendVar6483.x + weightedBlendVar6483.y + weightedBlendVar6483.z ) );
			fixed2 temp_cast_60 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5996 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5033 ) / temp_cast_60 ), _Texture_14_Normal_Index) , 0 );
			fixed2 temp_cast_61 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6006 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5033 ) / temp_cast_61 ), _Texture_14_Normal_Index) , 0 );
			float3 weightedBlendVar6484 = BlendComponents;
			float weightedAvg6484 = ( ( weightedBlendVar6484.x*texArray5996.w + weightedBlendVar6484.y*texArray6011.w + weightedBlendVar6484.z*texArray6006.w )/( weightedBlendVar6484.x + weightedBlendVar6484.y + weightedBlendVar6484.z ) );
			fixed ifLocalVar6686 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6686 = lerp( texArray5178.w , texArray6011.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6686 = lerp( weightedAvg6483 , weightedAvg6484 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6686 = lerp( texArray5178.w , texArray6011.w , UVmixDistance );
			fixed4 ifLocalVar7700 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar7700 = ifLocalVar6686;
			fixed4 Texture_14_H = ifLocalVar7700;
			fixed4 temp_cast_62 = (_Texture_15_Tesselation_Depth).xxxx;
			float2 appendResult5212 = float2( ( 1.0 / _Texture_15_Tiling ) , ( 1.0 / _Texture_15_Tiling ) );
			float4 texArray5246 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Normal_Index) , 0 );
			fixed2 temp_cast_63 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6051 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_63 ), _Texture_15_Normal_Index) , 0 );
			float4 texArray5227 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult5212 ), _Texture_15_Normal_Index) , 0 );
			float4 texArray5250 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult5212 ), _Texture_15_Normal_Index) , 0 );
			float3 weightedBlendVar6490 = BlendComponents;
			float weightedAvg6490 = ( ( weightedBlendVar6490.x*texArray5227.w + weightedBlendVar6490.y*texArray5246.w + weightedBlendVar6490.z*texArray5250.w )/( weightedBlendVar6490.x + weightedBlendVar6490.y + weightedBlendVar6490.z ) );
			fixed2 temp_cast_64 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6036 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5212 ) / temp_cast_64 ), _Texture_15_Normal_Index) , 0 );
			fixed2 temp_cast_65 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6046 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5212 ) / temp_cast_65 ), _Texture_15_Normal_Index) , 0 );
			float3 weightedBlendVar6491 = BlendComponents;
			float weightedAvg6491 = ( ( weightedBlendVar6491.x*texArray6036.w + weightedBlendVar6491.y*texArray6051.w + weightedBlendVar6491.z*texArray6046.w )/( weightedBlendVar6491.x + weightedBlendVar6491.y + weightedBlendVar6491.z ) );
			fixed ifLocalVar6692 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6692 = lerp( texArray5246.w , texArray6051.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6692 = lerp( weightedAvg6490 , weightedAvg6491 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6692 = lerp( texArray5246.w , texArray6051.w , UVmixDistance );
			fixed4 ifLocalVar7696 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar7696 = ifLocalVar6692;
			fixed4 Texture_15_H = ifLocalVar7696;
			fixed4 temp_cast_66 = (_Texture_16_Tesselation_Depth).xxxx;
			float2 appendResult5078 = float2( ( 1.0 / _Texture_16_Tiling ) , ( 1.0 / _Texture_16_Tiling ) );
			float4 texArray5099 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Normal_Index) , 0 );
			fixed2 temp_cast_67 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6091 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_67 ), _Texture_16_Normal_Index) , 0 );
			float4 texArray5082 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Front_Back * appendResult5078 ), _Texture_16_Normal_Index) , 0 );
			float4 texArray4731 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( Left_Right * appendResult5078 ), _Texture_16_Normal_Index) , 0 );
			float3 weightedBlendVar6497 = BlendComponents;
			float weightedAvg6497 = ( ( weightedBlendVar6497.x*texArray5082.w + weightedBlendVar6497.y*texArray5099.w + weightedBlendVar6497.z*texArray4731.w )/( weightedBlendVar6497.x + weightedBlendVar6497.y + weightedBlendVar6497.z ) );
			fixed2 temp_cast_68 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6076 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5078 ) / temp_cast_68 ), _Texture_16_Normal_Index) , 0 );
			fixed2 temp_cast_69 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6086 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5078 ) / temp_cast_69 ), _Texture_16_Normal_Index) , 0 );
			float3 weightedBlendVar6498 = BlendComponents;
			float weightedAvg6498 = ( ( weightedBlendVar6498.x*texArray6076.w + weightedBlendVar6498.y*texArray6091.w + weightedBlendVar6498.z*texArray6086.w )/( weightedBlendVar6498.x + weightedBlendVar6498.y + weightedBlendVar6498.z ) );
			fixed ifLocalVar6698 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6698 = lerp( texArray5099.w , texArray6091.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6698 = lerp( weightedAvg6497 , weightedAvg6498 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6698 = lerp( texArray5099.w , texArray6091.w , UVmixDistance );
			fixed4 ifLocalVar7693 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar7693 = ifLocalVar6698;
			fixed4 Texture_16_H = ifLocalVar7693;
			float4 layeredBlendVar6530 = tex2DNode4371;
			float4 layeredBlend6530 = ( lerp( lerp( lerp( lerp( layeredBlend6527 , ( temp_cast_54 * Texture_13_H ) , layeredBlendVar6530.x ) , ( temp_cast_58 * Texture_14_H ) , layeredBlendVar6530.y ) , ( temp_cast_62 * Texture_15_H ) , layeredBlendVar6530.z ) , ( temp_cast_66 * Texture_16_H ) , layeredBlendVar6530.w ) );
			float2 appendResult3679 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_70 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_71 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray6270 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( appendResult3679 * temp_cast_70 ) / temp_cast_71 ), _Texture_Snow_Normal_Index) , 0 );
			float4 layeredBlendVar7548 = tex2DNode4368;
			float layeredBlend7548 = ( lerp( lerp( lerp( lerp( 0.0 , ( _Texture_1_Tesselation_Depth * _Texture_1_Heightmap_MaxHeight ) , layeredBlendVar7548.x ) , ( _Texture_2_Heightmap_MaxHeight * _Texture_2_Tesselation_Depth ) , layeredBlendVar7548.y ) , ( _Texture_3_Heightmap_MaxHeight * _Texture_3_Tesselation_Depth ) , layeredBlendVar7548.z ) , ( _Texture_4_Heightmap_MaxHeight * _Texture_4_Tesselation_Depth ) , layeredBlendVar7548.w ) );
			float4 layeredBlendVar7549 = tex2DNode4369;
			float layeredBlend7549 = ( lerp( lerp( lerp( lerp( layeredBlend7548 , ( _Texture_5_Heightmap_MaxHeight * _Texture_5_Tesselation_Depth ) , layeredBlendVar7549.x ) , ( _Texture_6_Heightmap_MaxHeight * _Texture_6_Tesselation_Depth ) , layeredBlendVar7549.y ) , ( _Texture_7_Heightmap_MaxHeight * _Texture_7_Tesselation_Depth ) , layeredBlendVar7549.z ) , ( _Texture_8_Heightmap_MaxHeight * _Texture_8_Tesselation_Depth ) , layeredBlendVar7549.w ) );
			float4 layeredBlendVar7550 = tex2DNode4371;
			float layeredBlend7550 = ( lerp( lerp( lerp( lerp( layeredBlend7549 , ( _Texture_10_Heightmap_MaxHeight * _Texture_10_Tesselation_Depth ) , layeredBlendVar7550.x ) , ( _Texture_9_Heightmap_MaxHeight * _Texture_9_Tesselation_Depth ) , layeredBlendVar7550.y ) , ( _Texture_11_Heightmap_MaxHeight * _Texture_11_Tesselation_Depth ) , layeredBlendVar7550.z ) , ( _Texture_12_Heightmap_MaxHeight * _Texture_12_Tesselation_Depth ) , layeredBlendVar7550.w ) );
			float4 layeredBlendVar7551 = tex2DNode4370;
			float layeredBlend7551 = ( lerp( lerp( lerp( lerp( layeredBlend7550 , ( _Texture_13_Heightmap_MaxHeight * _Texture_13_Tesselation_Depth ) , layeredBlendVar7551.x ) , ( _Texture_14_Heightmap_MaxHeight * _Texture_14_Tesselation_Depth ) , layeredBlendVar7551.y ) , ( _Texture_15_Heightmap_MaxHeight * _Texture_15_Tesselation_Depth ) , layeredBlendVar7551.z ) , ( _Texture_16_Heightmap_MaxHeight * _Texture_16_Tesselation_Depth ) , layeredBlendVar7551.w ) );
			float2 appendResult3750 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_72 = (_Snow_Noise_Tiling).xx;
			float4 texArray4383 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( appendResult3750 * temp_cast_72 ), (float)_Texture_Snow_Noise_Index) , 0 );
			fixed2 temp_cast_74 = (_Snow_Noise_Tiling).xx;
			float4 texArray4385 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_74 ) * float2( 0.23,0.23 ) ), (float)_Texture_Snow_Noise_Index) , 0 );
			fixed2 temp_cast_76 = (_Snow_Noise_Tiling).xx;
			float4 texArray4384 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_76 ) * float2( 0.53,0.53 ) ), (float)_Texture_Snow_Noise_Index) , 0 );
			fixed SnowSlope = ( 1.0 - ( clamp( ( clamp( normalWorld.y , 0.0 , 0.9999 ) - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) , 0.0 , 2.0 ) * ( 1.0 / ( _Snow_Maximum_Angle / 90.0 ) ) ) );
			fixed Splat4_A = tex2DNode4371.a;
			fixed Splat4_B = tex2DNode4371.b;
			fixed Splat4_R = tex2DNode4371.r;
			fixed Splat3_A = tex2DNode4370.a;
			fixed Splat3_B = tex2DNode4370.b;
			fixed Splat3_R = tex2DNode4370.r;
			fixed Splat2_A = tex2DNode4369.a;
			fixed Splat2_B = tex2DNode4369.b;
			fixed Splat2_R = tex2DNode4369.r;
			fixed Splat1_R = tex2DNode4368.r;
			fixed Splat1_G = tex2DNode4368.g;
			fixed Splat1_B = tex2DNode4368.b;
			fixed Splat1_A = tex2DNode4368.a;
			fixed Splat2_G = tex2DNode4369.g;
			fixed Splat3_G = tex2DNode4370.g;
			fixed Splat4_G = tex2DNode4371.g;
			fixed4 temp_cast_78 = (lerp( ( ( ( _Snow_Tesselation_Depth * texArray6270.w ) + layeredBlend7551 ) * ( _Snow_Amount * 0.5 ) ) , 0.0 , ( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ) )).xxxx;
			fixed2 temp_cast_79 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray6267 = UNITY_SAMPLE_TEX2DARRAY_LOD(_Texture_Array_Normal, float3(( appendResult3679 * temp_cast_79 ), _Texture_Snow_Normal_Index) , 0 );
			fixed ifLocalVar7824 = 0;
			UNITY_BRANCH if( _Snow_Emission_AO_index > -1.0 )
				ifLocalVar7824 = lerp( ( pow( texArray6267.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , ( pow( texArray6270.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , UVmixDistance );
			fixed4 temp_cast_80 = (ifLocalVar7824).xxxx;
			fixed4 temp_cast_81 = (ifLocalVar7824).xxxx;
			float HeightMask6539 = saturate(pow(((( 1.0 - clamp( clamp( ( layeredBlend6530 + temp_cast_80 ) , float4( 0.0,0,0,0 ) , ( layeredBlend6530 + temp_cast_81 ) ) , float4( 0.0,0,0,0 ) , float4( 1.0,0,0,0 ) ) ).x*( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ))*4)+(( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) )*2),lerp( _Snow_Heightblend_Close , _Snow_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_83 = (ifLocalVar7824).xxxx;
			fixed4 temp_cast_84 = (ifLocalVar7824).xxxx;
			float HeightMask7581 = saturate(pow(((( 1.0 - clamp( clamp( ( layeredBlend6530 + temp_cast_83 ) , float4( 0.0,0,0,0 ) , ( layeredBlend6530 + temp_cast_84 ) ) , float4( 0.0,0,0,0 ) , float4( 1.0,0,0,0 ) ) ).x*( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ))*4)+(( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) )*2),clamp( lerp( _Snow_Heightblend_Close , _Snow_Heightblend_Far , UVmixDistance ) , 0.0 , 1.0 )));;
			fixed4 temp_cast_86 = (lerp( ( ( ( _Snow_Tesselation_Depth * texArray6270.w ) + layeredBlend7551 ) * ( _Snow_Amount * 0.5 ) ) , 0.0 , ( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ) )).xxxx;
			fixed4 ifLocalVar7633 = 0;
			UNITY_BRANCH if( distance( ase_worldPos , _WorldSpaceCameraPos ) > _Tess_Distance )
				ifLocalVar7633 = fixed4(0,0,0,0);
			else UNITY_BRANCH if( distance( ase_worldPos , _WorldSpaceCameraPos ) == _Tess_Distance )
				ifLocalVar7633 = lerp( layeredBlend6530 , max( layeredBlend6530 , temp_cast_78 ) , lerp( HeightMask6539 , HeightMask7581 , clamp( ( ( _Snow_Amount * 75.0 ) + ( 75.0 * -1.2 ) ) , 0.0 , 1.0 ) ) );
			else UNITY_BRANCH if( distance( ase_worldPos , _WorldSpaceCameraPos ) < _Tess_Distance )
				ifLocalVar7633 = lerp( layeredBlend6530 , max( layeredBlend6530 , temp_cast_86 ) , lerp( HeightMask6539 , HeightMask7581 , clamp( ( ( _Snow_Amount * 75.0 ) + ( 75.0 * -1.2 ) ) , 0.0 , 1.0 ) ) );
			v.vertex.xyz += ( ifLocalVar7633 * fixed4( normalWorld , 0.0 ) );
			 v.tangent.xyz=fixed4( cross( normalWorld , fixed3(0,0,1) ) , 0.0 );
			 v.tangent.w = -1;//.xyz;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult3897 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_0 = (( 1.0 / _Perlin_Normal_Tiling_Close )).xx;
			float4 texArray6256 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_0 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g853 = float2( texArray6256.x , texArray6256.y );
			float2 temp_cast_2 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_output_4_0_g853 = ( ( ( appendResult11_g853 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_2 );
			float2 temp_cast_3 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_cast_4 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_cast_5 = (_Perlin_Normal_Power_Close).xx;
			float temp_output_9_0_g853 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g853 , temp_output_4_0_g853 ) ) ) );
			float3 appendResult10_g853 = float3( temp_output_4_0_g853.x , temp_output_4_0_g853.y , temp_output_9_0_g853 );
			fixed2 temp_cast_6 = (( 1.0 / _Perlin_Normal_Tiling_Far )).xx;
			float4 texArray4374 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_6 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g854 = float2( texArray4374.x , texArray4374.y );
			float2 temp_cast_8 = (_Perlin_Normal_Power).xx;
			float2 temp_output_4_0_g854 = ( ( ( appendResult11_g854 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_8 );
			float2 temp_cast_9 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_10 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_11 = (_Perlin_Normal_Power).xx;
			float temp_output_9_0_g854 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g854 , temp_output_4_0_g854 ) ) ) );
			float3 appendResult10_g854 = float3( temp_output_4_0_g854.x , temp_output_4_0_g854.y , temp_output_9_0_g854 );
			fixed UVmixDistance = clamp( pow( ( distance( ase_worldPos , _WorldSpaceCameraPos ) / _UV_Mix_Start_Distance ) , _UV_Mix_Power ) , 0.0 , 1.0 );
			float2 texCoordDummy2588 = i.uv_DummyTex*float2( 1,1 ) + float2( 0,0 );
			fixed4 tex2DNode4371 = tex2D( _Texture_Splat_4, texCoordDummy2588 );
			fixed Splat4_A = tex2DNode4371.a;
			fixed Splat4_B = tex2DNode4371.b;
			fixed Splat4_G = tex2DNode4371.g;
			fixed Splat4_R = tex2DNode4371.r;
			fixed4 tex2DNode4370 = tex2D( _Texture_Splat_3, texCoordDummy2588 );
			fixed Splat3_A = tex2DNode4370.a;
			fixed Splat3_B = tex2DNode4370.b;
			fixed Splat3_G = tex2DNode4370.g;
			fixed Splat3_R = tex2DNode4370.r;
			fixed4 tex2DNode4369 = tex2D( _Texture_Splat_2, texCoordDummy2588 );
			fixed Splat2_A = tex2DNode4369.a;
			fixed Splat2_B = tex2DNode4369.b;
			fixed Splat2_G = tex2DNode4369.g;
			fixed Splat2_R = tex2DNode4369.r;
			fixed4 tex2DNode4368 = tex2D( _Texture_Splat_1, texCoordDummy2588 );
			fixed Splat1_R = tex2DNode4368.r;
			fixed Splat1_G = tex2DNode4368.g;
			fixed Splat1_A = tex2DNode4368.a;
			fixed Splat1_B = tex2DNode4368.b;
			fixed3 temp_cast_12 = (_Texture_1_Tesselation_Depth).xxx;
			float2 appendResult1998 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 Top_Bottom = appendResult1998;
			float2 appendResult3284 = float2( ( 1.0 / _Texture_1_Tiling ) , ( 1.0 / _Texture_1_Tiling ) );
			float4 texArray3300 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Normal_Index)  );
			fixed2 temp_cast_13 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5491 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_13 ), _Texture_1_Normal_Index)  );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			fixed3 BlendComponents = clamp( pow( ( ase_worldNormal * ase_worldNormal ) , 25.0 ) , float3( -1,-1,-1 ) , float3( 1,1,1 ) );
			float2 appendResult879 = float2( ase_worldPos.z , ase_worldPos.y );
			fixed2 Front_Back = appendResult879;
			float4 texArray3299 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3284 ), _Texture_1_Normal_Index)  );
			float2 appendResult2002 = float2( ase_worldPos.x , ase_worldPos.y );
			fixed2 Left_Right = appendResult2002;
			float4 texArray3301 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3284 ), _Texture_1_Normal_Index)  );
			float3 weightedBlendVar6391 = BlendComponents;
			float weightedAvg6391 = ( ( weightedBlendVar6391.x*texArray3299.w + weightedBlendVar6391.y*texArray3300.w + weightedBlendVar6391.z*texArray3301.w )/( weightedBlendVar6391.x + weightedBlendVar6391.y + weightedBlendVar6391.z ) );
			fixed2 temp_cast_14 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3284 ) / temp_cast_14 ), _Texture_1_Normal_Index)  );
			fixed2 temp_cast_15 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5489 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3284 ) / temp_cast_15 ), _Texture_1_Normal_Index)  );
			float3 weightedBlendVar6394 = BlendComponents;
			float weightedAvg6394 = ( ( weightedBlendVar6394.x*texArray5486.w + weightedBlendVar6394.y*texArray5491.w + weightedBlendVar6394.z*texArray5489.w )/( weightedBlendVar6394.x + weightedBlendVar6394.y + weightedBlendVar6394.z ) );
			fixed ifLocalVar6609 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6609 = lerp( texArray3300.w , texArray5491.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6609 = lerp( weightedAvg6391 , weightedAvg6394 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6609 = lerp( texArray3300.w , texArray5491.w , UVmixDistance );
			fixed3 ifLocalVar7727 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar7727 = ifLocalVar6609;
			fixed3 Texture_1_H = ifLocalVar7727;
			fixed3 temp_cast_16 = (_Texture_2_Tesselation_Depth).xxx;
			float2 appendResult3349 = float2( ( 1.0 / _Texture_2_Tiling ) , ( 1.0 / _Texture_2_Tiling ) );
			float4 texArray3350 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Normal_Index)  );
			fixed2 temp_cast_17 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5533 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_17 ), _Texture_2_Normal_Index)  );
			float4 texArray3384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3349 ), _Texture_2_Normal_Index)  );
			float4 texArray3351 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3349 ), _Texture_2_Normal_Index)  );
			float3 weightedBlendVar6399 = BlendComponents;
			float weightedAvg6399 = ( ( weightedBlendVar6399.x*texArray3384.w + weightedBlendVar6399.y*texArray3350.w + weightedBlendVar6399.z*texArray3351.w )/( weightedBlendVar6399.x + weightedBlendVar6399.y + weightedBlendVar6399.z ) );
			fixed2 temp_cast_18 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5530 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3349 ) / temp_cast_18 ), _Texture_2_Normal_Index)  );
			fixed2 temp_cast_19 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5532 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3349 ) / temp_cast_19 ), _Texture_2_Normal_Index)  );
			float3 weightedBlendVar6400 = BlendComponents;
			float weightedAvg6400 = ( ( weightedBlendVar6400.x*texArray5530.w + weightedBlendVar6400.y*texArray5533.w + weightedBlendVar6400.z*texArray5532.w )/( weightedBlendVar6400.x + weightedBlendVar6400.y + weightedBlendVar6400.z ) );
			fixed ifLocalVar6614 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6614 = lerp( texArray3350.w , texArray5533.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6614 = lerp( weightedAvg6399 , weightedAvg6400 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6614 = lerp( texArray3350.w , texArray5533.w , UVmixDistance );
			fixed3 ifLocalVar7731 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar7731 = ifLocalVar6614;
			fixed3 Texture_2_H = ifLocalVar7731;
			fixed3 temp_cast_20 = (_Texture_3_Tesselation_Depth).xxx;
			float2 appendResult3415 = float2( ( 1.0 / _Texture_3_Tiling ) , ( 1.0 / _Texture_3_Tiling ) );
			float4 texArray3416 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Normal_Index)  );
			fixed2 temp_cast_21 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_21 ), _Texture_3_Normal_Index)  );
			float4 texArray3445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3415 ), _Texture_3_Normal_Index)  );
			float4 texArray3417 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3415 ), _Texture_3_Normal_Index)  );
			float3 weightedBlendVar6406 = BlendComponents;
			float weightedAvg6406 = ( ( weightedBlendVar6406.x*texArray3445.w + weightedBlendVar6406.y*texArray3416.w + weightedBlendVar6406.z*texArray3417.w )/( weightedBlendVar6406.x + weightedBlendVar6406.y + weightedBlendVar6406.z ) );
			fixed2 temp_cast_22 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5560 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3415 ) / temp_cast_22 ), _Texture_3_Normal_Index)  );
			fixed2 temp_cast_23 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5572 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3415 ) / temp_cast_23 ), _Texture_3_Normal_Index)  );
			float3 weightedBlendVar6407 = BlendComponents;
			float weightedAvg6407 = ( ( weightedBlendVar6407.x*texArray5560.w + weightedBlendVar6407.y*texArray5586.w + weightedBlendVar6407.z*texArray5572.w )/( weightedBlendVar6407.x + weightedBlendVar6407.y + weightedBlendVar6407.z ) );
			fixed ifLocalVar6620 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6620 = lerp( texArray3416.w , texArray5586.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6620 = lerp( weightedAvg6406 , weightedAvg6407 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6620 = lerp( texArray3416.w , texArray5586.w , UVmixDistance );
			fixed3 ifLocalVar7735 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Index > -1.0 )
				ifLocalVar7735 = ifLocalVar6620;
			fixed3 Texture_3_H = ifLocalVar7735;
			fixed3 temp_cast_24 = (_Texture_4_Tesselation_Depth).xxx;
			float2 appendResult3482 = float2( ( 1.0 / _Texture_4_Tiling ) , ( 1.0 / _Texture_4_Tiling ) );
			float4 texArray3483 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Normal_Index)  );
			fixed2 temp_cast_25 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5615 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_25 ), _Texture_4_Normal_Index)  );
			float4 texArray3512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3482 ), _Texture_4_Normal_Index)  );
			float4 texArray3484 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3482 ), _Texture_4_Normal_Index)  );
			float3 weightedBlendVar6413 = BlendComponents;
			float weightedAvg6413 = ( ( weightedBlendVar6413.x*texArray3512.w + weightedBlendVar6413.y*texArray3483.w + weightedBlendVar6413.z*texArray3484.w )/( weightedBlendVar6413.x + weightedBlendVar6413.y + weightedBlendVar6413.z ) );
			fixed2 temp_cast_26 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5596 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3482 ) / temp_cast_26 ), _Texture_4_Normal_Index)  );
			fixed2 temp_cast_27 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5604 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3482 ) / temp_cast_27 ), _Texture_4_Normal_Index)  );
			float3 weightedBlendVar6414 = BlendComponents;
			float weightedAvg6414 = ( ( weightedBlendVar6414.x*texArray5596.w + weightedBlendVar6414.y*texArray5615.w + weightedBlendVar6414.z*texArray5604.w )/( weightedBlendVar6414.x + weightedBlendVar6414.y + weightedBlendVar6414.z ) );
			fixed ifLocalVar6626 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6626 = lerp( texArray3483.w , texArray5615.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6626 = lerp( weightedAvg6413 , weightedAvg6414 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6626 = lerp( texArray3483.w , texArray5615.w , UVmixDistance );
			fixed3 ifLocalVar7739 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Index > -1.0 )
				ifLocalVar7739 = ifLocalVar6626;
			fixed3 Texture_4_H = ifLocalVar7739;
			float4 layeredBlendVar6518 = tex2DNode4368;
			float3 layeredBlend6518 = ( lerp( lerp( lerp( lerp( 0.0 , ( temp_cast_12 * Texture_1_H ) , layeredBlendVar6518.x ) , ( temp_cast_16 * Texture_2_H ) , layeredBlendVar6518.y ) , ( temp_cast_20 * Texture_3_H ) , layeredBlendVar6518.z ) , ( temp_cast_24 * Texture_4_H ) , layeredBlendVar6518.w ) );
			fixed3 temp_cast_28 = (_Texture_5_Tesselation_Depth).xxx;
			float2 appendResult4399 = float2( ( 1.0 / _Texture_5_Tiling ) , ( 1.0 / _Texture_5_Tiling ) );
			float4 texArray4424 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Normal_Index)  );
			fixed2 temp_cast_29 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5655 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_29 ), _Texture_5_Normal_Index)  );
			float4 texArray4417 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4399 ), _Texture_5_Normal_Index)  );
			float4 texArray4422 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4399 ), _Texture_5_Normal_Index)  );
			float3 weightedBlendVar6420 = BlendComponents;
			float weightedAvg6420 = ( ( weightedBlendVar6420.x*texArray4417.w + weightedBlendVar6420.y*texArray4424.w + weightedBlendVar6420.z*texArray4422.w )/( weightedBlendVar6420.x + weightedBlendVar6420.y + weightedBlendVar6420.z ) );
			fixed2 temp_cast_30 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5636 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4399 ) / temp_cast_30 ), _Texture_5_Normal_Index)  );
			fixed2 temp_cast_31 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5644 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4399 ) / temp_cast_31 ), _Texture_5_Normal_Index)  );
			float3 weightedBlendVar6421 = BlendComponents;
			float weightedAvg6421 = ( ( weightedBlendVar6421.x*texArray5636.w + weightedBlendVar6421.y*texArray5655.w + weightedBlendVar6421.z*texArray5644.w )/( weightedBlendVar6421.x + weightedBlendVar6421.y + weightedBlendVar6421.z ) );
			fixed ifLocalVar6632 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6632 = lerp( texArray4424.w , texArray5655.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6632 = lerp( weightedAvg6420 , weightedAvg6421 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6632 = lerp( texArray4424.w , texArray5655.w , UVmixDistance );
			fixed3 ifLocalVar7743 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Index > -1.0 )
				ifLocalVar7743 = ifLocalVar6632;
			fixed3 Texture_5_H = ifLocalVar7743;
			fixed3 temp_cast_32 = (_Texture_6_Tesselation_Depth).xxx;
			float2 appendResult4471 = float2( ( 1.0 / _Texture_6_Tiling ) , ( 1.0 / _Texture_6_Tiling ) );
			float4 texArray4493 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Normal_Index)  );
			fixed2 temp_cast_33 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5695 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_33 ), _Texture_6_Normal_Index)  );
			float4 texArray4486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4471 ), _Texture_6_Normal_Index)  );
			float4 texArray4491 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4471 ), _Texture_6_Normal_Index)  );
			float3 weightedBlendVar6427 = BlendComponents;
			float weightedAvg6427 = ( ( weightedBlendVar6427.x*texArray4486.w + weightedBlendVar6427.y*texArray4493.w + weightedBlendVar6427.z*texArray4491.w )/( weightedBlendVar6427.x + weightedBlendVar6427.y + weightedBlendVar6427.z ) );
			fixed2 temp_cast_34 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5676 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4471 ) / temp_cast_34 ), _Texture_6_Normal_Index)  );
			fixed2 temp_cast_35 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5684 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4471 ) / temp_cast_35 ), _Texture_6_Normal_Index)  );
			float3 weightedBlendVar6428 = BlendComponents;
			float weightedAvg6428 = ( ( weightedBlendVar6428.x*texArray5676.w + weightedBlendVar6428.y*texArray5695.w + weightedBlendVar6428.z*texArray5684.w )/( weightedBlendVar6428.x + weightedBlendVar6428.y + weightedBlendVar6428.z ) );
			fixed ifLocalVar6638 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6638 = lerp( texArray4493.w , texArray5695.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6638 = lerp( weightedAvg6427 , weightedAvg6428 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6638 = lerp( texArray4493.w , texArray5695.w , UVmixDistance );
			fixed3 ifLocalVar7748 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Index > -1.0 )
				ifLocalVar7748 = ifLocalVar6638;
			fixed3 Texture_6_H = ifLocalVar7748;
			fixed3 temp_cast_36 = (_Texture_7_Tesselation_Depth).xxx;
			float2 appendResult4545 = float2( ( 1.0 / _Texture_7_Tiling ) , ( 1.0 / _Texture_7_Tiling ) );
			float4 texArray4567 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Normal_Index)  );
			fixed2 temp_cast_37 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5735 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_37 ), _Texture_7_Normal_Index)  );
			float4 texArray4560 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4545 ), _Texture_7_Normal_Index)  );
			float4 texArray4565 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4545 ), _Texture_7_Normal_Index)  );
			float3 weightedBlendVar6434 = BlendComponents;
			float weightedAvg6434 = ( ( weightedBlendVar6434.x*texArray4560.w + weightedBlendVar6434.y*texArray4567.w + weightedBlendVar6434.z*texArray4565.w )/( weightedBlendVar6434.x + weightedBlendVar6434.y + weightedBlendVar6434.z ) );
			fixed2 temp_cast_38 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5716 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4545 ) / temp_cast_38 ), _Texture_7_Normal_Index)  );
			fixed2 temp_cast_39 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5724 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4545 ) / temp_cast_39 ), _Texture_7_Normal_Index)  );
			float3 weightedBlendVar6435 = BlendComponents;
			float weightedAvg6435 = ( ( weightedBlendVar6435.x*texArray5716.w + weightedBlendVar6435.y*texArray5735.w + weightedBlendVar6435.z*texArray5724.w )/( weightedBlendVar6435.x + weightedBlendVar6435.y + weightedBlendVar6435.z ) );
			fixed ifLocalVar6644 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6644 = lerp( texArray4567.w , texArray5735.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6644 = lerp( weightedAvg6434 , weightedAvg6435 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6644 = lerp( texArray4567.w , texArray5735.w , UVmixDistance );
			fixed3 ifLocalVar7752 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Index > -1.0 )
				ifLocalVar7752 = ifLocalVar6644;
			fixed3 Texture_7_H = ifLocalVar7752;
			fixed3 temp_cast_40 = (_Texture_8_Tesselation_Depth).xxx;
			float2 appendResult4619 = float2( ( 1.0 / _Texture_8_Tiling ) , ( 1.0 / _Texture_8_Tiling ) );
			float4 texArray4641 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Normal_Index)  );
			fixed2 temp_cast_41 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5775 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_41 ), _Texture_8_Normal_Index)  );
			float4 texArray4634 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4619 ), _Texture_8_Normal_Index)  );
			float4 texArray4639 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4619 ), _Texture_8_Normal_Index)  );
			float3 weightedBlendVar6441 = BlendComponents;
			float weightedAvg6441 = ( ( weightedBlendVar6441.x*texArray4634.w + weightedBlendVar6441.y*texArray4641.w + weightedBlendVar6441.z*texArray4639.w )/( weightedBlendVar6441.x + weightedBlendVar6441.y + weightedBlendVar6441.z ) );
			fixed2 temp_cast_42 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5756 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4619 ) / temp_cast_42 ), _Texture_8_Normal_Index)  );
			fixed2 temp_cast_43 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5764 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4619 ) / temp_cast_43 ), _Texture_8_Normal_Index)  );
			float3 weightedBlendVar6442 = BlendComponents;
			float weightedAvg6442 = ( ( weightedBlendVar6442.x*texArray5756.w + weightedBlendVar6442.y*texArray5775.w + weightedBlendVar6442.z*texArray5764.w )/( weightedBlendVar6442.x + weightedBlendVar6442.y + weightedBlendVar6442.z ) );
			fixed ifLocalVar6650 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6650 = lerp( texArray4641.w , texArray5775.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6650 = lerp( weightedAvg6441 , weightedAvg6442 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6650 = lerp( texArray4641.w , texArray5775.w , UVmixDistance );
			fixed3 ifLocalVar7756 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Index > -1.0 )
				ifLocalVar7756 = ifLocalVar6650;
			fixed3 Texture_8_H = ifLocalVar7756;
			float4 layeredBlendVar6525 = tex2DNode4369;
			float3 layeredBlend6525 = ( lerp( lerp( lerp( lerp( layeredBlend6518 , ( temp_cast_28 * Texture_5_H ) , layeredBlendVar6525.x ) , ( temp_cast_32 * Texture_6_H ) , layeredBlendVar6525.y ) , ( temp_cast_36 * Texture_7_H ) , layeredBlendVar6525.z ) , ( temp_cast_40 * Texture_8_H ) , layeredBlendVar6525.w ) );
			fixed4 temp_cast_45 = (_Texture_9_Tesselation_Depth).xxxx;
			float2 appendResult4736 = float2( ( 1.0 / _Texture_9_Tiling ) , ( 1.0 / _Texture_9_Tiling ) );
			float4 texArray4788 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Normal_Index)  );
			fixed2 temp_cast_46 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5811 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_46 ), _Texture_9_Normal_Index)  );
			float4 texArray5285 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4736 ), _Texture_9_Normal_Index)  );
			float4 texArray4783 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4736 ), _Texture_9_Normal_Index)  );
			float3 weightedBlendVar6448 = BlendComponents;
			float weightedAvg6448 = ( ( weightedBlendVar6448.x*texArray5285.w + weightedBlendVar6448.y*texArray4788.w + weightedBlendVar6448.z*texArray4783.w )/( weightedBlendVar6448.x + weightedBlendVar6448.y + weightedBlendVar6448.z ) );
			fixed2 temp_cast_47 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5796 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4736 ) / temp_cast_47 ), _Texture_9_Normal_Index)  );
			fixed2 temp_cast_48 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5806 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4736 ) / temp_cast_48 ), _Texture_9_Normal_Index)  );
			float3 weightedBlendVar6449 = BlendComponents;
			float weightedAvg6449 = ( ( weightedBlendVar6449.x*texArray5796.w + weightedBlendVar6449.y*texArray5811.w + weightedBlendVar6449.z*texArray5806.w )/( weightedBlendVar6449.x + weightedBlendVar6449.y + weightedBlendVar6449.z ) );
			fixed ifLocalVar6668 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6668 = lerp( texArray4788.w , texArray5811.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar6668 = lerp( weightedAvg6448 , weightedAvg6449 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6668 = lerp( texArray4788.w , texArray5811.w , UVmixDistance );
			fixed4 ifLocalVar7721 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar7721 = ifLocalVar6668;
			fixed4 Texture_9_H = ifLocalVar7721;
			fixed4 temp_cast_49 = (_Texture_10_Tesselation_Depth).xxxx;
			float2 appendResult4738 = float2( ( 1.0 / _Texture_10_Tiling ) , ( 1.0 / _Texture_10_Tiling ) );
			float4 texArray4822 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Normal_Index)  );
			fixed2 temp_cast_50 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5851 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_50 ), _Texture_10_Normal_Index)  );
			float4 texArray4798 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4738 ), _Texture_10_Normal_Index)  );
			float4 texArray4791 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4738 ), _Texture_10_Normal_Index)  );
			float3 weightedBlendVar6455 = BlendComponents;
			float weightedAvg6455 = ( ( weightedBlendVar6455.x*texArray4798.w + weightedBlendVar6455.y*texArray4822.w + weightedBlendVar6455.z*texArray4791.w )/( weightedBlendVar6455.x + weightedBlendVar6455.y + weightedBlendVar6455.z ) );
			fixed2 temp_cast_51 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5836 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4738 ) / temp_cast_51 ), _Texture_10_Normal_Index)  );
			fixed2 temp_cast_52 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5846 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4738 ) / temp_cast_52 ), _Texture_10_Normal_Index)  );
			float3 weightedBlendVar6456 = BlendComponents;
			float weightedAvg6456 = ( ( weightedBlendVar6456.x*texArray5836.w + weightedBlendVar6456.y*texArray5851.w + weightedBlendVar6456.z*texArray5846.w )/( weightedBlendVar6456.x + weightedBlendVar6456.y + weightedBlendVar6456.z ) );
			fixed ifLocalVar6662 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6662 = lerp( texArray4822.w , texArray5851.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6662 = lerp( weightedAvg6455 , weightedAvg6456 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6662 = lerp( texArray4822.w , texArray5851.w , UVmixDistance );
			fixed4 ifLocalVar7717 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7717 = ifLocalVar6662;
			fixed4 Texture_10_H = ifLocalVar7717;
			fixed4 temp_cast_53 = (_Texture_11_Tesselation_Depth).xxxx;
			float2 appendResult4741 = float2( ( 1.0 / _Texture_11_Tiling ) , ( 1.0 / _Texture_11_Tiling ) );
			float4 texArray4856 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Normal_Index)  );
			fixed2 temp_cast_54 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5891 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_54 ), _Texture_11_Normal_Index)  );
			float4 texArray4828 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4741 ), _Texture_11_Normal_Index)  );
			float4 texArray4811 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4741 ), _Texture_11_Normal_Index)  );
			float3 weightedBlendVar6462 = BlendComponents;
			float weightedAvg6462 = ( ( weightedBlendVar6462.x*texArray4828.w + weightedBlendVar6462.y*texArray4856.w + weightedBlendVar6462.z*texArray4811.w )/( weightedBlendVar6462.x + weightedBlendVar6462.y + weightedBlendVar6462.z ) );
			fixed2 temp_cast_55 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5876 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4741 ) / temp_cast_55 ), _Texture_11_Normal_Index)  );
			fixed2 temp_cast_56 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5886 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4741 ) / temp_cast_56 ), _Texture_11_Normal_Index)  );
			float3 weightedBlendVar6463 = BlendComponents;
			float weightedAvg6463 = ( ( weightedBlendVar6463.x*texArray5876.w + weightedBlendVar6463.y*texArray5891.w + weightedBlendVar6463.z*texArray5886.w )/( weightedBlendVar6463.x + weightedBlendVar6463.y + weightedBlendVar6463.z ) );
			fixed ifLocalVar6656 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6656 = lerp( texArray4856.w , texArray5891.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6656 = lerp( weightedAvg6462 , weightedAvg6463 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6656 = lerp( texArray4856.w , texArray5891.w , UVmixDistance );
			fixed4 ifLocalVar7715 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Index > -1.0 )
				ifLocalVar7715 = ifLocalVar6656;
			fixed4 Texture_11_H = ifLocalVar7715;
			fixed4 temp_cast_57 = (_Texture_12_Tesselation_Depth).xxxx;
			float2 appendResult4751 = float2( ( 1.0 / _Texture_12_Tiling ) , ( 1.0 / _Texture_12_Tiling ) );
			float4 texArray4870 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Normal_Index)  );
			fixed2 temp_cast_58 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5931 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_58 ), _Texture_12_Normal_Index)  );
			float4 texArray4850 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4751 ), _Texture_12_Normal_Index)  );
			float4 texArray4852 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4751 ), _Texture_12_Normal_Index)  );
			float3 weightedBlendVar6469 = BlendComponents;
			float weightedAvg6469 = ( ( weightedBlendVar6469.x*texArray4850.w + weightedBlendVar6469.y*texArray4870.w + weightedBlendVar6469.z*texArray4852.w )/( weightedBlendVar6469.x + weightedBlendVar6469.y + weightedBlendVar6469.z ) );
			fixed2 temp_cast_59 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5916 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4751 ) / temp_cast_59 ), _Texture_12_Normal_Index)  );
			fixed2 temp_cast_60 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5926 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4751 ) / temp_cast_60 ), _Texture_12_Normal_Index)  );
			float3 weightedBlendVar6470 = BlendComponents;
			float weightedAvg6470 = ( ( weightedBlendVar6470.x*texArray5916.w + weightedBlendVar6470.y*texArray5931.w + weightedBlendVar6470.z*texArray5926.w )/( weightedBlendVar6470.x + weightedBlendVar6470.y + weightedBlendVar6470.z ) );
			fixed ifLocalVar6674 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6674 = lerp( texArray4870.w , texArray5931.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6674 = lerp( weightedAvg6469 , weightedAvg6470 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6674 = lerp( texArray4870.w , texArray5931.w , UVmixDistance );
			fixed4 ifLocalVar7708 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Index > -1.0 )
				ifLocalVar7708 = ifLocalVar6674;
			fixed4 Texture_12_H = ifLocalVar7708;
			float4 layeredBlendVar6527 = tex2DNode4370;
			float4 layeredBlend6527 = ( lerp( lerp( lerp( lerp( fixed4( layeredBlend6525 , 0.0 ) , ( temp_cast_45 * Texture_9_H ) , layeredBlendVar6527.x ) , ( temp_cast_49 * Texture_10_H ) , layeredBlendVar6527.y ) , ( temp_cast_53 * Texture_11_H ) , layeredBlendVar6527.z ) , ( temp_cast_57 * Texture_12_H ) , layeredBlendVar6527.w ) );
			fixed4 temp_cast_61 = (_Texture_13_Tesselation_Depth).xxxx;
			float2 appendResult5027 = float2( ( 1.0 / _Texture_13_Tiling ) , ( 1.0 / _Texture_13_Tiling ) );
			float4 texArray5120 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Normal_Index)  );
			fixed2 temp_cast_62 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5971 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_62 ), _Texture_13_Normal_Index)  );
			float4 texArray5127 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5027 ), _Texture_13_Normal_Index)  );
			float4 texArray5109 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5027 ), _Texture_13_Normal_Index)  );
			float3 weightedBlendVar6476 = BlendComponents;
			float weightedAvg6476 = ( ( weightedBlendVar6476.x*texArray5127.w + weightedBlendVar6476.y*texArray5120.w + weightedBlendVar6476.z*texArray5109.w )/( weightedBlendVar6476.x + weightedBlendVar6476.y + weightedBlendVar6476.z ) );
			fixed2 temp_cast_63 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5956 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5027 ) / temp_cast_63 ), _Texture_13_Normal_Index)  );
			fixed2 temp_cast_64 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5966 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5027 ) / temp_cast_64 ), _Texture_13_Normal_Index)  );
			float3 weightedBlendVar6477 = BlendComponents;
			float weightedAvg6477 = ( ( weightedBlendVar6477.x*texArray5956.w + weightedBlendVar6477.y*texArray5971.w + weightedBlendVar6477.z*texArray5966.w )/( weightedBlendVar6477.x + weightedBlendVar6477.y + weightedBlendVar6477.z ) );
			fixed ifLocalVar6680 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6680 = lerp( texArray5120.w , texArray5971.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6680 = lerp( weightedAvg6476 , weightedAvg6477 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6680 = lerp( texArray5120.w , texArray5971.w , UVmixDistance );
			fixed4 ifLocalVar7704 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Index > -1.0 )
				ifLocalVar7704 = ifLocalVar6680;
			fixed4 Texture_13_H = ifLocalVar7704;
			fixed4 temp_cast_65 = (_Texture_14_Tesselation_Depth).xxxx;
			float2 appendResult5033 = float2( ( 1.0 / _Texture_14_Tiling ) , ( 1.0 / _Texture_14_Tiling ) );
			float4 texArray5178 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Normal_Index)  );
			fixed2 temp_cast_66 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6011 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_66 ), _Texture_14_Normal_Index)  );
			float4 texArray5017 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5033 ), _Texture_14_Normal_Index)  );
			float4 texArray5170 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5033 ), _Texture_14_Normal_Index)  );
			float3 weightedBlendVar6483 = BlendComponents;
			float weightedAvg6483 = ( ( weightedBlendVar6483.x*texArray5017.w + weightedBlendVar6483.y*texArray5178.w + weightedBlendVar6483.z*texArray5170.w )/( weightedBlendVar6483.x + weightedBlendVar6483.y + weightedBlendVar6483.z ) );
			fixed2 temp_cast_67 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5996 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5033 ) / temp_cast_67 ), _Texture_14_Normal_Index)  );
			fixed2 temp_cast_68 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6006 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5033 ) / temp_cast_68 ), _Texture_14_Normal_Index)  );
			float3 weightedBlendVar6484 = BlendComponents;
			float weightedAvg6484 = ( ( weightedBlendVar6484.x*texArray5996.w + weightedBlendVar6484.y*texArray6011.w + weightedBlendVar6484.z*texArray6006.w )/( weightedBlendVar6484.x + weightedBlendVar6484.y + weightedBlendVar6484.z ) );
			fixed ifLocalVar6686 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6686 = lerp( texArray5178.w , texArray6011.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6686 = lerp( weightedAvg6483 , weightedAvg6484 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6686 = lerp( texArray5178.w , texArray6011.w , UVmixDistance );
			fixed4 ifLocalVar7700 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar7700 = ifLocalVar6686;
			fixed4 Texture_14_H = ifLocalVar7700;
			fixed4 temp_cast_69 = (_Texture_15_Tesselation_Depth).xxxx;
			float2 appendResult5212 = float2( ( 1.0 / _Texture_15_Tiling ) , ( 1.0 / _Texture_15_Tiling ) );
			float4 texArray5246 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Normal_Index)  );
			fixed2 temp_cast_70 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6051 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_70 ), _Texture_15_Normal_Index)  );
			float4 texArray5227 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5212 ), _Texture_15_Normal_Index)  );
			float4 texArray5250 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5212 ), _Texture_15_Normal_Index)  );
			float3 weightedBlendVar6490 = BlendComponents;
			float weightedAvg6490 = ( ( weightedBlendVar6490.x*texArray5227.w + weightedBlendVar6490.y*texArray5246.w + weightedBlendVar6490.z*texArray5250.w )/( weightedBlendVar6490.x + weightedBlendVar6490.y + weightedBlendVar6490.z ) );
			fixed2 temp_cast_71 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6036 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5212 ) / temp_cast_71 ), _Texture_15_Normal_Index)  );
			fixed2 temp_cast_72 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6046 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5212 ) / temp_cast_72 ), _Texture_15_Normal_Index)  );
			float3 weightedBlendVar6491 = BlendComponents;
			float weightedAvg6491 = ( ( weightedBlendVar6491.x*texArray6036.w + weightedBlendVar6491.y*texArray6051.w + weightedBlendVar6491.z*texArray6046.w )/( weightedBlendVar6491.x + weightedBlendVar6491.y + weightedBlendVar6491.z ) );
			fixed ifLocalVar6692 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6692 = lerp( texArray5246.w , texArray6051.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6692 = lerp( weightedAvg6490 , weightedAvg6491 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6692 = lerp( texArray5246.w , texArray6051.w , UVmixDistance );
			fixed4 ifLocalVar7696 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar7696 = ifLocalVar6692;
			fixed4 Texture_15_H = ifLocalVar7696;
			fixed4 temp_cast_73 = (_Texture_16_Tesselation_Depth).xxxx;
			float2 appendResult5078 = float2( ( 1.0 / _Texture_16_Tiling ) , ( 1.0 / _Texture_16_Tiling ) );
			float4 texArray5099 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Normal_Index)  );
			fixed2 temp_cast_74 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6091 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_74 ), _Texture_16_Normal_Index)  );
			float4 texArray5082 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5078 ), _Texture_16_Normal_Index)  );
			float4 texArray4731 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5078 ), _Texture_16_Normal_Index)  );
			float3 weightedBlendVar6497 = BlendComponents;
			float weightedAvg6497 = ( ( weightedBlendVar6497.x*texArray5082.w + weightedBlendVar6497.y*texArray5099.w + weightedBlendVar6497.z*texArray4731.w )/( weightedBlendVar6497.x + weightedBlendVar6497.y + weightedBlendVar6497.z ) );
			fixed2 temp_cast_75 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6076 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5078 ) / temp_cast_75 ), _Texture_16_Normal_Index)  );
			fixed2 temp_cast_76 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6086 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5078 ) / temp_cast_76 ), _Texture_16_Normal_Index)  );
			float3 weightedBlendVar6498 = BlendComponents;
			float weightedAvg6498 = ( ( weightedBlendVar6498.x*texArray6076.w + weightedBlendVar6498.y*texArray6091.w + weightedBlendVar6498.z*texArray6086.w )/( weightedBlendVar6498.x + weightedBlendVar6498.y + weightedBlendVar6498.z ) );
			fixed ifLocalVar6698 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6698 = lerp( texArray5099.w , texArray6091.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6698 = lerp( weightedAvg6497 , weightedAvg6498 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6698 = lerp( texArray5099.w , texArray6091.w , UVmixDistance );
			fixed4 ifLocalVar7693 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar7693 = ifLocalVar6698;
			fixed4 Texture_16_H = ifLocalVar7693;
			float4 layeredBlendVar6530 = tex2DNode4371;
			float4 layeredBlend6530 = ( lerp( lerp( lerp( lerp( layeredBlend6527 , ( temp_cast_61 * Texture_13_H ) , layeredBlendVar6530.x ) , ( temp_cast_65 * Texture_14_H ) , layeredBlendVar6530.y ) , ( temp_cast_69 * Texture_15_H ) , layeredBlendVar6530.z ) , ( temp_cast_73 * Texture_16_H ) , layeredBlendVar6530.w ) );
			float2 appendResult3679 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_77 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray6267 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3679 * temp_cast_77 ), _Texture_Snow_Normal_Index)  );
			fixed2 temp_cast_78 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_79 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray6270 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3679 * temp_cast_78 ) / temp_cast_79 ), _Texture_Snow_Normal_Index)  );
			fixed ifLocalVar7824 = 0;
			UNITY_BRANCH if( _Snow_Emission_AO_index > -1.0 )
				ifLocalVar7824 = lerp( ( pow( texArray6267.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , ( pow( texArray6270.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , UVmixDistance );
			fixed4 temp_cast_80 = (ifLocalVar7824).xxxx;
			fixed4 temp_cast_81 = (ifLocalVar7824).xxxx;
			float2 appendResult3750 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_83 = (_Snow_Noise_Tiling).xx;
			float4 texArray4383 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3750 * temp_cast_83 ), (float)_Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_85 = (_Snow_Noise_Tiling).xx;
			float4 texArray4385 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_85 ) * float2( 0.23,0.23 ) ), (float)_Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_87 = (_Snow_Noise_Tiling).xx;
			float4 texArray4384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_87 ) * float2( 0.53,0.53 ) ), (float)_Texture_Snow_Noise_Index)  );
			fixed SnowSlope = ( 1.0 - ( clamp( ( clamp( ase_worldNormal.y , 0.0 , 0.9999 ) - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) , 0.0 , 2.0 ) * ( 1.0 / ( _Snow_Maximum_Angle / 90.0 ) ) ) );
			float HeightMask6539 = saturate(pow(((( 1.0 - clamp( clamp( ( layeredBlend6530 + temp_cast_80 ) , float4( 0.0,0,0,0 ) , ( layeredBlend6530 + temp_cast_81 ) ) , float4( 0.0,0,0,0 ) , float4( 1.0,0,0,0 ) ) ).x*( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ))*4)+(( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) )*2),lerp( _Snow_Heightblend_Close , _Snow_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_89 = (_Texture_13_Heightmap_Depth).xxxx;
			fixed4 temp_cast_90 = (_Texture_13_Height_Contrast).xxxx;
			float HeightMask6231 = saturate(pow(((( temp_cast_89 * pow( Texture_13_H , temp_cast_90 ) ).x*Splat4_R)*4)+(Splat4_R*2),lerp( _Texture_13_Heightblend_Close , _Texture_13_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_92 = (_Texture_14_Heightmap_Depth).xxxx;
			fixed4 temp_cast_93 = (_Texture_14_Height_Contrast).xxxx;
			float HeightMask6234 = saturate(pow(((( temp_cast_92 * pow( Texture_14_H , temp_cast_93 ) ).x*Splat4_G)*4)+(Splat4_G*2),lerp( _Texture_14_Heightblend_Close , _Texture_14_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_95 = (_Texture_15_Heightmap_Depth).xxxx;
			fixed4 temp_cast_96 = (_Texture_15_Height_Contrast).xxxx;
			float HeightMask6237 = saturate(pow(((( temp_cast_95 * pow( Texture_15_H , temp_cast_96 ) ).x*Splat4_B)*4)+(Splat4_B*2),lerp( _Texture_15_Heightblend_Close , _Texture_15_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_98 = (_Texture_16_Heightmap_Depth).xxxx;
			fixed4 temp_cast_99 = (_Texture_16_Height_Contrast).xxxx;
			float HeightMask6240 = saturate(pow(((( temp_cast_98 * pow( Texture_16_H , temp_cast_99 ) ).x*Splat4_A)*4)+(Splat4_A*2),lerp( _Texture_16_Heightblend_Close , _Texture_16_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6533 = float4( HeightMask6231 , HeightMask6234 , HeightMask6237 , HeightMask6240 );
			fixed4 temp_cast_101 = (_Texture_9_Heightmap_Depth).xxxx;
			fixed4 temp_cast_102 = (_Texture_9_Height_Contrast).xxxx;
			float HeightMask6219 = saturate(pow(((( temp_cast_101 * pow( Texture_9_H , temp_cast_102 ) ).x*Splat3_R)*4)+(Splat3_R*2),lerp( _Texture_9_Heightblend_Close , _Texture_9_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_104 = (_Texture_10_Heightmap_Depth).xxxx;
			fixed4 temp_cast_105 = (_Texture_10_Height_Contrast).xxxx;
			float HeightMask6222 = saturate(pow(((( temp_cast_104 * pow( Texture_10_H , temp_cast_105 ) ).x*Splat3_G)*4)+(Splat3_G*2),lerp( _Texture_10_Heightblend_Close , _Texture_10_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_107 = (_Texture_11_Heightmap_Depth).xxxx;
			fixed4 temp_cast_108 = (_Texture_11_Height_Contrast).xxxx;
			float HeightMask6225 = saturate(pow(((( temp_cast_107 * pow( Texture_11_H , temp_cast_108 ) ).x*Splat3_B)*4)+(Splat3_B*2),lerp( _Texture_11_Heightblend_Close , _Texture_11_Heightblend_Far , UVmixDistance )));;
			fixed4 temp_cast_110 = (_Texture_12_Heightmap_Depth).xxxx;
			fixed4 temp_cast_111 = (_Texture_12_Height_Contrast).xxxx;
			float HeightMask6228 = saturate(pow(((( temp_cast_110 * pow( Texture_12_H , temp_cast_111 ) ).x*Splat3_A)*4)+(Splat3_A*2),lerp( _Texture_12_Heightblend_Close , _Texture_12_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6529 = float4( HeightMask6219 , HeightMask6222 , HeightMask6225 , HeightMask6228 );
			fixed3 temp_cast_113 = (_Texture_5_Heightmap_Depth).xxx;
			fixed3 temp_cast_114 = (_Texture_5_Height_Contrast).xxx;
			float HeightMask6205 = saturate(pow(((( temp_cast_113 * pow( Texture_5_H , temp_cast_114 ) ).x*Splat2_R)*4)+(Splat2_R*2),lerp( _Texture_5_Heightblend_Close , _Texture_5_Heightblend_Far , UVmixDistance )));;
			fixed3 temp_cast_116 = (_Texture_6_Heightmap_Depth).xxx;
			fixed3 temp_cast_117 = (_Texture_6_Height_Contrast).xxx;
			float HeightMask6208 = saturate(pow(((( temp_cast_116 * pow( Texture_6_H , temp_cast_117 ) ).x*Splat2_G)*4)+(Splat2_G*2),lerp( _Texture_6_Heightblend_Close , _Texture_6_Heightblend_Far , UVmixDistance )));;
			fixed3 temp_cast_119 = (_Texture_7_Heightmap_Depth).xxx;
			fixed3 temp_cast_120 = (_Texture_7_Height_Contrast).xxx;
			float HeightMask6211 = saturate(pow(((( temp_cast_119 * pow( Texture_7_H , temp_cast_120 ) ).x*Splat2_B)*4)+(Splat2_B*2),lerp( _Texture_7_Heightblend_Close , _Texture_7_Heightblend_Far , UVmixDistance )));;
			fixed3 temp_cast_122 = (_Texture_8_Heightmap_Depth).xxx;
			fixed3 temp_cast_123 = (_Texture_8_Height_Contrast).xxx;
			float HeightMask6214 = saturate(pow(((( temp_cast_122 * pow( Texture_8_H , temp_cast_123 ) ).x*Splat2_A)*4)+(Splat2_A*2),lerp( _Texture_8_Heightblend_Close , _Texture_8_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6524 = float4( HeightMask6205 , HeightMask6208 , HeightMask6211 , HeightMask6214 );
			fixed3 temp_cast_125 = (_Texture_1_Height_Contrast).xxx;
			fixed3 temp_cast_126 = (_Texture_1_Heightmap_Depth).xxx;
			float HeightMask6196 = saturate(pow(((( pow( Texture_1_H , temp_cast_125 ) * temp_cast_126 ).x*Splat1_R)*4)+(Splat1_R*2),lerp( _Texture_1_Heightblend_Close , _Texture_1_Heightblend_Far , UVmixDistance )));;
			fixed3 temp_cast_128 = (_Texture_2_Heightmap_Depth).xxx;
			fixed3 temp_cast_129 = (_Texture_2_Height_Contrast).xxx;
			float HeightMask6515 = saturate(pow(((( temp_cast_128 * pow( Texture_2_H , temp_cast_129 ) ).x*Splat1_G)*4)+(Splat1_G*2),lerp( _Texture_2_Heightblend_Close , _Texture_2_Heightblend_Far , UVmixDistance )));;
			fixed3 temp_cast_131 = (_Texture_3_Heightmap_Depth).xxx;
			fixed3 temp_cast_132 = (_Texture_3_Height_Contrast).xxx;
			float HeightMask6516 = saturate(pow(((( temp_cast_131 * pow( Texture_3_H , temp_cast_132 ) ).x*Splat1_B)*4)+(Splat1_B*2),lerp( _Texture_3_Heightblend_Close , _Texture_3_Heightblend_Far , UVmixDistance )));;
			fixed3 temp_cast_134 = (_Texture_4_Heightmap_Depth).xxx;
			fixed3 temp_cast_135 = (_Texture_4_Height_Contrast).xxx;
			float HeightMask6203 = saturate(pow(((( temp_cast_134 * pow( Texture_4_H , temp_cast_135 ) ).x*Splat1_A)*4)+(Splat1_A*2),lerp( _Texture_4_Heightblend_Close , _Texture_4_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6517 = float4( HeightMask6196 , HeightMask6515 , HeightMask6516 , HeightMask6203 );
			float2 appendResult11_g798 = float2( texArray3300.x , texArray3300.y );
			float2 temp_cast_137 = (_Texture_1_Normal_Power).xx;
			float2 temp_output_4_0_g798 = ( ( ( appendResult11_g798 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_137 );
			float2 temp_cast_138 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_139 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_140 = (_Texture_1_Normal_Power).xx;
			float temp_output_9_0_g798 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g798 , temp_output_4_0_g798 ) ) ) );
			float3 appendResult10_g798 = float3( temp_output_4_0_g798.x , temp_output_4_0_g798.y , temp_output_9_0_g798 );
			float2 appendResult11_g337 = float2( texArray3299.x , texArray3299.y );
			float2 temp_cast_141 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g337 = ( ( ( appendResult11_g337 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_141 );
			float2 temp_cast_142 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_cast_143 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_cast_144 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g337 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g337 , temp_output_4_0_g337 ) ) ) );
			float3 appendResult19_g337 = float3( temp_output_4_0_g337.y , temp_output_4_0_g337.x , temp_output_9_0_g337 );
			float3 appendResult6857 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g338 = float2( texArray3301.x , texArray3301.y );
			float2 temp_cast_145 = (_Texture_1_Normal_Power).xx;
			float2 temp_output_4_0_g338 = ( ( ( appendResult11_g338 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_145 );
			float2 temp_cast_146 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_147 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_148 = (_Texture_1_Normal_Power).xx;
			float temp_output_9_0_g338 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g338 , temp_output_4_0_g338 ) ) ) );
			float3 appendResult10_g338 = float3( temp_output_4_0_g338.x , temp_output_4_0_g338.y , temp_output_9_0_g338 );
			float3 appendResult6860 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6393 = BlendComponents;
			float3 weightedAvg6393 = ( ( weightedBlendVar6393.x*( appendResult19_g337 * appendResult6857 ) + weightedBlendVar6393.y*appendResult10_g798 + weightedBlendVar6393.z*( appendResult10_g338 * appendResult6860 ) )/( weightedBlendVar6393.x + weightedBlendVar6393.y + weightedBlendVar6393.z ) );
			fixed3 ifLocalVar6606 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6606 = appendResult10_g798;
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6606 = weightedAvg6393;
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6606 = appendResult10_g798;
			fixed3 ifLocalVar7726 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar7726 = ifLocalVar6606;
			fixed3 Normal_1 = ifLocalVar7726;
			float2 appendResult11_g802 = float2( texArray3350.x , texArray3350.y );
			float2 temp_cast_149 = (_Texture_2_Normal_Power).xx;
			float2 temp_output_4_0_g802 = ( ( ( appendResult11_g802 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_149 );
			float2 temp_cast_150 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_151 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_152 = (_Texture_2_Normal_Power).xx;
			float temp_output_9_0_g802 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g802 , temp_output_4_0_g802 ) ) ) );
			float3 appendResult10_g802 = float3( temp_output_4_0_g802.x , temp_output_4_0_g802.y , temp_output_9_0_g802 );
			float2 appendResult11_g331 = float2( texArray3384.x , texArray3384.y );
			float2 temp_cast_153 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g331 = ( ( ( appendResult11_g331 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_153 );
			float2 temp_cast_154 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_cast_155 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_cast_156 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g331 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g331 , temp_output_4_0_g331 ) ) ) );
			float3 appendResult19_g331 = float3( temp_output_4_0_g331.y , temp_output_4_0_g331.x , temp_output_9_0_g331 );
			float3 appendResult6864 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g333 = float2( texArray3351.x , texArray3351.y );
			float2 temp_cast_157 = (_Texture_2_Normal_Power).xx;
			float2 temp_output_4_0_g333 = ( ( ( appendResult11_g333 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_157 );
			float2 temp_cast_158 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_159 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_160 = (_Texture_2_Normal_Power).xx;
			float temp_output_9_0_g333 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g333 , temp_output_4_0_g333 ) ) ) );
			float3 appendResult10_g333 = float3( temp_output_4_0_g333.x , temp_output_4_0_g333.y , temp_output_9_0_g333 );
			float3 appendResult6867 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6401 = BlendComponents;
			float3 weightedAvg6401 = ( ( weightedBlendVar6401.x*( appendResult19_g331 * appendResult6864 ) + weightedBlendVar6401.y*appendResult10_g802 + weightedBlendVar6401.z*( appendResult10_g333 * appendResult6867 ) )/( weightedBlendVar6401.x + weightedBlendVar6401.y + weightedBlendVar6401.z ) );
			fixed3 ifLocalVar6613 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6613 = appendResult10_g802;
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6613 = weightedAvg6401;
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6613 = appendResult10_g802;
			fixed3 ifLocalVar7730 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar7730 = ifLocalVar6613;
			fixed3 Normal_2 = ifLocalVar7730;
			float2 appendResult11_g803 = float2( texArray3416.x , texArray3416.y );
			float2 temp_cast_161 = (_Texture_3_Normal_Power).xx;
			float2 temp_output_4_0_g803 = ( ( ( appendResult11_g803 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_161 );
			float2 temp_cast_162 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_163 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_164 = (_Texture_3_Normal_Power).xx;
			float temp_output_9_0_g803 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g803 , temp_output_4_0_g803 ) ) ) );
			float3 appendResult10_g803 = float3( temp_output_4_0_g803.x , temp_output_4_0_g803.y , temp_output_9_0_g803 );
			float2 appendResult11_g335 = float2( texArray3445.x , texArray3445.y );
			float2 temp_cast_165 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g335 = ( ( ( appendResult11_g335 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_165 );
			float2 temp_cast_166 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_cast_167 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_cast_168 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g335 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g335 , temp_output_4_0_g335 ) ) ) );
			float3 appendResult19_g335 = float3( temp_output_4_0_g335.y , temp_output_4_0_g335.x , temp_output_9_0_g335 );
			float3 appendResult6871 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g334 = float2( texArray3417.x , texArray3417.y );
			float2 temp_cast_169 = (_Texture_3_Normal_Power).xx;
			float2 temp_output_4_0_g334 = ( ( ( appendResult11_g334 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_169 );
			float2 temp_cast_170 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_171 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_172 = (_Texture_3_Normal_Power).xx;
			float temp_output_9_0_g334 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g334 , temp_output_4_0_g334 ) ) ) );
			float3 appendResult10_g334 = float3( temp_output_4_0_g334.x , temp_output_4_0_g334.y , temp_output_9_0_g334 );
			float3 appendResult6874 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6408 = BlendComponents;
			float3 weightedAvg6408 = ( ( weightedBlendVar6408.x*( appendResult19_g335 * appendResult6871 ) + weightedBlendVar6408.y*appendResult10_g803 + weightedBlendVar6408.z*( appendResult10_g334 * appendResult6874 ) )/( weightedBlendVar6408.x + weightedBlendVar6408.y + weightedBlendVar6408.z ) );
			fixed3 ifLocalVar6619 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6619 = appendResult10_g803;
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6619 = weightedAvg6408;
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6619 = appendResult10_g803;
			fixed3 ifLocalVar7734 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Index > -1.0 )
				ifLocalVar7734 = ifLocalVar6619;
			fixed3 Normal_3 = ifLocalVar7734;
			float2 appendResult11_g799 = float2( texArray3483.x , texArray3483.y );
			float2 temp_cast_173 = (_Texture_4_Normal_Power).xx;
			float2 temp_output_4_0_g799 = ( ( ( appendResult11_g799 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_173 );
			float2 temp_cast_174 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_175 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_176 = (_Texture_4_Normal_Power).xx;
			float temp_output_9_0_g799 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g799 , temp_output_4_0_g799 ) ) ) );
			float3 appendResult10_g799 = float3( temp_output_4_0_g799.x , temp_output_4_0_g799.y , temp_output_9_0_g799 );
			float2 appendResult11_g336 = float2( texArray3512.x , texArray3512.y );
			float2 temp_cast_177 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g336 = ( ( ( appendResult11_g336 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_177 );
			float2 temp_cast_178 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_cast_179 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_cast_180 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g336 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g336 , temp_output_4_0_g336 ) ) ) );
			float3 appendResult19_g336 = float3( temp_output_4_0_g336.y , temp_output_4_0_g336.x , temp_output_9_0_g336 );
			float3 appendResult6878 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g339 = float2( texArray3484.x , texArray3484.y );
			float2 temp_cast_181 = (_Texture_4_Normal_Power).xx;
			float2 temp_output_4_0_g339 = ( ( ( appendResult11_g339 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_181 );
			float2 temp_cast_182 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_183 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_184 = (_Texture_4_Normal_Power).xx;
			float temp_output_9_0_g339 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g339 , temp_output_4_0_g339 ) ) ) );
			float3 appendResult10_g339 = float3( temp_output_4_0_g339.x , temp_output_4_0_g339.y , temp_output_9_0_g339 );
			float3 appendResult6881 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6415 = BlendComponents;
			float3 weightedAvg6415 = ( ( weightedBlendVar6415.x*( appendResult19_g336 * appendResult6878 ) + weightedBlendVar6415.y*appendResult10_g799 + weightedBlendVar6415.z*( appendResult10_g339 * appendResult6881 ) )/( weightedBlendVar6415.x + weightedBlendVar6415.y + weightedBlendVar6415.z ) );
			fixed3 ifLocalVar6625 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6625 = appendResult10_g799;
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6625 = weightedAvg6415;
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6625 = appendResult10_g799;
			fixed3 ifLocalVar7738 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Index > -1.0 )
				ifLocalVar7738 = ifLocalVar6625;
			fixed3 Normal_4 = ifLocalVar7738;
			float4 layeredBlendVar7813 = appendResult6517;
			float3 layeredBlend7813 = ( lerp( lerp( lerp( lerp( fixed3(0,0,1) , Normal_1 , layeredBlendVar7813.x ) , Normal_2 , layeredBlendVar7813.y ) , Normal_3 , layeredBlendVar7813.z ) , Normal_4 , layeredBlendVar7813.w ) );
			float2 appendResult11_g827 = float2( texArray4424.x , texArray4424.y );
			float2 temp_cast_185 = (_Texture_5_Normal_Power).xx;
			float2 temp_output_4_0_g827 = ( ( ( appendResult11_g827 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_185 );
			float2 temp_cast_186 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_187 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_188 = (_Texture_5_Normal_Power).xx;
			float temp_output_9_0_g827 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g827 , temp_output_4_0_g827 ) ) ) );
			float3 appendResult10_g827 = float3( temp_output_4_0_g827.x , temp_output_4_0_g827.y , temp_output_9_0_g827 );
			float2 appendResult11_g804 = float2( texArray4417.x , texArray4417.y );
			float2 temp_cast_189 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g804 = ( ( ( appendResult11_g804 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_189 );
			float2 temp_cast_190 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_cast_191 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_cast_192 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g804 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g804 , temp_output_4_0_g804 ) ) ) );
			float3 appendResult19_g804 = float3( temp_output_4_0_g804.y , temp_output_4_0_g804.x , temp_output_9_0_g804 );
			float3 appendResult6885 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g801 = float2( texArray4422.x , texArray4422.y );
			float2 temp_cast_193 = (_Texture_5_Normal_Power).xx;
			float2 temp_output_4_0_g801 = ( ( ( appendResult11_g801 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_193 );
			float2 temp_cast_194 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_195 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_196 = (_Texture_5_Normal_Power).xx;
			float temp_output_9_0_g801 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g801 , temp_output_4_0_g801 ) ) ) );
			float3 appendResult10_g801 = float3( temp_output_4_0_g801.x , temp_output_4_0_g801.y , temp_output_9_0_g801 );
			float3 appendResult6888 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6422 = BlendComponents;
			float3 weightedAvg6422 = ( ( weightedBlendVar6422.x*( appendResult19_g804 * appendResult6885 ) + weightedBlendVar6422.y*appendResult10_g827 + weightedBlendVar6422.z*( appendResult10_g801 * appendResult6888 ) )/( weightedBlendVar6422.x + weightedBlendVar6422.y + weightedBlendVar6422.z ) );
			fixed3 ifLocalVar6631 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6631 = appendResult10_g827;
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6631 = weightedAvg6422;
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6631 = appendResult10_g827;
			fixed3 ifLocalVar7742 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Index > -1.0 )
				ifLocalVar7742 = ifLocalVar6631;
			fixed3 Normal_5 = ifLocalVar7742;
			float2 appendResult11_g829 = float2( texArray4493.x , texArray4493.y );
			float2 temp_cast_197 = (_Texture_6_Normal_Power).xx;
			float2 temp_output_4_0_g829 = ( ( ( appendResult11_g829 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_197 );
			float2 temp_cast_198 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_199 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_200 = (_Texture_6_Normal_Power).xx;
			float temp_output_9_0_g829 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g829 , temp_output_4_0_g829 ) ) ) );
			float3 appendResult10_g829 = float3( temp_output_4_0_g829.x , temp_output_4_0_g829.y , temp_output_9_0_g829 );
			float2 appendResult11_g795 = float2( texArray4486.x , texArray4486.y );
			float2 temp_cast_201 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g795 = ( ( ( appendResult11_g795 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_201 );
			float2 temp_cast_202 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_cast_203 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_cast_204 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g795 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g795 , temp_output_4_0_g795 ) ) ) );
			float3 appendResult19_g795 = float3( temp_output_4_0_g795.y , temp_output_4_0_g795.x , temp_output_9_0_g795 );
			float3 appendResult6892 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g790 = float2( texArray4491.x , texArray4491.y );
			float2 temp_cast_205 = (_Texture_6_Normal_Power).xx;
			float2 temp_output_4_0_g790 = ( ( ( appendResult11_g790 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_205 );
			float2 temp_cast_206 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_207 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_208 = (_Texture_6_Normal_Power).xx;
			float temp_output_9_0_g790 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g790 , temp_output_4_0_g790 ) ) ) );
			float3 appendResult10_g790 = float3( temp_output_4_0_g790.x , temp_output_4_0_g790.y , temp_output_9_0_g790 );
			float3 appendResult6895 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6429 = BlendComponents;
			float3 weightedAvg6429 = ( ( weightedBlendVar6429.x*( appendResult19_g795 * appendResult6892 ) + weightedBlendVar6429.y*appendResult10_g829 + weightedBlendVar6429.z*( appendResult10_g790 * appendResult6895 ) )/( weightedBlendVar6429.x + weightedBlendVar6429.y + weightedBlendVar6429.z ) );
			fixed3 ifLocalVar6637 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6637 = appendResult10_g829;
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6637 = weightedAvg6429;
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6637 = appendResult10_g829;
			fixed3 ifLocalVar7746 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Index > -1.0 )
				ifLocalVar7746 = ifLocalVar6637;
			fixed3 Normal_6 = ifLocalVar7746;
			float2 appendResult11_g828 = float2( texArray4567.x , texArray4567.y );
			float2 temp_cast_209 = (_Texture_7_Normal_Power).xx;
			float2 temp_output_4_0_g828 = ( ( ( appendResult11_g828 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_209 );
			float2 temp_cast_210 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_211 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_212 = (_Texture_7_Normal_Power).xx;
			float temp_output_9_0_g828 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g828 , temp_output_4_0_g828 ) ) ) );
			float3 appendResult10_g828 = float3( temp_output_4_0_g828.x , temp_output_4_0_g828.y , temp_output_9_0_g828 );
			float2 appendResult11_g794 = float2( texArray4560.x , texArray4560.y );
			float2 temp_cast_213 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g794 = ( ( ( appendResult11_g794 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_213 );
			float2 temp_cast_214 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_cast_215 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_cast_216 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g794 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g794 , temp_output_4_0_g794 ) ) ) );
			float3 appendResult19_g794 = float3( temp_output_4_0_g794.y , temp_output_4_0_g794.x , temp_output_9_0_g794 );
			float3 appendResult6899 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g800 = float2( texArray4565.x , texArray4565.y );
			float2 temp_cast_217 = (_Texture_7_Normal_Power).xx;
			float2 temp_output_4_0_g800 = ( ( ( appendResult11_g800 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_217 );
			float2 temp_cast_218 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_219 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_220 = (_Texture_7_Normal_Power).xx;
			float temp_output_9_0_g800 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g800 , temp_output_4_0_g800 ) ) ) );
			float3 appendResult10_g800 = float3( temp_output_4_0_g800.x , temp_output_4_0_g800.y , temp_output_9_0_g800 );
			float3 appendResult6902 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6436 = BlendComponents;
			float3 weightedAvg6436 = ( ( weightedBlendVar6436.x*( appendResult19_g794 * appendResult6899 ) + weightedBlendVar6436.y*appendResult10_g828 + weightedBlendVar6436.z*( appendResult10_g800 * appendResult6902 ) )/( weightedBlendVar6436.x + weightedBlendVar6436.y + weightedBlendVar6436.z ) );
			fixed3 ifLocalVar6643 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6643 = appendResult10_g828;
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6643 = weightedAvg6436;
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6643 = appendResult10_g828;
			fixed3 ifLocalVar7750 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Index > -1.0 )
				ifLocalVar7750 = ifLocalVar6643;
			fixed3 Normal_7 = ifLocalVar7750;
			float2 appendResult11_g826 = float2( texArray4641.x , texArray4641.y );
			float2 temp_cast_221 = (_Texture_8_Normal_Power).xx;
			float2 temp_output_4_0_g826 = ( ( ( appendResult11_g826 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_221 );
			float2 temp_cast_222 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_223 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_224 = (_Texture_8_Normal_Power).xx;
			float temp_output_9_0_g826 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g826 , temp_output_4_0_g826 ) ) ) );
			float3 appendResult10_g826 = float3( temp_output_4_0_g826.x , temp_output_4_0_g826.y , temp_output_9_0_g826 );
			float2 appendResult11_g796 = float2( texArray4634.x , texArray4634.y );
			float2 temp_cast_225 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g796 = ( ( ( appendResult11_g796 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_225 );
			float2 temp_cast_226 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_cast_227 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_cast_228 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g796 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g796 , temp_output_4_0_g796 ) ) ) );
			float3 appendResult19_g796 = float3( temp_output_4_0_g796.y , temp_output_4_0_g796.x , temp_output_9_0_g796 );
			float3 appendResult6906 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g797 = float2( texArray4639.x , texArray4639.y );
			float2 temp_cast_229 = (_Texture_8_Normal_Power).xx;
			float2 temp_output_4_0_g797 = ( ( ( appendResult11_g797 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_229 );
			float2 temp_cast_230 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_231 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_232 = (_Texture_8_Normal_Power).xx;
			float temp_output_9_0_g797 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g797 , temp_output_4_0_g797 ) ) ) );
			float3 appendResult10_g797 = float3( temp_output_4_0_g797.x , temp_output_4_0_g797.y , temp_output_9_0_g797 );
			float3 appendResult6909 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6443 = BlendComponents;
			float3 weightedAvg6443 = ( ( weightedBlendVar6443.x*( appendResult19_g796 * appendResult6906 ) + weightedBlendVar6443.y*appendResult10_g826 + weightedBlendVar6443.z*( appendResult10_g797 * appendResult6909 ) )/( weightedBlendVar6443.x + weightedBlendVar6443.y + weightedBlendVar6443.z ) );
			fixed3 ifLocalVar6649 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6649 = appendResult10_g826;
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6649 = weightedAvg6443;
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6649 = appendResult10_g826;
			fixed3 ifLocalVar7755 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Index > -1.0 )
				ifLocalVar7755 = ifLocalVar6649;
			fixed3 Normal_8 = ifLocalVar7755;
			float4 layeredBlendVar7815 = appendResult6524;
			float3 layeredBlend7815 = ( lerp( lerp( lerp( lerp( layeredBlend7813 , Normal_5 , layeredBlendVar7815.x ) , Normal_6 , layeredBlendVar7815.y ) , Normal_7 , layeredBlendVar7815.z ) , Normal_8 , layeredBlendVar7815.w ) );
			float2 appendResult11_g844 = float2( texArray4788.x , texArray4788.y );
			float2 temp_cast_233 = (_Texture_9_Normal_Power).xx;
			float2 temp_output_4_0_g844 = ( ( ( appendResult11_g844 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_233 );
			float2 temp_cast_234 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_235 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_236 = (_Texture_9_Normal_Power).xx;
			float temp_output_9_0_g844 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g844 , temp_output_4_0_g844 ) ) ) );
			float3 appendResult10_g844 = float3( temp_output_4_0_g844.x , temp_output_4_0_g844.y , temp_output_9_0_g844 );
			float2 appendResult11_g834 = float2( texArray5285.x , texArray5285.y );
			float2 temp_cast_237 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g834 = ( ( ( appendResult11_g834 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_237 );
			float2 temp_cast_238 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_cast_239 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_cast_240 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g834 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g834 , temp_output_4_0_g834 ) ) ) );
			float3 appendResult19_g834 = float3( temp_output_4_0_g834.y , temp_output_4_0_g834.x , temp_output_9_0_g834 );
			float3 appendResult6962 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g822 = float2( texArray4783.x , texArray4783.y );
			float2 temp_cast_241 = (_Texture_9_Normal_Power).xx;
			float2 temp_output_4_0_g822 = ( ( ( appendResult11_g822 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_241 );
			float2 temp_cast_242 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_243 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_244 = (_Texture_9_Normal_Power).xx;
			float temp_output_9_0_g822 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g822 , temp_output_4_0_g822 ) ) ) );
			float3 appendResult10_g822 = float3( temp_output_4_0_g822.x , temp_output_4_0_g822.y , temp_output_9_0_g822 );
			float3 appendResult6965 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6450 = BlendComponents;
			float3 weightedAvg6450 = ( ( weightedBlendVar6450.x*( appendResult19_g834 * appendResult6962 ) + weightedBlendVar6450.y*appendResult10_g844 + weightedBlendVar6450.z*( appendResult10_g822 * appendResult6965 ) )/( weightedBlendVar6450.x + weightedBlendVar6450.y + weightedBlendVar6450.z ) );
			fixed3 ifLocalVar6667 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6667 = appendResult10_g844;
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar6667 = weightedAvg6450;
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6667 = appendResult10_g844;
			fixed3 ifLocalVar7722 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar7722 = ifLocalVar6667;
			fixed3 Normal_9 = ifLocalVar7722;
			float2 appendResult11_g847 = float2( texArray4822.x , texArray4822.y );
			float2 temp_cast_245 = (_Texture_10_Normal_Power).xx;
			float2 temp_output_4_0_g847 = ( ( ( appendResult11_g847 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_245 );
			float2 temp_cast_246 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_247 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_248 = (_Texture_10_Normal_Power).xx;
			float temp_output_9_0_g847 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g847 , temp_output_4_0_g847 ) ) ) );
			float3 appendResult10_g847 = float3( temp_output_4_0_g847.x , temp_output_4_0_g847.y , temp_output_9_0_g847 );
			float2 appendResult11_g835 = float2( texArray4798.x , texArray4798.y );
			float2 temp_cast_249 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g835 = ( ( ( appendResult11_g835 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_249 );
			float2 temp_cast_250 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_cast_251 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_cast_252 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g835 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g835 , temp_output_4_0_g835 ) ) ) );
			float3 appendResult19_g835 = float3( temp_output_4_0_g835.y , temp_output_4_0_g835.x , temp_output_9_0_g835 );
			float3 appendResult6955 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g831 = float2( texArray4791.x , texArray4791.y );
			float2 temp_cast_253 = (_Texture_10_Normal_Power).xx;
			float2 temp_output_4_0_g831 = ( ( ( appendResult11_g831 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_253 );
			float2 temp_cast_254 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_255 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_256 = (_Texture_10_Normal_Power).xx;
			float temp_output_9_0_g831 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g831 , temp_output_4_0_g831 ) ) ) );
			float3 appendResult10_g831 = float3( temp_output_4_0_g831.x , temp_output_4_0_g831.y , temp_output_9_0_g831 );
			float3 appendResult6958 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6457 = BlendComponents;
			float3 weightedAvg6457 = ( ( weightedBlendVar6457.x*( appendResult19_g835 * appendResult6955 ) + weightedBlendVar6457.y*appendResult10_g847 + weightedBlendVar6457.z*( appendResult10_g831 * appendResult6958 ) )/( weightedBlendVar6457.x + weightedBlendVar6457.y + weightedBlendVar6457.z ) );
			fixed3 ifLocalVar6661 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6661 = appendResult10_g847;
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6661 = weightedAvg6457;
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6661 = appendResult10_g847;
			fixed3 ifLocalVar7718 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7718 = ifLocalVar6661;
			fixed3 Normal_10 = ifLocalVar7718;
			float2 appendResult11_g841 = float2( texArray4856.x , texArray4856.y );
			float2 temp_cast_257 = (_Texture_11_Normal_Power).xx;
			float2 temp_output_4_0_g841 = ( ( ( appendResult11_g841 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_257 );
			float2 temp_cast_258 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_259 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_260 = (_Texture_11_Normal_Power).xx;
			float temp_output_9_0_g841 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g841 , temp_output_4_0_g841 ) ) ) );
			float3 appendResult10_g841 = float3( temp_output_4_0_g841.x , temp_output_4_0_g841.y , temp_output_9_0_g841 );
			float2 appendResult11_g821 = float2( texArray4828.x , texArray4828.y );
			float2 temp_cast_261 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g821 = ( ( ( appendResult11_g821 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_261 );
			float2 temp_cast_262 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_cast_263 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_cast_264 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g821 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g821 , temp_output_4_0_g821 ) ) ) );
			float3 appendResult19_g821 = float3( temp_output_4_0_g821.y , temp_output_4_0_g821.x , temp_output_9_0_g821 );
			float3 appendResult6948 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g833 = float2( texArray4811.x , texArray4811.y );
			float2 temp_cast_265 = (_Texture_11_Normal_Power).xx;
			float2 temp_output_4_0_g833 = ( ( ( appendResult11_g833 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_265 );
			float2 temp_cast_266 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_267 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_268 = (_Texture_11_Normal_Power).xx;
			float temp_output_9_0_g833 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g833 , temp_output_4_0_g833 ) ) ) );
			float3 appendResult10_g833 = float3( temp_output_4_0_g833.x , temp_output_4_0_g833.y , temp_output_9_0_g833 );
			float3 appendResult6951 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6464 = BlendComponents;
			float3 weightedAvg6464 = ( ( weightedBlendVar6464.x*( appendResult19_g821 * appendResult6948 ) + weightedBlendVar6464.y*appendResult10_g841 + weightedBlendVar6464.z*( appendResult10_g833 * appendResult6951 ) )/( weightedBlendVar6464.x + weightedBlendVar6464.y + weightedBlendVar6464.z ) );
			fixed3 ifLocalVar6655 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6655 = appendResult10_g841;
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6655 = weightedAvg6464;
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6655 = appendResult10_g841;
			fixed3 ifLocalVar7712 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Index > -1.0 )
				ifLocalVar7712 = ifLocalVar6655;
			fixed3 Normal_11 = ifLocalVar7712;
			float2 appendResult11_g842 = float2( texArray4870.x , texArray4870.y );
			float2 temp_cast_269 = (_Texture_12_Normal_Power).xx;
			float2 temp_output_4_0_g842 = ( ( ( appendResult11_g842 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_269 );
			float2 temp_cast_270 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_271 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_272 = (_Texture_12_Normal_Power).xx;
			float temp_output_9_0_g842 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g842 , temp_output_4_0_g842 ) ) ) );
			float3 appendResult10_g842 = float3( temp_output_4_0_g842.x , temp_output_4_0_g842.y , temp_output_9_0_g842 );
			float2 appendResult11_g832 = float2( texArray4850.x , texArray4850.y );
			float2 temp_cast_273 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g832 = ( ( ( appendResult11_g832 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_273 );
			float2 temp_cast_274 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_cast_275 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_cast_276 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g832 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g832 , temp_output_4_0_g832 ) ) ) );
			float3 appendResult19_g832 = float3( temp_output_4_0_g832.y , temp_output_4_0_g832.x , temp_output_9_0_g832 );
			float3 appendResult6941 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g830 = float2( texArray4852.x , texArray4852.y );
			float2 temp_cast_277 = (_Texture_12_Normal_Power).xx;
			float2 temp_output_4_0_g830 = ( ( ( appendResult11_g830 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_277 );
			float2 temp_cast_278 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_279 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_280 = (_Texture_12_Normal_Power).xx;
			float temp_output_9_0_g830 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g830 , temp_output_4_0_g830 ) ) ) );
			float3 appendResult10_g830 = float3( temp_output_4_0_g830.x , temp_output_4_0_g830.y , temp_output_9_0_g830 );
			float3 appendResult6944 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6471 = BlendComponents;
			float3 weightedAvg6471 = ( ( weightedBlendVar6471.x*( appendResult19_g832 * appendResult6941 ) + weightedBlendVar6471.y*appendResult10_g842 + weightedBlendVar6471.z*( appendResult10_g830 * appendResult6944 ) )/( weightedBlendVar6471.x + weightedBlendVar6471.y + weightedBlendVar6471.z ) );
			fixed3 ifLocalVar6673 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6673 = appendResult10_g842;
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6673 = weightedAvg6471;
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6673 = appendResult10_g842;
			fixed3 ifLocalVar7709 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Index > -1.0 )
				ifLocalVar7709 = ifLocalVar6673;
			fixed3 Normal_12 = ifLocalVar7709;
			float4 layeredBlendVar7816 = appendResult6529;
			float3 layeredBlend7816 = ( lerp( lerp( lerp( lerp( layeredBlend7815 , Normal_9 , layeredBlendVar7816.x ) , Normal_10 , layeredBlendVar7816.y ) , Normal_11 , layeredBlendVar7816.z ) , Normal_12 , layeredBlendVar7816.w ) );
			float2 appendResult11_g848 = float2( texArray5120.x , texArray5120.y );
			float2 temp_cast_281 = (_Texture_13_Normal_Power).xx;
			float2 temp_output_4_0_g848 = ( ( ( appendResult11_g848 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_281 );
			float2 temp_cast_282 = (_Texture_13_Normal_Power).xx;
			float2 temp_cast_283 = (_Texture_13_Normal_Power).xx;
			float2 temp_cast_284 = (_Texture_13_Normal_Power).xx;
			float temp_output_9_0_g848 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g848 , temp_output_4_0_g848 ) ) ) );
			float3 appendResult10_g848 = float3( temp_output_4_0_g848.x , temp_output_4_0_g848.y , temp_output_9_0_g848 );
			float2 appendResult11_g836 = float2( texArray5127.x , texArray5127.y );
			float2 temp_cast_285 = (( _Texture_13_Normal_Power * -1 )).xx;
			float2 temp_output_4_0_g836 = ( ( ( appendResult11_g836 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_285 );
			float2 temp_cast_286 = (( _Texture_13_Normal_Power * -1 )).xx;
			float2 temp_cast_287 = (( _Texture_13_Normal_Power * -1 )).xx;
			float2 temp_cast_288 = (( _Texture_13_Normal_Power * -1 )).xx;
			float temp_output_9_0_g836 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g836 , temp_output_4_0_g836 ) ) ) );
			float3 appendResult19_g836 = float3( temp_output_4_0_g836.y , temp_output_4_0_g836.x , temp_output_9_0_g836 );
			float3 appendResult6934 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g846 = float2( texArray5109.x , texArray5109.y );
			float2 temp_cast_289 = (_Texture_13_Normal_Power).xx;
			float2 temp_output_4_0_g846 = ( ( ( appendResult11_g846 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_289 );
			float2 temp_cast_290 = (_Texture_13_Normal_Power).xx;
			float2 temp_cast_291 = (_Texture_13_Normal_Power).xx;
			float2 temp_cast_292 = (_Texture_13_Normal_Power).xx;
			float temp_output_9_0_g846 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g846 , temp_output_4_0_g846 ) ) ) );
			float3 appendResult10_g846 = float3( temp_output_4_0_g846.x , temp_output_4_0_g846.y , temp_output_9_0_g846 );
			float3 appendResult6937 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6478 = BlendComponents;
			float3 weightedAvg6478 = ( ( weightedBlendVar6478.x*( appendResult19_g836 * appendResult6934 ) + weightedBlendVar6478.y*appendResult10_g848 + weightedBlendVar6478.z*( appendResult10_g846 * appendResult6937 ) )/( weightedBlendVar6478.x + weightedBlendVar6478.y + weightedBlendVar6478.z ) );
			fixed3 ifLocalVar6679 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6679 = appendResult10_g848;
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6679 = weightedAvg6478;
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6679 = appendResult10_g848;
			fixed3 ifLocalVar7705 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Index > -1.0 )
				ifLocalVar7705 = ifLocalVar6679;
			fixed3 Normal_13 = ifLocalVar7705;
			float2 appendResult11_g849 = float2( texArray5178.x , texArray5178.y );
			float2 temp_cast_293 = (_Texture_14_Normal_Power).xx;
			float2 temp_output_4_0_g849 = ( ( ( appendResult11_g849 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_293 );
			float2 temp_cast_294 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_295 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_296 = (_Texture_14_Normal_Power).xx;
			float temp_output_9_0_g849 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g849 , temp_output_4_0_g849 ) ) ) );
			float3 appendResult10_g849 = float3( temp_output_4_0_g849.x , temp_output_4_0_g849.y , temp_output_9_0_g849 );
			float2 appendResult11_g843 = float2( texArray5017.x , texArray5017.y );
			float2 temp_cast_297 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g843 = ( ( ( appendResult11_g843 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_297 );
			float2 temp_cast_298 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_cast_299 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_cast_300 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g843 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g843 , temp_output_4_0_g843 ) ) ) );
			float3 appendResult19_g843 = float3( temp_output_4_0_g843.y , temp_output_4_0_g843.x , temp_output_9_0_g843 );
			float3 appendResult6927 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g838 = float2( texArray5170.x , texArray5170.y );
			float2 temp_cast_301 = (_Texture_14_Normal_Power).xx;
			float2 temp_output_4_0_g838 = ( ( ( appendResult11_g838 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_301 );
			float2 temp_cast_302 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_303 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_304 = (_Texture_14_Normal_Power).xx;
			float temp_output_9_0_g838 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g838 , temp_output_4_0_g838 ) ) ) );
			float3 appendResult10_g838 = float3( temp_output_4_0_g838.x , temp_output_4_0_g838.y , temp_output_9_0_g838 );
			float3 appendResult6930 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6485 = BlendComponents;
			float3 weightedAvg6485 = ( ( weightedBlendVar6485.x*( appendResult19_g843 * appendResult6927 ) + weightedBlendVar6485.y*appendResult10_g849 + weightedBlendVar6485.z*( appendResult10_g838 * appendResult6930 ) )/( weightedBlendVar6485.x + weightedBlendVar6485.y + weightedBlendVar6485.z ) );
			fixed3 ifLocalVar6685 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6685 = appendResult10_g849;
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6685 = weightedAvg6485;
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6685 = appendResult10_g849;
			fixed3 ifLocalVar7701 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar7701 = ifLocalVar6685;
			fixed3 Normal_14 = ifLocalVar7701;
			float2 appendResult11_g851 = float2( texArray5246.x , texArray5246.y );
			float2 temp_cast_305 = (_Texture_15_Normal_Power).xx;
			float2 temp_output_4_0_g851 = ( ( ( appendResult11_g851 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_305 );
			float2 temp_cast_306 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_307 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_308 = (_Texture_15_Normal_Power).xx;
			float temp_output_9_0_g851 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g851 , temp_output_4_0_g851 ) ) ) );
			float3 appendResult10_g851 = float3( temp_output_4_0_g851.x , temp_output_4_0_g851.y , temp_output_9_0_g851 );
			float2 appendResult11_g845 = float2( texArray5227.x , texArray5227.y );
			float2 temp_cast_309 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g845 = ( ( ( appendResult11_g845 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_309 );
			float2 temp_cast_310 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_cast_311 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_cast_312 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g845 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g845 , temp_output_4_0_g845 ) ) ) );
			float3 appendResult19_g845 = float3( temp_output_4_0_g845.y , temp_output_4_0_g845.x , temp_output_9_0_g845 );
			float3 appendResult6920 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g837 = float2( texArray5250.x , texArray5250.y );
			float2 temp_cast_313 = (_Texture_15_Normal_Power).xx;
			float2 temp_output_4_0_g837 = ( ( ( appendResult11_g837 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_313 );
			float2 temp_cast_314 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_315 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_316 = (_Texture_15_Normal_Power).xx;
			float temp_output_9_0_g837 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g837 , temp_output_4_0_g837 ) ) ) );
			float3 appendResult10_g837 = float3( temp_output_4_0_g837.x , temp_output_4_0_g837.y , temp_output_9_0_g837 );
			float3 appendResult6923 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6492 = BlendComponents;
			float3 weightedAvg6492 = ( ( weightedBlendVar6492.x*( appendResult19_g845 * appendResult6920 ) + weightedBlendVar6492.y*appendResult10_g851 + weightedBlendVar6492.z*( appendResult10_g837 * appendResult6923 ) )/( weightedBlendVar6492.x + weightedBlendVar6492.y + weightedBlendVar6492.z ) );
			fixed3 ifLocalVar6691 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6691 = appendResult10_g851;
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6691 = weightedAvg6492;
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6691 = appendResult10_g851;
			fixed3 ifLocalVar7697 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar7697 = ifLocalVar6691;
			fixed3 Normal_15 = ifLocalVar7697;
			float2 appendResult11_g850 = float2( texArray5099.x , texArray5099.y );
			float2 temp_cast_317 = (_Texture_16_Normal_Power).xx;
			float2 temp_output_4_0_g850 = ( ( ( appendResult11_g850 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_317 );
			float2 temp_cast_318 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_319 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_320 = (_Texture_16_Normal_Power).xx;
			float temp_output_9_0_g850 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g850 , temp_output_4_0_g850 ) ) ) );
			float3 appendResult10_g850 = float3( temp_output_4_0_g850.x , temp_output_4_0_g850.y , temp_output_9_0_g850 );
			float2 appendResult11_g839 = float2( texArray5082.x , texArray5082.y );
			float2 temp_cast_321 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g839 = ( ( ( appendResult11_g839 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_321 );
			float2 temp_cast_322 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_cast_323 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_cast_324 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g839 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g839 , temp_output_4_0_g839 ) ) ) );
			float3 appendResult19_g839 = float3( temp_output_4_0_g839.y , temp_output_4_0_g839.x , temp_output_9_0_g839 );
			float3 appendResult6913 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g840 = float2( texArray4731.x , texArray4731.y );
			float2 temp_cast_325 = (_Texture_16_Normal_Power).xx;
			float2 temp_output_4_0_g840 = ( ( ( appendResult11_g840 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_325 );
			float2 temp_cast_326 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_327 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_328 = (_Texture_16_Normal_Power).xx;
			float temp_output_9_0_g840 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g840 , temp_output_4_0_g840 ) ) ) );
			float3 appendResult10_g840 = float3( temp_output_4_0_g840.x , temp_output_4_0_g840.y , temp_output_9_0_g840 );
			float3 appendResult6916 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6499 = BlendComponents;
			float3 weightedAvg6499 = ( ( weightedBlendVar6499.x*( appendResult19_g839 * appendResult6913 ) + weightedBlendVar6499.y*appendResult10_g850 + weightedBlendVar6499.z*( appendResult10_g840 * appendResult6916 ) )/( weightedBlendVar6499.x + weightedBlendVar6499.y + weightedBlendVar6499.z ) );
			fixed3 ifLocalVar6697 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6697 = appendResult10_g850;
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6697 = weightedAvg6499;
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6697 = appendResult10_g850;
			fixed3 ifLocalVar7692 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar7692 = ifLocalVar6697;
			fixed3 Normal_16 = ifLocalVar7692;
			float4 layeredBlendVar7817 = appendResult6533;
			float3 layeredBlend7817 = ( lerp( lerp( lerp( lerp( layeredBlend7816 , Normal_13 , layeredBlendVar7817.x ) , Normal_14 , layeredBlendVar7817.y ) , Normal_15 , layeredBlendVar7817.z ) , Normal_16 , layeredBlendVar7817.w ) );
			fixed2 temp_cast_329 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray4382 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3679 * temp_cast_329 ), _Texture_Snow_Normal_Index)  );
			float2 appendResult11_g852 = float2( texArray4382.x , texArray4382.y );
			float2 temp_cast_330 = (_Snow_Normal_Scale).xx;
			float2 temp_output_4_0_g852 = ( ( ( appendResult11_g852 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_330 );
			float2 temp_cast_331 = (_Snow_Normal_Scale).xx;
			float2 temp_cast_332 = (_Snow_Normal_Scale).xx;
			float2 temp_cast_333 = (_Snow_Normal_Scale).xx;
			float temp_output_9_0_g852 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g852 , temp_output_4_0_g852 ) ) ) );
			float3 appendResult10_g852 = float3( temp_output_4_0_g852.x , temp_output_4_0_g852.y , temp_output_9_0_g852 );
			fixed3 ifLocalVar7819 = 0;
			UNITY_BRANCH if( _Texture_Snow_Normal_Index > 0.0 )
				ifLocalVar7819 = appendResult10_g852;
			o.Normal = BlendNormals( lerp( lerp( float3( 0,0,1 ) , lerp( appendResult10_g853 , appendResult10_g854 , UVmixDistance ) , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , lerp( appendResult10_g853 , appendResult10_g854 , UVmixDistance ) , ( _Snow_Perlin_Power * 0.5 ) ) , HeightMask6539 ) , normalize( lerp( lerp( normalize( layeredBlend7817 ) , lerp( normalize( layeredBlend7817 ) , ifLocalVar7819 , _Snow_Blend_Normal ) , HeightMask6539 ) , UnpackScaleNormal( tex2D( _Global_Normal_Map, texCoordDummy2588 ) ,_Global_Normalmap_Power ) , UVmixDistance ) ) );
			float4 texArray3292 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Albedo_Index)  );
			fixed2 temp_cast_334 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3293 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_334 ), _Texture_1_Albedo_Index)  );
			float4 texArray3287 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3284 ), _Texture_1_Albedo_Index)  );
			float4 texArray3294 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3284 ), _Texture_1_Albedo_Index)  );
			float3 weightedBlendVar6389 = BlendComponents;
			float4 weightedAvg6389 = ( ( weightedBlendVar6389.x*texArray3287 + weightedBlendVar6389.y*texArray3292 + weightedBlendVar6389.z*texArray3294 )/( weightedBlendVar6389.x + weightedBlendVar6389.y + weightedBlendVar6389.z ) );
			fixed2 temp_cast_335 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3291 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3284 ) / temp_cast_335 ), _Texture_1_Albedo_Index)  );
			fixed2 temp_cast_336 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3295 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3284 ) / temp_cast_336 ), _Texture_1_Albedo_Index)  );
			float3 weightedBlendVar6390 = BlendComponents;
			float4 weightedAvg6390 = ( ( weightedBlendVar6390.x*texArray3291 + weightedBlendVar6390.y*texArray3293 + weightedBlendVar6390.z*texArray3295 )/( weightedBlendVar6390.x + weightedBlendVar6390.y + weightedBlendVar6390.z ) );
			fixed4 ifLocalVar6607 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6607 = lerp( texArray3292 , texArray3293 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6607 = lerp( weightedAvg6389 , weightedAvg6390 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6607 = lerp( texArray3292 , texArray3293 , UVmixDistance );
			fixed4 ifLocalVar7725 = 0;
			UNITY_BRANCH if( _Texture_1_Albedo_Index > -1.0 )
				ifLocalVar7725 = ( ifLocalVar6607 * _Texture_1_Color );
			fixed4 Texture_1_Final = ifLocalVar7725;
			float4 texArray3338 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Albedo_Index)  );
			fixed2 temp_cast_337 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3339 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_337 ), _Texture_2_Albedo_Index)  );
			float4 texArray3355 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3349 ), _Texture_2_Albedo_Index)  );
			float4 texArray3341 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3349 ), _Texture_2_Albedo_Index)  );
			float3 weightedBlendVar6396 = BlendComponents;
			float4 weightedAvg6396 = ( ( weightedBlendVar6396.x*texArray3355 + weightedBlendVar6396.y*texArray3338 + weightedBlendVar6396.z*texArray3341 )/( weightedBlendVar6396.x + weightedBlendVar6396.y + weightedBlendVar6396.z ) );
			fixed2 temp_cast_338 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3356 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3349 ) / temp_cast_338 ), _Texture_2_Albedo_Index)  );
			fixed2 temp_cast_339 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3342 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3349 ) / temp_cast_339 ), _Texture_2_Albedo_Index)  );
			float3 weightedBlendVar6398 = BlendComponents;
			float4 weightedAvg6398 = ( ( weightedBlendVar6398.x*texArray3356 + weightedBlendVar6398.y*texArray3339 + weightedBlendVar6398.z*texArray3342 )/( weightedBlendVar6398.x + weightedBlendVar6398.y + weightedBlendVar6398.z ) );
			fixed4 ifLocalVar6612 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6612 = lerp( texArray3338 , texArray3339 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6612 = lerp( weightedAvg6396 , weightedAvg6398 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6612 = lerp( texArray3338 , texArray3339 , UVmixDistance );
			fixed4 ifLocalVar7729 = 0;
			UNITY_BRANCH if( _Texture_2_Albedo_Index > -1.0 )
				ifLocalVar7729 = ( ifLocalVar6612 * _Texture_2_Color );
			fixed4 Texture_2_Final = ifLocalVar7729;
			float4 texArray3405 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Albedo_Index)  );
			fixed2 temp_cast_340 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_340 ), _Texture_3_Albedo_Index)  );
			float4 texArray3419 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3415 ), _Texture_3_Albedo_Index)  );
			float4 texArray3408 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3415 ), _Texture_3_Albedo_Index)  );
			float3 weightedBlendVar6403 = BlendComponents;
			float4 weightedAvg6403 = ( ( weightedBlendVar6403.x*texArray3419 + weightedBlendVar6403.y*texArray3405 + weightedBlendVar6403.z*texArray3408 )/( weightedBlendVar6403.x + weightedBlendVar6403.y + weightedBlendVar6403.z ) );
			fixed2 temp_cast_341 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3420 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3415 ) / temp_cast_341 ), _Texture_3_Albedo_Index)  );
			fixed2 temp_cast_342 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3409 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3415 ) / temp_cast_342 ), _Texture_3_Albedo_Index)  );
			float3 weightedBlendVar6405 = BlendComponents;
			float4 weightedAvg6405 = ( ( weightedBlendVar6405.x*texArray3420 + weightedBlendVar6405.y*texArray3406 + weightedBlendVar6405.z*texArray3409 )/( weightedBlendVar6405.x + weightedBlendVar6405.y + weightedBlendVar6405.z ) );
			fixed4 ifLocalVar6618 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6618 = lerp( texArray3405 , texArray3406 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6618 = lerp( weightedAvg6403 , weightedAvg6405 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6618 = lerp( texArray3405 , texArray3406 , UVmixDistance );
			fixed4 ifLocalVar7733 = 0;
			UNITY_BRANCH if( _Texture_3_Albedo_Index > -1.0 )
				ifLocalVar7733 = ( ifLocalVar6618 * _Texture_3_Color );
			fixed4 Texture_3_Final = ifLocalVar7733;
			float4 texArray3472 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_343 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3473 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_343 ), _Texture_4_Albedo_Index)  );
			float4 texArray3486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3482 ), _Texture_4_Albedo_Index)  );
			float4 texArray3475 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3482 ), _Texture_4_Albedo_Index)  );
			float3 weightedBlendVar6410 = BlendComponents;
			float4 weightedAvg6410 = ( ( weightedBlendVar6410.x*texArray3486 + weightedBlendVar6410.y*texArray3472 + weightedBlendVar6410.z*texArray3475 )/( weightedBlendVar6410.x + weightedBlendVar6410.y + weightedBlendVar6410.z ) );
			fixed2 temp_cast_344 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3487 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3482 ) / temp_cast_344 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_345 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3476 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3482 ) / temp_cast_345 ), _Texture_4_Albedo_Index)  );
			float3 weightedBlendVar6412 = BlendComponents;
			float4 weightedAvg6412 = ( ( weightedBlendVar6412.x*texArray3487 + weightedBlendVar6412.y*texArray3473 + weightedBlendVar6412.z*texArray3476 )/( weightedBlendVar6412.x + weightedBlendVar6412.y + weightedBlendVar6412.z ) );
			fixed4 ifLocalVar6624 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6624 = lerp( texArray3472 , texArray3473 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6624 = lerp( weightedAvg6410 , weightedAvg6412 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6624 = lerp( texArray3472 , texArray3473 , UVmixDistance );
			fixed4 ifLocalVar7737 = 0;
			UNITY_BRANCH if( _Texture_4_Albedo_Index > -1.0 )
				ifLocalVar7737 = ( ifLocalVar6624 * _Texture_4_Color );
			fixed4 Texture_4_Final = ifLocalVar7737;
			float4 layeredBlendVar6512 = appendResult6517;
			float4 layeredBlend6512 = ( lerp( lerp( lerp( lerp( float4( 0,0,0,0 ) , Texture_1_Final , layeredBlendVar6512.x ) , Texture_2_Final , layeredBlendVar6512.y ) , Texture_3_Final , layeredBlendVar6512.z ) , Texture_4_Final , layeredBlendVar6512.w ) );
			float4 texArray4450 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Albedo_Index)  );
			fixed2 temp_cast_346 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_346 ), _Texture_5_Albedo_Index)  );
			float4 texArray4442 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4399 ), _Texture_5_Albedo_Index)  );
			float4 texArray4443 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4399 ), _Texture_5_Albedo_Index)  );
			float3 weightedBlendVar6417 = BlendComponents;
			float4 weightedAvg6417 = ( ( weightedBlendVar6417.x*texArray4442 + weightedBlendVar6417.y*texArray4450 + weightedBlendVar6417.z*texArray4443 )/( weightedBlendVar6417.x + weightedBlendVar6417.y + weightedBlendVar6417.z ) );
			fixed2 temp_cast_347 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4444 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4399 ) / temp_cast_347 ), _Texture_5_Albedo_Index)  );
			fixed2 temp_cast_348 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4439 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4399 ) / temp_cast_348 ), _Texture_5_Albedo_Index)  );
			float3 weightedBlendVar6419 = BlendComponents;
			float4 weightedAvg6419 = ( ( weightedBlendVar6419.x*texArray4444 + weightedBlendVar6419.y*texArray4445 + weightedBlendVar6419.z*texArray4439 )/( weightedBlendVar6419.x + weightedBlendVar6419.y + weightedBlendVar6419.z ) );
			fixed4 ifLocalVar6630 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6630 = lerp( texArray4450 , texArray4445 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6630 = lerp( weightedAvg6417 , weightedAvg6419 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6630 = lerp( texArray4450 , texArray4445 , UVmixDistance );
			fixed4 ifLocalVar7741 = 0;
			UNITY_BRANCH if( _Texture_5_Albedo_Index > -1.0 )
				ifLocalVar7741 = ( ifLocalVar6630 * _Texture_5_Color );
			fixed4 Texture_5_Final = ifLocalVar7741;
			float4 texArray4517 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Albedo_Index)  );
			fixed2 temp_cast_349 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_349 ), _Texture_6_Albedo_Index)  );
			float4 texArray4509 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4471 ), _Texture_6_Albedo_Index)  );
			float4 texArray4510 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4471 ), _Texture_6_Albedo_Index)  );
			float3 weightedBlendVar6424 = BlendComponents;
			float4 weightedAvg6424 = ( ( weightedBlendVar6424.x*texArray4509 + weightedBlendVar6424.y*texArray4517 + weightedBlendVar6424.z*texArray4510 )/( weightedBlendVar6424.x + weightedBlendVar6424.y + weightedBlendVar6424.z ) );
			fixed2 temp_cast_350 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4511 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4471 ) / temp_cast_350 ), _Texture_6_Albedo_Index)  );
			fixed2 temp_cast_351 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4506 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4471 ) / temp_cast_351 ), _Texture_6_Albedo_Index)  );
			float3 weightedBlendVar6426 = BlendComponents;
			float4 weightedAvg6426 = ( ( weightedBlendVar6426.x*texArray4511 + weightedBlendVar6426.y*texArray4512 + weightedBlendVar6426.z*texArray4506 )/( weightedBlendVar6426.x + weightedBlendVar6426.y + weightedBlendVar6426.z ) );
			fixed4 ifLocalVar6636 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6636 = lerp( texArray4517 , texArray4512 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6636 = lerp( weightedAvg6424 , weightedAvg6426 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6636 = lerp( texArray4517 , texArray4512 , UVmixDistance );
			fixed4 ifLocalVar7745 = 0;
			UNITY_BRANCH if( _Texture_6_Albedo_Index > -1.0 )
				ifLocalVar7745 = ( ifLocalVar6636 * _Texture_6_Color );
			fixed4 Texture_6_Final = ifLocalVar7745;
			float4 texArray4591 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Albedo_Index)  );
			fixed2 temp_cast_352 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_352 ), _Texture_7_Albedo_Index)  );
			float4 texArray4583 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4545 ), _Texture_7_Albedo_Index)  );
			float4 texArray4584 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4545 ), _Texture_7_Albedo_Index)  );
			float3 weightedBlendVar6431 = BlendComponents;
			float4 weightedAvg6431 = ( ( weightedBlendVar6431.x*texArray4583 + weightedBlendVar6431.y*texArray4591 + weightedBlendVar6431.z*texArray4584 )/( weightedBlendVar6431.x + weightedBlendVar6431.y + weightedBlendVar6431.z ) );
			fixed2 temp_cast_353 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4585 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4545 ) / temp_cast_353 ), _Texture_7_Albedo_Index)  );
			fixed2 temp_cast_354 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4580 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4545 ) / temp_cast_354 ), _Texture_7_Albedo_Index)  );
			float3 weightedBlendVar6433 = BlendComponents;
			float4 weightedAvg6433 = ( ( weightedBlendVar6433.x*texArray4585 + weightedBlendVar6433.y*texArray4586 + weightedBlendVar6433.z*texArray4580 )/( weightedBlendVar6433.x + weightedBlendVar6433.y + weightedBlendVar6433.z ) );
			fixed4 ifLocalVar6642 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6642 = lerp( texArray4591 , texArray4586 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6642 = lerp( weightedAvg6431 , weightedAvg6433 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6642 = lerp( texArray4591 , texArray4586 , UVmixDistance );
			fixed4 ifLocalVar7749 = 0;
			UNITY_BRANCH if( _Texture_7_Albedo_Index > -1.0 )
				ifLocalVar7749 = ( ifLocalVar6642 * _Texture_7_Color );
			fixed4 Texture_7_Final = ifLocalVar7749;
			float4 texArray4665 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Albedo_Index)  );
			fixed2 temp_cast_355 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4660 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_355 ), _Texture_8_Albedo_Index)  );
			float4 texArray4657 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4619 ), _Texture_8_Albedo_Index)  );
			float4 texArray4658 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4619 ), _Texture_8_Albedo_Index)  );
			float3 weightedBlendVar6438 = BlendComponents;
			float4 weightedAvg6438 = ( ( weightedBlendVar6438.x*texArray4657 + weightedBlendVar6438.y*texArray4665 + weightedBlendVar6438.z*texArray4658 )/( weightedBlendVar6438.x + weightedBlendVar6438.y + weightedBlendVar6438.z ) );
			fixed2 temp_cast_356 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4659 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4619 ) / temp_cast_356 ), _Texture_8_Albedo_Index)  );
			fixed2 temp_cast_357 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4654 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4619 ) / temp_cast_357 ), _Texture_8_Albedo_Index)  );
			float3 weightedBlendVar6440 = BlendComponents;
			float4 weightedAvg6440 = ( ( weightedBlendVar6440.x*texArray4659 + weightedBlendVar6440.y*texArray4660 + weightedBlendVar6440.z*texArray4654 )/( weightedBlendVar6440.x + weightedBlendVar6440.y + weightedBlendVar6440.z ) );
			fixed4 ifLocalVar6648 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6648 = lerp( texArray4665 , texArray4660 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6648 = lerp( weightedAvg6438 , weightedAvg6440 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6648 = lerp( texArray4665 , texArray4660 , UVmixDistance );
			fixed4 ifLocalVar7754 = 0;
			UNITY_BRANCH if( _Texture_8_Albedo_Index > -1.0 )
				ifLocalVar7754 = ( ifLocalVar6648 * _Texture_8_Color );
			fixed4 Texture_8_Final = ifLocalVar7754;
			float4 layeredBlendVar6520 = appendResult6524;
			float4 layeredBlend6520 = ( lerp( lerp( lerp( lerp( layeredBlend6512 , Texture_5_Final , layeredBlendVar6520.x ) , Texture_6_Final , layeredBlendVar6520.y ) , Texture_7_Final , layeredBlendVar6520.z ) , Texture_8_Final , layeredBlendVar6520.w ) );
			float4 texArray4723 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Albedo_Index)  );
			fixed2 temp_cast_358 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4889 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_358 ), _Texture_9_Albedo_Index)  );
			fixed4 ifLocalVar6666 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6666 = lerp( texArray4723 , texArray4889 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6666 = lerp( texArray4723 , texArray4889 , UVmixDistance );
			fixed4 ifLocalVar7724 = 0;
			UNITY_BRANCH if( _Texture_9_Albedo_Index > -1.0 )
				ifLocalVar7724 = ( ifLocalVar6666 * _Texture_9_Color );
			fixed4 Texture_9_Final = ifLocalVar7724;
			float4 texArray4899 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Albedo_Index)  );
			fixed2 temp_cast_359 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4913 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_359 ), _Texture_10_Albedo_Index)  );
			float4 texArray4886 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4738 ), _Texture_10_Albedo_Index)  );
			float4 texArray4877 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4738 ), _Texture_10_Albedo_Index)  );
			float3 weightedBlendVar6452 = BlendComponents;
			float4 weightedAvg6452 = ( ( weightedBlendVar6452.x*texArray4886 + weightedBlendVar6452.y*texArray4899 + weightedBlendVar6452.z*texArray4877 )/( weightedBlendVar6452.x + weightedBlendVar6452.y + weightedBlendVar6452.z ) );
			fixed2 temp_cast_360 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4894 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4738 ) / temp_cast_360 ), _Texture_10_Albedo_Index)  );
			fixed2 temp_cast_361 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4878 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4738 ) / temp_cast_361 ), _Texture_10_Albedo_Index)  );
			float3 weightedBlendVar6454 = BlendComponents;
			float4 weightedAvg6454 = ( ( weightedBlendVar6454.x*texArray4894 + weightedBlendVar6454.y*texArray4913 + weightedBlendVar6454.z*texArray4878 )/( weightedBlendVar6454.x + weightedBlendVar6454.y + weightedBlendVar6454.z ) );
			fixed4 ifLocalVar6660 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6660 = lerp( texArray4899 , texArray4913 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6660 = lerp( weightedAvg6452 , weightedAvg6454 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6660 = lerp( texArray4899 , texArray4913 , UVmixDistance );
			fixed4 ifLocalVar7719 = 0;
			UNITY_BRANCH if( _Texture_10_Albedo_Index > -1.0 )
				ifLocalVar7719 = ( ifLocalVar6660 * _Texture_10_Color );
			fixed4 Texture_10_Final = ifLocalVar7719;
			float4 texArray4928 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Albedo_Index)  );
			fixed2 temp_cast_362 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4923 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_362 ), _Texture_11_Albedo_Index)  );
			float4 texArray4917 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4741 ), _Texture_11_Albedo_Index)  );
			float4 texArray4911 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4741 ), _Texture_11_Albedo_Index)  );
			float3 weightedBlendVar6459 = BlendComponents;
			float4 weightedAvg6459 = ( ( weightedBlendVar6459.x*texArray4917 + weightedBlendVar6459.y*texArray4928 + weightedBlendVar6459.z*texArray4911 )/( weightedBlendVar6459.x + weightedBlendVar6459.y + weightedBlendVar6459.z ) );
			fixed2 temp_cast_363 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4898 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4741 ) / temp_cast_363 ), _Texture_11_Albedo_Index)  );
			fixed2 temp_cast_364 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4914 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4741 ) / temp_cast_364 ), _Texture_11_Albedo_Index)  );
			float3 weightedBlendVar6461 = BlendComponents;
			float4 weightedAvg6461 = ( ( weightedBlendVar6461.x*texArray4898 + weightedBlendVar6461.y*texArray4923 + weightedBlendVar6461.z*texArray4914 )/( weightedBlendVar6461.x + weightedBlendVar6461.y + weightedBlendVar6461.z ) );
			fixed4 ifLocalVar6654 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6654 = lerp( texArray4928 , texArray4923 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6654 = lerp( weightedAvg6459 , weightedAvg6461 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6654 = lerp( texArray4928 , texArray4923 , UVmixDistance );
			fixed4 ifLocalVar7713 = 0;
			UNITY_BRANCH if( _Texture_11_Albedo_Index > -1.0 )
				ifLocalVar7713 = ( ifLocalVar6654 * _Texture_11_Color );
			fixed4 Texture_11_Final = ifLocalVar7713;
			float4 texArray4954 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Albedo_Index)  );
			fixed2 temp_cast_365 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4952 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_365 ), _Texture_12_Albedo_Index)  );
			float4 texArray4926 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4751 ), _Texture_12_Albedo_Index)  );
			float4 texArray4927 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4751 ), _Texture_12_Albedo_Index)  );
			float3 weightedBlendVar6466 = BlendComponents;
			float4 weightedAvg6466 = ( ( weightedBlendVar6466.x*texArray4926 + weightedBlendVar6466.y*texArray4954 + weightedBlendVar6466.z*texArray4927 )/( weightedBlendVar6466.x + weightedBlendVar6466.y + weightedBlendVar6466.z ) );
			fixed2 temp_cast_366 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4919 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4751 ) / temp_cast_366 ), _Texture_12_Albedo_Index)  );
			fixed2 temp_cast_367 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4931 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4751 ) / temp_cast_367 ), _Texture_12_Albedo_Index)  );
			float3 weightedBlendVar6468 = BlendComponents;
			float4 weightedAvg6468 = ( ( weightedBlendVar6468.x*texArray4919 + weightedBlendVar6468.y*texArray4952 + weightedBlendVar6468.z*texArray4931 )/( weightedBlendVar6468.x + weightedBlendVar6468.y + weightedBlendVar6468.z ) );
			fixed4 ifLocalVar6672 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6672 = lerp( texArray4954 , texArray4952 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6672 = lerp( weightedAvg6466 , weightedAvg6468 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6672 = lerp( texArray4954 , texArray4952 , UVmixDistance );
			fixed4 ifLocalVar7710 = 0;
			UNITY_BRANCH if( _Texture_12_Albedo_Index > -1.0 )
				ifLocalVar7710 = ( ifLocalVar6672 * _Texture_12_Color );
			fixed4 Texture_12_Final = ifLocalVar7710;
			float4 layeredBlendVar6528 = appendResult6529;
			float4 layeredBlend6528 = ( lerp( lerp( lerp( lerp( layeredBlend6520 , Texture_9_Final , layeredBlendVar6528.x ) , Texture_10_Final , layeredBlendVar6528.y ) , Texture_11_Final , layeredBlendVar6528.z ) , Texture_12_Final , layeredBlendVar6528.w ) );
			float4 texArray5043 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Albedo_Index)  );
			fixed2 temp_cast_368 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5034 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_368 ), _Texture_13_Albedo_Index)  );
			float4 texArray5128 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5027 ), _Texture_13_Albedo_Index)  );
			float4 texArray5129 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5027 ), _Texture_13_Albedo_Index)  );
			float3 weightedBlendVar6473 = BlendComponents;
			float4 weightedAvg6473 = ( ( weightedBlendVar6473.x*texArray5128 + weightedBlendVar6473.y*texArray5043 + weightedBlendVar6473.z*texArray5129 )/( weightedBlendVar6473.x + weightedBlendVar6473.y + weightedBlendVar6473.z ) );
			fixed2 temp_cast_369 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5130 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5027 ) / temp_cast_369 ), _Texture_13_Albedo_Index)  );
			fixed2 temp_cast_370 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5121 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5027 ) / temp_cast_370 ), _Texture_13_Albedo_Index)  );
			float3 weightedBlendVar6475 = BlendComponents;
			float4 weightedAvg6475 = ( ( weightedBlendVar6475.x*texArray5130 + weightedBlendVar6475.y*texArray5034 + weightedBlendVar6475.z*texArray5121 )/( weightedBlendVar6475.x + weightedBlendVar6475.y + weightedBlendVar6475.z ) );
			fixed4 ifLocalVar6678 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6678 = lerp( texArray5043 , texArray5034 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6678 = lerp( weightedAvg6473 , weightedAvg6475 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6678 = lerp( texArray5043 , texArray5034 , UVmixDistance );
			fixed4 ifLocalVar7706 = 0;
			UNITY_BRANCH if( _Texture_13_Albedo_Index > -1.0 )
				ifLocalVar7706 = ( ifLocalVar6678 * _Texture_13_Color );
			fixed4 Texture_13_Final = ifLocalVar7706;
			float4 texArray5202 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Albedo_Index)  );
			fixed2 temp_cast_371 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5171 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_371 ), _Texture_14_Albedo_Index)  );
			float4 texArray5168 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5033 ), _Texture_14_Albedo_Index)  );
			float4 texArray5239 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5033 ), _Texture_14_Albedo_Index)  );
			float3 weightedBlendVar6480 = BlendComponents;
			float4 weightedAvg6480 = ( ( weightedBlendVar6480.x*texArray5168 + weightedBlendVar6480.y*texArray5202 + weightedBlendVar6480.z*texArray5239 )/( weightedBlendVar6480.x + weightedBlendVar6480.y + weightedBlendVar6480.z ) );
			fixed2 temp_cast_372 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5205 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5033 ) / temp_cast_372 ), _Texture_14_Albedo_Index)  );
			fixed2 temp_cast_373 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5241 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5033 ) / temp_cast_373 ), _Texture_14_Albedo_Index)  );
			float3 weightedBlendVar6482 = BlendComponents;
			float4 weightedAvg6482 = ( ( weightedBlendVar6482.x*texArray5205 + weightedBlendVar6482.y*texArray5171 + weightedBlendVar6482.z*texArray5241 )/( weightedBlendVar6482.x + weightedBlendVar6482.y + weightedBlendVar6482.z ) );
			fixed4 ifLocalVar6684 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6684 = lerp( texArray5202 , texArray5171 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6684 = lerp( weightedAvg6480 , weightedAvg6482 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6684 = lerp( texArray5202 , texArray5171 , UVmixDistance );
			fixed4 ifLocalVar7702 = 0;
			UNITY_BRANCH if( _Texture_14_Albedo_Index > -1.0 )
				ifLocalVar7702 = ( ifLocalVar6684 * _Texture_14_Color );
			fixed4 Texture_14_Final = ifLocalVar7702;
			float4 texArray5259 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Albedo_Index)  );
			fixed2 temp_cast_374 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5272 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_374 ), _Texture_15_Albedo_Index)  );
			float4 texArray5182 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5212 ), _Texture_15_Albedo_Index)  );
			float4 texArray5189 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5212 ), _Texture_15_Albedo_Index)  );
			float3 weightedBlendVar6487 = BlendComponents;
			float4 weightedAvg6487 = ( ( weightedBlendVar6487.x*texArray5182 + weightedBlendVar6487.y*texArray5259 + weightedBlendVar6487.z*texArray5189 )/( weightedBlendVar6487.x + weightedBlendVar6487.y + weightedBlendVar6487.z ) );
			fixed2 temp_cast_375 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5188 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5212 ) / temp_cast_375 ), _Texture_15_Albedo_Index)  );
			fixed2 temp_cast_376 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5247 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5212 ) / temp_cast_376 ), _Texture_15_Albedo_Index)  );
			float3 weightedBlendVar6489 = BlendComponents;
			float4 weightedAvg6489 = ( ( weightedBlendVar6489.x*texArray5188 + weightedBlendVar6489.y*texArray5272 + weightedBlendVar6489.z*texArray5247 )/( weightedBlendVar6489.x + weightedBlendVar6489.y + weightedBlendVar6489.z ) );
			fixed4 ifLocalVar6690 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6690 = lerp( texArray5259 , texArray5272 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6690 = lerp( weightedAvg6487 , weightedAvg6489 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6690 = lerp( texArray5259 , texArray5272 , UVmixDistance );
			fixed4 ifLocalVar7698 = 0;
			UNITY_BRANCH if( _Texture_15_Albedo_Index > -1.0 )
				ifLocalVar7698 = ( ifLocalVar6690 * _Texture_15_Color );
			fixed4 Texture_15_Final = ifLocalVar7698;
			float4 texArray5139 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Albedo_Index)  );
			fixed2 temp_cast_377 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5143 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_377 ), _Texture_16_Albedo_Index)  );
			float4 texArray5150 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float4 texArray5145 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float3 weightedBlendVar6494 = BlendComponents;
			float4 weightedAvg6494 = ( ( weightedBlendVar6494.x*texArray5150 + weightedBlendVar6494.y*texArray5139 + weightedBlendVar6494.z*texArray5145 )/( weightedBlendVar6494.x + weightedBlendVar6494.y + weightedBlendVar6494.z ) );
			fixed2 temp_cast_378 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5144 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5078 ) / temp_cast_378 ), _Texture_16_Albedo_Index)  );
			fixed2 temp_cast_379 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5154 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5078 ) / temp_cast_379 ), _Texture_16_Albedo_Index)  );
			float3 weightedBlendVar6496 = BlendComponents;
			float4 weightedAvg6496 = ( ( weightedBlendVar6496.x*texArray5144 + weightedBlendVar6496.y*texArray5143 + weightedBlendVar6496.z*texArray5154 )/( weightedBlendVar6496.x + weightedBlendVar6496.y + weightedBlendVar6496.z ) );
			fixed4 ifLocalVar6696 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6696 = lerp( texArray5139 , texArray5143 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6696 = lerp( weightedAvg6494 , weightedAvg6496 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6696 = lerp( texArray5139 , texArray5143 , UVmixDistance );
			fixed4 ifLocalVar7691 = 0;
			UNITY_BRANCH if( _Texture_16_Albedo_Index > -1.0 )
				ifLocalVar7691 = ( ifLocalVar6696 * _Texture_16_Color );
			fixed4 Texture_16_Final = ifLocalVar7691;
			float4 layeredBlendVar6532 = appendResult6533;
			float4 layeredBlend6532 = ( lerp( lerp( lerp( lerp( layeredBlend6528 , Texture_13_Final , layeredBlendVar6532.x ) , Texture_14_Final , layeredBlendVar6532.y ) , Texture_15_Final , layeredBlendVar6532.z ) , Texture_16_Final , layeredBlendVar6532.w ) );
			float3 appendResult3857 = float3( layeredBlend6532.x , layeredBlend6532.y , layeredBlend6532.z );
			fixed2 temp_cast_381 = (( _Geological_Map_Offset_Close + ( ( 1.0 / _Geological_Tiling_Close ) * ase_worldPos.y ) )).xx;
			fixed4 tex2DNode6968 = tex2D( _Texture_Geological_Map, temp_cast_381 );
			float3 appendResult6970 = float3( tex2DNode6968.r , tex2DNode6968.g , tex2DNode6968.b );
			fixed3 temp_cast_382 = (_Geological_Map_Close_Power).xxx;
			fixed2 temp_cast_383 = (( ( ase_worldPos.y * ( 1.0 / _Geological_Tiling_Far ) ) + _Geological_Map_Offset_Far )).xx;
			fixed4 tex2DNode6969 = tex2D( _Texture_Geological_Map, temp_cast_383 );
			float3 appendResult6971 = float3( tex2DNode6969.r , tex2DNode6969.g , tex2DNode6969.b );
			fixed3 temp_cast_384 = (_Geological_Map_Far_Power).xxx;
			fixed3 temp_cast_385 = (( ( _Texture_16_Geological_Power * Splat4_A ) + ( ( _Texture_15_Geological_Power * Splat4_B ) + ( ( _Texture_14_Geological_Power * Splat4_G ) + ( ( _Texture_13_Geological_Power * Splat4_R ) + ( ( _Texture_12_Geological_Power * Splat3_A ) + ( ( _Texture_11_Geological_Power * Splat3_B ) + ( ( _Texture_10_Geological_Power * Splat3_G ) + ( ( _Texture_9_Geological_Power * Splat3_R ) + ( ( _Texture_8_Geological_Power * Splat2_A ) + ( ( _Texture_7_Geological_Power * Splat2_B ) + ( ( _Texture_6_Geological_Power * Splat2_G ) + ( ( _Texture_5_Geological_Power * Splat2_R ) + ( ( _Texture_1_Geological_Power * Splat1_R ) + ( ( _Texture_2_Geological_Power * Splat1_G ) + ( ( _Texture_4_Geological_Power * Splat1_A ) + ( _Texture_3_Geological_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )).xxx;
			fixed2 temp_cast_387 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray4378 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( appendResult3679 * temp_cast_387 ), _Texture_Snow_Index)  );
			fixed2 temp_cast_388 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_389 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray4379 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( appendResult3679 * temp_cast_388 ) / temp_cast_389 ), _Texture_Snow_Index)  );
			fixed4 ifLocalVar7821 = 0;
			UNITY_BRANCH if( _Texture_Snow_Index > 1.0 )
				ifLocalVar7821 = ( lerp( texArray4378 , texArray4379 , UVmixDistance ) * _Snow_Color );
			float3 appendResult1410 = float3( ifLocalVar7821.x , ifLocalVar7821.y , ifLocalVar7821.z );
			o.Albedo = lerp( ( saturate( ( fixed4( appendResult3857 , 0.0 ) + fixed4( ( lerp( ( ( appendResult6970 + float3( -0.3,-0.3,-0.3 ) ) * temp_cast_382 ) , ( ( appendResult6971 + float3( -0.3,-0.3,-0.3 ) ) * temp_cast_384 ) , UVmixDistance ) * temp_cast_385 ) , 0.0 ) ) )) , fixed4( appendResult1410 , 0.0 ) , HeightMask6539 ).rgb;
			fixed3 temp_cast_392 = (_Terrain_Specular).xxx;
			fixed3 temp_cast_393 = (_Snow_Specular).xxx;
			o.Specular = lerp( ( ( appendResult3857 * float3( 0.3,0.3,0.3 ) ) * temp_cast_392 ) , ( clamp( appendResult1410 , float3( 0,0,0 ) , float3( 0.5,0.5,0.5 ) ) * temp_cast_393 ) , HeightMask6539 );
			o.Smoothness = lerp( ( layeredBlend6532.w * _Terrain_Smoothness ) , ifLocalVar7821.w , HeightMask6539 );
			float4 texArray7323 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Emission_AO_Index)  );
			float4 texArray7322 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3284 ), _Texture_1_Emission_AO_Index)  );
			float4 texArray7324 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3284 ), _Texture_1_Emission_AO_Index)  );
			float3 weightedBlendVar7327 = BlendComponents;
			float weightedAvg7327 = ( ( weightedBlendVar7327.x*texArray7322.w + weightedBlendVar7327.y*texArray7323.w + weightedBlendVar7327.z*texArray7324.w )/( weightedBlendVar7327.x + weightedBlendVar7327.y + weightedBlendVar7327.z ) );
			fixed ifLocalVar7325 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar7325 = texArray7323.w;
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar7325 = weightedAvg7327;
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar7325 = texArray7323.w;
			fixed ifLocalVar7766 = 0;
			UNITY_BRANCH if( _Texture_1_Emission_AO_Index > -1.0 )
				ifLocalVar7766 = lerp( ifLocalVar7325 , 1.0 , UVmixDistance );
			fixed Texture_1_E_AO = ifLocalVar7766;
			float4 texArray7336 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Emission_AO_Index)  );
			float4 texArray7345 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3349 ), _Texture_2_Emission_AO_Index)  );
			float4 texArray7335 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3349 ), _Texture_2_Emission_AO_Index)  );
			float3 weightedBlendVar7342 = BlendComponents;
			float weightedAvg7342 = ( ( weightedBlendVar7342.x*texArray7345.w + weightedBlendVar7342.y*texArray7336.w + weightedBlendVar7342.z*texArray7335.w )/( weightedBlendVar7342.x + weightedBlendVar7342.y + weightedBlendVar7342.z ) );
			fixed ifLocalVar7343 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar7343 = texArray7336.w;
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar7343 = weightedAvg7342;
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar7343 = texArray7336.w;
			fixed ifLocalVar7769 = 0;
			UNITY_BRANCH if( _Texture_2_Emission_AO_Index > -1.0 )
				ifLocalVar7769 = lerp( ifLocalVar7343 , 1.0 , UVmixDistance );
			fixed Texture_2_E_AO = ifLocalVar7769;
			float4 texArray7352 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Emission_AO_Index)  );
			float4 texArray7353 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3415 ), _Texture_3_Emission_AO_Index)  );
			float4 texArray7346 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3415 ), _Texture_3_Emission_AO_Index)  );
			float3 weightedBlendVar7350 = BlendComponents;
			float weightedAvg7350 = ( ( weightedBlendVar7350.x*texArray7353.w + weightedBlendVar7350.y*texArray7352.w + weightedBlendVar7350.z*texArray7346.w )/( weightedBlendVar7350.x + weightedBlendVar7350.y + weightedBlendVar7350.z ) );
			fixed ifLocalVar7351 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar7351 = texArray7352.w;
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar7351 = weightedAvg7350;
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar7351 = texArray7352.w;
			fixed ifLocalVar7771 = 0;
			UNITY_BRANCH if( _Texture_3_Emission_AO_Index > -1.0 )
				ifLocalVar7771 = lerp( ifLocalVar7351 , 1.0 , UVmixDistance );
			fixed Texture_3_E_AO = ifLocalVar7771;
			float4 texArray7364 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Emission_AO_Index)  );
			float4 texArray7365 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3482 ), _Texture_4_Emission_AO_Index)  );
			float4 texArray7358 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3482 ), _Texture_4_Emission_AO_Index)  );
			float3 weightedBlendVar7362 = BlendComponents;
			float weightedAvg7362 = ( ( weightedBlendVar7362.x*texArray7365.w + weightedBlendVar7362.y*texArray7364.w + weightedBlendVar7362.z*texArray7358.w )/( weightedBlendVar7362.x + weightedBlendVar7362.y + weightedBlendVar7362.z ) );
			fixed ifLocalVar7363 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar7363 = texArray7364.w;
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar7363 = weightedAvg7362;
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar7363 = texArray7364.w;
			fixed ifLocalVar7773 = 0;
			UNITY_BRANCH if( _Texture_4_Emission_AO_Index > -1.0 )
				ifLocalVar7773 = lerp( ifLocalVar7363 , 1.0 , UVmixDistance );
			fixed Texture_4_E_AO = ifLocalVar7773;
			float4 layeredBlendVar7765 = appendResult6517;
			float layeredBlend7765 = ( lerp( lerp( lerp( lerp( 1.0 , clamp( Texture_1_E_AO , ( 1.0 - _Texture_1_AO_Power ) , 1 ) , layeredBlendVar7765.x ) , clamp( Texture_2_E_AO , ( 1.0 - _Texture_2_AO_Power ) , 1.0 ) , layeredBlendVar7765.y ) , clamp( Texture_3_E_AO , ( 1.0 - _Texture_3_AO_Power ) , 1.0 ) , layeredBlendVar7765.z ) , clamp( Texture_4_E_AO , ( 1.0 - _Texture_4_AO_Power ) , 1.0 ) , layeredBlendVar7765.w ) );
			float4 texArray7376 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Emission_AO_Index)  );
			float4 texArray7377 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4399 ), _Texture_5_Emission_AO_Index)  );
			float4 texArray7370 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4399 ), _Texture_5_Emission_AO_Index)  );
			float3 weightedBlendVar7374 = BlendComponents;
			float weightedAvg7374 = ( ( weightedBlendVar7374.x*texArray7377.w + weightedBlendVar7374.y*texArray7376.w + weightedBlendVar7374.z*texArray7370.w )/( weightedBlendVar7374.x + weightedBlendVar7374.y + weightedBlendVar7374.z ) );
			fixed ifLocalVar7375 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar7375 = texArray7376.w;
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar7375 = weightedAvg7374;
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar7375 = texArray7376.w;
			fixed ifLocalVar7775 = 0;
			UNITY_BRANCH if( _Texture_5_Emission_AO_Index > -1.0 )
				ifLocalVar7775 = lerp( ifLocalVar7375 , 1.0 , UVmixDistance );
			fixed Texture_5_E_AO = ifLocalVar7775;
			float4 texArray7388 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Emission_AO_Index)  );
			float4 texArray7389 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4471 ), _Texture_6_Emission_AO_Index)  );
			float4 texArray7382 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4471 ), _Texture_6_Emission_AO_Index)  );
			float3 weightedBlendVar7386 = BlendComponents;
			float weightedAvg7386 = ( ( weightedBlendVar7386.x*texArray7389.w + weightedBlendVar7386.y*texArray7388.w + weightedBlendVar7386.z*texArray7382.w )/( weightedBlendVar7386.x + weightedBlendVar7386.y + weightedBlendVar7386.z ) );
			fixed ifLocalVar7387 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar7387 = texArray7388.w;
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar7387 = weightedAvg7386;
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar7387 = texArray7388.w;
			fixed ifLocalVar7777 = 0;
			UNITY_BRANCH if( _Texture_6_Emission_AO_Index > -1.0 )
				ifLocalVar7777 = lerp( ifLocalVar7387 , 1.0 , UVmixDistance );
			fixed Texture_6_E_AO = ifLocalVar7777;
			float4 texArray7400 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Emission_AO_Index)  );
			float4 texArray7401 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4545 ), _Texture_7_Emission_AO_Index)  );
			float4 texArray7394 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4545 ), _Texture_7_Emission_AO_Index)  );
			float3 weightedBlendVar7398 = BlendComponents;
			float weightedAvg7398 = ( ( weightedBlendVar7398.x*texArray7401.w + weightedBlendVar7398.y*texArray7400.w + weightedBlendVar7398.z*texArray7394.w )/( weightedBlendVar7398.x + weightedBlendVar7398.y + weightedBlendVar7398.z ) );
			fixed ifLocalVar7399 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar7399 = texArray7400.w;
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar7399 = weightedAvg7398;
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar7399 = texArray7400.w;
			fixed ifLocalVar7779 = 0;
			UNITY_BRANCH if( _Texture_7_Emission_AO_Index > -1.0 )
				ifLocalVar7779 = lerp( ifLocalVar7399 , 1.0 , UVmixDistance );
			fixed Texture_7_E_AO = ifLocalVar7779;
			float4 texArray7412 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Emission_AO_Index)  );
			float4 texArray7413 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4619 ), _Texture_8_Emission_AO_Index)  );
			float4 texArray7406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4619 ), _Texture_8_Emission_AO_Index)  );
			float3 weightedBlendVar7410 = BlendComponents;
			float weightedAvg7410 = ( ( weightedBlendVar7410.x*texArray7413.w + weightedBlendVar7410.y*texArray7412.w + weightedBlendVar7410.z*texArray7406.w )/( weightedBlendVar7410.x + weightedBlendVar7410.y + weightedBlendVar7410.z ) );
			fixed ifLocalVar7411 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar7411 = texArray7412.w;
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar7411 = weightedAvg7410;
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar7411 = texArray7412.w;
			fixed ifLocalVar7781 = 0;
			UNITY_BRANCH if( _Texture_8_Emission_AO_Index > -1.0 )
				ifLocalVar7781 = lerp( ifLocalVar7411 , 1.0 , UVmixDistance );
			fixed Texture_8_E_AO = ifLocalVar7781;
			float4 layeredBlendVar7806 = appendResult6524;
			float layeredBlend7806 = ( lerp( lerp( lerp( lerp( layeredBlend7765 , clamp( Texture_5_E_AO , ( 1.0 - _Texture_5_AO_Power ) , 1.0 ) , layeredBlendVar7806.x ) , clamp( Texture_6_E_AO , ( 1.0 - _Texture_6_AO_Power ) , 1.0 ) , layeredBlendVar7806.y ) , clamp( Texture_7_E_AO , ( 1.0 - _Texture_7_AO_Power ) , 1.0 ) , layeredBlendVar7806.z ) , clamp( Texture_8_E_AO , ( 1.0 - _Texture_8_AO_Power ) , 1.0 ) , layeredBlendVar7806.w ) );
			float4 texArray7508 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Emission_AO_Index)  );
			float4 texArray7509 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4736 ), _Texture_9_Emission_AO_Index)  );
			float4 texArray7502 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4736 ), _Texture_9_Emission_AO_Index)  );
			float3 weightedBlendVar7506 = BlendComponents;
			float weightedAvg7506 = ( ( weightedBlendVar7506.x*texArray7509.w + weightedBlendVar7506.y*texArray7508.w + weightedBlendVar7506.z*texArray7502.w )/( weightedBlendVar7506.x + weightedBlendVar7506.y + weightedBlendVar7506.z ) );
			fixed ifLocalVar7507 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar7507 = texArray7508.w;
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar7507 = weightedAvg7506;
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar7507 = texArray7508.w;
			fixed ifLocalVar7797 = 0;
			UNITY_BRANCH if( _Texture_9_Emission_AO_Index > -1.0 )
				ifLocalVar7797 = lerp( ifLocalVar7507 , 1.0 , UVmixDistance );
			fixed Texture_9_E_AO = ifLocalVar7797;
			float4 texArray7496 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Emission_AO_Index)  );
			float4 texArray7497 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4738 ), _Texture_10_Emission_AO_Index)  );
			float4 texArray7490 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4738 ), _Texture_10_Emission_AO_Index)  );
			float3 weightedBlendVar7494 = BlendComponents;
			float weightedAvg7494 = ( ( weightedBlendVar7494.x*texArray7497.w + weightedBlendVar7494.y*texArray7496.w + weightedBlendVar7494.z*texArray7490.w )/( weightedBlendVar7494.x + weightedBlendVar7494.y + weightedBlendVar7494.z ) );
			fixed ifLocalVar7495 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar7495 = texArray7496.w;
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar7495 = weightedAvg7494;
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar7495 = texArray7496.w;
			fixed ifLocalVar7795 = 0;
			UNITY_BRANCH if( _Texture_10_Emission_AO_Index > -1.0 )
				ifLocalVar7795 = lerp( ifLocalVar7495 , 1.0 , UVmixDistance );
			fixed Texture_10_E_AO = ifLocalVar7795;
			float4 texArray7484 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Emission_AO_Index)  );
			float4 texArray7485 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4741 ), _Texture_11_Emission_AO_Index)  );
			float4 texArray7478 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4741 ), _Texture_11_Emission_AO_Index)  );
			float3 weightedBlendVar7482 = BlendComponents;
			float weightedAvg7482 = ( ( weightedBlendVar7482.x*texArray7485.w + weightedBlendVar7482.y*texArray7484.w + weightedBlendVar7482.z*texArray7478.w )/( weightedBlendVar7482.x + weightedBlendVar7482.y + weightedBlendVar7482.z ) );
			fixed ifLocalVar7483 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar7483 = texArray7484.w;
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar7483 = weightedAvg7482;
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar7483 = texArray7484.w;
			fixed ifLocalVar7793 = 0;
			UNITY_BRANCH if( _Texture_11_Emission_AO_Index > -1.0 )
				ifLocalVar7793 = lerp( ifLocalVar7483 , 1.0 , UVmixDistance );
			fixed Texture_11_E_AO = ifLocalVar7793;
			float4 texArray7472 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Emission_AO_Index)  );
			float4 texArray7473 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4751 ), _Texture_12_Emission_AO_Index)  );
			float4 texArray7466 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4751 ), _Texture_12_Emission_AO_Index)  );
			float3 weightedBlendVar7470 = BlendComponents;
			float weightedAvg7470 = ( ( weightedBlendVar7470.x*texArray7473.w + weightedBlendVar7470.y*texArray7472.w + weightedBlendVar7470.z*texArray7466.w )/( weightedBlendVar7470.x + weightedBlendVar7470.y + weightedBlendVar7470.z ) );
			fixed ifLocalVar7471 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar7471 = texArray7472.w;
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar7471 = weightedAvg7470;
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar7471 = texArray7472.w;
			fixed ifLocalVar7791 = 0;
			UNITY_BRANCH if( _Texture_12_Emission_AO_Index > -1.0 )
				ifLocalVar7791 = lerp( ifLocalVar7471 , 1.0 , UVmixDistance );
			fixed Texture_12_E_AO = ifLocalVar7791;
			float4 layeredBlendVar7807 = appendResult6529;
			float layeredBlend7807 = ( lerp( lerp( lerp( lerp( layeredBlend7806 , clamp( Texture_9_E_AO , ( 1.0 - _Texture_9_AO_Power ) , 1.0 ) , layeredBlendVar7807.x ) , clamp( Texture_10_E_AO , ( 1.0 - _Texture_10_AO_Power ) , 1.0 ) , layeredBlendVar7807.y ) , clamp( Texture_11_E_AO , ( 1.0 - _Texture_11_AO_Power ) , 1.0 ) , layeredBlendVar7807.z ) , clamp( Texture_12_E_AO , ( 1.0 - _Texture_12_AO_Power ) , 1.0 ) , layeredBlendVar7807.w ) );
			float4 texArray7460 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Emission_AO_Index)  );
			float4 texArray7461 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5027 ), _Texture_13_Emission_AO_Index)  );
			float4 texArray7454 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5027 ), _Texture_13_Emission_AO_Index)  );
			float3 weightedBlendVar7458 = BlendComponents;
			float weightedAvg7458 = ( ( weightedBlendVar7458.x*texArray7461.w + weightedBlendVar7458.y*texArray7460.w + weightedBlendVar7458.z*texArray7454.w )/( weightedBlendVar7458.x + weightedBlendVar7458.y + weightedBlendVar7458.z ) );
			fixed ifLocalVar7459 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar7459 = texArray7460.w;
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar7459 = weightedAvg7458;
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar7459 = texArray7460.w;
			fixed ifLocalVar7789 = 0;
			UNITY_BRANCH if( _Texture_13_Emission_AO_Index > -1.0 )
				ifLocalVar7789 = lerp( ifLocalVar7459 , 1.0 , UVmixDistance );
			fixed Texture_13_E_AO = ifLocalVar7789;
			float4 texArray7448 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Emission_AO_Index)  );
			float4 texArray7449 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5033 ), _Texture_14_Emission_AO_Index)  );
			float4 texArray7442 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5033 ), _Texture_14_Emission_AO_Index)  );
			float3 weightedBlendVar7446 = BlendComponents;
			float weightedAvg7446 = ( ( weightedBlendVar7446.x*texArray7449.w + weightedBlendVar7446.y*texArray7448.w + weightedBlendVar7446.z*texArray7442.w )/( weightedBlendVar7446.x + weightedBlendVar7446.y + weightedBlendVar7446.z ) );
			fixed ifLocalVar7447 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar7447 = texArray7448.w;
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar7447 = weightedAvg7446;
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar7447 = texArray7448.w;
			fixed ifLocalVar7787 = 0;
			UNITY_BRANCH if( _Texture_14_Emission_AO_Index > -1.0 )
				ifLocalVar7787 = lerp( ifLocalVar7447 , 1.0 , UVmixDistance );
			fixed Texture_14_E_AO = ifLocalVar7787;
			float4 texArray7436 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Emission_AO_Index)  );
			float4 texArray7437 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5212 ), _Texture_15_Emission_AO_Index)  );
			float4 texArray7430 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5212 ), _Texture_15_Emission_AO_Index)  );
			float3 weightedBlendVar7434 = BlendComponents;
			float weightedAvg7434 = ( ( weightedBlendVar7434.x*texArray7437.w + weightedBlendVar7434.y*texArray7436.w + weightedBlendVar7434.z*texArray7430.w )/( weightedBlendVar7434.x + weightedBlendVar7434.y + weightedBlendVar7434.z ) );
			fixed ifLocalVar7435 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar7435 = texArray7436.w;
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar7435 = weightedAvg7434;
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar7435 = texArray7436.w;
			fixed ifLocalVar7785 = 0;
			UNITY_BRANCH if( _Texture_15_Emission_AO_Index > -1.0 )
				ifLocalVar7785 = lerp( ifLocalVar7435 , 1.0 , UVmixDistance );
			fixed Texture_15_E_AO = ifLocalVar7785;
			float4 texArray7424 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Emission_AO_Index)  );
			float4 texArray7425 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5078 ), _Texture_16_Emission_AO_Index)  );
			float4 texArray7418 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5078 ), _Texture_16_Emission_AO_Index)  );
			float3 weightedBlendVar7422 = BlendComponents;
			float weightedAvg7422 = ( ( weightedBlendVar7422.x*texArray7425.w + weightedBlendVar7422.y*texArray7424.w + weightedBlendVar7422.z*texArray7418.w )/( weightedBlendVar7422.x + weightedBlendVar7422.y + weightedBlendVar7422.z ) );
			fixed ifLocalVar7423 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar7423 = texArray7424.w;
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar7423 = weightedAvg7422;
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar7423 = texArray7424.w;
			fixed ifLocalVar7783 = 0;
			UNITY_BRANCH if( _Texture_16_Emission_AO_Index > -1.0 )
				ifLocalVar7783 = lerp( ifLocalVar7423 , 1.0 , UVmixDistance );
			fixed Texture_16_E_AO = ifLocalVar7783;
			float4 layeredBlendVar7808 = appendResult6533;
			float layeredBlend7808 = ( lerp( lerp( lerp( lerp( layeredBlend7807 , clamp( Texture_13_E_AO , ( 1.0 - _Texture_13_AO_Power ) , 1.0 ) , layeredBlendVar7808.x ) , clamp( Texture_14_E_AO , ( 1.0 - _Texture_14_AO_Power ) , 1.0 ) , layeredBlendVar7808.y ) , clamp( Texture_15_E_AO , ( 1.0 - _Texture_15_AO_Power ) , 1.0 ) , layeredBlendVar7808.z ) , clamp( Texture_16_E_AO , ( 1.0 - _Texture_16_AO_Power ) , 1.0 ) , layeredBlendVar7808.w ) );
			fixed2 temp_cast_394 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray7530 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( appendResult3679 * temp_cast_394 ), _Snow_Emission_AO_index)  );
			fixed ifLocalVar7802 = 0;
			UNITY_BRANCH if( _Snow_Emission_AO_index > -1.0 )
				ifLocalVar7802 = lerp( clamp( texArray7530.w , ( 1.0 - _Snow_Ambient_Occlusion_Power ) , 1.0 ) , 1.0 , UVmixDistance );
			#ifdef _USE_AO_TEXTURE_ON
			float staticSwitch7761 = clamp( lerp( layeredBlend7808 , ifLocalVar7802 , HeightMask6539 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#else
			float staticSwitch7761 = clamp( ( ( 1.0 + BlendNormals( lerp( lerp( float3( 0,0,1 ) , lerp( appendResult10_g853 , appendResult10_g854 , UVmixDistance ) , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , lerp( appendResult10_g853 , appendResult10_g854 , UVmixDistance ) , ( _Snow_Perlin_Power * 0.5 ) ) , HeightMask6539 ) , normalize( lerp( lerp( normalize( layeredBlend7817 ) , lerp( normalize( layeredBlend7817 ) , ifLocalVar7819 , _Snow_Blend_Normal ) , HeightMask6539 ) , UnpackScaleNormal( tex2D( _Global_Normal_Map, texCoordDummy2588 ) ,_Global_Normalmap_Power ) , UVmixDistance ) ) ).y ) * 0.5 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#endif
			#ifdef _USE_AO_ON
			float staticSwitch7762 = staticSwitch7761;
			#else
			float staticSwitch7762 = 1.0;
			#endif
			o.Occlusion = staticSwitch7762;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers d3d9 gles 
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows noforwardadd vertex:vertexDataFunc tessellate:tessFunction tessphong:_TessPhongStrength 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	Dependency "BaseMapShader" = "CTS Terrain Shader Advanced LOD"
}