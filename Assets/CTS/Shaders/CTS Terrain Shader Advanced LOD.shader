Shader "CTS Terrain Shader Advanced LOD"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Geological_Tiling_Far("Geological_Tiling_Far", Range( 0 , 1000)) = 87
		_Geological_Map_Offset_Far("Geological_Map_Offset _Far", Range( 0 , 1)) = 1
		_Geological_Map_Far_Power("Geological_Map_Far_Power", Range( 0 , 2)) = 1
		_Perlin_Normal_Tiling_Far("Perlin_Normal_Tiling_Far", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Power("Perlin_Normal_Power", Range( 0 , 2)) = 1
		_Terrain_Smoothness("Terrain_Smoothness", Range( 0 , 2)) = 1
		_Terrain_Specular("Terrain_Specular", Range( 0 , 3)) = 1
		_Global_Normalmap_Power("Global_Normalmap_Power", Range( 0 , 10)) = 0
		_Texture_1_Tiling("Texture_1_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_2_Tiling("Texture_2_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_3_Tiling("Texture_3_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_4_Tiling("Texture_4_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_5_Tiling("Texture_5_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_6_Tiling("Texture_6_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_7_Tiling("Texture_7_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_8_Tiling("Texture_8_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_9_Tiling("Texture_9_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_10_Tiling("Texture_10_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_11_Tiling("Texture_11_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_12_Tiling("Texture_12_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_13_Tiling("Texture_13_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_14_Tiling("Texture_14_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_15_Tiling("Texture_15_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_16_Tiling("Texture_16_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_1_Far_Multiplier("Texture_1_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_2_Far_Multiplier("Texture_2_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_3_Far_Multiplier("Texture_3_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_4_Far_Multiplier("Texture_4_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_5_Far_Multiplier("Texture_5_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_6_Far_Multiplier("Texture_6_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_7_Far_Multiplier("Texture_7_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_8_Far_Multiplier("Texture_8_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_9_Far_Multiplier("Texture_9_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_10_Far_Multiplier("Texture_10_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_11_Far_Multiplier("Texture_11_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_12_Far_Multiplier("Texture_12_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_13_Far_Multiplier("Texture_13_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_14_Far_Multiplier("Texture_14_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_15_Far_Multiplier("Texture_15_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_16_Far_Multiplier("Texture_16_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_1_Perlin_Power("Texture_1_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_2_Perlin_Power("Texture_2_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_3_Perlin_Power("Texture_3_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_4_Perlin_Power("Texture_4_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_5_Perlin_Power("Texture_5_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_6_Perlin_Power("Texture_6_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_7_Perlin_Power("Texture_7_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_8_Perlin_Power("Texture_8_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_9_Perlin_Power("Texture_9_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_10_Perlin_Power("Texture_10_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_11_Perlin_Power("Texture_11_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_12_Perlin_Power("Texture_12_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_13_Perlin_Power("Texture_13_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_14_Perlin_Power("Texture_14_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_15_Perlin_Power("Texture_15_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_16_Perlin_Power("Texture_16_Perlin_Power", Range( 0 , 2)) = 0
		_Snow_Heightmap_Depth("Snow_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_14_Heightmap_Depth("Texture_14_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_13_Heightmap_Depth("Texture_13_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_11_Heightmap_Depth("Texture_11_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_15_Heightmap_Depth("Texture_15_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_16_Heightmap_Depth("Texture_16_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_12_Heightmap_Depth("Texture_12_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_6_Heightmap_Depth("Texture_6_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_Array_Albedo("Texture_Array_Albedo", 2DArray ) = "" {}
		_Texture_10_Heightmap_Depth("Texture_10_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_9_Heightmap_Depth("Texture_9_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_5_Heightmap_Depth("Texture_5_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_7_Heightmap_Depth("Texture_7_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_3_Heightmap_Depth("Texture_3_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_8_Heightmap_Depth("Texture_8_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_4_Heightmap_Depth("Texture_4_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_2_Heightmap_Depth("Texture_2_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_1_Heightmap_Depth("Texture_1_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_1_Tesselation_Depth("Texture_1_Tesselation_Depth", Range( 0 , 10)) = 1
		_Snow_Height_Contrast("Snow_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_14_Height_Contrast("Texture_14_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_16_Height_Contrast("Texture_16_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_15_Height_Contrast("Texture_15_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_9_Height_Contrast("Texture_9_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_13_Height_Contrast("Texture_13_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_10_Height_Contrast("Texture_10_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_11_Height_Contrast("Texture_11_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_12_Height_Contrast("Texture_12_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_6_Height_Contrast("Texture_6_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_8_Height_Contrast("Texture_8_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_7_Height_Contrast("Texture_7_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_3_Height_Contrast("Texture_3_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_5_Height_Contrast("Texture_5_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_4_Height_Contrast("Texture_4_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_1_Height_Contrast("Texture_1_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_2_Height_Contrast("Texture_2_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_2_Tesselation_Depth("Texture_2_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_3_Tesselation_Depth("Texture_3_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_4_Tesselation_Depth("Texture_4_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_5_Tesselation_Depth("Texture_5_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_6_Tesselation_Depth("Texture_6_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_7_Tesselation_Depth("Texture_7_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_8_Tesselation_Depth("Texture_8_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_9_Tesselation_Depth("Texture_9_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_10_Tesselation_Depth("Texture_10_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_11_Tesselation_Depth("Texture_11_Tesselation_Depth", Range( 0 , 10)) = 0
		_Texture_12_Tesselation_Depth("Texture_12_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_13_Tesselation_Depth("Texture_13_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_14_Tesselation_Depth("Texture_14_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_16_Heightblend_Far("Texture_16_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_15_Heightblend_Far("Texture_15_Heightblend_Far", Range( 1 , 10)) = 5
		_Snow_Heightblend_Far("Snow_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_14_Heightblend_Far("Texture_14_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_12_Heightblend_Far("Texture_12_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_Snow_Noise_Index("Texture_Snow_Noise_Index", Int) = -1
		_Texture_13_Heightblend_Far("Texture_13_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_7_Heightblend_Far("Texture_7_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_11_Heightblend_Far("Texture_11_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_10_Heightblend_Far("Texture_10_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_9_Heightblend_Far("Texture_9_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_8_Heightblend_Far("Texture_8_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_6_Heightblend_Far("Texture_6_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_5_Heightblend_Far("Texture_5_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_3_Heightblend_Far("Texture_3_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_4_Heightblend_Far("Texture_4_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_15_Tesselation_Depth("Texture_15_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_2_Heightblend_Far("Texture_2_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_1_Heightblend_Far("Texture_1_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_16_Tesselation_Depth("Texture_16_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_1_Geological_Power("Texture_1_Geological_Power", Range( 0 , 2)) = 1
		_Texture_2_Geological_Power("Texture_2_Geological_Power", Range( 0 , 2)) = 1
		_Texture_3_Geological_Power("Texture_3_Geological_Power", Range( 0 , 2)) = 1
		_Texture_4_Geological_Power("Texture_4_Geological_Power", Range( 0 , 2)) = 1
		_Texture_5_Geological_Power("Texture_5_Geological_Power", Range( 0 , 2)) = 1
		_Texture_6_Geological_Power("Texture_6_Geological_Power", Range( 0 , 2)) = 1
		_Texture_7_Geological_Power("Texture_7_Geological_Power", Range( 0 , 2)) = 1
		_Texture_8_Geological_Power("Texture_8_Geological_Power", Range( 0 , 2)) = 1
		_Texture_9_Geological_Power("Texture_9_Geological_Power", Range( 0 , 2)) = 1
		_Texture_10_Geological_Power("Texture_10_Geological_Power", Range( 0 , 2)) = 1
		_Texture_11_Geological_Power("Texture_11_Geological_Power", Range( 0 , 2)) = 1
		_Texture_12_Geological_Power("Texture_12_Geological_Power", Range( 0 , 2)) = 1
		_Texture_13_Geological_Power("Texture_13_Geological_Power", Range( 0 , 2)) = 1
		_Texture_14_Geological_Power("Texture_14_Geological_Power", Range( 0 , 2)) = 1
		_Texture_15_Geological_Power("Texture_15_Geological_Power", Range( 0 , 2)) = 1
		_Texture_16_Geological_Power("Texture_16_Geological_Power", Range( 0 , 2)) = 1
		_Snow_Specular("Snow_Specular", Range( 0 , 3)) = 1
		_Snow_Color("Snow_Color", Vector) = (1,1,1,1)
		_Snow_Amount("Snow_Amount", Range( 0 , 2)) = 0
		_Snow_Tiling("Snow_Tiling", Range( 0.001 , 20)) = 15
		_Snow_Tiling_Far_Multiplier("Snow_Tiling_Far_Multiplier", Range( 0.001 , 20)) = 1
		_Snow_Perlin_Power("Snow_Perlin_Power", Range( 0 , 2)) = 0
		_Snow_Noise_Power("Snow_Noise_Power", Range( 0 , 1)) = 1
		_Snow_Noise_Tiling("Snow_Noise_Tiling", Range( 0.001 , 1)) = 0.02
		_Snow_Min_Height("Snow_Min_Height", Range( -1000 , 10000)) = -1000
		_Snow_Min_Height_Blending("Snow_Min_Height_Blending", Range( 0 , 500)) = 1
		_Snow_Maximum_Angle("Snow_Maximum_Angle", Range( 0.001 , 180)) = 30
		_Snow_Maximum_Angle_Hardness("Snow_Maximum_Angle_Hardness", Range( 0.001 , 10)) = 1
		_Texture_1_Snow_Reduction("Texture_1_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_2_Snow_Reduction("Texture_2_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_3_Snow_Reduction("Texture_3_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_4_Snow_Reduction("Texture_4_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_5_Snow_Reduction("Texture_5_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_6_Snow_Reduction("Texture_6_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_7_Snow_Reduction("Texture_7_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_8_Snow_Reduction("Texture_8_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_9_Snow_Reduction("Texture_9_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_10_Snow_Reduction("Texture_10_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_11_Snow_Reduction("Texture_11_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_12_Snow_Reduction("Texture_12_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_13_Snow_Reduction("Texture_13_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_14_Snow_Reduction("Texture_14_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_15_Snow_Reduction("Texture_15_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_16_Snow_Reduction("Texture_16_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_Array_Normal("Texture_Array_Normal", 2DArray ) = "" {}
		_Texture_Perlin_Normal_Index("Texture_Perlin_Normal_Index", Int) = -1
		_Texture_Snow_Index("Texture_Snow_Index", Int) = -1
		_Texture_Snow_Normal_Index("Texture_Snow_Normal_Index", Int) = -1
		_Texture_9_Color("Texture_9_Color", Vector) = (1,1,1,1)
		_Texture_10_Color("Texture_10_Color", Vector) = (1,1,1,1)
		_Texture_13_Color("Texture_13_Color", Vector) = (1,1,1,1)
		_Texture_11_Color("Texture_11_Color", Vector) = (1,1,1,1)
		_Texture_12_Color("Texture_12_Color", Vector) = (1,1,1,1)
		_Texture_15_Color("Texture_15_Color", Vector) = (1,1,1,1)
		_Texture_14_Color("Texture_14_Color", Vector) = (1,1,1,1)
		_Texture_8_Color("Texture_8_Color", Vector) = (1,1,1,1)
		_Texture_16_Color("Texture_16_Color", Vector) = (1,1,1,1)
		_Texture_7_Color("Texture_7_Color", Vector) = (1,1,1,1)
		_Texture_5_Color("Texture_5_Color", Vector) = (1,1,1,1)
		_Texture_6_Color("Texture_6_Color", Vector) = (1,1,1,1)
		_Texture_4_Color("Texture_4_Color", Vector) = (1,1,1,1)
		_Texture_1_Color("Texture_1_Color", Vector) = (1,1,1,1)
		_Texture_3_Color("Texture_3_Color", Vector) = (1,1,1,1)
		_Texture_2_Color("Texture_2_Color", Vector) = (1,1,1,1)
		_Texture_Geological_Map("Texture_Geological_Map", 2D) = "white" {}
		_Texture_Splat_1("Texture_Splat_1", 2D) = "black" {}
		_Texture_Splat_2("Texture_Splat_2", 2D) = "black" {}
		_Texture_Splat_3("Texture_Splat_3", 2D) = "black" {}
		_Texture_Splat_4("Texture_Splat_4", 2D) = "black" {}
		_Global_Normal_Map("Global_Normal_Map", 2D) = "bump" {}
		_Texture_1_Albedo_Index("Texture_1_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_1_Normal_Index("Texture_1_Normal_Index", Range( -1 , 100)) = -1
		_Texture_2_Albedo_Index("Texture_2_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_2_Normal_Index("Texture_2_Normal_Index", Range( -1 , 100)) = -1
		_Texture_3_Normal_Index("Texture_3_Normal_Index", Range( -1 , 100)) = -1
		_Texture_3_Albedo_Index("Texture_3_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_4_Normal_Index("Texture_4_Normal_Index", Range( -1 , 100)) = -1
		_Texture_4_Albedo_Index("Texture_4_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_5_Albedo_Index("Texture_5_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_5_Normal_Index("Texture_5_Normal_Index", Range( -1 , 100)) = -1
		_Texture_6_Normal_Index("Texture_6_Normal_Index", Range( -1 , 100)) = -1
		_Texture_6_Albedo_Index("Texture_6_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_7_Albedo_Index("Texture_7_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_7_Normal_Index("Texture_7_Normal_Index", Range( -1 , 100)) = -1
		_Texture_8_Albedo_Index("Texture_8_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_8_Normal_Index("Texture_8_Normal_Index", Range( -1 , 100)) = -1
		_Texture_16_Normal_Index("Texture_16_Normal_Index", Range( -1 , 100)) = -1
		_Texture_15_Normal_Index("Texture_15_Normal_Index", Range( -1 , 100)) = -1
		_Texture_16_Albedo_Index("Texture_16_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_15_Albedo_Index("Texture_15_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_14_Normal_Index("Texture_14_Normal_Index", Range( -1 , 100)) = -1
		_Texture_14_Albedo_Index("Texture_14_Albedo_Index", Range( -1 , 100)) = -1
		_Ambient_Occlusion_Power("Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		_Texture_13_Normal_Index("Texture_13_Normal_Index", Range( -1 , 100)) = -1
		_Texture_13_Albedo_Index("Texture_13_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_12_Normal_Index("Texture_12_Normal_Index", Range( -1 , 100)) = -1
		_Texture_12_Albedo_Index("Texture_12_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_11_Normal_Index("Texture_11_Normal_Index", Range( -1 , 100)) = -1
		_Texture_11_Albedo_Index("Texture_11_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_10_Albedo_Index("Texture_10_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_10_Normal_Index("Texture_10_Normal_Index", Range( -1 , 100)) = -1
		_Texture_9_Normal_Index("Texture_9_Normal_Index", Range( -1 , 100)) = -1
		_Texture_9_Albedo_Index("Texture_9_Albedo_Index", Range( -1 , 100)) = -1
		[Toggle]_Use_AO_Texture("Use_AO_Texture", Int) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+100" }
		Cull Back
		ZTest LEqual
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.5
		#pragma multi_compile __ _USE_AO_TEXTURE_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float2 texcoord_0;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform fixed _Perlin_Normal_Tiling_Far;
		uniform int _Texture_Perlin_Normal_Index;
		uniform fixed _Perlin_Normal_Power;
		uniform fixed _Texture_16_Perlin_Power;
		uniform sampler2D _Texture_Splat_4;
		uniform fixed _Texture_15_Perlin_Power;
		uniform fixed _Texture_14_Perlin_Power;
		uniform fixed _Texture_13_Perlin_Power;
		uniform fixed _Texture_12_Perlin_Power;
		uniform sampler2D _Texture_Splat_3;
		uniform fixed _Texture_11_Perlin_Power;
		uniform fixed _Texture_10_Perlin_Power;
		uniform fixed _Texture_9_Perlin_Power;
		uniform fixed _Texture_8_Perlin_Power;
		uniform sampler2D _Texture_Splat_2;
		uniform fixed _Texture_7_Perlin_Power;
		uniform fixed _Texture_6_Perlin_Power;
		uniform fixed _Texture_5_Perlin_Power;
		uniform fixed _Texture_1_Perlin_Power;
		uniform sampler2D _Texture_Splat_1;
		uniform fixed _Texture_2_Perlin_Power;
		uniform fixed _Texture_4_Perlin_Power;
		uniform fixed _Texture_3_Perlin_Power;
		uniform fixed _Snow_Perlin_Power;
		uniform fixed _Texture_1_Tesselation_Depth;
		uniform fixed _Texture_1_Normal_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Normal );
		uniform fixed _Texture_1_Tiling;
		uniform fixed _Texture_1_Far_Multiplier;
		uniform fixed _Texture_2_Tesselation_Depth;
		uniform fixed _Texture_2_Normal_Index;
		uniform fixed _Texture_2_Tiling;
		uniform fixed _Texture_2_Far_Multiplier;
		uniform fixed _Texture_3_Tesselation_Depth;
		uniform fixed _Texture_3_Normal_Index;
		uniform fixed _Texture_3_Tiling;
		uniform fixed _Texture_3_Far_Multiplier;
		uniform fixed _Texture_4_Tesselation_Depth;
		uniform fixed _Texture_4_Normal_Index;
		uniform fixed _Texture_4_Tiling;
		uniform fixed _Texture_4_Far_Multiplier;
		uniform fixed _Texture_5_Tesselation_Depth;
		uniform fixed _Texture_5_Normal_Index;
		uniform fixed _Texture_5_Tiling;
		uniform fixed _Texture_5_Far_Multiplier;
		uniform fixed _Texture_6_Tesselation_Depth;
		uniform fixed _Texture_6_Normal_Index;
		uniform fixed _Texture_6_Tiling;
		uniform fixed _Texture_6_Far_Multiplier;
		uniform fixed _Texture_7_Tesselation_Depth;
		uniform fixed _Texture_7_Normal_Index;
		uniform fixed _Texture_7_Tiling;
		uniform fixed _Texture_7_Far_Multiplier;
		uniform fixed _Texture_8_Tesselation_Depth;
		uniform fixed _Texture_8_Normal_Index;
		uniform fixed _Texture_8_Tiling;
		uniform fixed _Texture_8_Far_Multiplier;
		uniform fixed _Texture_9_Tesselation_Depth;
		uniform fixed _Texture_9_Normal_Index;
		uniform fixed _Texture_9_Tiling;
		uniform fixed _Texture_9_Far_Multiplier;
		uniform fixed _Texture_10_Tesselation_Depth;
		uniform fixed _Texture_10_Normal_Index;
		uniform fixed _Texture_10_Tiling;
		uniform fixed _Texture_10_Far_Multiplier;
		uniform fixed _Texture_11_Tesselation_Depth;
		uniform fixed _Texture_11_Normal_Index;
		uniform fixed _Texture_11_Tiling;
		uniform fixed _Texture_11_Far_Multiplier;
		uniform fixed _Texture_12_Tesselation_Depth;
		uniform fixed _Texture_12_Normal_Index;
		uniform fixed _Texture_12_Tiling;
		uniform fixed _Texture_12_Far_Multiplier;
		uniform fixed _Texture_13_Tesselation_Depth;
		uniform fixed _Texture_13_Normal_Index;
		uniform fixed _Texture_13_Tiling;
		uniform fixed _Texture_13_Far_Multiplier;
		uniform fixed _Texture_14_Tesselation_Depth;
		uniform fixed _Texture_14_Normal_Index;
		uniform fixed _Texture_14_Tiling;
		uniform fixed _Texture_14_Far_Multiplier;
		uniform fixed _Texture_15_Tesselation_Depth;
		uniform fixed _Texture_15_Normal_Index;
		uniform fixed _Texture_15_Tiling;
		uniform fixed _Texture_15_Far_Multiplier;
		uniform fixed _Texture_16_Tesselation_Depth;
		uniform fixed _Texture_16_Normal_Index;
		uniform fixed _Texture_16_Tiling;
		uniform fixed _Texture_16_Far_Multiplier;
		uniform fixed _Snow_Tiling;
		uniform fixed _Snow_Tiling_Far_Multiplier;
		uniform int _Texture_Snow_Normal_Index;
		uniform fixed _Snow_Height_Contrast;
		uniform fixed _Snow_Heightmap_Depth;
		uniform fixed _Snow_Amount;
		uniform fixed _Snow_Noise_Tiling;
		uniform int _Texture_Snow_Noise_Index;
		uniform fixed _Snow_Noise_Power;
		uniform fixed _Snow_Maximum_Angle_Hardness;
		uniform fixed _Snow_Maximum_Angle;
		uniform fixed _Snow_Min_Height;
		uniform fixed _Snow_Min_Height_Blending;
		uniform fixed _Texture_16_Snow_Reduction;
		uniform fixed _Texture_15_Snow_Reduction;
		uniform fixed _Texture_13_Snow_Reduction;
		uniform fixed _Texture_12_Snow_Reduction;
		uniform fixed _Texture_11_Snow_Reduction;
		uniform fixed _Texture_9_Snow_Reduction;
		uniform fixed _Texture_8_Snow_Reduction;
		uniform fixed _Texture_7_Snow_Reduction;
		uniform fixed _Texture_5_Snow_Reduction;
		uniform fixed _Texture_1_Snow_Reduction;
		uniform fixed _Texture_2_Snow_Reduction;
		uniform fixed _Texture_3_Snow_Reduction;
		uniform fixed _Texture_4_Snow_Reduction;
		uniform fixed _Texture_6_Snow_Reduction;
		uniform fixed _Texture_10_Snow_Reduction;
		uniform fixed _Texture_14_Snow_Reduction;
		uniform fixed _Snow_Heightblend_Far;
		uniform fixed _Global_Normalmap_Power;
		uniform sampler2D _Global_Normal_Map;
		uniform fixed _Texture_13_Heightmap_Depth;
		uniform fixed _Texture_13_Height_Contrast;
		uniform fixed _Texture_13_Heightblend_Far;
		uniform fixed _Texture_14_Heightmap_Depth;
		uniform fixed _Texture_14_Height_Contrast;
		uniform fixed _Texture_14_Heightblend_Far;
		uniform fixed _Texture_15_Heightmap_Depth;
		uniform fixed _Texture_15_Height_Contrast;
		uniform fixed _Texture_15_Heightblend_Far;
		uniform fixed _Texture_16_Heightmap_Depth;
		uniform fixed _Texture_16_Height_Contrast;
		uniform fixed _Texture_16_Heightblend_Far;
		uniform fixed _Texture_9_Heightmap_Depth;
		uniform fixed _Texture_9_Height_Contrast;
		uniform fixed _Texture_9_Heightblend_Far;
		uniform fixed _Texture_10_Heightmap_Depth;
		uniform fixed _Texture_10_Height_Contrast;
		uniform fixed _Texture_10_Heightblend_Far;
		uniform fixed _Texture_11_Heightmap_Depth;
		uniform fixed _Texture_11_Height_Contrast;
		uniform fixed _Texture_11_Heightblend_Far;
		uniform fixed _Texture_12_Heightmap_Depth;
		uniform fixed _Texture_12_Height_Contrast;
		uniform fixed _Texture_12_Heightblend_Far;
		uniform fixed _Texture_5_Heightmap_Depth;
		uniform fixed _Texture_5_Height_Contrast;
		uniform fixed _Texture_5_Heightblend_Far;
		uniform fixed _Texture_6_Heightmap_Depth;
		uniform fixed _Texture_6_Height_Contrast;
		uniform fixed _Texture_6_Heightblend_Far;
		uniform fixed _Texture_7_Heightmap_Depth;
		uniform fixed _Texture_7_Height_Contrast;
		uniform fixed _Texture_7_Heightblend_Far;
		uniform fixed _Texture_8_Heightmap_Depth;
		uniform fixed _Texture_8_Height_Contrast;
		uniform fixed _Texture_8_Heightblend_Far;
		uniform fixed _Texture_1_Height_Contrast;
		uniform fixed _Texture_1_Heightmap_Depth;
		uniform fixed _Texture_1_Heightblend_Far;
		uniform fixed _Texture_2_Heightmap_Depth;
		uniform fixed _Texture_2_Height_Contrast;
		uniform fixed _Texture_2_Heightblend_Far;
		uniform fixed _Texture_3_Heightmap_Depth;
		uniform fixed _Texture_3_Height_Contrast;
		uniform fixed _Texture_3_Heightblend_Far;
		uniform fixed _Texture_4_Heightmap_Depth;
		uniform fixed _Texture_4_Height_Contrast;
		uniform fixed _Texture_4_Heightblend_Far;
		uniform fixed _Texture_1_Albedo_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Albedo );
		uniform fixed4 _Texture_1_Color;
		uniform fixed _Texture_2_Albedo_Index;
		uniform fixed4 _Texture_2_Color;
		uniform fixed _Texture_3_Albedo_Index;
		uniform fixed4 _Texture_3_Color;
		uniform fixed _Texture_4_Albedo_Index;
		uniform fixed4 _Texture_4_Color;
		uniform fixed _Texture_5_Albedo_Index;
		uniform fixed4 _Texture_5_Color;
		uniform fixed _Texture_6_Albedo_Index;
		uniform fixed4 _Texture_6_Color;
		uniform fixed _Texture_7_Albedo_Index;
		uniform fixed4 _Texture_7_Color;
		uniform fixed _Texture_8_Albedo_Index;
		uniform fixed4 _Texture_8_Color;
		uniform fixed _Texture_9_Albedo_Index;
		uniform fixed4 _Texture_9_Color;
		uniform fixed _Texture_10_Albedo_Index;
		uniform fixed4 _Texture_10_Color;
		uniform fixed _Texture_11_Albedo_Index;
		uniform fixed4 _Texture_11_Color;
		uniform fixed _Texture_12_Albedo_Index;
		uniform fixed4 _Texture_12_Color;
		uniform fixed _Texture_13_Albedo_Index;
		uniform fixed4 _Texture_13_Color;
		uniform fixed _Texture_14_Albedo_Index;
		uniform fixed4 _Texture_14_Color;
		uniform fixed _Texture_15_Albedo_Index;
		uniform fixed4 _Texture_15_Color;
		uniform fixed _Texture_16_Albedo_Index;
		uniform fixed4 _Texture_16_Color;
		uniform sampler2D _Texture_Geological_Map;
		uniform fixed _Geological_Tiling_Far;
		uniform fixed _Geological_Map_Offset_Far;
		uniform fixed _Geological_Map_Far_Power;
		uniform fixed _Texture_16_Geological_Power;
		uniform fixed _Texture_15_Geological_Power;
		uniform fixed _Texture_14_Geological_Power;
		uniform fixed _Texture_13_Geological_Power;
		uniform fixed _Texture_12_Geological_Power;
		uniform fixed _Texture_11_Geological_Power;
		uniform fixed _Texture_10_Geological_Power;
		uniform fixed _Texture_9_Geological_Power;
		uniform fixed _Texture_8_Geological_Power;
		uniform fixed _Texture_7_Geological_Power;
		uniform fixed _Texture_6_Geological_Power;
		uniform fixed _Texture_5_Geological_Power;
		uniform fixed _Texture_1_Geological_Power;
		uniform fixed _Texture_2_Geological_Power;
		uniform fixed _Texture_4_Geological_Power;
		uniform fixed _Texture_3_Geological_Power;
		uniform int _Texture_Snow_Index;
		uniform fixed4 _Snow_Color;
		uniform fixed _Terrain_Specular;
		uniform fixed _Snow_Specular;
		uniform fixed _Terrain_Smoothness;
		uniform fixed _Ambient_Occlusion_Power;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float3 normalWorld = UnityObjectToWorldNormal( v.normal );
			v.vertex.xyz += 0.0;
			 v.tangent.xyz=cross( normalWorld , fixed3(0,0,1) );
			 v.tangent.w = -1;//;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult3897 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_0 = (( 1.0 / _Perlin_Normal_Tiling_Far )).xx;
			float4 texArray4374 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_0 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g668 = float2( texArray4374.x , texArray4374.y );
			float2 temp_cast_2 = (_Perlin_Normal_Power).xx;
			float2 temp_output_4_0_g668 = ( ( ( appendResult11_g668 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_2 );
			float2 temp_cast_3 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_4 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_5 = (_Perlin_Normal_Power).xx;
			float temp_output_9_0_g668 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g668 , temp_output_4_0_g668 ) ) ) );
			float3 appendResult10_g668 = float3( temp_output_4_0_g668.x , temp_output_4_0_g668.y , temp_output_9_0_g668 );
			fixed4 tex2DNode4371 = tex2D( _Texture_Splat_4, i.texcoord_0 );
			fixed Splat4_A = tex2DNode4371.a;
			fixed Splat4_B = tex2DNode4371.b;
			fixed Splat4_G = tex2DNode4371.g;
			fixed Splat4_R = tex2DNode4371.r;
			fixed4 tex2DNode4370 = tex2D( _Texture_Splat_3, i.texcoord_0 );
			fixed Splat3_A = tex2DNode4370.a;
			fixed Splat3_B = tex2DNode4370.b;
			fixed Splat3_G = tex2DNode4370.g;
			fixed Splat3_R = tex2DNode4370.r;
			fixed4 tex2DNode4369 = tex2D( _Texture_Splat_2, i.texcoord_0 );
			fixed Splat2_A = tex2DNode4369.a;
			fixed Splat2_B = tex2DNode4369.b;
			fixed Splat2_G = tex2DNode4369.g;
			fixed Splat2_R = tex2DNode4369.r;
			fixed4 tex2DNode4368 = tex2D( _Texture_Splat_1, i.texcoord_0 );
			fixed Splat1_R = tex2DNode4368.r;
			fixed Splat1_G = tex2DNode4368.g;
			fixed Splat1_A = tex2DNode4368.a;
			fixed Splat1_B = tex2DNode4368.b;
			fixed4 temp_cast_6 = (_Texture_1_Tesselation_Depth).xxxx;
			float2 appendResult1998 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 Top_Bottom = appendResult1998;
			float2 appendResult3284 = float2( ( 1.0 / _Texture_1_Tiling ) , ( 1.0 / _Texture_1_Tiling ) );
			fixed2 temp_cast_7 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3299 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_7 ), _Texture_1_Normal_Index)  );
			fixed4 ifLocalVar7658 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar7658 = texArray3299.w;
			fixed4 Texture_1_H = ifLocalVar7658;
			fixed4 temp_cast_8 = (_Texture_2_Tesselation_Depth).xxxx;
			float2 appendResult3349 = float2( ( 1.0 / _Texture_2_Tiling ) , ( 1.0 / _Texture_2_Tiling ) );
			fixed2 temp_cast_9 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5533 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_9 ), _Texture_2_Normal_Index)  );
			fixed4 ifLocalVar7662 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar7662 = texArray5533.w;
			fixed4 Texture_2_H = ifLocalVar7662;
			fixed4 temp_cast_10 = (_Texture_3_Tesselation_Depth).xxxx;
			float2 appendResult3415 = float2( ( 1.0 / _Texture_3_Tiling ) , ( 1.0 / _Texture_3_Tiling ) );
			fixed2 temp_cast_11 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_11 ), _Texture_3_Normal_Index)  );
			fixed4 ifLocalVar7664 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Index > -1.0 )
				ifLocalVar7664 = texArray5586.w;
			fixed4 Texture_3_H = ifLocalVar7664;
			fixed4 temp_cast_12 = (_Texture_4_Tesselation_Depth).xxxx;
			float2 appendResult3482 = float2( ( 1.0 / _Texture_4_Tiling ) , ( 1.0 / _Texture_4_Tiling ) );
			fixed2 temp_cast_13 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5615 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_13 ), _Texture_4_Normal_Index)  );
			fixed4 ifLocalVar7666 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Index > -1.0 )
				ifLocalVar7666 = texArray5615.w;
			fixed4 Texture_4_H = ifLocalVar7666;
			float4 layeredBlendVar6518 = tex2DNode4368;
			float4 layeredBlend6518 = ( lerp( lerp( lerp( lerp( 0.0 , ( temp_cast_6 * Texture_1_H ) , layeredBlendVar6518.x ) , ( temp_cast_8 * Texture_2_H ) , layeredBlendVar6518.y ) , ( temp_cast_10 * Texture_3_H ) , layeredBlendVar6518.z ) , ( temp_cast_12 * Texture_4_H ) , layeredBlendVar6518.w ) );
			fixed4 temp_cast_14 = (_Texture_5_Tesselation_Depth).xxxx;
			float2 appendResult4399 = float2( ( 1.0 / _Texture_5_Tiling ) , ( 1.0 / _Texture_5_Tiling ) );
			fixed2 temp_cast_15 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5655 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_15 ), _Texture_5_Normal_Index)  );
			fixed4 ifLocalVar7668 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Index > -1.0 )
				ifLocalVar7668 = texArray5655.w;
			fixed4 Texture_5_H = ifLocalVar7668;
			fixed4 temp_cast_16 = (_Texture_6_Tesselation_Depth).xxxx;
			float2 appendResult4471 = float2( ( 1.0 / _Texture_6_Tiling ) , ( 1.0 / _Texture_6_Tiling ) );
			fixed2 temp_cast_17 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5695 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_17 ), _Texture_6_Normal_Index)  );
			fixed4 ifLocalVar7670 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Index > -1.0 )
				ifLocalVar7670 = texArray5695.w;
			fixed4 Texture_6_H = ifLocalVar7670;
			fixed4 temp_cast_18 = (_Texture_7_Tesselation_Depth).xxxx;
			float2 appendResult4545 = float2( ( 1.0 / _Texture_7_Tiling ) , ( 1.0 / _Texture_7_Tiling ) );
			fixed2 temp_cast_19 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5735 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_19 ), _Texture_7_Normal_Index)  );
			fixed4 ifLocalVar7672 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Index > -1.0 )
				ifLocalVar7672 = texArray5735.w;
			fixed4 Texture_7_H = ifLocalVar7672;
			fixed4 temp_cast_20 = (_Texture_8_Tesselation_Depth).xxxx;
			float2 appendResult4619 = float2( ( 1.0 / _Texture_8_Tiling ) , ( 1.0 / _Texture_8_Tiling ) );
			fixed2 temp_cast_21 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5775 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_21 ), _Texture_8_Normal_Index)  );
			fixed4 ifLocalVar7674 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Index > -1.0 )
				ifLocalVar7674 = texArray5775.w;
			fixed4 Texture_8_H = ifLocalVar7674;
			float4 layeredBlendVar6525 = tex2DNode4369;
			float4 layeredBlend6525 = ( lerp( lerp( lerp( lerp( layeredBlend6518 , ( temp_cast_14 * Texture_5_H ) , layeredBlendVar6525.x ) , ( temp_cast_16 * Texture_6_H ) , layeredBlendVar6525.y ) , ( temp_cast_18 * Texture_7_H ) , layeredBlendVar6525.z ) , ( temp_cast_20 * Texture_8_H ) , layeredBlendVar6525.w ) );
			fixed4 temp_cast_22 = (_Texture_9_Tesselation_Depth).xxxx;
			float2 appendResult4736 = float2( ( 1.0 / _Texture_9_Tiling ) , ( 1.0 / _Texture_9_Tiling ) );
			fixed2 temp_cast_23 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5811 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_23 ), _Texture_9_Normal_Index)  );
			fixed4 ifLocalVar7689 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar7689 = texArray5811.w;
			fixed4 Texture_9_H = ifLocalVar7689;
			fixed4 temp_cast_24 = (_Texture_10_Tesselation_Depth).xxxx;
			float2 appendResult4738 = float2( ( 1.0 / _Texture_10_Tiling ) , ( 1.0 / _Texture_10_Tiling ) );
			fixed2 temp_cast_25 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5851 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_25 ), _Texture_10_Normal_Index)  );
			fixed4 ifLocalVar7687 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7687 = texArray5851.w;
			fixed4 Texture_10_H = ifLocalVar7687;
			fixed4 temp_cast_26 = (_Texture_11_Tesselation_Depth).xxxx;
			float2 appendResult4741 = float2( ( 1.0 / _Texture_11_Tiling ) , ( 1.0 / _Texture_11_Tiling ) );
			fixed2 temp_cast_27 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5891 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_27 ), _Texture_11_Normal_Index)  );
			fixed4 ifLocalVar7685 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Index > -1.0 )
				ifLocalVar7685 = texArray5891.w;
			fixed4 Texture_11_H = ifLocalVar7685;
			fixed4 temp_cast_28 = (_Texture_12_Tesselation_Depth).xxxx;
			float2 appendResult4751 = float2( ( 1.0 / _Texture_12_Tiling ) , ( 1.0 / _Texture_12_Tiling ) );
			fixed2 temp_cast_29 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5931 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_29 ), _Texture_12_Normal_Index)  );
			fixed4 ifLocalVar7683 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Index > -1.0 )
				ifLocalVar7683 = texArray5931.w;
			fixed4 Texture_12_H = ifLocalVar7683;
			float4 layeredBlendVar6527 = tex2DNode4370;
			float4 layeredBlend6527 = ( lerp( lerp( lerp( lerp( layeredBlend6525 , ( temp_cast_22 * Texture_9_H ) , layeredBlendVar6527.x ) , ( temp_cast_24 * Texture_10_H ) , layeredBlendVar6527.y ) , ( temp_cast_26 * Texture_11_H ) , layeredBlendVar6527.z ) , ( temp_cast_28 * Texture_12_H ) , layeredBlendVar6527.w ) );
			fixed4 temp_cast_30 = (_Texture_13_Tesselation_Depth).xxxx;
			float2 appendResult5027 = float2( ( 1.0 / _Texture_13_Tiling ) , ( 1.0 / _Texture_13_Tiling ) );
			fixed2 temp_cast_31 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5971 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_31 ), _Texture_13_Normal_Index)  );
			fixed4 ifLocalVar7681 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Index > -1.0 )
				ifLocalVar7681 = texArray5971.w;
			fixed4 Texture_13_H = ifLocalVar7681;
			fixed4 temp_cast_32 = (_Texture_14_Tesselation_Depth).xxxx;
			float2 appendResult5033 = float2( ( 1.0 / _Texture_14_Tiling ) , ( 1.0 / _Texture_14_Tiling ) );
			fixed2 temp_cast_33 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6011 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_33 ), _Texture_14_Normal_Index)  );
			fixed4 ifLocalVar7679 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar7679 = texArray6011.w;
			fixed4 Texture_14_H = ifLocalVar7679;
			fixed4 temp_cast_34 = (_Texture_15_Tesselation_Depth).xxxx;
			float2 appendResult5212 = float2( ( 1.0 / _Texture_15_Tiling ) , ( 1.0 / _Texture_15_Tiling ) );
			fixed2 temp_cast_35 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6051 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_35 ), _Texture_15_Normal_Index)  );
			fixed4 ifLocalVar7677 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar7677 = texArray6051.w;
			fixed4 Texture_15_H = ifLocalVar7677;
			fixed4 temp_cast_36 = (_Texture_16_Tesselation_Depth).xxxx;
			float2 appendResult5078 = float2( ( 1.0 / _Texture_16_Tiling ) , ( 1.0 / _Texture_16_Tiling ) );
			fixed2 temp_cast_37 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6091 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_37 ), _Texture_16_Normal_Index)  );
			fixed4 ifLocalVar7675 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar7675 = texArray6091.w;
			fixed4 Texture_16_H = ifLocalVar7675;
			float4 layeredBlendVar6530 = tex2DNode4371;
			float4 layeredBlend6530 = ( lerp( lerp( lerp( lerp( layeredBlend6527 , ( temp_cast_30 * Texture_13_H ) , layeredBlendVar6530.x ) , ( temp_cast_32 * Texture_14_H ) , layeredBlendVar6530.y ) , ( temp_cast_34 * Texture_15_H ) , layeredBlendVar6530.z ) , ( temp_cast_36 * Texture_16_H ) , layeredBlendVar6530.w ) );
			float2 appendResult3679 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_38 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_39 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray6270 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3679 * temp_cast_38 ) / temp_cast_39 ), (float)_Texture_Snow_Normal_Index)  );
			fixed4 temp_cast_41 = (( pow( texArray6270.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth )).xxxx;
			fixed4 temp_cast_42 = (( pow( texArray6270.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth )).xxxx;
			float2 appendResult3750 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_44 = (_Snow_Noise_Tiling).xx;
			float4 texArray4383 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3750 * temp_cast_44 ), (float)_Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_46 = (_Snow_Noise_Tiling).xx;
			float4 texArray4385 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_46 ) * float2( 0.23,0.23 ) ), (float)_Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_48 = (_Snow_Noise_Tiling).xx;
			float4 texArray4384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_48 ) * float2( 0.53,0.53 ) ), (float)_Texture_Snow_Noise_Index)  );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			fixed SnowSlope = ( 1.0 - ( clamp( ( clamp( ase_worldNormal.y , 0.0 , 0.9999 ) - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) , 0.0 , 2.0 ) * ( 1.0 / ( 1.0 - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) ) ) );
			float HeightMask6539 = saturate(pow(((( 1.0 - clamp( clamp( ( layeredBlend6530 + temp_cast_41 ) , float4( 0.0,0,0,0 ) , ( layeredBlend6530 + temp_cast_42 ) ) , float4( 0.0,0,0,0 ) , float4( 1.0,0,0,0 ) ) ).x*( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ))*4)+(( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) )*2),_Snow_Heightblend_Far));;
			o.Normal = BlendNormals( lerp( lerp( float3( 0,0,1 ) , appendResult10_g668 , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , appendResult10_g668 , ( _Snow_Perlin_Power * 0.5 ) ) , HeightMask6539 ) , normalize( UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) ) );
			fixed4 temp_cast_50 = (_Texture_13_Heightmap_Depth).xxxx;
			fixed4 temp_cast_51 = (_Texture_13_Height_Contrast).xxxx;
			float HeightMask6231 = saturate(pow(((( temp_cast_50 * pow( Texture_13_H , temp_cast_51 ) ).x*Splat4_R)*4)+(Splat4_R*2),_Texture_13_Heightblend_Far));;
			fixed4 temp_cast_53 = (_Texture_14_Heightmap_Depth).xxxx;
			fixed4 temp_cast_54 = (_Texture_14_Height_Contrast).xxxx;
			float HeightMask6234 = saturate(pow(((( temp_cast_53 * pow( Texture_14_H , temp_cast_54 ) ).x*Splat4_G)*4)+(Splat4_G*2),_Texture_14_Heightblend_Far));;
			fixed4 temp_cast_56 = (_Texture_15_Heightmap_Depth).xxxx;
			fixed4 temp_cast_57 = (_Texture_15_Height_Contrast).xxxx;
			float HeightMask6237 = saturate(pow(((( temp_cast_56 * pow( Texture_15_H , temp_cast_57 ) ).x*Splat4_B)*4)+(Splat4_B*2),_Texture_15_Heightblend_Far));;
			fixed4 temp_cast_59 = (_Texture_16_Heightmap_Depth).xxxx;
			fixed4 temp_cast_60 = (_Texture_16_Height_Contrast).xxxx;
			float HeightMask6240 = saturate(pow(((( temp_cast_59 * pow( Texture_16_H , temp_cast_60 ) ).x*Splat4_A)*4)+(Splat4_A*2),_Texture_16_Heightblend_Far));;
			float4 appendResult6533 = float4( HeightMask6231 , HeightMask6234 , HeightMask6237 , HeightMask6240 );
			fixed4 temp_cast_62 = (_Texture_9_Heightmap_Depth).xxxx;
			fixed4 temp_cast_63 = (_Texture_9_Height_Contrast).xxxx;
			float HeightMask6219 = saturate(pow(((( temp_cast_62 * pow( Texture_9_H , temp_cast_63 ) ).x*Splat3_R)*4)+(Splat3_R*2),_Texture_9_Heightblend_Far));;
			fixed4 temp_cast_65 = (_Texture_10_Heightmap_Depth).xxxx;
			fixed4 temp_cast_66 = (_Texture_10_Height_Contrast).xxxx;
			float HeightMask6222 = saturate(pow(((( temp_cast_65 * pow( Texture_10_H , temp_cast_66 ) ).x*Splat3_G)*4)+(Splat3_G*2),_Texture_10_Heightblend_Far));;
			fixed4 temp_cast_68 = (_Texture_11_Heightmap_Depth).xxxx;
			fixed4 temp_cast_69 = (_Texture_11_Height_Contrast).xxxx;
			float HeightMask6225 = saturate(pow(((( temp_cast_68 * pow( Texture_11_H , temp_cast_69 ) ).x*Splat3_B)*4)+(Splat3_B*2),_Texture_11_Heightblend_Far));;
			fixed4 temp_cast_71 = (_Texture_12_Heightmap_Depth).xxxx;
			fixed4 temp_cast_72 = (_Texture_12_Height_Contrast).xxxx;
			float HeightMask6228 = saturate(pow(((( temp_cast_71 * pow( Texture_12_H , temp_cast_72 ) ).x*Splat3_A)*4)+(Splat3_A*2),_Texture_12_Heightblend_Far));;
			float4 appendResult6529 = float4( HeightMask6219 , HeightMask6222 , HeightMask6225 , HeightMask6228 );
			fixed4 temp_cast_74 = (_Texture_5_Heightmap_Depth).xxxx;
			fixed4 temp_cast_75 = (_Texture_5_Height_Contrast).xxxx;
			float HeightMask6205 = saturate(pow(((( temp_cast_74 * pow( Texture_5_H , temp_cast_75 ) ).x*Splat2_R)*4)+(Splat2_R*2),_Texture_5_Heightblend_Far));;
			fixed4 temp_cast_77 = (_Texture_6_Heightmap_Depth).xxxx;
			fixed4 temp_cast_78 = (_Texture_6_Height_Contrast).xxxx;
			float HeightMask6208 = saturate(pow(((( temp_cast_77 * pow( Texture_6_H , temp_cast_78 ) ).x*Splat2_G)*4)+(Splat2_G*2),_Texture_6_Heightblend_Far));;
			fixed4 temp_cast_80 = (_Texture_7_Heightmap_Depth).xxxx;
			fixed4 temp_cast_81 = (_Texture_7_Height_Contrast).xxxx;
			float HeightMask6211 = saturate(pow(((( temp_cast_80 * pow( Texture_7_H , temp_cast_81 ) ).x*Splat2_B)*4)+(Splat2_B*2),_Texture_7_Heightblend_Far));;
			fixed4 temp_cast_83 = (_Texture_8_Heightmap_Depth).xxxx;
			fixed4 temp_cast_84 = (_Texture_8_Height_Contrast).xxxx;
			float HeightMask6214 = saturate(pow(((( temp_cast_83 * pow( Texture_8_H , temp_cast_84 ) ).x*Splat2_A)*4)+(Splat2_A*2),_Texture_8_Heightblend_Far));;
			float4 appendResult6524 = float4( HeightMask6205 , HeightMask6208 , HeightMask6211 , HeightMask6214 );
			fixed4 temp_cast_86 = (_Texture_1_Height_Contrast).xxxx;
			fixed4 temp_cast_87 = (_Texture_1_Heightmap_Depth).xxxx;
			float HeightMask6196 = saturate(pow(((( pow( Texture_1_H , temp_cast_86 ) * temp_cast_87 ).x*Splat1_R)*4)+(Splat1_R*2),_Texture_1_Heightblend_Far));;
			fixed4 temp_cast_89 = (_Texture_2_Heightmap_Depth).xxxx;
			fixed4 temp_cast_90 = (_Texture_2_Height_Contrast).xxxx;
			float HeightMask6515 = saturate(pow(((( temp_cast_89 * pow( Texture_2_H , temp_cast_90 ) ).x*Splat1_G)*4)+(Splat1_G*2),_Texture_2_Heightblend_Far));;
			fixed4 temp_cast_92 = (_Texture_3_Heightmap_Depth).xxxx;
			fixed4 temp_cast_93 = (_Texture_3_Height_Contrast).xxxx;
			float HeightMask6516 = saturate(pow(((( temp_cast_92 * pow( Texture_3_H , temp_cast_93 ) ).x*Splat1_B)*4)+(Splat1_B*2),_Texture_3_Heightblend_Far));;
			fixed4 temp_cast_95 = (_Texture_4_Heightmap_Depth).xxxx;
			fixed4 temp_cast_96 = (_Texture_4_Height_Contrast).xxxx;
			float HeightMask6203 = saturate(pow(((( temp_cast_95 * pow( Texture_4_H , temp_cast_96 ) ).x*Splat1_A)*4)+(Splat1_A*2),_Texture_4_Heightblend_Far));;
			float4 appendResult6517 = float4( HeightMask6196 , HeightMask6515 , HeightMask6516 , HeightMask6203 );
			fixed2 temp_cast_98 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3292 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_98 ), _Texture_1_Albedo_Index)  );
			fixed4 ifLocalVar7657 = 0;
			UNITY_BRANCH if( _Texture_1_Albedo_Index > -1.0 )
				ifLocalVar7657 = ( texArray3292 * _Texture_1_Color );
			fixed4 Texture_1_Final = ifLocalVar7657;
			fixed2 temp_cast_99 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3339 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_99 ), _Texture_2_Albedo_Index)  );
			fixed4 ifLocalVar7661 = 0;
			UNITY_BRANCH if( _Texture_2_Albedo_Index > -1.0 )
				ifLocalVar7661 = ( texArray3339 * _Texture_2_Color );
			fixed4 Texture_2_Final = ifLocalVar7661;
			fixed2 temp_cast_100 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_100 ), _Texture_3_Albedo_Index)  );
			fixed4 ifLocalVar7663 = 0;
			UNITY_BRANCH if( _Texture_3_Albedo_Index > -1.0 )
				ifLocalVar7663 = ( texArray3406 * _Texture_3_Color );
			fixed4 Texture_3_Final = ifLocalVar7663;
			fixed2 temp_cast_101 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3473 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_101 ), _Texture_4_Albedo_Index)  );
			fixed4 ifLocalVar7665 = 0;
			UNITY_BRANCH if( _Texture_4_Albedo_Index > -1.0 )
				ifLocalVar7665 = ( texArray3473 * _Texture_4_Color );
			fixed4 Texture_4_Final = ifLocalVar7665;
			float4 layeredBlendVar6512 = appendResult6517;
			float4 layeredBlend6512 = ( lerp( lerp( lerp( lerp( float4( 1,1,1,0 ) , Texture_1_Final , layeredBlendVar6512.x ) , Texture_2_Final , layeredBlendVar6512.y ) , Texture_3_Final , layeredBlendVar6512.z ) , Texture_4_Final , layeredBlendVar6512.w ) );
			fixed2 temp_cast_102 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_102 ), _Texture_5_Albedo_Index)  );
			fixed4 ifLocalVar7667 = 0;
			UNITY_BRANCH if( _Texture_5_Albedo_Index > -1.0 )
				ifLocalVar7667 = ( texArray4445 * _Texture_5_Color );
			fixed4 Texture_5_Final = ifLocalVar7667;
			fixed2 temp_cast_103 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_103 ), _Texture_6_Albedo_Index)  );
			fixed4 ifLocalVar7669 = 0;
			UNITY_BRANCH if( _Texture_6_Albedo_Index > -1.0 )
				ifLocalVar7669 = ( texArray4512 * _Texture_6_Color );
			fixed4 Texture_6_Final = ifLocalVar7669;
			fixed2 temp_cast_104 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_104 ), _Texture_7_Albedo_Index)  );
			fixed4 ifLocalVar7671 = 0;
			UNITY_BRANCH if( _Texture_7_Albedo_Index > -1.0 )
				ifLocalVar7671 = ( texArray4586 * _Texture_7_Color );
			fixed4 Texture_7_Final = ifLocalVar7671;
			fixed2 temp_cast_105 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4660 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_105 ), _Texture_8_Albedo_Index)  );
			fixed4 ifLocalVar7673 = 0;
			UNITY_BRANCH if( _Texture_8_Albedo_Index > -1.0 )
				ifLocalVar7673 = ( texArray4660 * _Texture_8_Color );
			fixed4 Texture_8_Final = ifLocalVar7673;
			float4 layeredBlendVar6520 = appendResult6524;
			float4 layeredBlend6520 = ( lerp( lerp( lerp( lerp( layeredBlend6512 , Texture_5_Final , layeredBlendVar6520.x ) , Texture_6_Final , layeredBlendVar6520.y ) , Texture_7_Final , layeredBlendVar6520.z ) , Texture_8_Final , layeredBlendVar6520.w ) );
			fixed2 temp_cast_106 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4889 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_106 ), _Texture_9_Albedo_Index)  );
			fixed4 ifLocalVar7690 = 0;
			UNITY_BRANCH if( _Texture_9_Albedo_Index > -1.0 )
				ifLocalVar7690 = ( texArray4889 * _Texture_9_Color );
			fixed4 Texture_9_Final = ifLocalVar7690;
			fixed2 temp_cast_107 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4913 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_107 ), _Texture_10_Albedo_Index)  );
			fixed4 ifLocalVar7688 = 0;
			UNITY_BRANCH if( _Texture_10_Albedo_Index > -1.0 )
				ifLocalVar7688 = ( texArray4913 * _Texture_10_Color );
			fixed4 Texture_10_Final = ifLocalVar7688;
			fixed2 temp_cast_108 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4923 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_108 ), _Texture_11_Albedo_Index)  );
			fixed4 ifLocalVar7686 = 0;
			UNITY_BRANCH if( _Texture_11_Albedo_Index > -1.0 )
				ifLocalVar7686 = ( texArray4923 * _Texture_11_Color );
			fixed4 Texture_11_Final = ifLocalVar7686;
			fixed2 temp_cast_109 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4952 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_109 ), _Texture_12_Albedo_Index)  );
			fixed4 ifLocalVar7684 = 0;
			UNITY_BRANCH if( _Texture_12_Albedo_Index > -1.0 )
				ifLocalVar7684 = ( texArray4952 * _Texture_12_Color );
			fixed4 Texture_12_Final = ifLocalVar7684;
			float4 layeredBlendVar6528 = appendResult6529;
			float4 layeredBlend6528 = ( lerp( lerp( lerp( lerp( layeredBlend6520 , Texture_9_Final , layeredBlendVar6528.x ) , Texture_10_Final , layeredBlendVar6528.y ) , Texture_11_Final , layeredBlendVar6528.z ) , Texture_12_Final , layeredBlendVar6528.w ) );
			fixed2 temp_cast_110 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5034 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_110 ), _Texture_13_Albedo_Index)  );
			fixed4 ifLocalVar7682 = 0;
			UNITY_BRANCH if( _Texture_13_Albedo_Index > -1.0 )
				ifLocalVar7682 = ( texArray5034 * _Texture_13_Color );
			fixed4 Texture_13_Final = ifLocalVar7682;
			fixed2 temp_cast_111 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5171 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_111 ), _Texture_14_Albedo_Index)  );
			fixed4 ifLocalVar7680 = 0;
			UNITY_BRANCH if( _Texture_14_Albedo_Index > -1.0 )
				ifLocalVar7680 = ( texArray5171 * _Texture_14_Color );
			fixed4 Texture_14_Final = ifLocalVar7680;
			fixed2 temp_cast_112 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5272 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_112 ), _Texture_15_Albedo_Index)  );
			fixed4 ifLocalVar7678 = 0;
			UNITY_BRANCH if( _Texture_15_Albedo_Index > -1.0 )
				ifLocalVar7678 = ( texArray5272 * _Texture_15_Color );
			fixed4 Texture_15_Final = ifLocalVar7678;
			fixed2 temp_cast_113 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5143 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_113 ), _Texture_16_Albedo_Index)  );
			fixed4 ifLocalVar7676 = 0;
			UNITY_BRANCH if( _Texture_16_Albedo_Index > -1.0 )
				ifLocalVar7676 = ( texArray5143 * _Texture_16_Color );
			fixed4 Texture_16_Final = ifLocalVar7676;
			float4 layeredBlendVar6532 = appendResult6533;
			float4 layeredBlend6532 = ( lerp( lerp( lerp( lerp( layeredBlend6528 , Texture_13_Final , layeredBlendVar6532.x ) , Texture_14_Final , layeredBlendVar6532.y ) , Texture_15_Final , layeredBlendVar6532.z ) , Texture_16_Final , layeredBlendVar6532.w ) );
			float3 appendResult3857 = float3( layeredBlend6532.x , layeredBlend6532.y , layeredBlend6532.z );
			fixed2 temp_cast_115 = (( ( ase_worldPos.y * ( 1.0 / _Geological_Tiling_Far ) ) + _Geological_Map_Offset_Far )).xx;
			fixed4 tex2DNode6969 = tex2D( _Texture_Geological_Map, temp_cast_115 );
			float3 appendResult6971 = float3( tex2DNode6969.r , tex2DNode6969.g , tex2DNode6969.b );
			fixed3 temp_cast_116 = (_Geological_Map_Far_Power).xxx;
			fixed3 temp_cast_117 = (( ( _Texture_16_Geological_Power * Splat4_A ) + ( ( _Texture_15_Geological_Power * Splat4_B ) + ( ( _Texture_14_Geological_Power * Splat4_G ) + ( ( _Texture_13_Geological_Power * Splat4_R ) + ( ( _Texture_12_Geological_Power * Splat3_A ) + ( ( _Texture_11_Geological_Power * Splat3_B ) + ( ( _Texture_10_Geological_Power * Splat3_G ) + ( ( _Texture_9_Geological_Power * Splat3_R ) + ( ( _Texture_8_Geological_Power * Splat2_A ) + ( ( _Texture_7_Geological_Power * Splat2_B ) + ( ( _Texture_6_Geological_Power * Splat2_G ) + ( ( _Texture_5_Geological_Power * Splat2_R ) + ( ( _Texture_1_Geological_Power * Splat1_R ) + ( ( _Texture_2_Geological_Power * Splat1_G ) + ( ( _Texture_4_Geological_Power * Splat1_A ) + ( _Texture_3_Geological_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )).xxx;
			fixed2 temp_cast_119 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_120 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray4379 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( appendResult3679 * temp_cast_119 ) / temp_cast_120 ), (float)_Texture_Snow_Index)  );
			float4 SnowAlbedo = ( texArray4379 * _Snow_Color );
			float3 appendResult1410 = float3( SnowAlbedo.x , SnowAlbedo.y , SnowAlbedo.z );
			o.Albedo = lerp( ( saturate( ( fixed4( appendResult3857 , 0.0 ) + fixed4( ( ( ( appendResult6971 + float3( -0.3,-0.3,-0.3 ) ) * temp_cast_116 ) * temp_cast_117 ) , 0.0 ) ) )) , fixed4( appendResult1410 , 0.0 ) , HeightMask6539 ).rgb;
			fixed3 temp_cast_124 = (_Terrain_Specular).xxx;
			fixed3 temp_cast_125 = (_Snow_Specular).xxx;
			o.Specular = lerp( ( ( appendResult3857 * float3( 0.3,0.3,0.3 ) ) * temp_cast_124 ) , ( clamp( appendResult1410 , float3( 0,0,0 ) , float3( 0.5,0.5,0.5 ) ) * temp_cast_125 ) , HeightMask6539 );
			o.Smoothness = lerp( ( layeredBlend6532.w * _Terrain_Smoothness ) , SnowAlbedo.w , HeightMask6539 );
			#ifdef _USE_AO_TEXTURE_ON
			float staticSwitch7703 = 1.0;
			#else
			float staticSwitch7703 = clamp( ( ( 1.0 + BlendNormals( lerp( lerp( float3( 0,0,1 ) , appendResult10_g668 , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , appendResult10_g668 , ( _Snow_Perlin_Power * 0.5 ) ) , HeightMask6539 ) , normalize( UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) ) ).y ) * 0.5 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#endif
			o.Occlusion = staticSwitch7703;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers d3d9 gles 
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows noforwardadd vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	Dependency "BaseMapShader" = "CTS Terrain Shader Advanced LOD"
}