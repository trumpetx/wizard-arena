Shader "CTS Terrain Shader Basic LOD"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Geological_Tiling_Far("Geological_Tiling_Far", Range( 0 , 1000)) = 87
		_Geological_Map_Offset_Far("Geological_Map_Offset _Far", Range( 0 , 1)) = 1
		_Geological_Map_Far_Power("Geological_Map_Far_Power", Range( 0 , 1)) = 1
		_Perlin_Normal_Tiling_Far("Perlin_Normal_Tiling_Far", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Power("Perlin_Normal_Power", Range( 0 , 10)) = 1
		_Terrain_Smoothness("Terrain_Smoothness", Range( 0 , 2)) = 1
		_Terrain_Specular("Terrain_Specular", Range( 0 , 3)) = 1
		_Global_Normalmap_Power("Global_Normalmap_Power", Range( 0 , 10)) = 0
		_Texture_1_Tiling("Texture_1_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_2_Tiling("Texture_2_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_3_Tiling("Texture_3_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_4_Tiling("Texture_4_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_5_Tiling("Texture_5_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_6_Tiling("Texture_6_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_7_Tiling("Texture_7_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_8_Tiling("Texture_8_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_9_Tiling("Texture_9_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_10_Tiling("Texture_10_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_11_Tiling("Texture_11_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_12_Tiling("Texture_12_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_13_Tiling("Texture_13_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_14_Tiling("Texture_14_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_15_Tiling("Texture_15_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_16_Tiling("Texture_16_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_1_Far_Multiplier("Texture_1_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_2_Far_Multiplier("Texture_2_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_3_Far_Multiplier("Texture_3_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_4_Far_Multiplier("Texture_4_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_5_Far_Multiplier("Texture_5_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_6_Far_Multiplier("Texture_6_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_7_Far_Multiplier("Texture_7_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_8_Far_Multiplier("Texture_8_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_9_Far_Multiplier("Texture_9_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_10_Far_Multiplier("Texture_10_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_11_Far_Multiplier("Texture_11_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_12_Far_Multiplier("Texture_12_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_13_Far_Multiplier("Texture_13_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_14_Far_Multiplier("Texture_14_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_15_Far_Multiplier("Texture_15_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_16_Far_Multiplier("Texture_16_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_1_Perlin_Power("Texture_1_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_2_Perlin_Power("Texture_2_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_3_Perlin_Power("Texture_3_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_4_Perlin_Power("Texture_4_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_5_Perlin_Power("Texture_5_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_6_Perlin_Power("Texture_6_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_7_Perlin_Power("Texture_7_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_8_Perlin_Power("Texture_8_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_9_Perlin_Power("Texture_9_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_10_Perlin_Power("Texture_10_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_11_Perlin_Power("Texture_11_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_12_Perlin_Power("Texture_12_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_13_Perlin_Power("Texture_13_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_14_Perlin_Power("Texture_14_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_15_Perlin_Power("Texture_15_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_16_Perlin_Power("Texture_16_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_1_Geological_Power("Texture_1_Geological_Power", Range( 0 , 5)) = 1
		_Texture_2_Geological_Power("Texture_2_Geological_Power", Range( 0 , 5)) = 1
		_Texture_3_Geological_Power("Texture_3_Geological_Power", Range( 0 , 5)) = 1
		_Texture_4_Geological_Power("Texture_4_Geological_Power", Range( 0 , 5)) = 1
		_Texture_5_Geological_Power("Texture_5_Geological_Power", Range( 0 , 5)) = 1
		_Texture_6_Geological_Power("Texture_6_Geological_Power", Range( 0 , 5)) = 1
		_Texture_Array_Normal("Texture_Array_Normal", 2DArray ) = "" {}
		_Texture_7_Geological_Power("Texture_7_Geological_Power", Range( 0 , 5)) = 1
		_Texture_8_Geological_Power("Texture_8_Geological_Power", Range( 0 , 5)) = 1
		_Texture_Array_Albedo("Texture_Array_Albedo", 2DArray ) = "" {}
		_Texture_9_Geological_Power("Texture_9_Geological_Power", Range( 0 , 5)) = 1
		_Texture_10_Geological_Power("Texture_10_Geological_Power", Range( 0 , 5)) = 1
		_Texture_11_Geological_Power("Texture_11_Geological_Power", Range( 0 , 5)) = 1
		_Texture_12_Geological_Power("Texture_12_Geological_Power", Range( 0 , 5)) = 1
		_Texture_13_Geological_Power("Texture_13_Geological_Power", Range( 0 , 5)) = 1
		_Texture_14_Geological_Power("Texture_14_Geological_Power", Range( 0 , 5)) = 1
		_Texture_15_Geological_Power("Texture_15_Geological_Power", Range( 0 , 5)) = 1
		_Texture_16_Geological_Power("Texture_16_Geological_Power", Range( 0 , 5)) = 1
		_Snow_Specular("Snow_Specular", Range( 0 , 3)) = 1
		_Snow_Blend_Normal("Snow_Blend_Normal", Range( 0 , 1)) = 0.8
		_Snow_Amount("Snow_Amount", Range( 0 , 2)) = 0
		_Snow_Tiling("Snow_Tiling", Range( 0.001 , 20)) = 15
		_Snow_Tiling_Far_Multiplier("Snow_Tiling_Far_Multiplier", Range( 0.001 , 20)) = 1
		_Snow_Perlin_Power("Snow_Perlin_Power", Range( 0 , 1)) = 0
		_Snow_Noise_Power("Snow_Noise_Power", Range( 0 , 1)) = 1
		_Snow_Noise_Tiling("Snow_Noise_Tiling", Range( 0.001 , 1)) = 0.02
		_Snow_Min_Height("Snow_Min_Height", Range( -1000 , 10000)) = -1000
		_Snow_Min_Height_Blending("Snow_Min_Height_Blending", Range( 0 , 500)) = 1
		_Snow_Maximum_Angle("Snow_Maximum_Angle", Range( 0.001 , 180)) = 30
		_Snow_Maximum_Angle_Hardness("Snow_Maximum_Angle_Hardness", Range( 0.01 , 3)) = 1
		_Texture_1_Snow_Reduction("Texture_1_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_2_Snow_Reduction("Texture_2_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_3_Snow_Reduction("Texture_3_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_4_Snow_Reduction("Texture_4_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_5_Snow_Reduction("Texture_5_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_6_Snow_Reduction("Texture_6_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_7_Snow_Reduction("Texture_7_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_8_Snow_Reduction("Texture_8_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_9_Snow_Reduction("Texture_9_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_10_Snow_Reduction("Texture_10_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_11_Snow_Reduction("Texture_11_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_12_Snow_Reduction("Texture_12_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_13_Snow_Reduction("Texture_13_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_14_Snow_Reduction("Texture_14_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_15_Snow_Reduction("Texture_15_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_16_Snow_Reduction("Texture_16_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_Perlin_Normal_Index("Texture_Perlin_Normal_Index", Int) = -1
		_Texture_Splat_1("Texture_Splat_1", 2D) = "black" {}
		_Texture_Splat_2("Texture_Splat_2", 2D) = "black" {}
		_Texture_Splat_3("Texture_Splat_3", 2D) = "black" {}
		_Texture_Splat_4("Texture_Splat_4", 2D) = "black" {}
		_Global_Normal_Map("Global_Normal_Map", 2D) = "bump" {}
		_Texture_Geological_Map("Texture_Geological_Map", 2D) = "white" {}
		_Texture_4_Color("Texture_4_Color", Vector) = (1,1,1,1)
		_Snow_Color("Snow_Color", Vector) = (1,1,1,1)
		_Texture_16_Color("Texture_16_Color", Vector) = (1,1,1,1)
		_Texture_8_Color("Texture_8_Color", Vector) = (1,1,1,1)
		_Texture_7_Color("Texture_7_Color", Vector) = (1,1,1,1)
		_Texture_6_Color("Texture_6_Color", Vector) = (1,1,1,1)
		_Texture_5_Color("Texture_5_Color", Vector) = (1,1,1,1)
		_Texture_3_Color("Texture_3_Color", Vector) = (1,1,1,1)
		_Texture_13_Color("Texture_13_Color", Vector) = (1,1,1,1)
		_Texture_15_Color("Texture_15_Color", Vector) = (1,1,1,1)
		_Texture_14_Color("Texture_14_Color", Vector) = (1,1,1,1)
		_Texture_9_Color("Texture_9_Color", Vector) = (1,1,1,1)
		_Texture_12_Color("Texture_12_Color", Vector) = (1,1,1,1)
		_Texture_11_Color("Texture_11_Color", Vector) = (1,1,1,1)
		_Texture_10_Color("Texture_10_Color", Vector) = (1,1,1,1)
		_Texture_1_Color("Texture_1_Color", Vector) = (1,1,1,1)
		_Texture_2_Color("Texture_2_Color", Vector) = (1,1,1,1)
		_Ambient_Occlusion_Power("Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		_Texture_10_Albedo_Index("Texture_10_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_Snow_Noise_Index("Texture_Snow_Noise_Index", Range( -1 , 100)) = -1
		_Texture_Snow_Index("Texture_Snow_Index", Range( -1 , 100)) = -1
		_Texture_9_Albedo_Index("Texture_9_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_11_Albedo_Index("Texture_11_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_12_Albedo_Index("Texture_12_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_13_Albedo_Index("Texture_13_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_14_Albedo_Index("Texture_14_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_8_Albedo_Index("Texture_8_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_7_Albedo_Index("Texture_7_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_15_Albedo_Index("Texture_15_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_16_Albedo_Index("Texture_16_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_6_Albedo_Index("Texture_6_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_3_Albedo_Index("Texture_3_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_2_Albedo_Index("Texture_2_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_5_Albedo_Index("Texture_5_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_4_Albedo_Index("Texture_4_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_1_Albedo_Index("Texture_1_Albedo_Index", Range( -1 , 100)) = -1
		[Toggle]_Use_AO("Use_AO", Int) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+100" }
		Cull Back
		ZTest LEqual
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.5
		#pragma multi_compile __ _USE_AO_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float2 texcoord_0;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Normal );
		uniform fixed _Perlin_Normal_Tiling_Far;
		uniform int _Texture_Perlin_Normal_Index;
		uniform fixed _Perlin_Normal_Power;
		uniform fixed _Texture_16_Perlin_Power;
		uniform sampler2D _Texture_Splat_4;
		uniform fixed _Texture_15_Perlin_Power;
		uniform fixed _Texture_14_Perlin_Power;
		uniform fixed _Texture_13_Perlin_Power;
		uniform fixed _Texture_12_Perlin_Power;
		uniform sampler2D _Texture_Splat_3;
		uniform fixed _Texture_11_Perlin_Power;
		uniform fixed _Texture_10_Perlin_Power;
		uniform fixed _Texture_9_Perlin_Power;
		uniform fixed _Texture_8_Perlin_Power;
		uniform sampler2D _Texture_Splat_2;
		uniform fixed _Texture_7_Perlin_Power;
		uniform fixed _Texture_6_Perlin_Power;
		uniform fixed _Texture_5_Perlin_Power;
		uniform fixed _Texture_1_Perlin_Power;
		uniform sampler2D _Texture_Splat_1;
		uniform fixed _Texture_2_Perlin_Power;
		uniform fixed _Texture_4_Perlin_Power;
		uniform fixed _Texture_3_Perlin_Power;
		uniform fixed _Snow_Perlin_Power;
		uniform fixed _Snow_Blend_Normal;
		uniform fixed _Snow_Amount;
		uniform fixed _Texture_Snow_Noise_Index;
		uniform fixed _Snow_Noise_Tiling;
		uniform fixed _Snow_Noise_Power;
		uniform fixed _Snow_Maximum_Angle;
		uniform fixed _Snow_Maximum_Angle_Hardness;
		uniform fixed _Snow_Min_Height;
		uniform fixed _Snow_Min_Height_Blending;
		uniform fixed _Texture_16_Snow_Reduction;
		uniform fixed _Texture_15_Snow_Reduction;
		uniform fixed _Texture_13_Snow_Reduction;
		uniform fixed _Texture_12_Snow_Reduction;
		uniform fixed _Texture_11_Snow_Reduction;
		uniform fixed _Texture_9_Snow_Reduction;
		uniform fixed _Texture_8_Snow_Reduction;
		uniform fixed _Texture_7_Snow_Reduction;
		uniform fixed _Texture_5_Snow_Reduction;
		uniform fixed _Texture_1_Snow_Reduction;
		uniform fixed _Texture_2_Snow_Reduction;
		uniform fixed _Texture_3_Snow_Reduction;
		uniform fixed _Texture_4_Snow_Reduction;
		uniform fixed _Texture_6_Snow_Reduction;
		uniform fixed _Texture_10_Snow_Reduction;
		uniform fixed _Texture_14_Snow_Reduction;
		uniform fixed _Global_Normalmap_Power;
		uniform sampler2D _Global_Normal_Map;
		uniform fixed _Texture_1_Albedo_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Albedo );
		uniform fixed _Texture_1_Tiling;
		uniform fixed _Texture_1_Far_Multiplier;
		uniform fixed4 _Texture_1_Color;
		uniform fixed _Texture_2_Albedo_Index;
		uniform fixed _Texture_2_Tiling;
		uniform fixed _Texture_2_Far_Multiplier;
		uniform fixed4 _Texture_2_Color;
		uniform fixed _Texture_3_Albedo_Index;
		uniform fixed _Texture_3_Tiling;
		uniform fixed _Texture_3_Far_Multiplier;
		uniform fixed4 _Texture_3_Color;
		uniform fixed _Texture_4_Albedo_Index;
		uniform fixed _Texture_4_Tiling;
		uniform fixed _Texture_4_Far_Multiplier;
		uniform fixed4 _Texture_4_Color;
		uniform fixed _Texture_5_Albedo_Index;
		uniform fixed _Texture_5_Tiling;
		uniform fixed _Texture_5_Far_Multiplier;
		uniform fixed4 _Texture_5_Color;
		uniform fixed _Texture_6_Albedo_Index;
		uniform fixed _Texture_6_Tiling;
		uniform fixed _Texture_6_Far_Multiplier;
		uniform fixed4 _Texture_6_Color;
		uniform fixed _Texture_7_Albedo_Index;
		uniform fixed _Texture_7_Tiling;
		uniform fixed _Texture_7_Far_Multiplier;
		uniform fixed4 _Texture_7_Color;
		uniform fixed _Texture_8_Albedo_Index;
		uniform fixed _Texture_8_Tiling;
		uniform fixed _Texture_8_Far_Multiplier;
		uniform fixed4 _Texture_8_Color;
		uniform fixed _Texture_9_Albedo_Index;
		uniform fixed _Texture_9_Tiling;
		uniform fixed _Texture_9_Far_Multiplier;
		uniform fixed4 _Texture_9_Color;
		uniform fixed _Texture_10_Albedo_Index;
		uniform fixed _Texture_10_Tiling;
		uniform fixed _Texture_10_Far_Multiplier;
		uniform fixed4 _Texture_10_Color;
		uniform fixed _Texture_11_Albedo_Index;
		uniform fixed _Texture_11_Tiling;
		uniform fixed _Texture_11_Far_Multiplier;
		uniform fixed4 _Texture_11_Color;
		uniform fixed _Texture_12_Albedo_Index;
		uniform fixed _Texture_12_Tiling;
		uniform fixed _Texture_12_Far_Multiplier;
		uniform fixed4 _Texture_12_Color;
		uniform fixed _Texture_13_Albedo_Index;
		uniform fixed _Texture_13_Tiling;
		uniform fixed _Texture_13_Far_Multiplier;
		uniform fixed4 _Texture_13_Color;
		uniform fixed _Texture_14_Albedo_Index;
		uniform fixed _Texture_14_Tiling;
		uniform fixed _Texture_14_Far_Multiplier;
		uniform fixed4 _Texture_14_Color;
		uniform fixed _Texture_15_Albedo_Index;
		uniform fixed _Texture_15_Tiling;
		uniform fixed _Texture_15_Far_Multiplier;
		uniform fixed4 _Texture_15_Color;
		uniform fixed _Texture_16_Albedo_Index;
		uniform fixed _Texture_16_Tiling;
		uniform fixed _Texture_16_Far_Multiplier;
		uniform fixed4 _Texture_16_Color;
		uniform fixed _Geological_Map_Far_Power;
		uniform sampler2D _Texture_Geological_Map;
		uniform fixed _Geological_Tiling_Far;
		uniform fixed _Geological_Map_Offset_Far;
		uniform fixed _Texture_16_Geological_Power;
		uniform fixed _Texture_15_Geological_Power;
		uniform fixed _Texture_14_Geological_Power;
		uniform fixed _Texture_13_Geological_Power;
		uniform fixed _Texture_12_Geological_Power;
		uniform fixed _Texture_11_Geological_Power;
		uniform fixed _Texture_10_Geological_Power;
		uniform fixed _Texture_9_Geological_Power;
		uniform fixed _Texture_8_Geological_Power;
		uniform fixed _Texture_7_Geological_Power;
		uniform fixed _Texture_6_Geological_Power;
		uniform fixed _Texture_5_Geological_Power;
		uniform fixed _Texture_1_Geological_Power;
		uniform fixed _Texture_2_Geological_Power;
		uniform fixed _Texture_4_Geological_Power;
		uniform fixed _Texture_3_Geological_Power;
		uniform fixed _Texture_Snow_Index;
		uniform fixed _Snow_Tiling;
		uniform fixed _Snow_Tiling_Far_Multiplier;
		uniform fixed4 _Snow_Color;
		uniform fixed _Terrain_Specular;
		uniform fixed _Snow_Specular;
		uniform fixed _Terrain_Smoothness;
		uniform fixed _Ambient_Occlusion_Power;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float3 normalWorld = UnityObjectToWorldNormal( v.normal );
			v.vertex.xyz += 0.0;
			 v.tangent.xyz=cross( normalWorld , fixed3(0,0,1) );
			 v.tangent.w = -1;//;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult3897 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_0 = (( 1.0 / _Perlin_Normal_Tiling_Far )).xx;
			float4 texArray4374 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_0 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g223 = float2( texArray4374.x , texArray4374.y );
			float2 temp_cast_2 = (_Perlin_Normal_Power).xx;
			float2 temp_output_4_0_g223 = ( ( ( appendResult11_g223 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_2 );
			float2 temp_cast_3 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_4 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_5 = (_Perlin_Normal_Power).xx;
			float temp_output_9_0_g223 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g223 , temp_output_4_0_g223 ) ) ) );
			float3 appendResult10_g223 = float3( temp_output_4_0_g223.x , temp_output_4_0_g223.y , temp_output_9_0_g223 );
			fixed4 tex2DNode4371 = tex2D( _Texture_Splat_4, i.texcoord_0 );
			fixed Splat4_A = tex2DNode4371.a;
			fixed Splat4_B = tex2DNode4371.b;
			fixed Splat4_G = tex2DNode4371.g;
			fixed Splat4_R = tex2DNode4371.r;
			fixed4 tex2DNode4370 = tex2D( _Texture_Splat_3, i.texcoord_0 );
			fixed Splat3_A = tex2DNode4370.a;
			fixed Splat3_B = tex2DNode4370.b;
			fixed Splat3_G = tex2DNode4370.g;
			fixed Splat3_R = tex2DNode4370.r;
			fixed4 tex2DNode4369 = tex2D( _Texture_Splat_2, i.texcoord_0 );
			fixed Splat2_A = tex2DNode4369.a;
			fixed Splat2_B = tex2DNode4369.b;
			fixed Splat2_G = tex2DNode4369.g;
			fixed Splat2_R = tex2DNode4369.r;
			fixed4 tex2DNode4368 = tex2D( _Texture_Splat_1, i.texcoord_0 );
			fixed Splat1_R = tex2DNode4368.r;
			fixed Splat1_G = tex2DNode4368.g;
			fixed Splat1_A = tex2DNode4368.a;
			fixed Splat1_B = tex2DNode4368.b;
			fixed3 _Vector0 = fixed3(0,0,1);
			float2 appendResult3750 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_6 = (_Snow_Noise_Tiling).xx;
			float4 texArray4383 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3750 * temp_cast_6 ), _Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_7 = (_Snow_Noise_Tiling).xx;
			float4 texArray4385 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_7 ) * float2( 0.23,0.23 ) ), _Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_8 = (_Snow_Noise_Tiling).xx;
			float4 texArray4384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_8 ) * float2( 0.53,0.53 ) ), _Texture_Snow_Noise_Index)  );
			fixed ifLocalVar6135 = 0;
			UNITY_BRANCH if( _Texture_Snow_Noise_Index > -1.0 )
				ifLocalVar6135 = lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , _Snow_Noise_Power );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			fixed SnowSlope = pow( ( 1.0 - ( clamp( ( clamp( ase_worldNormal.y , 0.0 , 0.9999 ) - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) , 0.0 , 2.0 ) * ( 1.0 / ( 1.0 - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) ) ) ) , _Snow_Maximum_Angle_Hardness );
			float HeightMask5474 = saturate(pow(((( ( _Vector0.y + 1.0 ) * 0.5 )*saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ))*4)+(saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) )*2),5.0));;
			fixed3 temp_cast_9 = (WorldNormalVector( i , lerp( _Vector0 , lerp( _Vector0 , _Vector0 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_10 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_11 = (0.0).xxx;
			o.Normal = BlendNormals( lerp( lerp( float3( 0,0,1 ) , appendResult10_g223 , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , appendResult10_g223 , ( _Snow_Perlin_Power * 0.5 ) ) , lerp( saturate( clamp( ( lerp( _Vector0 , temp_cast_9 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_10 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_11 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6135 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x ) , normalize( UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) ) );
			float2 appendResult1998 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 Top_Bottom = appendResult1998;
			float2 appendResult3284 = float2( ( 1.0 / _Texture_1_Tiling ) , ( 1.0 / _Texture_1_Tiling ) );
			fixed2 temp_cast_13 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3292 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_13 ), _Texture_1_Albedo_Index)  );
			fixed4 ifLocalVar6119 = 0;
			UNITY_BRANCH if( _Texture_1_Albedo_Index > -1.0 )
				ifLocalVar6119 = ( texArray3292 * _Texture_1_Color );
			fixed4 Texture_1_Final = ifLocalVar6119;
			float2 appendResult3349 = float2( ( 1.0 / _Texture_2_Tiling ) , ( 1.0 / _Texture_2_Tiling ) );
			fixed2 temp_cast_14 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3339 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_14 ), _Texture_2_Albedo_Index)  );
			fixed4 ifLocalVar6120 = 0;
			UNITY_BRANCH if( _Texture_2_Albedo_Index > -1.0 )
				ifLocalVar6120 = ( texArray3339 * _Texture_2_Color );
			fixed4 Texture_2_Final = ifLocalVar6120;
			float2 appendResult3415 = float2( ( 1.0 / _Texture_3_Tiling ) , ( 1.0 / _Texture_3_Tiling ) );
			fixed2 temp_cast_15 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_15 ), _Texture_3_Albedo_Index)  );
			fixed4 ifLocalVar6121 = 0;
			UNITY_BRANCH if( _Texture_3_Albedo_Index > -1.0 )
				ifLocalVar6121 = ( texArray3406 * _Texture_3_Color );
			fixed4 Texture_3_Final = ifLocalVar6121;
			fixed2 temp_cast_16 = (( 1.0 / _Texture_4_Tiling )).xx;
			fixed2 temp_cast_17 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3473 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * temp_cast_16 ) / temp_cast_17 ), _Texture_4_Albedo_Index)  );
			fixed4 ifLocalVar6122 = 0;
			UNITY_BRANCH if( _Texture_4_Albedo_Index > -1.0 )
				ifLocalVar6122 = ( texArray3473 * _Texture_4_Color );
			fixed4 Texture_4_Final = ifLocalVar6122;
			float4 layeredBlendVar5643 = tex2DNode4368;
			float4 layeredBlend5643 = ( lerp( lerp( lerp( lerp( float4( 0,0,0,0 ) , Texture_1_Final , layeredBlendVar5643.x ) , Texture_2_Final , layeredBlendVar5643.y ) , Texture_3_Final , layeredBlendVar5643.z ) , Texture_4_Final , layeredBlendVar5643.w ) );
			float2 appendResult4399 = float2( ( 1.0 / _Texture_5_Tiling ) , ( 1.0 / _Texture_5_Tiling ) );
			fixed2 temp_cast_18 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_18 ), _Texture_5_Albedo_Index)  );
			fixed4 ifLocalVar6123 = 0;
			UNITY_BRANCH if( _Texture_5_Albedo_Index > -1.0 )
				ifLocalVar6123 = ( texArray4445 * _Texture_5_Color );
			fixed4 Texture_5_Final = ifLocalVar6123;
			float2 appendResult4471 = float2( ( 1.0 / _Texture_6_Tiling ) , ( 1.0 / _Texture_6_Tiling ) );
			fixed2 temp_cast_19 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_19 ), _Texture_6_Albedo_Index)  );
			fixed4 ifLocalVar6124 = 0;
			UNITY_BRANCH if( _Texture_6_Albedo_Index > -1.0 )
				ifLocalVar6124 = ( texArray4512 * _Texture_6_Color );
			fixed4 Texture_6_Final = ifLocalVar6124;
			float2 appendResult4545 = float2( ( 1.0 / _Texture_7_Tiling ) , ( 1.0 / _Texture_7_Tiling ) );
			fixed2 temp_cast_20 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_20 ), _Texture_7_Albedo_Index)  );
			fixed4 ifLocalVar6125 = 0;
			UNITY_BRANCH if( _Texture_7_Albedo_Index > -1.0 )
				ifLocalVar6125 = ( texArray4586 * _Texture_7_Color );
			fixed4 Texture_7_Final = ifLocalVar6125;
			float2 appendResult4619 = float2( ( 1.0 / _Texture_8_Tiling ) , ( 1.0 / _Texture_8_Tiling ) );
			fixed2 temp_cast_21 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4660 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_21 ), _Texture_8_Albedo_Index)  );
			fixed4 ifLocalVar6126 = 0;
			UNITY_BRANCH if( _Texture_8_Albedo_Index > -1.0 )
				ifLocalVar6126 = ( texArray4660 * _Texture_8_Color );
			fixed4 Texture_8_Final = ifLocalVar6126;
			float4 layeredBlendVar5644 = tex2DNode4369;
			float4 layeredBlend5644 = ( lerp( lerp( lerp( lerp( layeredBlend5643 , Texture_5_Final , layeredBlendVar5644.x ) , Texture_6_Final , layeredBlendVar5644.y ) , Texture_7_Final , layeredBlendVar5644.z ) , Texture_8_Final , layeredBlendVar5644.w ) );
			float2 appendResult4736 = float2( ( 1.0 / _Texture_9_Tiling ) , ( 1.0 / _Texture_9_Tiling ) );
			fixed2 temp_cast_22 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4889 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_22 ), _Texture_9_Albedo_Index)  );
			fixed4 ifLocalVar6134 = 0;
			UNITY_BRANCH if( _Texture_9_Albedo_Index > -1.0 )
				ifLocalVar6134 = ( texArray4889 * _Texture_9_Color );
			fixed4 Texture_9_Final = ifLocalVar6134;
			float2 appendResult4738 = float2( ( 1.0 / _Texture_10_Tiling ) , ( 1.0 / _Texture_10_Tiling ) );
			fixed2 temp_cast_23 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4913 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_23 ), _Texture_10_Albedo_Index)  );
			fixed4 ifLocalVar6133 = 0;
			UNITY_BRANCH if( _Texture_10_Albedo_Index > -1.0 )
				ifLocalVar6133 = ( texArray4913 * _Texture_10_Color );
			fixed4 Texture_10_Final = ifLocalVar6133;
			float2 appendResult4741 = float2( ( 1.0 / _Texture_11_Tiling ) , ( 1.0 / _Texture_11_Tiling ) );
			fixed2 temp_cast_24 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4923 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_24 ), _Texture_11_Albedo_Index)  );
			fixed4 ifLocalVar6132 = 0;
			UNITY_BRANCH if( _Texture_11_Albedo_Index > -1.0 )
				ifLocalVar6132 = ( texArray4923 * _Texture_11_Color );
			fixed4 Texture_11_Final = ifLocalVar6132;
			float2 appendResult4751 = float2( ( 1.0 / _Texture_12_Tiling ) , ( 1.0 / _Texture_12_Tiling ) );
			fixed2 temp_cast_25 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4952 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_25 ), _Texture_12_Albedo_Index)  );
			fixed4 ifLocalVar6131 = 0;
			UNITY_BRANCH if( _Texture_12_Albedo_Index > -1.0 )
				ifLocalVar6131 = ( texArray4952 * _Texture_12_Color );
			fixed4 Texture_12_Final = ifLocalVar6131;
			float4 layeredBlendVar5645 = tex2DNode4370;
			float4 layeredBlend5645 = ( lerp( lerp( lerp( lerp( layeredBlend5644 , Texture_9_Final , layeredBlendVar5645.x ) , Texture_10_Final , layeredBlendVar5645.y ) , Texture_11_Final , layeredBlendVar5645.z ) , Texture_12_Final , layeredBlendVar5645.w ) );
			float2 appendResult5027 = float2( ( 1.0 / _Texture_13_Tiling ) , ( 1.0 / _Texture_13_Tiling ) );
			fixed2 temp_cast_26 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5034 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_26 ), _Texture_13_Albedo_Index)  );
			fixed4 ifLocalVar6130 = 0;
			UNITY_BRANCH if( _Texture_13_Albedo_Index > -1.0 )
				ifLocalVar6130 = ( texArray5034 * _Texture_13_Color );
			fixed4 Texture_13_Final = ifLocalVar6130;
			float2 appendResult5033 = float2( ( 1.0 / _Texture_14_Tiling ) , ( 1.0 / _Texture_14_Tiling ) );
			fixed2 temp_cast_27 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5171 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_27 ), _Texture_14_Albedo_Index)  );
			fixed4 ifLocalVar6129 = 0;
			UNITY_BRANCH if( _Texture_14_Albedo_Index > -1.0 )
				ifLocalVar6129 = ( texArray5171 * _Texture_14_Color );
			fixed4 Texture_14_Final = ifLocalVar6129;
			float2 appendResult5212 = float2( ( 1.0 / _Texture_15_Tiling ) , ( 1.0 / _Texture_15_Tiling ) );
			fixed2 temp_cast_28 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5272 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_28 ), _Texture_15_Albedo_Index)  );
			fixed4 ifLocalVar6128 = 0;
			UNITY_BRANCH if( _Texture_15_Albedo_Index > -1.0 )
				ifLocalVar6128 = ( texArray5272 * _Texture_15_Color );
			fixed4 Texture_15_Final = ifLocalVar6128;
			float2 appendResult5078 = float2( ( 1.0 / _Texture_16_Tiling ) , ( 1.0 / _Texture_16_Tiling ) );
			fixed2 temp_cast_29 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5145 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_29 ), _Texture_16_Albedo_Index)  );
			fixed4 ifLocalVar6127 = 0;
			UNITY_BRANCH if( _Texture_16_Albedo_Index > -1.0 )
				ifLocalVar6127 = ( texArray5145 * _Texture_16_Color );
			fixed4 Texture_16_Final = ifLocalVar6127;
			float4 layeredBlendVar5646 = tex2DNode4371;
			float4 layeredBlend5646 = ( lerp( lerp( lerp( lerp( layeredBlend5645 , Texture_13_Final , layeredBlendVar5646.x ) , Texture_14_Final , layeredBlendVar5646.y ) , Texture_15_Final , layeredBlendVar5646.z ) , Texture_16_Final , layeredBlendVar5646.w ) );
			float3 appendResult3857 = float3( layeredBlend5646.x , layeredBlend5646.y , layeredBlend5646.z );
			fixed3 temp_cast_31 = (_Geological_Map_Far_Power).xxx;
			fixed2 temp_cast_32 = (( ( ase_worldPos.y * ( 1.0 / _Geological_Tiling_Far ) ) + _Geological_Map_Offset_Far )).xx;
			fixed4 tex2DNode5983 = tex2D( _Texture_Geological_Map, temp_cast_32 );
			float3 appendResult5985 = float3( tex2DNode5983.r , tex2DNode5983.g , tex2DNode5983.b );
			fixed3 temp_cast_33 = (( ( _Texture_16_Geological_Power * Splat4_A ) + ( ( _Texture_15_Geological_Power * Splat4_B ) + ( ( _Texture_14_Geological_Power * Splat4_G ) + ( ( _Texture_13_Geological_Power * Splat4_R ) + ( ( _Texture_12_Geological_Power * Splat3_A ) + ( ( _Texture_11_Geological_Power * Splat3_B ) + ( ( _Texture_10_Geological_Power * Splat3_G ) + ( ( _Texture_9_Geological_Power * Splat3_R ) + ( ( _Texture_8_Geological_Power * Splat2_A ) + ( ( _Texture_7_Geological_Power * Splat2_B ) + ( ( _Texture_6_Geological_Power * Splat2_G ) + ( ( _Texture_5_Geological_Power * Splat2_R ) + ( ( _Texture_1_Geological_Power * Splat1_R ) + ( ( _Texture_2_Geological_Power * Splat1_G ) + ( ( _Texture_4_Geological_Power * Splat1_A ) + ( _Texture_3_Geological_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )).xxx;
			float2 appendResult3679 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_35 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_36 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray4379 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( appendResult3679 * temp_cast_35 ) / temp_cast_36 ), _Texture_Snow_Index)  );
			fixed4 ifLocalVar6136 = 0;
			UNITY_BRANCH if( _Texture_Snow_Index > -1.0 )
				ifLocalVar6136 = ( texArray4379 * _Snow_Color );
			float3 appendResult1410 = float3( ifLocalVar6136.x , ifLocalVar6136.y , ifLocalVar6136.z );
			fixed3 temp_cast_38 = (WorldNormalVector( i , lerp( _Vector0 , lerp( _Vector0 , _Vector0 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_39 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_40 = (0.0).xxx;
			o.Albedo = lerp( clamp( ( saturate( ( fixed4( appendResult3857 , 0.0 ) + fixed4( ( ( temp_cast_31 * ( appendResult5985 + float3( -0.3,-0.3,-0.3 ) ) ) * temp_cast_33 ) , 0.0 ) ) )) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) ) , fixed4( appendResult1410 , 0.0 ) , lerp( saturate( clamp( ( lerp( _Vector0 , temp_cast_38 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_39 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_40 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6135 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x ).rgb;
			fixed3 temp_cast_43 = (_Terrain_Specular).xxx;
			fixed3 temp_cast_44 = (_Snow_Specular).xxx;
			fixed3 temp_cast_45 = (WorldNormalVector( i , lerp( _Vector0 , lerp( _Vector0 , _Vector0 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_46 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_47 = (0.0).xxx;
			o.Specular = lerp( ( ( appendResult3857 * float3( 0.3,0.3,0.3 ) ) * temp_cast_43 ) , ( clamp( appendResult1410 , float3( 0,0,0 ) , float3( 0.5,0.5,0.5 ) ) * temp_cast_44 ) , lerp( saturate( clamp( ( lerp( _Vector0 , temp_cast_45 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_46 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_47 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6135 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x );
			fixed3 temp_cast_49 = (WorldNormalVector( i , lerp( _Vector0 , lerp( _Vector0 , _Vector0 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_50 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_51 = (0.0).xxx;
			o.Smoothness = lerp( ( layeredBlend5646.w * _Terrain_Smoothness ) , ifLocalVar6136.w , lerp( saturate( clamp( ( lerp( _Vector0 , temp_cast_49 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_50 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_51 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6135 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x );
			fixed3 temp_cast_53 = (WorldNormalVector( i , lerp( _Vector0 , lerp( _Vector0 , _Vector0 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_54 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_55 = (0.0).xxx;
			#ifdef _USE_AO_ON
			float staticSwitch6142 = clamp( ( ( 1.0 + BlendNormals( lerp( lerp( float3( 0,0,1 ) , appendResult10_g223 , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , appendResult10_g223 , ( _Snow_Perlin_Power * 0.5 ) ) , lerp( saturate( clamp( ( lerp( _Vector0 , temp_cast_53 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6135 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_54 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_55 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6135 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x ) , normalize( UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) ) ).y ) * 0.5 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#else
			float staticSwitch6142 = 1.0;
			#endif
			o.Occlusion = staticSwitch6142;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers d3d9 gles 
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows noforwardadd vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	Dependency "BaseMapShader" = "CTS Terrain Shader Basic LOD"
}