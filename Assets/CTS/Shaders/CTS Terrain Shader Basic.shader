Shader "CTS Terrain Shader Basic"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Geological_Tiling_Far("Geological_Tiling_Far", Range( 0 , 1000)) = 87
		_Geological_Tiling_Close("Geological_Tiling_Close", Range( 0 , 1000)) = 87
		_Geological_Map_Offset_Far("Geological_Map_Offset _Far", Range( 0 , 1)) = 1
		_Geological_Map_Offset_Close("Geological_Map_Offset _Close", Range( 0 , 1)) = 1
		_Geological_Map_Close_Power("Geological_Map_Close_Power", Range( 0 , 1)) = 0
		_Geological_Map_Far_Power("Geological_Map_Far_Power", Range( 0 , 1)) = 1
		_UV_Mix_Power("UV_Mix_Power", Range( 0.01 , 10)) = 4
		_UV_Mix_Start_Distance("UV_Mix_Start_Distance", Range( 0 , 100000)) = 400
		_Perlin_Normal_Tiling_Close("Perlin_Normal_Tiling_Close", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Tiling_Far("Perlin_Normal_Tiling_Far", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Power("Perlin_Normal_Power", Range( 0 , 10)) = 1
		_Perlin_Normal_Power_Close("Perlin_Normal_Power_Close", Range( 0 , 10)) = 0.5
		_Terrain_Smoothness("Terrain_Smoothness", Range( 0 , 2)) = 1
		_Terrain_Specular("Terrain_Specular", Range( 0 , 3)) = 1
		_Global_Normalmap_Power("Global_Normalmap_Power", Range( 0 , 10)) = 0
		_Texture_1_Tiling("Texture_1_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_2_Tiling("Texture_2_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_3_Tiling("Texture_3_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_4_Tiling("Texture_4_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_5_Tiling("Texture_5_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_6_Tiling("Texture_6_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_7_Tiling("Texture_7_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_8_Tiling("Texture_8_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_9_Tiling("Texture_9_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_10_Tiling("Texture_10_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_11_Tiling("Texture_11_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_12_Tiling("Texture_12_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_13_Tiling("Texture_13_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_14_Tiling("Texture_14_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_15_Tiling("Texture_15_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_16_Tiling("Texture_16_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_1_Far_Multiplier("Texture_1_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_2_Far_Multiplier("Texture_2_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_3_Far_Multiplier("Texture_3_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_4_Far_Multiplier("Texture_4_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_5_Far_Multiplier("Texture_5_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_6_Far_Multiplier("Texture_6_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_7_Far_Multiplier("Texture_7_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_8_Far_Multiplier("Texture_8_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_9_Far_Multiplier("Texture_9_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_10_Far_Multiplier("Texture_10_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_11_Far_Multiplier("Texture_11_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_12_Far_Multiplier("Texture_12_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_13_Far_Multiplier("Texture_13_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_14_Far_Multiplier("Texture_14_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_15_Far_Multiplier("Texture_15_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_16_Far_Multiplier("Texture_16_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_1_Perlin_Power("Texture_1_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_2_Perlin_Power("Texture_2_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_3_Perlin_Power("Texture_3_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_4_Perlin_Power("Texture_4_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_5_Perlin_Power("Texture_5_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_6_Perlin_Power("Texture_6_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_7_Perlin_Power("Texture_7_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_8_Perlin_Power("Texture_8_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_9_Perlin_Power("Texture_9_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_10_Perlin_Power("Texture_10_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_11_Perlin_Power("Texture_11_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_12_Perlin_Power("Texture_12_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_13_Perlin_Power("Texture_13_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_14_Perlin_Power("Texture_14_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_15_Perlin_Power("Texture_15_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_16_Perlin_Power("Texture_16_Perlin_Power", Range( 0 , 1)) = 0
		_Texture_1_Geological_Power("Texture_1_Geological_Power", Range( 0 , 5)) = 1
		_Texture_2_Geological_Power("Texture_2_Geological_Power", Range( 0 , 5)) = 1
		_Texture_3_Geological_Power("Texture_3_Geological_Power", Range( 0 , 5)) = 1
		_Texture_4_Geological_Power("Texture_4_Geological_Power", Range( 0 , 5)) = 1
		_Texture_5_Geological_Power("Texture_5_Geological_Power", Range( 0 , 5)) = 1
		_Texture_6_Geological_Power("Texture_6_Geological_Power", Range( 0 , 5)) = 1
		_Texture_7_Geological_Power("Texture_7_Geological_Power", Range( 0 , 5)) = 1
		_Texture_8_Geological_Power("Texture_8_Geological_Power", Range( 0 , 5)) = 1
		_Texture_9_Geological_Power("Texture_9_Geological_Power", Range( 0 , 5)) = 1
		_Texture_10_Geological_Power("Texture_10_Geological_Power", Range( 0 , 5)) = 1
		_Texture_11_Geological_Power("Texture_11_Geological_Power", Range( 0 , 5)) = 1
		_Texture_12_Geological_Power("Texture_12_Geological_Power", Range( 0 , 5)) = 1
		_Texture_13_Geological_Power("Texture_13_Geological_Power", Range( 0 , 5)) = 1
		_Texture_14_Geological_Power("Texture_14_Geological_Power", Range( 0 , 5)) = 1
		_Texture_15_Geological_Power("Texture_15_Geological_Power", Range( 0 , 5)) = 1
		_Texture_16_Geological_Power("Texture_16_Geological_Power", Range( 0 , 5)) = 1
		_Snow_Specular("Snow_Specular", Range( 0 , 3)) = 1
		_Snow_Normal_Scale("Snow_Normal_Scale", Range( 0 , 5)) = 1
		_Snow_Blend_Normal("Snow_Blend_Normal", Range( 0 , 1)) = 0.8
		_Snow_Amount("Snow_Amount", Range( 0 , 2)) = 0
		_Snow_Tiling("Snow_Tiling", Range( 0.001 , 20)) = 15
		_Snow_Tiling_Far_Multiplier("Snow_Tiling_Far_Multiplier", Range( 0.001 , 20)) = 1
		_Snow_Perlin_Power("Snow_Perlin_Power", Range( 0 , 1)) = 0
		_Snow_Noise_Power("Snow_Noise_Power", Range( 0 , 1)) = 1
		_Snow_Noise_Tiling("Snow_Noise_Tiling", Range( 0.001 , 1)) = 0.02
		_Snow_Min_Height("Snow_Min_Height", Range( -1000 , 10000)) = -1000
		_Snow_Min_Height_Blending("Snow_Min_Height_Blending", Range( 0 , 500)) = 1
		_Snow_Maximum_Angle("Snow_Maximum_Angle", Range( 0.001 , 180)) = 30
		_Snow_Maximum_Angle_Hardness("Snow_Maximum_Angle_Hardness", Range( 0.01 , 3)) = 1
		_Texture_1_Snow_Reduction("Texture_1_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_2_Snow_Reduction("Texture_2_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_3_Snow_Reduction("Texture_3_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_4_Snow_Reduction("Texture_4_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_5_Snow_Reduction("Texture_5_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_6_Snow_Reduction("Texture_6_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_7_Snow_Reduction("Texture_7_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_8_Snow_Reduction("Texture_8_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_9_Snow_Reduction("Texture_9_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_10_Snow_Reduction("Texture_10_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_11_Snow_Reduction("Texture_11_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_12_Snow_Reduction("Texture_12_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_13_Snow_Reduction("Texture_13_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_14_Snow_Reduction("Texture_14_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_15_Snow_Reduction("Texture_15_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_16_Snow_Reduction("Texture_16_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_Array_Normal("Texture_Array_Normal", 2DArray ) = "" {}
		_Texture_Array_Albedo("Texture_Array_Albedo", 2DArray ) = "" {}
		_Texture_Perlin_Normal_Index("Texture_Perlin_Normal_Index", Int) = -1
		_Texture_1_Normal_Power("Texture_1_Normal_Power", Range( 0 , 5)) = 1
		_Texture_2_Normal_Power("Texture_2_Normal_Power", Range( 0 , 5)) = 1
		_Texture_3_Normal_Power("Texture_3_Normal_Power", Range( 0 , 5)) = 1
		_Texture_4_Normal_Power("Texture_4_Normal_Power", Range( 0 , 5)) = 1
		_Texture_5_Normal_Power("Texture_5_Normal_Power", Range( 0 , 5)) = 1
		_Texture_6_Normal_Power("Texture_6_Normal_Power", Range( 0 , 5)) = 1
		_Texture_7_Normal_Power("Texture_7_Normal_Power", Range( 0 , 5)) = 1
		_Texture_8_Normal_Power("Texture_8_Normal_Power", Range( 0 , 5)) = 1
		_Texture_9_Normal_Power("Texture_9_Normal_Power", Range( 0 , 5)) = 1
		_Texture_10_Normal_Power("Texture_10_Normal_Power", Range( 0 , 5)) = 1
		_Texture_11_Normal_Power("Texture_11_Normal_Power", Range( 0 , 5)) = 1
		_Texture_12_Normal_Power("Texture_12_Normal_Power", Range( 0 , 5)) = 1
		_Texture_13_Normal_Powerr("Texture_13_Normal_Powerr", Range( 0 , 5)) = 1
		_Texture_14_Normal_Power("Texture_14_Normal_Power", Range( 0 , 5)) = 1
		_Texture_15_Normal_Power("Texture_15_Normal_Power", Range( 0 , 5)) = 1
		_Texture_16_Normal_Power("Texture_16_Normal_Power", Range( 0 , 5)) = 1
		_Texture_Splat_1("Texture_Splat_1", 2D) = "black" {}
		_Texture_Splat_2("Texture_Splat_2", 2D) = "black" {}
		_Texture_Splat_3("Texture_Splat_3", 2D) = "black" {}
		_Texture_Splat_4("Texture_Splat_4", 2D) = "black" {}
		_Global_Normal_Map("Global_Normal_Map", 2D) = "bump" {}
		_Ambient_Occlusion_Power("Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		_Texture_3_Triplanar("Texture_3_Triplanar", Range( 0 , 1)) = 0
		_Texture_Snow_Noise_Index("Texture_Snow_Noise_Index", Range( -1 , 100)) = -1
		_Texture_Snow_Index("Texture_Snow_Index", Range( -1 , 100)) = -1
		_Texture_Snow_Normal_Index("Texture_Snow_Normal_Index", Range( -1 , 100)) = -1
		_Texture_4_Triplanar("Texture_4_Triplanar", Range( 0 , 1)) = 0
		_Texture_2_Triplanar("Texture_2_Triplanar", Range( 0 , 1)) = 0
		_Texture_6_Triplanar("Texture_6_Triplanar", Range( 0 , 1)) = 0
		_Texture_8_Triplanar("Texture_8_Triplanar", Range( 0 , 1)) = 0
		_Texture_9_Triplanar("Texture_9_Triplanar", Range( 0 , 1)) = 0
		_Texture_10_Triplanar("Texture_10_Triplanar", Range( 0 , 1)) = 0
		_Texture_11_Triplanar("Texture_11_Triplanar", Range( 0 , 1)) = 0
		_Texture_12_Triplanar("Texture_12_Triplanar", Range( 0 , 1)) = 0
		_Texture_13_Triplanar("Texture_13_Triplanar", Range( 0 , 1)) = 0
		_Texture_14_Triplanar("Texture_14_Triplanar", Range( 0 , 1)) = 0
		_Texture_15_Triplanar("Texture_15_Triplanar", Range( 0 , 1)) = 0
		_Texture_16_Triplanar("Texture_16_Triplanar", Range( 0 , 1)) = 0
		_Texture_7_Triplanar("Texture_7_Triplanar", Range( 0 , 1)) = 0
		_Texture_5_Triplanar("Texture_5_Triplanar", Range( 0 , 1)) = 0
		_Texture_1_Triplanar("Texture_1_Triplanar", Range( 0 , 1)) = 0
		_Texture_4_Color("Texture_4_Color", Vector) = (1,1,1,1)
		_Snow_Color("Snow_Color", Vector) = (1,1,1,1)
		_Texture_16_Color("Texture_16_Color", Vector) = (1,1,1,1)
		_Texture_8_Color("Texture_8_Color", Vector) = (1,1,1,1)
		_Texture_7_Color("Texture_7_Color", Vector) = (1,1,1,1)
		_Texture_6_Color("Texture_6_Color", Vector) = (1,1,1,1)
		_Texture_5_Color("Texture_5_Color", Vector) = (1,1,1,1)
		_Texture_2_Color("Texture_2_Color", Vector) = (1,1,1,1)
		_Texture_3_Color("Texture_3_Color", Vector) = (1,1,1,1)
		_Texture_13_Color("Texture_13_Color", Vector) = (1,1,1,1)
		_Texture_15_Color("Texture_15_Color", Vector) = (1,1,1,1)
		_Texture_14_Color("Texture_14_Color", Vector) = (1,1,1,1)
		_Texture_9_Color("Texture_9_Color", Vector) = (1,1,1,1)
		_Texture_12_Color("Texture_12_Color", Vector) = (1,1,1,1)
		_Texture_11_Color("Texture_11_Color", Vector) = (1,1,1,1)
		_Texture_10_Color("Texture_10_Color", Vector) = (1,1,1,1)
		_Texture_1_Color("Texture_1_Color", Vector) = (1,1,1,1)
		_Texture_1_Albedo_Index("Texture_1_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_1_Normal_Index("Texture_1_Normal_Index", Range( -1 , 100)) = -1
		_Texture_2_Albedo_Index("Texture_2_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_3_Albedo_Index("Texture_3_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_3_Normal_Index("Texture_3_Normal_Index", Range( -1 , 100)) = -1
		_Texture_4_Albedo_Index("Texture_4_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_4_Normal_Index("Texture_4_Normal_Index", Range( -1 , 100)) = -1
		_Texture_5_Albedo_Index("Texture_5_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_5_Normal_Index("Texture_5_Normal_Index", Range( -1 , 100)) = -1
		_Texture_6_Normal_Index("Texture_6_Normal_Index", Range( -1 , 100)) = -1
		_Texture_6_Albedo_Index("Texture_6_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_7_Normal_Index("Texture_7_Normal_Index", Range( -1 , 100)) = -1
		_Texture_8_Albedo_Index("Texture_8_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_2_Normal_Index("Texture_2_Normal_Index", Range( -1 , 100)) = -1
		_Texture_8_Normal_Index("Texture_8_Normal_Index", Range( -1 , 100)) = -1
		_Texture_16_Normal_Index("Texture_16_Normal_Index", Range( -1 , 100)) = -1
		_Texture_15_Normal_Index("Texture_15_Normal_Index", Range( -1 , 100)) = -1
		_Texture_15_Albedo_Index("Texture_15_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_Geological_Map("Texture_Geological_Map", 2D) = "white" {}
		_Texture_7_Albedo_Index("Texture_7_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_14_Normal_Index("Texture_14_Normal_Index", Range( -1 , 100)) = -1
		_Texture_13_Normal_Index("Texture_13_Normal_Index", Range( -1 , 100)) = -1
		_Texture_14_Albedo_Index("Texture_14_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_13_Albedo_Index("Texture_13_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_12_Normal_Index("Texture_12_Normal_Index", Range( -1 , 100)) = -1
		_Texture_12_Albedo_Index("Texture_12_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_11_Normal_Index("Texture_11_Normal_Index", Range( -1 , 100)) = -1
		_Texture_11_Albedo_Index("Texture_11_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_10_Normal_Index("Texture_10_Normal_Index", Range( -1 , 100)) = -1
		_Texture_10_Albedo_Index("Texture_10_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_9_Normal_Index("Texture_9_Normal_Index", Range( -1 , 100)) = -1
		_Texture_9_Albedo_Index("Texture_9_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_16_Albedo_Index("Texture_16_Albedo_Index", Range( -1 , 100)) = -1
		[Toggle]_Use_AO("Use_AO", Int) = 0
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+100" }
		Cull Back
		ZTest LEqual
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.5
		#pragma multi_compile __ _USE_AO_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float2 texcoord_0;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform fixed _Perlin_Normal_Tiling_Close;
		uniform int _Texture_Perlin_Normal_Index;
		uniform fixed _Perlin_Normal_Power_Close;
		uniform fixed _Perlin_Normal_Tiling_Far;
		uniform fixed _Perlin_Normal_Power;
		uniform fixed _UV_Mix_Start_Distance;
		uniform fixed _UV_Mix_Power;
		uniform fixed _Texture_16_Perlin_Power;
		uniform sampler2D _Texture_Splat_4;
		uniform fixed _Texture_15_Perlin_Power;
		uniform fixed _Texture_14_Perlin_Power;
		uniform fixed _Texture_13_Perlin_Power;
		uniform fixed _Texture_12_Perlin_Power;
		uniform sampler2D _Texture_Splat_3;
		uniform fixed _Texture_11_Perlin_Power;
		uniform fixed _Texture_10_Perlin_Power;
		uniform fixed _Texture_9_Perlin_Power;
		uniform fixed _Texture_8_Perlin_Power;
		uniform sampler2D _Texture_Splat_2;
		uniform fixed _Texture_7_Perlin_Power;
		uniform fixed _Texture_6_Perlin_Power;
		uniform fixed _Texture_5_Perlin_Power;
		uniform fixed _Texture_1_Perlin_Power;
		uniform sampler2D _Texture_Splat_1;
		uniform fixed _Texture_2_Perlin_Power;
		uniform fixed _Texture_4_Perlin_Power;
		uniform fixed _Texture_3_Perlin_Power;
		uniform fixed _Snow_Perlin_Power;
		uniform fixed _Texture_1_Normal_Index;
		uniform fixed _Texture_1_Triplanar;
		uniform fixed _Texture_1_Tiling;
		uniform fixed _Texture_1_Normal_Power;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Normal );
		uniform fixed _Texture_2_Normal_Index;
		uniform fixed _Texture_2_Triplanar;
		uniform fixed _Texture_2_Tiling;
		uniform fixed _Texture_2_Normal_Power;
		uniform fixed _Texture_3_Normal_Index;
		uniform fixed _Texture_3_Triplanar;
		uniform fixed _Texture_3_Tiling;
		uniform fixed _Texture_3_Normal_Power;
		uniform fixed _Texture_3_Far_Multiplier;
		uniform fixed _Texture_4_Normal_Index;
		uniform fixed _Texture_4_Triplanar;
		uniform fixed _Texture_4_Tiling;
		uniform fixed _Texture_4_Normal_Power;
		uniform fixed _Texture_5_Normal_Index;
		uniform fixed _Texture_5_Triplanar;
		uniform fixed _Texture_5_Tiling;
		uniform fixed _Texture_5_Normal_Power;
		uniform fixed _Texture_6_Normal_Index;
		uniform fixed _Texture_6_Triplanar;
		uniform fixed _Texture_6_Tiling;
		uniform fixed _Texture_6_Normal_Power;
		uniform fixed _Texture_7_Normal_Index;
		uniform fixed _Texture_7_Triplanar;
		uniform fixed _Texture_7_Tiling;
		uniform fixed _Texture_7_Normal_Power;
		uniform fixed _Texture_8_Normal_Index;
		uniform fixed _Texture_8_Triplanar;
		uniform fixed _Texture_8_Tiling;
		uniform fixed _Texture_8_Normal_Power;
		uniform fixed _Texture_9_Normal_Index;
		uniform fixed _Texture_9_Triplanar;
		uniform fixed _Texture_9_Tiling;
		uniform fixed _Texture_9_Normal_Power;
		uniform fixed _Texture_10_Normal_Index;
		uniform fixed _Texture_10_Triplanar;
		uniform fixed _Texture_10_Tiling;
		uniform fixed _Texture_10_Normal_Power;
		uniform fixed _Texture_11_Normal_Index;
		uniform fixed _Texture_11_Triplanar;
		uniform fixed _Texture_11_Tiling;
		uniform fixed _Texture_11_Normal_Power;
		uniform fixed _Texture_12_Normal_Index;
		uniform fixed _Texture_12_Triplanar;
		uniform fixed _Texture_12_Tiling;
		uniform fixed _Texture_12_Normal_Power;
		uniform fixed _Texture_13_Normal_Index;
		uniform fixed _Texture_13_Triplanar;
		uniform fixed _Texture_13_Tiling;
		uniform fixed _Texture_13_Normal_Powerr;
		uniform fixed _Texture_14_Normal_Index;
		uniform fixed _Texture_14_Triplanar;
		uniform fixed _Texture_14_Tiling;
		uniform fixed _Texture_14_Normal_Power;
		uniform fixed _Texture_15_Normal_Index;
		uniform fixed _Texture_15_Triplanar;
		uniform fixed _Texture_15_Tiling;
		uniform fixed _Texture_15_Normal_Power;
		uniform fixed _Texture_16_Normal_Index;
		uniform fixed _Texture_16_Triplanar;
		uniform fixed _Texture_16_Tiling;
		uniform fixed _Texture_16_Normal_Power;
		uniform fixed _Texture_Snow_Normal_Index;
		uniform fixed _Snow_Tiling;
		uniform fixed _Snow_Normal_Scale;
		uniform fixed _Snow_Blend_Normal;
		uniform fixed _Snow_Amount;
		uniform fixed _Texture_Snow_Noise_Index;
		uniform fixed _Snow_Noise_Tiling;
		uniform fixed _Snow_Noise_Power;
		uniform fixed _Snow_Maximum_Angle;
		uniform fixed _Snow_Maximum_Angle_Hardness;
		uniform fixed _Snow_Min_Height;
		uniform fixed _Snow_Min_Height_Blending;
		uniform fixed _Texture_16_Snow_Reduction;
		uniform fixed _Texture_15_Snow_Reduction;
		uniform fixed _Texture_13_Snow_Reduction;
		uniform fixed _Texture_12_Snow_Reduction;
		uniform fixed _Texture_11_Snow_Reduction;
		uniform fixed _Texture_9_Snow_Reduction;
		uniform fixed _Texture_8_Snow_Reduction;
		uniform fixed _Texture_7_Snow_Reduction;
		uniform fixed _Texture_5_Snow_Reduction;
		uniform fixed _Texture_1_Snow_Reduction;
		uniform fixed _Texture_2_Snow_Reduction;
		uniform fixed _Texture_3_Snow_Reduction;
		uniform fixed _Texture_4_Snow_Reduction;
		uniform fixed _Texture_6_Snow_Reduction;
		uniform fixed _Texture_10_Snow_Reduction;
		uniform fixed _Texture_14_Snow_Reduction;
		uniform fixed _Global_Normalmap_Power;
		uniform sampler2D _Global_Normal_Map;
		uniform fixed _Texture_1_Albedo_Index;
		uniform fixed _Texture_1_Far_Multiplier;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Albedo );
		uniform fixed4 _Texture_1_Color;
		uniform fixed _Texture_2_Albedo_Index;
		uniform fixed _Texture_2_Far_Multiplier;
		uniform fixed4 _Texture_2_Color;
		uniform fixed _Texture_3_Albedo_Index;
		uniform fixed4 _Texture_3_Color;
		uniform fixed _Texture_4_Albedo_Index;
		uniform fixed _Texture_4_Far_Multiplier;
		uniform fixed4 _Texture_4_Color;
		uniform fixed _Texture_5_Albedo_Index;
		uniform fixed _Texture_5_Far_Multiplier;
		uniform fixed4 _Texture_5_Color;
		uniform fixed _Texture_6_Albedo_Index;
		uniform fixed _Texture_6_Far_Multiplier;
		uniform fixed4 _Texture_6_Color;
		uniform fixed _Texture_7_Albedo_Index;
		uniform fixed _Texture_7_Far_Multiplier;
		uniform fixed4 _Texture_7_Color;
		uniform fixed _Texture_8_Albedo_Index;
		uniform fixed _Texture_8_Far_Multiplier;
		uniform fixed4 _Texture_8_Color;
		uniform fixed _Texture_9_Albedo_Index;
		uniform fixed _Texture_9_Far_Multiplier;
		uniform fixed4 _Texture_9_Color;
		uniform fixed _Texture_10_Albedo_Index;
		uniform fixed _Texture_10_Far_Multiplier;
		uniform fixed4 _Texture_10_Color;
		uniform fixed _Texture_11_Albedo_Index;
		uniform fixed _Texture_11_Far_Multiplier;
		uniform fixed4 _Texture_11_Color;
		uniform fixed _Texture_12_Albedo_Index;
		uniform fixed _Texture_12_Far_Multiplier;
		uniform fixed4 _Texture_12_Color;
		uniform fixed _Texture_13_Albedo_Index;
		uniform fixed _Texture_13_Far_Multiplier;
		uniform fixed4 _Texture_13_Color;
		uniform fixed _Texture_14_Albedo_Index;
		uniform fixed _Texture_14_Far_Multiplier;
		uniform fixed4 _Texture_14_Color;
		uniform fixed _Texture_15_Albedo_Index;
		uniform fixed _Texture_15_Far_Multiplier;
		uniform fixed4 _Texture_15_Color;
		uniform fixed _Texture_16_Albedo_Index;
		uniform fixed _Texture_16_Far_Multiplier;
		uniform fixed4 _Texture_16_Color;
		uniform fixed _Geological_Map_Close_Power;
		uniform sampler2D _Texture_Geological_Map;
		uniform fixed _Geological_Map_Offset_Close;
		uniform fixed _Geological_Tiling_Close;
		uniform fixed _Geological_Map_Far_Power;
		uniform fixed _Geological_Tiling_Far;
		uniform fixed _Geological_Map_Offset_Far;
		uniform fixed _Texture_16_Geological_Power;
		uniform fixed _Texture_15_Geological_Power;
		uniform fixed _Texture_14_Geological_Power;
		uniform fixed _Texture_13_Geological_Power;
		uniform fixed _Texture_12_Geological_Power;
		uniform fixed _Texture_11_Geological_Power;
		uniform fixed _Texture_10_Geological_Power;
		uniform fixed _Texture_9_Geological_Power;
		uniform fixed _Texture_8_Geological_Power;
		uniform fixed _Texture_7_Geological_Power;
		uniform fixed _Texture_6_Geological_Power;
		uniform fixed _Texture_5_Geological_Power;
		uniform fixed _Texture_1_Geological_Power;
		uniform fixed _Texture_2_Geological_Power;
		uniform fixed _Texture_4_Geological_Power;
		uniform fixed _Texture_3_Geological_Power;
		uniform fixed _Texture_Snow_Index;
		uniform fixed _Snow_Tiling_Far_Multiplier;
		uniform fixed4 _Snow_Color;
		uniform fixed _Terrain_Specular;
		uniform fixed _Snow_Specular;
		uniform fixed _Terrain_Smoothness;
		uniform fixed _Ambient_Occlusion_Power;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float3 normalWorld = UnityObjectToWorldNormal( v.normal );
			v.vertex.xyz += 0.0;
			 v.tangent.xyz=cross( normalWorld , fixed3(0,0,1) );
			 v.tangent.w = -1;//;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult3897 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_0 = (( 1.0 / _Perlin_Normal_Tiling_Close )).xx;
			float4 texArray5480 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_0 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g273 = float2( texArray5480.x , texArray5480.y );
			float2 temp_cast_2 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_output_4_0_g273 = ( ( ( appendResult11_g273 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_2 );
			float2 temp_cast_3 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_cast_4 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_cast_5 = (_Perlin_Normal_Power_Close).xx;
			float temp_output_9_0_g273 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g273 , temp_output_4_0_g273 ) ) ) );
			float3 appendResult10_g273 = float3( temp_output_4_0_g273.x , temp_output_4_0_g273.y , temp_output_9_0_g273 );
			fixed2 temp_cast_6 = (( 1.0 / _Perlin_Normal_Tiling_Far )).xx;
			float4 texArray4374 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_6 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g272 = float2( texArray4374.x , texArray4374.y );
			float2 temp_cast_8 = (_Perlin_Normal_Power).xx;
			float2 temp_output_4_0_g272 = ( ( ( appendResult11_g272 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_8 );
			float2 temp_cast_9 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_10 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_11 = (_Perlin_Normal_Power).xx;
			float temp_output_9_0_g272 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g272 , temp_output_4_0_g272 ) ) ) );
			float3 appendResult10_g272 = float3( temp_output_4_0_g272.x , temp_output_4_0_g272.y , temp_output_9_0_g272 );
			fixed UVmixDistance = clamp( pow( ( distance( ase_worldPos , _WorldSpaceCameraPos ) / _UV_Mix_Start_Distance ) , _UV_Mix_Power ) , 0.0 , 1.0 );
			fixed4 tex2DNode4371 = tex2D( _Texture_Splat_4, i.texcoord_0 );
			fixed Splat4_A = tex2DNode4371.a;
			fixed Splat4_B = tex2DNode4371.b;
			fixed Splat4_G = tex2DNode4371.g;
			fixed Splat4_R = tex2DNode4371.r;
			fixed4 tex2DNode4370 = tex2D( _Texture_Splat_3, i.texcoord_0 );
			fixed Splat3_A = tex2DNode4370.a;
			fixed Splat3_B = tex2DNode4370.b;
			fixed Splat3_G = tex2DNode4370.g;
			fixed Splat3_R = tex2DNode4370.r;
			fixed4 tex2DNode4369 = tex2D( _Texture_Splat_2, i.texcoord_0 );
			fixed Splat2_A = tex2DNode4369.a;
			fixed Splat2_B = tex2DNode4369.b;
			fixed Splat2_G = tex2DNode4369.g;
			fixed Splat2_R = tex2DNode4369.r;
			fixed4 tex2DNode4368 = tex2D( _Texture_Splat_1, i.texcoord_0 );
			fixed Splat1_R = tex2DNode4368.r;
			fixed Splat1_G = tex2DNode4368.g;
			fixed Splat1_A = tex2DNode4368.a;
			fixed Splat1_B = tex2DNode4368.b;
			float2 appendResult1998 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 Top_Bottom = appendResult1998;
			float2 appendResult3284 = float2( ( 1.0 / _Texture_1_Tiling ) , ( 1.0 / _Texture_1_Tiling ) );
			float4 texArray3300 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Normal_Index)  );
			float2 appendResult11_g232 = float2( texArray3300.x , texArray3300.y );
			float2 temp_cast_12 = (_Texture_1_Normal_Power).xx;
			float2 temp_output_4_0_g232 = ( ( ( appendResult11_g232 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_12 );
			float2 temp_cast_13 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_14 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_15 = (_Texture_1_Normal_Power).xx;
			float temp_output_9_0_g232 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g232 , temp_output_4_0_g232 ) ) ) );
			float3 appendResult10_g232 = float3( temp_output_4_0_g232.x , temp_output_4_0_g232.y , temp_output_9_0_g232 );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			fixed3 BlendComponents = clamp( pow( ( ase_worldNormal * ase_worldNormal ) , 25.0 ) , float3( -1,-1,-1 ) , float3( 1,1,1 ) );
			float2 appendResult879 = float2( ase_worldPos.z , ase_worldPos.y );
			fixed2 Front_Back = appendResult879;
			float4 texArray3299 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3284 ), _Texture_1_Normal_Index)  );
			float2 appendResult11_g137 = float2( texArray3299.x , texArray3299.y );
			float2 temp_cast_16 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g137 = ( ( ( appendResult11_g137 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_16 );
			float2 temp_cast_17 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_cast_18 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_cast_19 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g137 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g137 , temp_output_4_0_g137 ) ) ) );
			float3 appendResult19_g137 = float3( temp_output_4_0_g137.y , temp_output_4_0_g137.x , temp_output_9_0_g137 );
			float3 appendResult5874 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult2002 = float2( ase_worldPos.x , ase_worldPos.y );
			fixed2 Left_Right = appendResult2002;
			float4 texArray3301 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3284 ), _Texture_1_Normal_Index)  );
			float2 appendResult11_g129 = float2( texArray3301.x , texArray3301.y );
			float2 temp_cast_20 = (_Texture_1_Normal_Power).xx;
			float2 temp_output_4_0_g129 = ( ( ( appendResult11_g129 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_20 );
			float2 temp_cast_21 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_22 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_23 = (_Texture_1_Normal_Power).xx;
			float temp_output_9_0_g129 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g129 , temp_output_4_0_g129 ) ) ) );
			float3 appendResult10_g129 = float3( temp_output_4_0_g129.x , temp_output_4_0_g129.y , temp_output_9_0_g129 );
			float3 appendResult5876 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5676 = BlendComponents;
			float3 weightedAvg5676 = ( ( weightedBlendVar5676.x*( appendResult19_g137 * appendResult5874 ) + weightedBlendVar5676.y*appendResult10_g232 + weightedBlendVar5676.z*( appendResult10_g129 * appendResult5876 ) )/( weightedBlendVar5676.x + weightedBlendVar5676.y + weightedBlendVar5676.z ) );
			fixed3 ifLocalVar5783 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar5783 = appendResult10_g232;
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar5783 = weightedAvg5676;
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar5783 = appendResult10_g232;
			fixed3 ifLocalVar6127 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar6127 = ifLocalVar5783;
			fixed3 Normal_1 = ifLocalVar6127;
			float2 appendResult3349 = float2( ( 1.0 / _Texture_2_Tiling ) , ( 1.0 / _Texture_2_Tiling ) );
			float4 texArray3350 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Normal_Index)  );
			float2 appendResult11_g227 = float2( texArray3350.x , texArray3350.y );
			float2 temp_cast_24 = (_Texture_2_Normal_Power).xx;
			float2 temp_output_4_0_g227 = ( ( ( appendResult11_g227 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_24 );
			float2 temp_cast_25 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_26 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_27 = (_Texture_2_Normal_Power).xx;
			float temp_output_9_0_g227 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g227 , temp_output_4_0_g227 ) ) ) );
			float3 appendResult10_g227 = float3( temp_output_4_0_g227.x , temp_output_4_0_g227.y , temp_output_9_0_g227 );
			float4 texArray3384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3349 ), _Texture_2_Normal_Index)  );
			float2 appendResult11_g135 = float2( texArray3384.x , texArray3384.y );
			float2 temp_cast_28 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g135 = ( ( ( appendResult11_g135 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_28 );
			float2 temp_cast_29 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_cast_30 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_cast_31 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g135 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g135 , temp_output_4_0_g135 ) ) ) );
			float3 appendResult19_g135 = float3( temp_output_4_0_g135.y , temp_output_4_0_g135.x , temp_output_9_0_g135 );
			float3 appendResult5879 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray3351 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3349 ), _Texture_2_Normal_Index)  );
			float2 appendResult11_g133 = float2( texArray3351.x , texArray3351.y );
			float2 temp_cast_32 = (_Texture_2_Normal_Power).xx;
			float2 temp_output_4_0_g133 = ( ( ( appendResult11_g133 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_32 );
			float2 temp_cast_33 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_34 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_35 = (_Texture_2_Normal_Power).xx;
			float temp_output_9_0_g133 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g133 , temp_output_4_0_g133 ) ) ) );
			float3 appendResult10_g133 = float3( temp_output_4_0_g133.x , temp_output_4_0_g133.y , temp_output_9_0_g133 );
			float3 appendResult5882 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5499 = BlendComponents;
			float3 weightedAvg5499 = ( ( weightedBlendVar5499.x*( appendResult19_g135 * appendResult5879 ) + weightedBlendVar5499.y*appendResult10_g227 + weightedBlendVar5499.z*( appendResult10_g133 * appendResult5882 ) )/( weightedBlendVar5499.x + weightedBlendVar5499.y + weightedBlendVar5499.z ) );
			fixed3 ifLocalVar5774 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar5774 = appendResult10_g227;
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar5774 = weightedAvg5499;
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar5774 = appendResult10_g227;
			fixed3 ifLocalVar6129 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar6129 = ifLocalVar5774;
			fixed3 Normal_2 = ifLocalVar6129;
			float2 appendResult3415 = float2( ( 1.0 / _Texture_3_Tiling ) , ( 1.0 / _Texture_3_Tiling ) );
			float4 texArray3416 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Normal_Index)  );
			float2 appendResult11_g224 = float2( texArray3416.x , texArray3416.y );
			float2 temp_cast_36 = (_Texture_3_Normal_Power).xx;
			float2 temp_output_4_0_g224 = ( ( ( appendResult11_g224 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_36 );
			float2 temp_cast_37 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_38 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_39 = (_Texture_3_Normal_Power).xx;
			float temp_output_9_0_g224 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g224 , temp_output_4_0_g224 ) ) ) );
			float3 appendResult10_g224 = float3( temp_output_4_0_g224.x , temp_output_4_0_g224.y , temp_output_9_0_g224 );
			fixed2 temp_cast_40 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3415 ) / temp_cast_40 ), _Texture_3_Normal_Index)  );
			float2 appendResult11_g134 = float2( texArray3445.x , texArray3445.y );
			float2 temp_cast_41 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g134 = ( ( ( appendResult11_g134 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_41 );
			float2 temp_cast_42 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_cast_43 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_cast_44 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g134 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g134 , temp_output_4_0_g134 ) ) ) );
			float3 appendResult19_g134 = float3( temp_output_4_0_g134.y , temp_output_4_0_g134.x , temp_output_9_0_g134 );
			float3 appendResult5886 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray3417 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3415 ), _Texture_3_Normal_Index)  );
			float2 appendResult11_g130 = float2( texArray3417.x , texArray3417.y );
			float2 temp_cast_45 = (_Texture_3_Normal_Power).xx;
			float2 temp_output_4_0_g130 = ( ( ( appendResult11_g130 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_45 );
			float2 temp_cast_46 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_47 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_48 = (_Texture_3_Normal_Power).xx;
			float temp_output_9_0_g130 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g130 , temp_output_4_0_g130 ) ) ) );
			float3 appendResult10_g130 = float3( temp_output_4_0_g130.x , temp_output_4_0_g130.y , temp_output_9_0_g130 );
			float3 appendResult5889 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5503 = BlendComponents;
			float3 weightedAvg5503 = ( ( weightedBlendVar5503.x*( appendResult19_g134 * appendResult5886 ) + weightedBlendVar5503.y*appendResult10_g224 + weightedBlendVar5503.z*( appendResult10_g130 * appendResult5889 ) )/( weightedBlendVar5503.x + weightedBlendVar5503.y + weightedBlendVar5503.z ) );
			fixed3 ifLocalVar5776 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar5776 = appendResult10_g224;
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar5776 = weightedAvg5503;
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar5776 = appendResult10_g224;
			fixed3 ifLocalVar6131 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Index > -1.0 )
				ifLocalVar6131 = ifLocalVar5776;
			fixed3 Normal_3 = ifLocalVar6131;
			fixed2 temp_cast_49 = (( 1.0 / _Texture_4_Tiling )).xx;
			float4 texArray3483 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * temp_cast_49 ), _Texture_4_Normal_Index)  );
			float2 appendResult11_g225 = float2( texArray3483.x , texArray3483.y );
			float2 temp_cast_50 = (_Texture_4_Normal_Power).xx;
			float2 temp_output_4_0_g225 = ( ( ( appendResult11_g225 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_50 );
			float2 temp_cast_51 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_52 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_53 = (_Texture_4_Normal_Power).xx;
			float temp_output_9_0_g225 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g225 , temp_output_4_0_g225 ) ) ) );
			float3 appendResult10_g225 = float3( temp_output_4_0_g225.x , temp_output_4_0_g225.y , temp_output_9_0_g225 );
			fixed2 temp_cast_54 = (( 1.0 / _Texture_4_Tiling )).xx;
			float4 texArray3512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * temp_cast_54 ), _Texture_4_Normal_Index)  );
			float2 appendResult11_g132 = float2( texArray3512.x , texArray3512.y );
			float2 temp_cast_55 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g132 = ( ( ( appendResult11_g132 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_55 );
			float2 temp_cast_56 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_cast_57 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_cast_58 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g132 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g132 , temp_output_4_0_g132 ) ) ) );
			float3 appendResult19_g132 = float3( temp_output_4_0_g132.y , temp_output_4_0_g132.x , temp_output_9_0_g132 );
			float3 appendResult5893 = float3( ase_worldNormal.x , -1 , 1 );
			fixed2 temp_cast_59 = (( 1.0 / _Texture_4_Tiling )).xx;
			float4 texArray3484 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * temp_cast_59 ), _Texture_4_Normal_Index)  );
			float2 appendResult11_g136 = float2( texArray3484.x , texArray3484.y );
			float2 temp_cast_60 = (_Texture_4_Normal_Power).xx;
			float2 temp_output_4_0_g136 = ( ( ( appendResult11_g136 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_60 );
			float2 temp_cast_61 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_62 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_63 = (_Texture_4_Normal_Power).xx;
			float temp_output_9_0_g136 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g136 , temp_output_4_0_g136 ) ) ) );
			float3 appendResult10_g136 = float3( temp_output_4_0_g136.x , temp_output_4_0_g136.y , temp_output_9_0_g136 );
			float3 appendResult5896 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5507 = BlendComponents;
			float3 weightedAvg5507 = ( ( weightedBlendVar5507.x*( appendResult19_g132 * appendResult5893 ) + weightedBlendVar5507.y*appendResult10_g225 + weightedBlendVar5507.z*( appendResult10_g136 * appendResult5896 ) )/( weightedBlendVar5507.x + weightedBlendVar5507.y + weightedBlendVar5507.z ) );
			fixed3 ifLocalVar5999 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar5999 = appendResult10_g225;
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar5999 = weightedAvg5507;
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar5999 = appendResult10_g225;
			fixed3 ifLocalVar6133 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Index > -1.0 )
				ifLocalVar6133 = ifLocalVar5999;
			fixed3 Normal_4 = ifLocalVar6133;
			float4 layeredBlendVar5639 = tex2DNode4368;
			float3 layeredBlend5639 = ( lerp( lerp( lerp( lerp( float3( 0,0,1 ) , Normal_1 , layeredBlendVar5639.x ) , Normal_2 , layeredBlendVar5639.y ) , Normal_3 , layeredBlendVar5639.z ) , Normal_4 , layeredBlendVar5639.w ) );
			float2 appendResult4399 = float2( ( 1.0 / _Texture_5_Tiling ) , ( 1.0 / _Texture_5_Tiling ) );
			float4 texArray4424 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Normal_Index)  );
			float2 appendResult11_g250 = float2( texArray4424.x , texArray4424.y );
			float2 temp_cast_64 = (_Texture_5_Normal_Power).xx;
			float2 temp_output_4_0_g250 = ( ( ( appendResult11_g250 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_64 );
			float2 temp_cast_65 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_66 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_67 = (_Texture_5_Normal_Power).xx;
			float temp_output_9_0_g250 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g250 , temp_output_4_0_g250 ) ) ) );
			float3 appendResult10_g250 = float3( temp_output_4_0_g250.x , temp_output_4_0_g250.y , temp_output_9_0_g250 );
			float4 texArray4417 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4399 ), _Texture_5_Normal_Index)  );
			float2 appendResult11_g223 = float2( texArray4417.x , texArray4417.y );
			float2 temp_cast_68 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g223 = ( ( ( appendResult11_g223 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_68 );
			float2 temp_cast_69 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_cast_70 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_cast_71 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g223 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g223 , temp_output_4_0_g223 ) ) ) );
			float3 appendResult19_g223 = float3( temp_output_4_0_g223.y , temp_output_4_0_g223.x , temp_output_9_0_g223 );
			float3 appendResult5900 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4422 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4399 ), _Texture_5_Normal_Index)  );
			float2 appendResult11_g221 = float2( texArray4422.x , texArray4422.y );
			float2 temp_cast_72 = (_Texture_5_Normal_Power).xx;
			float2 temp_output_4_0_g221 = ( ( ( appendResult11_g221 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_72 );
			float2 temp_cast_73 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_74 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_75 = (_Texture_5_Normal_Power).xx;
			float temp_output_9_0_g221 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g221 , temp_output_4_0_g221 ) ) ) );
			float3 appendResult10_g221 = float3( temp_output_4_0_g221.x , temp_output_4_0_g221.y , temp_output_9_0_g221 );
			float3 appendResult5903 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5511 = BlendComponents;
			float3 weightedAvg5511 = ( ( weightedBlendVar5511.x*( appendResult19_g223 * appendResult5900 ) + weightedBlendVar5511.y*appendResult10_g250 + weightedBlendVar5511.z*( appendResult10_g221 * appendResult5903 ) )/( weightedBlendVar5511.x + weightedBlendVar5511.y + weightedBlendVar5511.z ) );
			fixed3 ifLocalVar5806 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar5806 = appendResult10_g250;
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar5806 = weightedAvg5511;
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar5806 = appendResult10_g250;
			fixed3 ifLocalVar6135 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Index > -1.0 )
				ifLocalVar6135 = ifLocalVar5806;
			fixed3 Normal_5 = ifLocalVar6135;
			float2 appendResult4471 = float2( ( 1.0 / _Texture_6_Tiling ) , ( 1.0 / _Texture_6_Tiling ) );
			float4 texArray4493 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Normal_Index)  );
			float2 appendResult11_g254 = float2( texArray4493.x , texArray4493.y );
			float2 temp_cast_76 = (_Texture_6_Normal_Power).xx;
			float2 temp_output_4_0_g254 = ( ( ( appendResult11_g254 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_76 );
			float2 temp_cast_77 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_78 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_79 = (_Texture_6_Normal_Power).xx;
			float temp_output_9_0_g254 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g254 , temp_output_4_0_g254 ) ) ) );
			float3 appendResult10_g254 = float3( temp_output_4_0_g254.x , temp_output_4_0_g254.y , temp_output_9_0_g254 );
			float4 texArray4486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4471 ), _Texture_6_Normal_Index)  );
			float2 appendResult11_g230 = float2( texArray4486.x , texArray4486.y );
			float2 temp_cast_80 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g230 = ( ( ( appendResult11_g230 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_80 );
			float2 temp_cast_81 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_cast_82 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_cast_83 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g230 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g230 , temp_output_4_0_g230 ) ) ) );
			float3 appendResult19_g230 = float3( temp_output_4_0_g230.y , temp_output_4_0_g230.x , temp_output_9_0_g230 );
			float3 appendResult5907 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4491 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4471 ), _Texture_6_Normal_Index)  );
			float2 appendResult11_g222 = float2( texArray4491.x , texArray4491.y );
			float2 temp_cast_84 = (_Texture_6_Normal_Power).xx;
			float2 temp_output_4_0_g222 = ( ( ( appendResult11_g222 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_84 );
			float2 temp_cast_85 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_86 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_87 = (_Texture_6_Normal_Power).xx;
			float temp_output_9_0_g222 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g222 , temp_output_4_0_g222 ) ) ) );
			float3 appendResult10_g222 = float3( temp_output_4_0_g222.x , temp_output_4_0_g222.y , temp_output_9_0_g222 );
			float3 appendResult5910 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5519 = BlendComponents;
			float3 weightedAvg5519 = ( ( weightedBlendVar5519.x*( appendResult19_g230 * appendResult5907 ) + weightedBlendVar5519.y*appendResult10_g254 + weightedBlendVar5519.z*( appendResult10_g222 * appendResult5910 ) )/( weightedBlendVar5519.x + weightedBlendVar5519.y + weightedBlendVar5519.z ) );
			fixed3 ifLocalVar5807 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar5807 = appendResult10_g254;
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar5807 = weightedAvg5519;
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar5807 = appendResult10_g254;
			fixed3 ifLocalVar6138 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Index > -1.0 )
				ifLocalVar6138 = ifLocalVar5807;
			fixed3 Normal_6 = ifLocalVar6138;
			float2 appendResult4545 = float2( ( 1.0 / _Texture_7_Tiling ) , ( 1.0 / _Texture_7_Tiling ) );
			float4 texArray4567 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Normal_Index)  );
			float2 appendResult11_g251 = float2( texArray4567.x , texArray4567.y );
			float2 temp_cast_88 = (_Texture_7_Normal_Power).xx;
			float2 temp_output_4_0_g251 = ( ( ( appendResult11_g251 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_88 );
			float2 temp_cast_89 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_90 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_91 = (_Texture_7_Normal_Power).xx;
			float temp_output_9_0_g251 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g251 , temp_output_4_0_g251 ) ) ) );
			float3 appendResult10_g251 = float3( temp_output_4_0_g251.x , temp_output_4_0_g251.y , temp_output_9_0_g251 );
			float4 texArray4560 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4545 ), _Texture_7_Normal_Index)  );
			float2 appendResult11_g226 = float2( texArray4560.x , texArray4560.y );
			float2 temp_cast_92 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g226 = ( ( ( appendResult11_g226 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_92 );
			float2 temp_cast_93 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_cast_94 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_cast_95 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g226 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g226 , temp_output_4_0_g226 ) ) ) );
			float3 appendResult19_g226 = float3( temp_output_4_0_g226.y , temp_output_4_0_g226.x , temp_output_9_0_g226 );
			float3 appendResult5914 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4565 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4545 ), _Texture_7_Normal_Index)  );
			float2 appendResult11_g228 = float2( texArray4565.x , texArray4565.y );
			float2 temp_cast_96 = (_Texture_7_Normal_Power).xx;
			float2 temp_output_4_0_g228 = ( ( ( appendResult11_g228 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_96 );
			float2 temp_cast_97 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_98 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_99 = (_Texture_7_Normal_Power).xx;
			float temp_output_9_0_g228 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g228 , temp_output_4_0_g228 ) ) ) );
			float3 appendResult10_g228 = float3( temp_output_4_0_g228.x , temp_output_4_0_g228.y , temp_output_9_0_g228 );
			float3 appendResult5917 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5523 = BlendComponents;
			float3 weightedAvg5523 = ( ( weightedBlendVar5523.x*( appendResult19_g226 * appendResult5914 ) + weightedBlendVar5523.y*appendResult10_g251 + weightedBlendVar5523.z*( appendResult10_g228 * appendResult5917 ) )/( weightedBlendVar5523.x + weightedBlendVar5523.y + weightedBlendVar5523.z ) );
			fixed3 ifLocalVar5853 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar5853 = appendResult10_g251;
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar5853 = weightedAvg5523;
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar5853 = appendResult10_g251;
			fixed3 ifLocalVar6140 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Index > -1.0 )
				ifLocalVar6140 = ifLocalVar5853;
			fixed3 Normal_7 = ifLocalVar6140;
			float2 appendResult4619 = float2( ( 1.0 / _Texture_8_Tiling ) , ( 1.0 / _Texture_8_Tiling ) );
			float4 texArray4641 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Normal_Index)  );
			float2 appendResult11_g243 = float2( texArray4641.x , texArray4641.y );
			float2 temp_cast_100 = (_Texture_8_Normal_Power).xx;
			float2 temp_output_4_0_g243 = ( ( ( appendResult11_g243 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_100 );
			float2 temp_cast_101 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_102 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_103 = (_Texture_8_Normal_Power).xx;
			float temp_output_9_0_g243 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g243 , temp_output_4_0_g243 ) ) ) );
			float3 appendResult10_g243 = float3( temp_output_4_0_g243.x , temp_output_4_0_g243.y , temp_output_9_0_g243 );
			float4 texArray4634 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4619 ), _Texture_8_Normal_Index)  );
			float2 appendResult11_g231 = float2( texArray4634.x , texArray4634.y );
			float2 temp_cast_104 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g231 = ( ( ( appendResult11_g231 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_104 );
			float2 temp_cast_105 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_cast_106 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_cast_107 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g231 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g231 , temp_output_4_0_g231 ) ) ) );
			float3 appendResult19_g231 = float3( temp_output_4_0_g231.y , temp_output_4_0_g231.x , temp_output_9_0_g231 );
			float3 appendResult5921 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4639 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4619 ), _Texture_8_Normal_Index)  );
			float2 appendResult11_g229 = float2( texArray4639.x , texArray4639.y );
			float2 temp_cast_108 = (_Texture_8_Normal_Power).xx;
			float2 temp_output_4_0_g229 = ( ( ( appendResult11_g229 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_108 );
			float2 temp_cast_109 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_110 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_111 = (_Texture_8_Normal_Power).xx;
			float temp_output_9_0_g229 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g229 , temp_output_4_0_g229 ) ) ) );
			float3 appendResult10_g229 = float3( temp_output_4_0_g229.x , temp_output_4_0_g229.y , temp_output_9_0_g229 );
			float3 appendResult5924 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5527 = BlendComponents;
			float3 weightedAvg5527 = ( ( weightedBlendVar5527.x*( appendResult19_g231 * appendResult5921 ) + weightedBlendVar5527.y*appendResult10_g243 + weightedBlendVar5527.z*( appendResult10_g229 * appendResult5924 ) )/( weightedBlendVar5527.x + weightedBlendVar5527.y + weightedBlendVar5527.z ) );
			fixed3 ifLocalVar5813 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar5813 = appendResult10_g243;
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar5813 = weightedAvg5527;
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar5813 = appendResult10_g243;
			fixed3 ifLocalVar6142 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Index > -1.0 )
				ifLocalVar6142 = ifLocalVar5813;
			fixed3 Normal_8 = ifLocalVar6142;
			float4 layeredBlendVar5640 = tex2DNode4369;
			float3 layeredBlend5640 = ( lerp( lerp( lerp( lerp( layeredBlend5639 , Normal_5 , layeredBlendVar5640.x ) , Normal_6 , layeredBlendVar5640.y ) , Normal_7 , layeredBlendVar5640.z ) , Normal_8 , layeredBlendVar5640.w ) );
			float2 appendResult4736 = float2( ( 1.0 / _Texture_9_Tiling ) , ( 1.0 / _Texture_9_Tiling ) );
			float4 texArray4788 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Normal_Index)  );
			float2 appendResult11_g266 = float2( texArray4788.x , texArray4788.y );
			float2 temp_cast_112 = (_Texture_9_Normal_Power).xx;
			float2 temp_output_4_0_g266 = ( ( ( appendResult11_g266 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_112 );
			float2 temp_cast_113 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_114 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_115 = (_Texture_9_Normal_Power).xx;
			float temp_output_9_0_g266 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g266 , temp_output_4_0_g266 ) ) ) );
			float3 appendResult10_g266 = float3( temp_output_4_0_g266.x , temp_output_4_0_g266.y , temp_output_9_0_g266 );
			float4 texArray5285 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4736 ), _Texture_9_Normal_Index)  );
			float2 appendResult11_g248 = float2( texArray5285.x , texArray5285.y );
			float2 temp_cast_116 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g248 = ( ( ( appendResult11_g248 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_116 );
			float2 temp_cast_117 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_cast_118 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_cast_119 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g248 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g248 , temp_output_4_0_g248 ) ) ) );
			float3 appendResult19_g248 = float3( temp_output_4_0_g248.y , temp_output_4_0_g248.x , temp_output_9_0_g248 );
			float3 appendResult5977 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4783 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4736 ), _Texture_9_Normal_Index)  );
			float2 appendResult11_g252 = float2( texArray4783.x , texArray4783.y );
			float2 temp_cast_120 = (_Texture_9_Normal_Power).xx;
			float2 temp_output_4_0_g252 = ( ( ( appendResult11_g252 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_120 );
			float2 temp_cast_121 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_122 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_123 = (_Texture_9_Normal_Power).xx;
			float temp_output_9_0_g252 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g252 , temp_output_4_0_g252 ) ) ) );
			float3 appendResult10_g252 = float3( temp_output_4_0_g252.x , temp_output_4_0_g252.y , temp_output_9_0_g252 );
			float3 appendResult5980 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5491 = BlendComponents;
			float3 weightedAvg5491 = ( ( weightedBlendVar5491.x*( appendResult19_g248 * appendResult5977 ) + weightedBlendVar5491.y*appendResult10_g266 + weightedBlendVar5491.z*( appendResult10_g252 * appendResult5980 ) )/( weightedBlendVar5491.x + weightedBlendVar5491.y + weightedBlendVar5491.z ) );
			fixed3 ifLocalVar5846 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar5846 = appendResult10_g266;
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar5846 = weightedAvg5491;
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar5846 = appendResult10_g266;
			fixed3 ifLocalVar6144 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar6144 = ifLocalVar5846;
			fixed3 Normal_9 = ifLocalVar6144;
			float2 appendResult4738 = float2( ( 1.0 / _Texture_10_Tiling ) , ( 1.0 / _Texture_10_Tiling ) );
			float4 texArray4822 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Normal_Index)  );
			float2 appendResult11_g260 = float2( texArray4822.x , texArray4822.y );
			float2 temp_cast_124 = (_Texture_10_Normal_Power).xx;
			float2 temp_output_4_0_g260 = ( ( ( appendResult11_g260 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_124 );
			float2 temp_cast_125 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_126 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_127 = (_Texture_10_Normal_Power).xx;
			float temp_output_9_0_g260 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g260 , temp_output_4_0_g260 ) ) ) );
			float3 appendResult10_g260 = float3( temp_output_4_0_g260.x , temp_output_4_0_g260.y , temp_output_9_0_g260 );
			float4 texArray4798 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4738 ), _Texture_10_Normal_Index)  );
			float2 appendResult11_g247 = float2( texArray4798.x , texArray4798.y );
			float2 temp_cast_128 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g247 = ( ( ( appendResult11_g247 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_128 );
			float2 temp_cast_129 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_cast_130 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_cast_131 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g247 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g247 , temp_output_4_0_g247 ) ) ) );
			float3 appendResult19_g247 = float3( temp_output_4_0_g247.y , temp_output_4_0_g247.x , temp_output_9_0_g247 );
			float3 appendResult5970 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4791 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4738 ), _Texture_10_Normal_Index)  );
			float2 appendResult11_g245 = float2( texArray4791.x , texArray4791.y );
			float2 temp_cast_132 = (_Texture_10_Normal_Power).xx;
			float2 temp_output_4_0_g245 = ( ( ( appendResult11_g245 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_132 );
			float2 temp_cast_133 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_134 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_135 = (_Texture_10_Normal_Power).xx;
			float temp_output_9_0_g245 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g245 , temp_output_4_0_g245 ) ) ) );
			float3 appendResult10_g245 = float3( temp_output_4_0_g245.x , temp_output_4_0_g245.y , temp_output_9_0_g245 );
			float3 appendResult5973 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5531 = BlendComponents;
			float3 weightedAvg5531 = ( ( weightedBlendVar5531.x*( appendResult19_g247 * appendResult5970 ) + weightedBlendVar5531.y*appendResult10_g260 + weightedBlendVar5531.z*( appendResult10_g245 * appendResult5973 ) )/( weightedBlendVar5531.x + weightedBlendVar5531.y + weightedBlendVar5531.z ) );
			fixed3 ifLocalVar5842 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar5842 = appendResult10_g260;
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar5842 = weightedAvg5531;
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar5842 = appendResult10_g260;
			fixed3 ifLocalVar6146 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar6146 = ifLocalVar5842;
			fixed3 Normal_10 = ifLocalVar6146;
			float2 appendResult4741 = float2( ( 1.0 / _Texture_11_Tiling ) , ( 1.0 / _Texture_11_Tiling ) );
			float4 texArray4856 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Normal_Index)  );
			float2 appendResult11_g262 = float2( texArray4856.x , texArray4856.y );
			float2 temp_cast_136 = (_Texture_11_Normal_Power).xx;
			float2 temp_output_4_0_g262 = ( ( ( appendResult11_g262 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_136 );
			float2 temp_cast_137 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_138 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_139 = (_Texture_11_Normal_Power).xx;
			float temp_output_9_0_g262 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g262 , temp_output_4_0_g262 ) ) ) );
			float3 appendResult10_g262 = float3( temp_output_4_0_g262.x , temp_output_4_0_g262.y , temp_output_9_0_g262 );
			float4 texArray4828 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4741 ), _Texture_11_Normal_Index)  );
			float2 appendResult11_g246 = float2( texArray4828.x , texArray4828.y );
			float2 temp_cast_140 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g246 = ( ( ( appendResult11_g246 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_140 );
			float2 temp_cast_141 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_cast_142 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_cast_143 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g246 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g246 , temp_output_4_0_g246 ) ) ) );
			float3 appendResult19_g246 = float3( temp_output_4_0_g246.y , temp_output_4_0_g246.x , temp_output_9_0_g246 );
			float3 appendResult5963 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4811 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4741 ), _Texture_11_Normal_Index)  );
			float2 appendResult11_g253 = float2( texArray4811.x , texArray4811.y );
			float2 temp_cast_144 = (_Texture_11_Normal_Power).xx;
			float2 temp_output_4_0_g253 = ( ( ( appendResult11_g253 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_144 );
			float2 temp_cast_145 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_146 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_147 = (_Texture_11_Normal_Power).xx;
			float temp_output_9_0_g253 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g253 , temp_output_4_0_g253 ) ) ) );
			float3 appendResult10_g253 = float3( temp_output_4_0_g253.x , temp_output_4_0_g253.y , temp_output_9_0_g253 );
			float3 appendResult5966 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5535 = BlendComponents;
			float3 weightedAvg5535 = ( ( weightedBlendVar5535.x*( appendResult19_g246 * appendResult5963 ) + weightedBlendVar5535.y*appendResult10_g262 + weightedBlendVar5535.z*( appendResult10_g253 * appendResult5966 ) )/( weightedBlendVar5535.x + weightedBlendVar5535.y + weightedBlendVar5535.z ) );
			fixed3 ifLocalVar5838 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar5838 = appendResult10_g262;
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar5838 = weightedAvg5535;
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar5838 = appendResult10_g262;
			fixed3 ifLocalVar6148 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Index > -1.0 )
				ifLocalVar6148 = ifLocalVar5838;
			fixed3 Normal_11 = ifLocalVar6148;
			float2 appendResult4751 = float2( ( 1.0 / _Texture_12_Tiling ) , ( 1.0 / _Texture_12_Tiling ) );
			float4 texArray4870 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Normal_Index)  );
			float2 appendResult11_g263 = float2( texArray4870.x , texArray4870.y );
			float2 temp_cast_148 = (_Texture_12_Normal_Power).xx;
			float2 temp_output_4_0_g263 = ( ( ( appendResult11_g263 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_148 );
			float2 temp_cast_149 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_150 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_151 = (_Texture_12_Normal_Power).xx;
			float temp_output_9_0_g263 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g263 , temp_output_4_0_g263 ) ) ) );
			float3 appendResult10_g263 = float3( temp_output_4_0_g263.x , temp_output_4_0_g263.y , temp_output_9_0_g263 );
			float4 texArray4850 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4751 ), _Texture_12_Normal_Index)  );
			float2 appendResult11_g249 = float2( texArray4850.x , texArray4850.y );
			float2 temp_cast_152 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g249 = ( ( ( appendResult11_g249 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_152 );
			float2 temp_cast_153 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_cast_154 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_cast_155 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g249 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g249 , temp_output_4_0_g249 ) ) ) );
			float3 appendResult19_g249 = float3( temp_output_4_0_g249.y , temp_output_4_0_g249.x , temp_output_9_0_g249 );
			float3 appendResult5956 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4852 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4751 ), _Texture_12_Normal_Index)  );
			float2 appendResult11_g244 = float2( texArray4852.x , texArray4852.y );
			float2 temp_cast_156 = (_Texture_12_Normal_Power).xx;
			float2 temp_output_4_0_g244 = ( ( ( appendResult11_g244 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_156 );
			float2 temp_cast_157 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_158 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_159 = (_Texture_12_Normal_Power).xx;
			float temp_output_9_0_g244 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g244 , temp_output_4_0_g244 ) ) ) );
			float3 appendResult10_g244 = float3( temp_output_4_0_g244.x , temp_output_4_0_g244.y , temp_output_9_0_g244 );
			float3 appendResult5959 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5539 = BlendComponents;
			float3 weightedAvg5539 = ( ( weightedBlendVar5539.x*( appendResult19_g249 * appendResult5956 ) + weightedBlendVar5539.y*appendResult10_g263 + weightedBlendVar5539.z*( appendResult10_g244 * appendResult5959 ) )/( weightedBlendVar5539.x + weightedBlendVar5539.y + weightedBlendVar5539.z ) );
			fixed3 ifLocalVar5834 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar5834 = appendResult10_g263;
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar5834 = weightedAvg5539;
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar5834 = appendResult10_g263;
			fixed3 ifLocalVar6150 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Index > -1.0 )
				ifLocalVar6150 = ifLocalVar5834;
			fixed3 Normal_12 = ifLocalVar6150;
			float4 layeredBlendVar5641 = tex2DNode4370;
			float3 layeredBlend5641 = ( lerp( lerp( lerp( lerp( layeredBlend5640 , Normal_9 , layeredBlendVar5641.x ) , Normal_10 , layeredBlendVar5641.y ) , Normal_11 , layeredBlendVar5641.z ) , Normal_12 , layeredBlendVar5641.w ) );
			float2 appendResult5027 = float2( ( 1.0 / _Texture_13_Tiling ) , ( 1.0 / _Texture_13_Tiling ) );
			float4 texArray5120 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Normal_Index)  );
			float2 appendResult11_g269 = float2( texArray5120.x , texArray5120.y );
			float2 temp_cast_160 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_output_4_0_g269 = ( ( ( appendResult11_g269 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_160 );
			float2 temp_cast_161 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_162 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_163 = (_Texture_13_Normal_Powerr).xx;
			float temp_output_9_0_g269 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g269 , temp_output_4_0_g269 ) ) ) );
			float3 appendResult10_g269 = float3( temp_output_4_0_g269.x , temp_output_4_0_g269.y , temp_output_9_0_g269 );
			float4 texArray5127 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5027 ), _Texture_13_Normal_Index)  );
			float2 appendResult11_g258 = float2( texArray5127.x , texArray5127.y );
			float2 temp_cast_164 = (( _Texture_13_Normal_Powerr * -1.0 )).xx;
			float2 temp_output_4_0_g258 = ( ( ( appendResult11_g258 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_164 );
			float2 temp_cast_165 = (( _Texture_13_Normal_Powerr * -1.0 )).xx;
			float2 temp_cast_166 = (( _Texture_13_Normal_Powerr * -1.0 )).xx;
			float2 temp_cast_167 = (( _Texture_13_Normal_Powerr * -1.0 )).xx;
			float temp_output_9_0_g258 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g258 , temp_output_4_0_g258 ) ) ) );
			float3 appendResult19_g258 = float3( temp_output_4_0_g258.y , temp_output_4_0_g258.x , temp_output_9_0_g258 );
			float3 appendResult5949 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray5109 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5027 ), _Texture_13_Normal_Index)  );
			float2 appendResult11_g255 = float2( texArray5109.x , texArray5109.y );
			float2 temp_cast_168 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_output_4_0_g255 = ( ( ( appendResult11_g255 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_168 );
			float2 temp_cast_169 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_170 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_171 = (_Texture_13_Normal_Powerr).xx;
			float temp_output_9_0_g255 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g255 , temp_output_4_0_g255 ) ) ) );
			float3 appendResult10_g255 = float3( temp_output_4_0_g255.x , temp_output_4_0_g255.y , temp_output_9_0_g255 );
			float3 appendResult5952 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5543 = BlendComponents;
			float3 weightedAvg5543 = ( ( weightedBlendVar5543.x*( appendResult19_g258 * appendResult5949 ) + weightedBlendVar5543.y*appendResult10_g269 + weightedBlendVar5543.z*( appendResult10_g255 * appendResult5952 ) )/( weightedBlendVar5543.x + weightedBlendVar5543.y + weightedBlendVar5543.z ) );
			fixed3 ifLocalVar5830 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar5830 = appendResult10_g269;
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar5830 = weightedAvg5543;
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar5830 = appendResult10_g269;
			fixed3 ifLocalVar6152 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Index > -1.0 )
				ifLocalVar6152 = ifLocalVar5830;
			fixed3 Normal_13 = ifLocalVar6152;
			float2 appendResult5033 = float2( ( 1.0 / _Texture_14_Tiling ) , ( 1.0 / _Texture_14_Tiling ) );
			float4 texArray5178 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Normal_Index)  );
			float2 appendResult11_g270 = float2( texArray5178.x , texArray5178.y );
			float2 temp_cast_172 = (_Texture_14_Normal_Power).xx;
			float2 temp_output_4_0_g270 = ( ( ( appendResult11_g270 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_172 );
			float2 temp_cast_173 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_174 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_175 = (_Texture_14_Normal_Power).xx;
			float temp_output_9_0_g270 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g270 , temp_output_4_0_g270 ) ) ) );
			float3 appendResult10_g270 = float3( temp_output_4_0_g270.x , temp_output_4_0_g270.y , temp_output_9_0_g270 );
			float4 texArray5017 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5033 ), _Texture_14_Normal_Index)  );
			float2 appendResult11_g257 = float2( texArray5017.x , texArray5017.y );
			float2 temp_cast_176 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g257 = ( ( ( appendResult11_g257 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_176 );
			float2 temp_cast_177 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_cast_178 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_cast_179 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g257 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g257 , temp_output_4_0_g257 ) ) ) );
			float3 appendResult19_g257 = float3( temp_output_4_0_g257.y , temp_output_4_0_g257.x , temp_output_9_0_g257 );
			float3 appendResult5942 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray5170 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5033 ), _Texture_14_Normal_Index)  );
			float2 appendResult11_g264 = float2( texArray5170.x , texArray5170.y );
			float2 temp_cast_180 = (_Texture_14_Normal_Power).xx;
			float2 temp_output_4_0_g264 = ( ( ( appendResult11_g264 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_180 );
			float2 temp_cast_181 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_182 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_183 = (_Texture_14_Normal_Power).xx;
			float temp_output_9_0_g264 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g264 , temp_output_4_0_g264 ) ) ) );
			float3 appendResult10_g264 = float3( temp_output_4_0_g264.x , temp_output_4_0_g264.y , temp_output_9_0_g264 );
			float3 appendResult5945 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5547 = BlendComponents;
			float3 weightedAvg5547 = ( ( weightedBlendVar5547.x*( appendResult19_g257 * appendResult5942 ) + weightedBlendVar5547.y*appendResult10_g270 + weightedBlendVar5547.z*( appendResult10_g264 * appendResult5945 ) )/( weightedBlendVar5547.x + weightedBlendVar5547.y + weightedBlendVar5547.z ) );
			fixed3 ifLocalVar5826 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar5826 = appendResult10_g270;
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar5826 = weightedAvg5547;
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar5826 = appendResult10_g270;
			fixed3 ifLocalVar6154 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar6154 = ifLocalVar5826;
			fixed3 Normal_14 = ifLocalVar6154;
			float2 appendResult5212 = float2( ( 1.0 / _Texture_15_Tiling ) , ( 1.0 / _Texture_15_Tiling ) );
			float4 texArray5246 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Normal_Index)  );
			float2 appendResult11_g267 = float2( texArray5246.x , texArray5246.y );
			float2 temp_cast_184 = (_Texture_15_Normal_Power).xx;
			float2 temp_output_4_0_g267 = ( ( ( appendResult11_g267 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_184 );
			float2 temp_cast_185 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_186 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_187 = (_Texture_15_Normal_Power).xx;
			float temp_output_9_0_g267 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g267 , temp_output_4_0_g267 ) ) ) );
			float3 appendResult10_g267 = float3( temp_output_4_0_g267.x , temp_output_4_0_g267.y , temp_output_9_0_g267 );
			float4 texArray5227 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5212 ), _Texture_15_Normal_Index)  );
			float2 appendResult11_g261 = float2( texArray5227.x , texArray5227.y );
			float2 temp_cast_188 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g261 = ( ( ( appendResult11_g261 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_188 );
			float2 temp_cast_189 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_cast_190 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_cast_191 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g261 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g261 , temp_output_4_0_g261 ) ) ) );
			float3 appendResult19_g261 = float3( temp_output_4_0_g261.y , temp_output_4_0_g261.x , temp_output_9_0_g261 );
			float3 appendResult5935 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray5250 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5212 ), _Texture_15_Normal_Index)  );
			float2 appendResult11_g259 = float2( texArray5250.x , texArray5250.y );
			float2 temp_cast_192 = (_Texture_15_Normal_Power).xx;
			float2 temp_output_4_0_g259 = ( ( ( appendResult11_g259 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_192 );
			float2 temp_cast_193 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_194 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_195 = (_Texture_15_Normal_Power).xx;
			float temp_output_9_0_g259 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g259 , temp_output_4_0_g259 ) ) ) );
			float3 appendResult10_g259 = float3( temp_output_4_0_g259.x , temp_output_4_0_g259.y , temp_output_9_0_g259 );
			float3 appendResult5938 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5551 = BlendComponents;
			float3 weightedAvg5551 = ( ( weightedBlendVar5551.x*( appendResult19_g261 * appendResult5935 ) + weightedBlendVar5551.y*appendResult10_g267 + weightedBlendVar5551.z*( appendResult10_g259 * appendResult5938 ) )/( weightedBlendVar5551.x + weightedBlendVar5551.y + weightedBlendVar5551.z ) );
			fixed3 ifLocalVar5822 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar5822 = appendResult10_g267;
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar5822 = weightedAvg5551;
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar5822 = appendResult10_g267;
			fixed3 ifLocalVar6156 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar6156 = ifLocalVar5822;
			fixed3 Normal_15 = ifLocalVar6156;
			float2 appendResult5078 = float2( ( 1.0 / _Texture_16_Tiling ) , ( 1.0 / _Texture_16_Tiling ) );
			float4 texArray5099 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Normal_Index)  );
			float2 appendResult11_g268 = float2( texArray5099.x , texArray5099.y );
			float2 temp_cast_196 = (_Texture_16_Normal_Power).xx;
			float2 temp_output_4_0_g268 = ( ( ( appendResult11_g268 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_196 );
			float2 temp_cast_197 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_198 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_199 = (_Texture_16_Normal_Power).xx;
			float temp_output_9_0_g268 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g268 , temp_output_4_0_g268 ) ) ) );
			float3 appendResult10_g268 = float3( temp_output_4_0_g268.x , temp_output_4_0_g268.y , temp_output_9_0_g268 );
			float4 texArray5082 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5078 ), _Texture_16_Normal_Index)  );
			float2 appendResult11_g256 = float2( texArray5082.x , texArray5082.y );
			float2 temp_cast_200 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g256 = ( ( ( appendResult11_g256 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_200 );
			float2 temp_cast_201 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_cast_202 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_cast_203 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g256 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g256 , temp_output_4_0_g256 ) ) ) );
			float3 appendResult19_g256 = float3( temp_output_4_0_g256.y , temp_output_4_0_g256.x , temp_output_9_0_g256 );
			float3 appendResult5928 = float3( ase_worldNormal.x , -1 , 1 );
			float4 texArray4731 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5078 ), _Texture_16_Normal_Index)  );
			float2 appendResult11_g265 = float2( texArray4731.x , texArray4731.y );
			float2 temp_cast_204 = (_Texture_16_Normal_Power).xx;
			float2 temp_output_4_0_g265 = ( ( ( appendResult11_g265 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_204 );
			float2 temp_cast_205 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_206 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_207 = (_Texture_16_Normal_Power).xx;
			float temp_output_9_0_g265 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g265 , temp_output_4_0_g265 ) ) ) );
			float3 appendResult10_g265 = float3( temp_output_4_0_g265.x , temp_output_4_0_g265.y , temp_output_9_0_g265 );
			float3 appendResult5931 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar5555 = BlendComponents;
			float3 weightedAvg5555 = ( ( weightedBlendVar5555.x*( appendResult19_g256 * appendResult5928 ) + weightedBlendVar5555.y*appendResult10_g268 + weightedBlendVar5555.z*( appendResult10_g265 * appendResult5931 ) )/( weightedBlendVar5555.x + weightedBlendVar5555.y + weightedBlendVar5555.z ) );
			fixed3 ifLocalVar5818 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar5818 = appendResult10_g268;
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar5818 = weightedAvg5555;
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar5818 = appendResult10_g268;
			fixed3 ifLocalVar6158 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar6158 = ifLocalVar5818;
			fixed3 Normal_16 = ifLocalVar6158;
			float4 layeredBlendVar5642 = tex2DNode4371;
			float3 layeredBlend5642 = ( lerp( lerp( lerp( lerp( layeredBlend5641 , Normal_13 , layeredBlendVar5642.x ) , Normal_14 , layeredBlendVar5642.y ) , Normal_15 , layeredBlendVar5642.z ) , Normal_16 , layeredBlendVar5642.w ) );
			float2 appendResult3679 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_208 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray4382 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3679 * temp_cast_208 ), _Texture_Snow_Normal_Index)  );
			float2 appendResult11_g271 = float2( texArray4382.x , texArray4382.y );
			float2 temp_cast_209 = (_Snow_Normal_Scale).xx;
			float2 temp_output_4_0_g271 = ( ( ( appendResult11_g271 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_209 );
			float2 temp_cast_210 = (_Snow_Normal_Scale).xx;
			float2 temp_cast_211 = (_Snow_Normal_Scale).xx;
			float2 temp_cast_212 = (_Snow_Normal_Scale).xx;
			float temp_output_9_0_g271 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g271 , temp_output_4_0_g271 ) ) ) );
			float3 appendResult10_g271 = float3( temp_output_4_0_g271.x , temp_output_4_0_g271.y , temp_output_9_0_g271 );
			fixed3 ifLocalVar6166 = 0;
			UNITY_BRANCH if( _Texture_Snow_Normal_Index > -1.0 )
				ifLocalVar6166 = appendResult10_g271;
			float2 appendResult3750 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_213 = (_Snow_Noise_Tiling).xx;
			float4 texArray4383 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3750 * temp_cast_213 ), _Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_214 = (_Snow_Noise_Tiling).xx;
			float4 texArray4385 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_214 ) * float2( 0.23,0.23 ) ), _Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_215 = (_Snow_Noise_Tiling).xx;
			float4 texArray4384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_215 ) * float2( 0.53,0.53 ) ), _Texture_Snow_Noise_Index)  );
			fixed ifLocalVar6160 = 0;
			UNITY_BRANCH if( _Texture_Snow_Noise_Index > -1.0 )
				ifLocalVar6160 = lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , _Snow_Noise_Power );
			fixed SnowSlope = pow( ( 1.0 - ( clamp( ( clamp( ase_worldNormal.y , 0.0 , 0.9999 ) - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) , 0.0 , 2.0 ) * ( 1.0 / ( 1.0 - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) ) ) ) , _Snow_Maximum_Angle_Hardness );
			float HeightMask5474 = saturate(pow(((( ( normalize( layeredBlend5642 ).y + 1.0 ) * 0.5 )*saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ))*4)+(saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) )*2),5.0));;
			fixed3 temp_cast_216 = (WorldNormalVector( i , lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_217 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_218 = (0.0).xxx;
			o.Normal = BlendNormals( lerp( lerp( float3( 0,0,1 ) , lerp( appendResult10_g273 , appendResult10_g272 , UVmixDistance ) , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , lerp( appendResult10_g273 , appendResult10_g272 , UVmixDistance ) , ( _Snow_Perlin_Power * 0.5 ) ) , lerp( saturate( clamp( ( lerp( normalize( layeredBlend5642 ) , temp_cast_216 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_217 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_218 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x ) , normalize( lerp( lerp( lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) , normalize( layeredBlend5642 ) , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ) , UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) , UVmixDistance ) ) );
			float4 texArray3292 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Albedo_Index)  );
			fixed2 temp_cast_220 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3293 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_220 ), _Texture_1_Albedo_Index)  );
			float4 texArray3287 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3284 ), _Texture_1_Albedo_Index)  );
			float4 texArray3294 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3284 ), _Texture_1_Albedo_Index)  );
			float3 weightedBlendVar5674 = BlendComponents;
			float4 weightedAvg5674 = ( ( weightedBlendVar5674.x*texArray3287 + weightedBlendVar5674.y*texArray3292 + weightedBlendVar5674.z*texArray3294 )/( weightedBlendVar5674.x + weightedBlendVar5674.y + weightedBlendVar5674.z ) );
			fixed2 temp_cast_221 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3291 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3284 ) / temp_cast_221 ), _Texture_1_Albedo_Index)  );
			fixed2 temp_cast_222 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3295 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3284 ) / temp_cast_222 ), _Texture_1_Albedo_Index)  );
			float3 weightedBlendVar5675 = BlendComponents;
			float4 weightedAvg5675 = ( ( weightedBlendVar5675.x*texArray3291 + weightedBlendVar5675.y*texArray3293 + weightedBlendVar5675.z*texArray3295 )/( weightedBlendVar5675.x + weightedBlendVar5675.y + weightedBlendVar5675.z ) );
			fixed4 ifLocalVar5782 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar5782 = lerp( texArray3292 , texArray3293 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar5782 = lerp( weightedAvg5674 , weightedAvg5675 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar5782 = lerp( texArray3292 , texArray3293 , UVmixDistance );
			fixed4 ifLocalVar6126 = 0;
			UNITY_BRANCH if( _Texture_1_Albedo_Index > -1.0 )
				ifLocalVar6126 = ( ifLocalVar5782 * _Texture_1_Color );
			fixed4 Texture_1_Final = ifLocalVar6126;
			float4 texArray3338 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Albedo_Index)  );
			fixed2 temp_cast_223 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3339 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_223 ), _Texture_2_Albedo_Index)  );
			float4 texArray3355 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3349 ), _Texture_2_Albedo_Index)  );
			float4 texArray3341 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3349 ), _Texture_2_Albedo_Index)  );
			float3 weightedBlendVar5496 = BlendComponents;
			float4 weightedAvg5496 = ( ( weightedBlendVar5496.x*texArray3355 + weightedBlendVar5496.y*texArray3338 + weightedBlendVar5496.z*texArray3341 )/( weightedBlendVar5496.x + weightedBlendVar5496.y + weightedBlendVar5496.z ) );
			fixed2 temp_cast_224 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3356 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3349 ) / temp_cast_224 ), _Texture_2_Albedo_Index)  );
			fixed2 temp_cast_225 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3342 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3349 ) / temp_cast_225 ), _Texture_2_Albedo_Index)  );
			float3 weightedBlendVar5497 = BlendComponents;
			float4 weightedAvg5497 = ( ( weightedBlendVar5497.x*texArray3356 + weightedBlendVar5497.y*texArray3339 + weightedBlendVar5497.z*texArray3342 )/( weightedBlendVar5497.x + weightedBlendVar5497.y + weightedBlendVar5497.z ) );
			fixed4 ifLocalVar5771 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar5771 = lerp( texArray3338 , texArray3339 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar5771 = lerp( weightedAvg5496 , weightedAvg5497 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar5771 = lerp( texArray3338 , texArray3339 , UVmixDistance );
			fixed4 ifLocalVar6128 = 0;
			UNITY_BRANCH if( _Texture_2_Albedo_Index > -1.0 )
				ifLocalVar6128 = ( ifLocalVar5771 * _Texture_2_Color );
			fixed4 Texture_2_Final = ifLocalVar6128;
			float4 texArray3405 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Albedo_Index)  );
			fixed2 temp_cast_226 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_226 ), _Texture_3_Albedo_Index)  );
			float4 texArray3419 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3415 ), _Texture_3_Albedo_Index)  );
			float4 texArray3408 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3415 ), _Texture_3_Albedo_Index)  );
			float3 weightedBlendVar5500 = BlendComponents;
			float4 weightedAvg5500 = ( ( weightedBlendVar5500.x*texArray3419 + weightedBlendVar5500.y*texArray3405 + weightedBlendVar5500.z*texArray3408 )/( weightedBlendVar5500.x + weightedBlendVar5500.y + weightedBlendVar5500.z ) );
			fixed2 temp_cast_227 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3420 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3415 ) / temp_cast_227 ), _Texture_3_Albedo_Index)  );
			fixed2 temp_cast_228 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3409 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3415 ) / temp_cast_228 ), _Texture_3_Albedo_Index)  );
			float3 weightedBlendVar5501 = BlendComponents;
			float4 weightedAvg5501 = ( ( weightedBlendVar5501.x*texArray3420 + weightedBlendVar5501.y*texArray3406 + weightedBlendVar5501.z*texArray3409 )/( weightedBlendVar5501.x + weightedBlendVar5501.y + weightedBlendVar5501.z ) );
			fixed4 ifLocalVar5775 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar5775 = lerp( texArray3405 , texArray3406 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar5775 = lerp( weightedAvg5500 , weightedAvg5501 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar5775 = lerp( texArray3405 , texArray3406 , UVmixDistance );
			fixed4 ifLocalVar6130 = 0;
			UNITY_BRANCH if( _Texture_3_Albedo_Index > -1.0 )
				ifLocalVar6130 = ( ifLocalVar5775 * _Texture_3_Color );
			fixed4 Texture_3_Final = ifLocalVar6130;
			fixed2 temp_cast_229 = (( 1.0 / _Texture_4_Tiling )).xx;
			float4 texArray3472 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * temp_cast_229 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_230 = (( 1.0 / _Texture_4_Tiling )).xx;
			fixed2 temp_cast_231 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3473 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * temp_cast_230 ) / temp_cast_231 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_232 = (( 1.0 / _Texture_4_Tiling )).xx;
			float4 texArray3486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * temp_cast_232 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_233 = (( 1.0 / _Texture_4_Tiling )).xx;
			float4 texArray3475 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * temp_cast_233 ), _Texture_4_Albedo_Index)  );
			float3 weightedBlendVar5504 = BlendComponents;
			float4 weightedAvg5504 = ( ( weightedBlendVar5504.x*texArray3486 + weightedBlendVar5504.y*texArray3472 + weightedBlendVar5504.z*texArray3475 )/( weightedBlendVar5504.x + weightedBlendVar5504.y + weightedBlendVar5504.z ) );
			fixed2 temp_cast_234 = (( 1.0 / _Texture_4_Tiling )).xx;
			fixed2 temp_cast_235 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3487 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * temp_cast_234 ) / temp_cast_235 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_236 = (( 1.0 / _Texture_4_Tiling )).xx;
			fixed2 temp_cast_237 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3476 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * temp_cast_236 ) / temp_cast_237 ), _Texture_4_Albedo_Index)  );
			float3 weightedBlendVar5505 = BlendComponents;
			float4 weightedAvg5505 = ( ( weightedBlendVar5505.x*texArray3487 + weightedBlendVar5505.y*texArray3473 + weightedBlendVar5505.z*texArray3476 )/( weightedBlendVar5505.x + weightedBlendVar5505.y + weightedBlendVar5505.z ) );
			fixed4 ifLocalVar5777 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar5777 = lerp( texArray3472 , texArray3473 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar5777 = lerp( weightedAvg5504 , weightedAvg5505 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar5777 = lerp( texArray3472 , texArray3473 , UVmixDistance );
			fixed4 ifLocalVar6132 = 0;
			UNITY_BRANCH if( _Texture_4_Albedo_Index > -1.0 )
				ifLocalVar6132 = ( ifLocalVar5777 * _Texture_4_Color );
			fixed4 Texture_4_Final = ifLocalVar6132;
			float4 layeredBlendVar5643 = tex2DNode4368;
			float4 layeredBlend5643 = ( lerp( lerp( lerp( lerp( float4( 0,0,0,0 ) , Texture_1_Final , layeredBlendVar5643.x ) , Texture_2_Final , layeredBlendVar5643.y ) , Texture_3_Final , layeredBlendVar5643.z ) , Texture_4_Final , layeredBlendVar5643.w ) );
			float4 texArray4450 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Albedo_Index)  );
			fixed2 temp_cast_238 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_238 ), _Texture_5_Albedo_Index)  );
			float4 texArray4442 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4399 ), _Texture_5_Albedo_Index)  );
			float4 texArray4443 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4399 ), _Texture_5_Albedo_Index)  );
			float3 weightedBlendVar5512 = BlendComponents;
			float4 weightedAvg5512 = ( ( weightedBlendVar5512.x*texArray4442 + weightedBlendVar5512.y*texArray4450 + weightedBlendVar5512.z*texArray4443 )/( weightedBlendVar5512.x + weightedBlendVar5512.y + weightedBlendVar5512.z ) );
			fixed2 temp_cast_239 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4444 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4399 ) / temp_cast_239 ), _Texture_5_Albedo_Index)  );
			fixed2 temp_cast_240 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4439 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4399 ) / temp_cast_240 ), _Texture_5_Albedo_Index)  );
			float3 weightedBlendVar5514 = BlendComponents;
			float4 weightedAvg5514 = ( ( weightedBlendVar5514.x*texArray4444 + weightedBlendVar5514.y*texArray4445 + weightedBlendVar5514.z*texArray4439 )/( weightedBlendVar5514.x + weightedBlendVar5514.y + weightedBlendVar5514.z ) );
			fixed4 ifLocalVar5804 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar5804 = lerp( texArray4450 , texArray4445 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar5804 = lerp( weightedAvg5512 , weightedAvg5514 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar5804 = lerp( texArray4450 , texArray4445 , UVmixDistance );
			fixed4 ifLocalVar6134 = 0;
			UNITY_BRANCH if( _Texture_5_Albedo_Index > -1.0 )
				ifLocalVar6134 = ( ifLocalVar5804 * _Texture_5_Color );
			fixed4 Texture_5_Final = ifLocalVar6134;
			float4 texArray4517 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Albedo_Index)  );
			fixed2 temp_cast_241 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_241 ), _Texture_6_Albedo_Index)  );
			float4 texArray4509 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4471 ), _Texture_6_Albedo_Index)  );
			float4 texArray4510 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4471 ), _Texture_6_Albedo_Index)  );
			float3 weightedBlendVar5520 = BlendComponents;
			float4 weightedAvg5520 = ( ( weightedBlendVar5520.x*texArray4509 + weightedBlendVar5520.y*texArray4517 + weightedBlendVar5520.z*texArray4510 )/( weightedBlendVar5520.x + weightedBlendVar5520.y + weightedBlendVar5520.z ) );
			fixed2 temp_cast_242 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4511 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4471 ) / temp_cast_242 ), _Texture_6_Albedo_Index)  );
			fixed2 temp_cast_243 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4506 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4471 ) / temp_cast_243 ), _Texture_6_Albedo_Index)  );
			float3 weightedBlendVar5522 = BlendComponents;
			float4 weightedAvg5522 = ( ( weightedBlendVar5522.x*texArray4511 + weightedBlendVar5522.y*texArray4512 + weightedBlendVar5522.z*texArray4506 )/( weightedBlendVar5522.x + weightedBlendVar5522.y + weightedBlendVar5522.z ) );
			fixed4 ifLocalVar5809 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar5809 = lerp( texArray4517 , texArray4512 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar5809 = lerp( weightedAvg5520 , weightedAvg5522 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar5809 = lerp( texArray4517 , texArray4512 , UVmixDistance );
			fixed4 ifLocalVar6136 = 0;
			UNITY_BRANCH if( _Texture_6_Albedo_Index > -1.0 )
				ifLocalVar6136 = ( ifLocalVar5809 * _Texture_6_Color );
			fixed4 Texture_6_Final = ifLocalVar6136;
			float4 texArray4591 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Albedo_Index)  );
			fixed2 temp_cast_244 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_244 ), _Texture_7_Albedo_Index)  );
			float4 texArray4583 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4545 ), _Texture_7_Albedo_Index)  );
			float4 texArray4584 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4545 ), _Texture_7_Albedo_Index)  );
			float3 weightedBlendVar5524 = BlendComponents;
			float4 weightedAvg5524 = ( ( weightedBlendVar5524.x*texArray4583 + weightedBlendVar5524.y*texArray4591 + weightedBlendVar5524.z*texArray4584 )/( weightedBlendVar5524.x + weightedBlendVar5524.y + weightedBlendVar5524.z ) );
			fixed2 temp_cast_245 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4585 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4545 ) / temp_cast_245 ), _Texture_7_Albedo_Index)  );
			fixed2 temp_cast_246 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4580 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4545 ) / temp_cast_246 ), _Texture_7_Albedo_Index)  );
			float3 weightedBlendVar5526 = BlendComponents;
			float4 weightedAvg5526 = ( ( weightedBlendVar5526.x*texArray4585 + weightedBlendVar5526.y*texArray4586 + weightedBlendVar5526.z*texArray4580 )/( weightedBlendVar5526.x + weightedBlendVar5526.y + weightedBlendVar5526.z ) );
			fixed4 ifLocalVar5812 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar5812 = lerp( texArray4591 , texArray4586 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar5812 = lerp( weightedAvg5524 , weightedAvg5526 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar5812 = lerp( texArray4591 , texArray4586 , UVmixDistance );
			fixed4 ifLocalVar6139 = 0;
			UNITY_BRANCH if( _Texture_7_Albedo_Index > -1.0 )
				ifLocalVar6139 = ( ifLocalVar5812 * _Texture_7_Color );
			fixed4 Texture_7_Final = ifLocalVar6139;
			float4 texArray4665 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Albedo_Index)  );
			fixed2 temp_cast_247 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4660 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_247 ), _Texture_8_Albedo_Index)  );
			float4 texArray4657 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4619 ), _Texture_8_Albedo_Index)  );
			float4 texArray4658 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4619 ), _Texture_8_Albedo_Index)  );
			float3 weightedBlendVar5528 = BlendComponents;
			float4 weightedAvg5528 = ( ( weightedBlendVar5528.x*texArray4657 + weightedBlendVar5528.y*texArray4665 + weightedBlendVar5528.z*texArray4658 )/( weightedBlendVar5528.x + weightedBlendVar5528.y + weightedBlendVar5528.z ) );
			fixed2 temp_cast_248 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4659 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4619 ) / temp_cast_248 ), _Texture_8_Albedo_Index)  );
			fixed2 temp_cast_249 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4654 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4619 ) / temp_cast_249 ), _Texture_8_Albedo_Index)  );
			float3 weightedBlendVar5530 = BlendComponents;
			float4 weightedAvg5530 = ( ( weightedBlendVar5530.x*texArray4659 + weightedBlendVar5530.y*texArray4660 + weightedBlendVar5530.z*texArray4654 )/( weightedBlendVar5530.x + weightedBlendVar5530.y + weightedBlendVar5530.z ) );
			fixed4 ifLocalVar5815 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar5815 = lerp( texArray4665 , texArray4660 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar5815 = lerp( weightedAvg5528 , weightedAvg5530 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar5815 = lerp( texArray4665 , texArray4660 , UVmixDistance );
			fixed4 ifLocalVar6141 = 0;
			UNITY_BRANCH if( _Texture_8_Albedo_Index > -1.0 )
				ifLocalVar6141 = ( ifLocalVar5815 * _Texture_8_Color );
			fixed4 Texture_8_Final = ifLocalVar6141;
			float4 layeredBlendVar5644 = tex2DNode4369;
			float4 layeredBlend5644 = ( lerp( lerp( lerp( lerp( layeredBlend5643 , Texture_5_Final , layeredBlendVar5644.x ) , Texture_6_Final , layeredBlendVar5644.y ) , Texture_7_Final , layeredBlendVar5644.z ) , Texture_8_Final , layeredBlendVar5644.w ) );
			float4 texArray4723 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Albedo_Index)  );
			fixed2 temp_cast_250 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4889 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_250 ), _Texture_9_Albedo_Index)  );
			float4 texArray5286 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4736 ), _Texture_9_Albedo_Index)  );
			float4 texArray4858 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4736 ), _Texture_9_Albedo_Index)  );
			float3 weightedBlendVar5489 = BlendComponents;
			float4 weightedAvg5489 = ( ( weightedBlendVar5489.x*texArray5286 + weightedBlendVar5489.y*texArray4723 + weightedBlendVar5489.z*texArray4858 )/( weightedBlendVar5489.x + weightedBlendVar5489.y + weightedBlendVar5489.z ) );
			fixed2 temp_cast_251 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4719 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4736 ) / temp_cast_251 ), _Texture_9_Albedo_Index)  );
			fixed2 temp_cast_252 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4865 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4736 ) / temp_cast_252 ), _Texture_9_Albedo_Index)  );
			float3 weightedBlendVar5490 = BlendComponents;
			float4 weightedAvg5490 = ( ( weightedBlendVar5490.x*texArray4719 + weightedBlendVar5490.y*texArray4889 + weightedBlendVar5490.z*texArray4865 )/( weightedBlendVar5490.x + weightedBlendVar5490.y + weightedBlendVar5490.z ) );
			fixed4 ifLocalVar5844 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar5844 = lerp( texArray4723 , texArray4889 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar5844 = lerp( weightedAvg5489 , weightedAvg5490 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar5844 = lerp( texArray4723 , texArray4889 , UVmixDistance );
			fixed4 ifLocalVar6143 = 0;
			UNITY_BRANCH if( _Texture_9_Albedo_Index > -1.0 )
				ifLocalVar6143 = ( ifLocalVar5844 * _Texture_9_Color );
			fixed4 Texture_9_Final = ifLocalVar6143;
			float4 texArray4899 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Albedo_Index)  );
			fixed2 temp_cast_253 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4913 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_253 ), _Texture_10_Albedo_Index)  );
			float4 texArray4886 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4738 ), _Texture_10_Albedo_Index)  );
			float4 texArray4877 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4738 ), _Texture_10_Albedo_Index)  );
			float3 weightedBlendVar5532 = BlendComponents;
			float4 weightedAvg5532 = ( ( weightedBlendVar5532.x*texArray4886 + weightedBlendVar5532.y*texArray4899 + weightedBlendVar5532.z*texArray4877 )/( weightedBlendVar5532.x + weightedBlendVar5532.y + weightedBlendVar5532.z ) );
			fixed2 temp_cast_254 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4894 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4738 ) / temp_cast_254 ), _Texture_10_Albedo_Index)  );
			fixed2 temp_cast_255 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4878 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4738 ) / temp_cast_255 ), _Texture_10_Albedo_Index)  );
			float3 weightedBlendVar5534 = BlendComponents;
			float4 weightedAvg5534 = ( ( weightedBlendVar5534.x*texArray4894 + weightedBlendVar5534.y*texArray4913 + weightedBlendVar5534.z*texArray4878 )/( weightedBlendVar5534.x + weightedBlendVar5534.y + weightedBlendVar5534.z ) );
			fixed4 ifLocalVar5840 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar5840 = lerp( texArray4899 , texArray4913 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar5840 = lerp( weightedAvg5532 , weightedAvg5534 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar5840 = lerp( texArray4899 , texArray4913 , UVmixDistance );
			fixed4 ifLocalVar6145 = 0;
			UNITY_BRANCH if( _Texture_10_Albedo_Index > -1.0 )
				ifLocalVar6145 = ( ifLocalVar5840 * _Texture_10_Color );
			fixed4 Texture_10_Final = ifLocalVar6145;
			float4 texArray4928 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Albedo_Index)  );
			fixed2 temp_cast_256 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4923 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_256 ), _Texture_11_Albedo_Index)  );
			float4 texArray4917 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4741 ), _Texture_11_Albedo_Index)  );
			float4 texArray4911 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4741 ), _Texture_11_Albedo_Index)  );
			float3 weightedBlendVar5536 = BlendComponents;
			float4 weightedAvg5536 = ( ( weightedBlendVar5536.x*texArray4917 + weightedBlendVar5536.y*texArray4928 + weightedBlendVar5536.z*texArray4911 )/( weightedBlendVar5536.x + weightedBlendVar5536.y + weightedBlendVar5536.z ) );
			fixed2 temp_cast_257 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4898 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4741 ) / temp_cast_257 ), _Texture_11_Albedo_Index)  );
			fixed2 temp_cast_258 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4914 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4741 ) / temp_cast_258 ), _Texture_11_Albedo_Index)  );
			float3 weightedBlendVar5538 = BlendComponents;
			float4 weightedAvg5538 = ( ( weightedBlendVar5538.x*texArray4898 + weightedBlendVar5538.y*texArray4923 + weightedBlendVar5538.z*texArray4914 )/( weightedBlendVar5538.x + weightedBlendVar5538.y + weightedBlendVar5538.z ) );
			fixed4 ifLocalVar5836 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar5836 = lerp( texArray4928 , texArray4923 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar5836 = lerp( weightedAvg5536 , weightedAvg5538 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar5836 = lerp( texArray4928 , texArray4923 , UVmixDistance );
			fixed4 ifLocalVar6147 = 0;
			UNITY_BRANCH if( _Texture_11_Albedo_Index > -1.0 )
				ifLocalVar6147 = ( ifLocalVar5836 * _Texture_11_Color );
			fixed4 Texture_11_Final = ifLocalVar6147;
			float4 texArray4954 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Albedo_Index)  );
			fixed2 temp_cast_259 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4952 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_259 ), _Texture_12_Albedo_Index)  );
			float4 texArray4926 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4751 ), _Texture_12_Albedo_Index)  );
			float4 texArray4927 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4751 ), _Texture_12_Albedo_Index)  );
			float3 weightedBlendVar5540 = BlendComponents;
			float4 weightedAvg5540 = ( ( weightedBlendVar5540.x*texArray4926 + weightedBlendVar5540.y*texArray4954 + weightedBlendVar5540.z*texArray4927 )/( weightedBlendVar5540.x + weightedBlendVar5540.y + weightedBlendVar5540.z ) );
			fixed2 temp_cast_260 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4919 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4751 ) / temp_cast_260 ), _Texture_12_Albedo_Index)  );
			fixed2 temp_cast_261 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4931 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4751 ) / temp_cast_261 ), _Texture_12_Albedo_Index)  );
			float3 weightedBlendVar5542 = BlendComponents;
			float4 weightedAvg5542 = ( ( weightedBlendVar5542.x*texArray4919 + weightedBlendVar5542.y*texArray4952 + weightedBlendVar5542.z*texArray4931 )/( weightedBlendVar5542.x + weightedBlendVar5542.y + weightedBlendVar5542.z ) );
			fixed4 ifLocalVar5832 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar5832 = lerp( texArray4954 , texArray4952 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar5832 = lerp( weightedAvg5540 , weightedAvg5542 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar5832 = lerp( texArray4954 , texArray4952 , UVmixDistance );
			fixed4 ifLocalVar6149 = 0;
			UNITY_BRANCH if( _Texture_12_Albedo_Index > -1.0 )
				ifLocalVar6149 = ( ifLocalVar5832 * _Texture_12_Color );
			fixed4 Texture_12_Final = ifLocalVar6149;
			float4 layeredBlendVar5645 = tex2DNode4370;
			float4 layeredBlend5645 = ( lerp( lerp( lerp( lerp( layeredBlend5644 , Texture_9_Final , layeredBlendVar5645.x ) , Texture_10_Final , layeredBlendVar5645.y ) , Texture_11_Final , layeredBlendVar5645.z ) , Texture_12_Final , layeredBlendVar5645.w ) );
			float4 texArray5043 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Albedo_Index)  );
			fixed2 temp_cast_262 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5034 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_262 ), _Texture_13_Albedo_Index)  );
			float4 texArray5128 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5027 ), _Texture_13_Albedo_Index)  );
			float4 texArray5129 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5027 ), _Texture_13_Albedo_Index)  );
			float3 weightedBlendVar5544 = BlendComponents;
			float4 weightedAvg5544 = ( ( weightedBlendVar5544.x*texArray5128 + weightedBlendVar5544.y*texArray5043 + weightedBlendVar5544.z*texArray5129 )/( weightedBlendVar5544.x + weightedBlendVar5544.y + weightedBlendVar5544.z ) );
			fixed2 temp_cast_263 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5130 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5027 ) / temp_cast_263 ), _Texture_13_Albedo_Index)  );
			fixed2 temp_cast_264 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5121 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5027 ) / temp_cast_264 ), _Texture_13_Albedo_Index)  );
			float3 weightedBlendVar5546 = BlendComponents;
			float4 weightedAvg5546 = ( ( weightedBlendVar5546.x*texArray5130 + weightedBlendVar5546.y*texArray5034 + weightedBlendVar5546.z*texArray5121 )/( weightedBlendVar5546.x + weightedBlendVar5546.y + weightedBlendVar5546.z ) );
			fixed4 ifLocalVar5828 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar5828 = lerp( texArray5043 , texArray5034 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar5828 = lerp( weightedAvg5544 , weightedAvg5546 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar5828 = lerp( texArray5043 , texArray5034 , UVmixDistance );
			fixed4 ifLocalVar6151 = 0;
			UNITY_BRANCH if( _Texture_13_Albedo_Index > -1.0 )
				ifLocalVar6151 = ( ifLocalVar5828 * _Texture_13_Color );
			fixed4 Texture_13_Final = ifLocalVar6151;
			float4 texArray5202 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Albedo_Index)  );
			fixed2 temp_cast_265 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5171 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_265 ), _Texture_14_Albedo_Index)  );
			float4 texArray5168 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5033 ), _Texture_14_Albedo_Index)  );
			float4 texArray5239 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5033 ), _Texture_14_Albedo_Index)  );
			float3 weightedBlendVar5548 = BlendComponents;
			float4 weightedAvg5548 = ( ( weightedBlendVar5548.x*texArray5168 + weightedBlendVar5548.y*texArray5202 + weightedBlendVar5548.z*texArray5239 )/( weightedBlendVar5548.x + weightedBlendVar5548.y + weightedBlendVar5548.z ) );
			fixed2 temp_cast_266 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5205 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5033 ) / temp_cast_266 ), _Texture_14_Albedo_Index)  );
			fixed2 temp_cast_267 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5241 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5033 ) / temp_cast_267 ), _Texture_14_Albedo_Index)  );
			float3 weightedBlendVar5550 = BlendComponents;
			float4 weightedAvg5550 = ( ( weightedBlendVar5550.x*texArray5205 + weightedBlendVar5550.y*texArray5171 + weightedBlendVar5550.z*texArray5241 )/( weightedBlendVar5550.x + weightedBlendVar5550.y + weightedBlendVar5550.z ) );
			fixed4 ifLocalVar5824 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar5824 = lerp( texArray5202 , texArray5171 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar5824 = lerp( weightedAvg5548 , weightedAvg5550 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar5824 = lerp( texArray5202 , texArray5171 , UVmixDistance );
			fixed4 ifLocalVar6153 = 0;
			UNITY_BRANCH if( _Texture_14_Albedo_Index > -1.0 )
				ifLocalVar6153 = ( ifLocalVar5824 * _Texture_14_Color );
			fixed4 Texture_14_Final = ifLocalVar6153;
			float4 texArray5259 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Albedo_Index)  );
			fixed2 temp_cast_268 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5272 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_268 ), _Texture_15_Albedo_Index)  );
			float4 texArray5182 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5212 ), _Texture_15_Albedo_Index)  );
			float4 texArray5189 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5212 ), _Texture_15_Albedo_Index)  );
			float3 weightedBlendVar5552 = BlendComponents;
			float4 weightedAvg5552 = ( ( weightedBlendVar5552.x*texArray5182 + weightedBlendVar5552.y*texArray5259 + weightedBlendVar5552.z*texArray5189 )/( weightedBlendVar5552.x + weightedBlendVar5552.y + weightedBlendVar5552.z ) );
			fixed2 temp_cast_269 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5188 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5212 ) / temp_cast_269 ), _Texture_15_Albedo_Index)  );
			fixed2 temp_cast_270 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5247 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5212 ) / temp_cast_270 ), _Texture_15_Albedo_Index)  );
			float3 weightedBlendVar5554 = BlendComponents;
			float4 weightedAvg5554 = ( ( weightedBlendVar5554.x*texArray5188 + weightedBlendVar5554.y*texArray5272 + weightedBlendVar5554.z*texArray5247 )/( weightedBlendVar5554.x + weightedBlendVar5554.y + weightedBlendVar5554.z ) );
			fixed4 ifLocalVar5820 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar5820 = lerp( texArray5259 , texArray5272 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar5820 = lerp( weightedAvg5552 , weightedAvg5554 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar5820 = lerp( texArray5259 , texArray5272 , UVmixDistance );
			fixed4 ifLocalVar6155 = 0;
			UNITY_BRANCH if( _Texture_15_Albedo_Index > -1.0 )
				ifLocalVar6155 = ( ifLocalVar5820 * _Texture_15_Color );
			fixed4 Texture_15_Final = ifLocalVar6155;
			fixed2 temp_cast_271 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5143 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_271 ), _Texture_16_Albedo_Index)  );
			float4 texArray5145 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float4 texArray5150 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float4 texArray5139 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float3 weightedBlendVar5556 = BlendComponents;
			float4 weightedAvg5556 = ( ( weightedBlendVar5556.x*texArray5150 + weightedBlendVar5556.y*texArray5139 + weightedBlendVar5556.z*texArray5145 )/( weightedBlendVar5556.x + weightedBlendVar5556.y + weightedBlendVar5556.z ) );
			fixed2 temp_cast_272 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5144 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5078 ) / temp_cast_272 ), _Texture_16_Albedo_Index)  );
			fixed2 temp_cast_273 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5154 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5078 ) / temp_cast_273 ), _Texture_16_Albedo_Index)  );
			float3 weightedBlendVar5558 = BlendComponents;
			float4 weightedAvg5558 = ( ( weightedBlendVar5558.x*texArray5144 + weightedBlendVar5558.y*texArray5143 + weightedBlendVar5558.z*texArray5154 )/( weightedBlendVar5558.x + weightedBlendVar5558.y + weightedBlendVar5558.z ) );
			fixed4 ifLocalVar5816 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar5816 = lerp( texArray5143 , texArray5145 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar5816 = lerp( weightedAvg5556 , weightedAvg5558 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar5816 = lerp( texArray5143 , texArray5145 , UVmixDistance );
			fixed4 ifLocalVar6157 = 0;
			UNITY_BRANCH if( _Texture_16_Albedo_Index > -1.0 )
				ifLocalVar6157 = ( ifLocalVar5816 * _Texture_16_Color );
			fixed4 Texture_16_Final = ifLocalVar6157;
			float4 layeredBlendVar5646 = tex2DNode4371;
			float4 layeredBlend5646 = ( lerp( lerp( lerp( lerp( layeredBlend5645 , Texture_13_Final , layeredBlendVar5646.x ) , Texture_14_Final , layeredBlendVar5646.y ) , Texture_15_Final , layeredBlendVar5646.z ) , Texture_16_Final , layeredBlendVar5646.w ) );
			float3 appendResult3857 = float3( layeredBlend5646.x , layeredBlend5646.y , layeredBlend5646.z );
			fixed3 temp_cast_275 = (_Geological_Map_Close_Power).xxx;
			fixed2 temp_cast_276 = (( _Geological_Map_Offset_Close + ( ( 1.0 / _Geological_Tiling_Close ) * ase_worldPos.y ) )).xx;
			fixed4 tex2DNode5984 = tex2D( _Texture_Geological_Map, temp_cast_276 );
			float3 appendResult5986 = float3( tex2DNode5984.r , tex2DNode5984.g , tex2DNode5984.b );
			fixed3 temp_cast_277 = (_Geological_Map_Far_Power).xxx;
			fixed2 temp_cast_278 = (( ( ase_worldPos.y * ( 1.0 / _Geological_Tiling_Far ) ) + _Geological_Map_Offset_Far )).xx;
			fixed4 tex2DNode5983 = tex2D( _Texture_Geological_Map, temp_cast_278 );
			float3 appendResult5985 = float3( tex2DNode5983.r , tex2DNode5983.g , tex2DNode5983.b );
			fixed3 temp_cast_279 = (( ( _Texture_16_Geological_Power * Splat4_A ) + ( ( _Texture_15_Geological_Power * Splat4_B ) + ( ( _Texture_14_Geological_Power * Splat4_G ) + ( ( _Texture_13_Geological_Power * Splat4_R ) + ( ( _Texture_12_Geological_Power * Splat3_A ) + ( ( _Texture_11_Geological_Power * Splat3_B ) + ( ( _Texture_10_Geological_Power * Splat3_G ) + ( ( _Texture_9_Geological_Power * Splat3_R ) + ( ( _Texture_8_Geological_Power * Splat2_A ) + ( ( _Texture_7_Geological_Power * Splat2_B ) + ( ( _Texture_6_Geological_Power * Splat2_G ) + ( ( _Texture_5_Geological_Power * Splat2_R ) + ( ( _Texture_1_Geological_Power * Splat1_R ) + ( ( _Texture_2_Geological_Power * Splat1_G ) + ( ( _Texture_4_Geological_Power * Splat1_A ) + ( _Texture_3_Geological_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )).xxx;
			fixed2 temp_cast_281 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray4378 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( appendResult3679 * temp_cast_281 ), _Texture_Snow_Index)  );
			fixed2 temp_cast_282 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_283 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray4379 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( appendResult3679 * temp_cast_282 ) / temp_cast_283 ), _Texture_Snow_Index)  );
			fixed4 ifLocalVar6165 = 0;
			UNITY_BRANCH if( _Texture_Snow_Index > -1.0 )
				ifLocalVar6165 = ( lerp( texArray4378 , texArray4379 , UVmixDistance ) * _Snow_Color );
			float3 appendResult1410 = float3( ifLocalVar6165.x , ifLocalVar6165.y , ifLocalVar6165.z );
			fixed3 temp_cast_285 = (WorldNormalVector( i , lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_286 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_287 = (0.0).xxx;
			o.Albedo = lerp( clamp( ( saturate( ( fixed4( appendResult3857 , 0.0 ) + fixed4( ( lerp( ( temp_cast_275 * ( appendResult5986 + float3( -0.3,-0.3,-0.3 ) ) ) , ( temp_cast_277 * ( appendResult5985 + float3( -0.3,-0.3,-0.3 ) ) ) , UVmixDistance ) * temp_cast_279 ) , 0.0 ) ) )) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) ) , fixed4( appendResult1410 , 0.0 ) , lerp( saturate( clamp( ( lerp( normalize( layeredBlend5642 ) , temp_cast_285 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_286 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_287 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x ).rgb;
			fixed3 temp_cast_290 = (_Terrain_Specular).xxx;
			fixed3 temp_cast_291 = (_Snow_Specular).xxx;
			fixed3 temp_cast_292 = (WorldNormalVector( i , lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_293 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_294 = (0.0).xxx;
			o.Specular = lerp( ( ( appendResult3857 * float3( 0.3,0.3,0.3 ) ) * temp_cast_290 ) , ( clamp( appendResult1410 , float3( 0,0,0 ) , float3( 0.5,0.5,0.5 ) ) * temp_cast_291 ) , lerp( saturate( clamp( ( lerp( normalize( layeredBlend5642 ) , temp_cast_292 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_293 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_294 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x );
			fixed3 temp_cast_296 = (WorldNormalVector( i , lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_297 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_298 = (0.0).xxx;
			o.Smoothness = lerp( ( layeredBlend5646.w * _Terrain_Smoothness ) , ifLocalVar6165.w , lerp( saturate( clamp( ( lerp( normalize( layeredBlend5642 ) , temp_cast_296 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_297 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_298 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x );
			fixed3 temp_cast_300 = (WorldNormalVector( i , lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) ).y).xxx;
			fixed3 temp_cast_301 = (clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 )).xxx;
			fixed3 temp_cast_302 = (0.0).xxx;
			#ifdef _USE_AO_ON
			float staticSwitch6164 = clamp( ( ( 1.0 + BlendNormals( lerp( lerp( float3( 0,0,1 ) , lerp( appendResult10_g273 , appendResult10_g272 , UVmixDistance ) , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , lerp( appendResult10_g273 , appendResult10_g272 , UVmixDistance ) , ( _Snow_Perlin_Power * 0.5 ) ) , lerp( saturate( clamp( ( lerp( normalize( layeredBlend5642 ) , temp_cast_300 , pow( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) , 2.0 ) ) * temp_cast_301 ) , float3( 0,0,0 ) , float3( 1,0,0 ) ) ) , temp_cast_302 , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ).x ) , normalize( lerp( lerp( lerp( normalize( layeredBlend5642 ) , lerp( normalize( layeredBlend5642 ) , ifLocalVar6166 , ( _Snow_Blend_Normal * saturate( ( ( clamp( lerp( 0.0 , lerp( ( _Snow_Amount * clamp( ( ifLocalVar6160 * _Snow_Amount ) , 0.4 , 1.0 ) ) , 0.0 , clamp( SnowSlope , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) / 1.5 ) * _Snow_Amount ) ) ) ) , HeightMask5474 ) , normalize( layeredBlend5642 ) , clamp( pow( ( ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) * ( ifLocalVar6160 * 5.0 ) ) , 3.0 ) , 0.0 , 1.0 ) ) , UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) , UVmixDistance ) ) ).y ) * 0.5 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#else
			float staticSwitch6164 = 1.0;
			#endif
			o.Occlusion = staticSwitch6164;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers d3d9 gles 
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows noforwardadd vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	Dependency "BaseMapShader" = "CTS Terrain Shader Basic LOD"
}