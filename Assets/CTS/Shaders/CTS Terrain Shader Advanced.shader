Shader "CTS Terrain Shader Advanced"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Geological_Tiling_Close("Geological_Tiling_Close", Range( 0 , 1000)) = 87
		_Geological_Tiling_Far("Geological_Tiling_Far", Range( 0 , 1000)) = 87
		_Geological_Map_Offset_Far("Geological_Map_Offset _Far", Range( 0 , 1)) = 1
		_Geological_Map_Offset_Close("Geological_Map_Offset _Close", Range( 0 , 1)) = 1
		_Geological_Map_Close_Power("Geological_Map_Close_Power", Range( 0 , 2)) = 0
		_Geological_Map_Far_Power("Geological_Map_Far_Power", Range( 0 , 2)) = 1
		_UV_Mix_Power("UV_Mix_Power", Range( 0.01 , 10)) = 4
		_UV_Mix_Start_Distance("UV_Mix_Start_Distance", Range( 0 , 100000)) = 400
		_Perlin_Normal_Tiling_Close("Perlin_Normal_Tiling_Close", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Tiling_Far("Perlin_Normal_Tiling_Far", Range( 0.01 , 1000)) = 40
		_Perlin_Normal_Power("Perlin_Normal_Power", Range( 0 , 2)) = 1
		_Perlin_Normal_Power_Close("Perlin_Normal_Power_Close", Range( 0 , 1)) = 0.5
		_Terrain_Smoothness("Terrain_Smoothness", Range( 0 , 2)) = 1
		_Terrain_Specular("Terrain_Specular", Range( 0 , 3)) = 1
		_Texture_4_AO_Power("Texture_4_AO_Power", Range( 0 , 1)) = 1
		_Texture_15_AO_Power("Texture_15_AO_Power", Range( 0 , 1)) = 1
		_Texture_3_AO_Power("Texture_3_AO_Power", Range( 0 , 1)) = 1
		_Texture_8_AO_Power("Texture_8_AO_Power", Range( 0 , 1)) = 1
		_Texture_6_AO_Power("Texture_6_AO_Power", Range( 0 , 1)) = 1
		_Texture_16_AO_Power("Texture_16_AO_Power", Range( 0 , 1)) = 1
		_Texture_7_AO_Power("Texture_7_AO_Power", Range( 0 , 1)) = 1
		_Texture_5_AO_Power("Texture_5_AO_Power", Range( 0 , 1)) = 1
		_Texture_12_AO_Power("Texture_12_AO_Power", Range( 0 , 1)) = 1
		_Texture_1_AO_Power("Texture_1_AO_Power", Range( 0 , 1)) = 1
		_Texture_11_AO_Power("Texture_11_AO_Power", Range( 0 , 1)) = 1
		_Texture_10_AO_Power("Texture_10_AO_Power", Range( 0 , 1)) = 1
		_Texture_13_AO_Power("Texture_13_AO_Power", Range( 0 , 1)) = 1
		_Texture_14_AO_Power("Texture_14_AO_Power", Range( 0 , 1)) = 1
		_Texture_2_AO_Power("Texture_2_AO_Power", Range( 0 , 1)) = 1
		_Texture_9_AO_Power("Texture_9_AO_Power", Range( 0 , 1)) = 1
		_Global_Normalmap_Power("Global_Normalmap_Power", Range( 0 , 10)) = 0
		_Texture_1_Tiling("Texture_1_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_2_Tiling("Texture_2_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_3_Tiling("Texture_3_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_4_Tiling("Texture_4_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_5_Tiling("Texture_5_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_6_Tiling("Texture_6_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_7_Tiling("Texture_7_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_8_Tiling("Texture_8_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_9_Tiling("Texture_9_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_10_Tiling("Texture_10_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_11_Tiling("Texture_11_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_12_Tiling("Texture_12_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_13_Tiling("Texture_13_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_14_Tiling("Texture_14_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_15_Tiling("Texture_15_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_16_Tiling("Texture_16_Tiling", Range( 0.0001 , 100)) = 15
		_Texture_1_Far_Multiplier("Texture_1_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_2_Far_Multiplier("Texture_2_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_3_Far_Multiplier("Texture_3_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_4_Far_Multiplier("Texture_4_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_5_Far_Multiplier("Texture_5_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_6_Far_Multiplier("Texture_6_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_7_Far_Multiplier("Texture_7_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_8_Far_Multiplier("Texture_8_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_9_Far_Multiplier("Texture_9_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_10_Far_Multiplier("Texture_10_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_11_Far_Multiplier("Texture_11_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_12_Far_Multiplier("Texture_12_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_13_Far_Multiplier("Texture_13_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_14_Far_Multiplier("Texture_14_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_15_Far_Multiplier("Texture_15_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_16_Far_Multiplier("Texture_16_Far_Multiplier", Range( 0 , 10)) = 3
		_Texture_1_Perlin_Power("Texture_1_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_2_Perlin_Power("Texture_2_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_3_Perlin_Power("Texture_3_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_4_Perlin_Power("Texture_4_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_5_Perlin_Power("Texture_5_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_6_Perlin_Power("Texture_6_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_7_Perlin_Power("Texture_7_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_8_Perlin_Power("Texture_8_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_9_Perlin_Power("Texture_9_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_10_Perlin_Power("Texture_10_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_11_Perlin_Power("Texture_11_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_12_Perlin_Power("Texture_12_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_13_Perlin_Power("Texture_13_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_14_Perlin_Power("Texture_14_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_15_Perlin_Power("Texture_15_Perlin_Power", Range( 0 , 2)) = 0
		_Texture_16_Perlin_Power("Texture_16_Perlin_Power", Range( 0 , 2)) = 0
		_Snow_Heightmap_Depth("Snow_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_1_Heightmap_Depth("Texture_1_Heightmap_Depth", Range( 0 , 10)) = 1
		_Snow_Height_Contrast("Snow_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_14_Height_Contrast("Texture_14_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_16_Height_Contrast("Texture_16_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_15_Height_Contrast("Texture_15_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_9_Height_Contrast("Texture_9_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_13_Height_Contrast("Texture_13_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_10_Height_Contrast("Texture_10_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_11_Height_Contrast("Texture_11_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_12_Height_Contrast("Texture_12_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_6_Height_Contrast("Texture_6_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_8_Height_Contrast("Texture_8_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_7_Height_Contrast("Texture_7_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_3_Height_Contrast("Texture_3_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_5_Height_Contrast("Texture_5_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_4_Height_Contrast("Texture_4_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_1_Height_Contrast("Texture_1_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_2_Height_Contrast("Texture_2_Height_Contrast", Range( 0 , 10)) = 1
		_Texture_2_Heightmap_Depth("Texture_2_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_3_Heightmap_Depth("Texture_3_Heightmap_Depth", Range( 0 , 10)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		_Texture_4_Heightmap_Depth("Texture_4_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_5_Heightmap_Depth("Texture_5_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_6_Heightmap_Depth("Texture_6_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_7_Heightmap_Depth("Texture_7_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_8_Tesselation_Depth("Texture_8_Tesselation_Depth", Range( 0 , 10)) = 1
		_Texture_9_Heightmap_Depth("Texture_9_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_10_Heightmap_Depth("Texture_10_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_11_Heightmap_Depth("Texture_11_Heightmap_Depth", Range( 0 , 10)) = 0
		_Texture_12_Heightmap_Depth("Texture_12_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_13_Heightmap_Depth("Texture_13_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_14_Heightmap_Depth("Texture_14_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_15_Heightmap_Depth("Texture_15_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_16_Heightmap_Depth("Texture_16_Heightmap_Depth", Range( 0 , 10)) = 1
		_Texture_3_Heightblend_Far("Texture_3_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_1_Heightblend_Far("Texture_1_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_4_Heightblend_Far("Texture_4_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_9_Heightblend_Far("Texture_9_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_15_Heightblend_Far("Texture_15_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_16_Heightblend_Far("Texture_16_Heightblend_Far", Range( 1 , 10)) = 5
		_Snow_Heightblend_Far("Snow_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_14_Heightblend_Far("Texture_14_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_13_Heightblend_Far("Texture_13_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_12_Heightblend_Far("Texture_12_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_16_Heightblend_Close("Texture_16_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_15_Heightblend_Close("Texture_15_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_11_Heightblend_Far("Texture_11_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_13_Heightblend_Close("Texture_13_Heightblend_Close", Range( 1 , 10)) = 5
		_Snow_Heightblend_Close("Snow_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_14_Heightblend_Close("Texture_14_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_12_Heightblend_Close("Texture_12_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_10_Heightblend_Far("Texture_10_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_11_Heightblend_Close("Texture_11_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_8_Heightblend_Far("Texture_8_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_10_Heightblend_Close("Texture_10_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_2_Heightblend_Far("Texture_2_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_9_Heightblend_Close("Texture_9_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_7_Heightblend_Far("Texture_7_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_8_Heightblend_Close("Texture_8_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_7_Heightblend_Close("Texture_7_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_6_Heightblend_Far("Texture_6_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_Snow_Noise_Index("Texture_Snow_Noise_Index", Int) = -1
		_Texture_5_Heightblend_Far("Texture_5_Heightblend_Far", Range( 1 , 10)) = 5
		_Texture_6_Heightblend_Close("Texture_6_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_3_Heightblend_Close("Texture_3_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_5_Heightblend_Close("Texture_5_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_2_Heightblend_Close("Texture_2_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_1_Heightblend_Close("Texture_1_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_4_Heightblend_Close("Texture_4_Heightblend_Close", Range( 1 , 10)) = 5
		_Texture_1_Geological_Power("Texture_1_Geological_Power", Range( 0 , 2)) = 1
		_Texture_2_Geological_Power("Texture_2_Geological_Power", Range( 0 , 2)) = 1
		_Texture_3_Geological_Power("Texture_3_Geological_Power", Range( 0 , 2)) = 1
		_Texture_4_Geological_Power("Texture_4_Geological_Power", Range( 0 , 2)) = 1
		_Texture_5_Geological_Power("Texture_5_Geological_Power", Range( 0 , 2)) = 1
		_Texture_6_Geological_Power("Texture_6_Geological_Power", Range( 0 , 2)) = 1
		_Texture_7_Geological_Power("Texture_7_Geological_Power", Range( 0 , 2)) = 1
		_Texture_8_Geological_Power("Texture_8_Geological_Power", Range( 0 , 2)) = 1
		_Texture_9_Geological_Power("Texture_9_Geological_Power", Range( 0 , 2)) = 1
		_Texture_10_Geological_Power("Texture_10_Geological_Power", Range( 0 , 2)) = 1
		_Texture_11_Geological_Power("Texture_11_Geological_Power", Range( 0 , 2)) = 1
		_Texture_12_Geological_Power("Texture_12_Geological_Power", Range( 0 , 2)) = 1
		_Texture_13_Geological_Power("Texture_13_Geological_Power", Range( 0 , 2)) = 1
		_Texture_14_Geological_Power("Texture_14_Geological_Power", Range( 0 , 2)) = 1
		_Texture_15_Geological_Power("Texture_15_Geological_Power", Range( 0 , 2)) = 1
		_Texture_16_Geological_Power("Texture_16_Geological_Power", Range( 0 , 2)) = 1
		_Snow_Specular("Snow_Specular", Range( 0 , 3)) = 1
		_Snow_Normal_Scale("Snow_Normal_Scale", Range( 0 , 5)) = 1
		_Snow_Blend_Normal("Snow_Blend_Normal", Range( 0 , 1)) = 0.8
		_Snow_Amount("Snow_Amount", Range( 0 , 2)) = 0
		_Snow_Tiling("Snow_Tiling", Range( 0.001 , 20)) = 15
		_Snow_Tiling_Far_Multiplier("Snow_Tiling_Far_Multiplier", Range( 0.001 , 20)) = 1
		_Snow_Perlin_Power("Snow_Perlin_Power", Range( 0 , 2)) = 0
		_Snow_Noise_Power("Snow_Noise_Power", Range( 0 , 1)) = 1
		_Snow_Noise_Tiling("Snow_Noise_Tiling", Range( 0.001 , 1)) = 0.02
		_Snow_Min_Height("Snow_Min_Height", Range( -1000 , 10000)) = -1000
		_Snow_Min_Height_Blending("Snow_Min_Height_Blending", Range( 0 , 500)) = 1
		_Snow_Maximum_Angle("Snow_Maximum_Angle", Range( 0.001 , 180)) = 30
		_Snow_Maximum_Angle_Hardness("Snow_Maximum_Angle_Hardness", Range( 0.001 , 10)) = 1
		_Texture_1_Snow_Reduction("Texture_1_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_2_Snow_Reduction("Texture_2_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_3_Snow_Reduction("Texture_3_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_4_Snow_Reduction("Texture_4_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_5_Snow_Reduction("Texture_5_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_6_Snow_Reduction("Texture_6_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_7_Snow_Reduction("Texture_7_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_8_Snow_Reduction("Texture_8_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_9_Snow_Reduction("Texture_9_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_10_Snow_Reduction("Texture_10_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_11_Snow_Reduction("Texture_11_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_Geological_Map("Texture_Geological_Map", 2D) = "white" {}
		_Snow_Color("Snow_Color", Vector) = (1,1,1,1)
		_Texture_12_Snow_Reduction("Texture_12_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_13_Snow_Reduction("Texture_13_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_14_Snow_Reduction("Texture_14_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_15_Snow_Reduction("Texture_15_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_16_Snow_Reduction("Texture_16_Snow_Reduction", Range( 0 , 1)) = 0
		_Texture_Array_Normal("Texture_Array_Normal", 2DArray ) = "" {}
		_Texture_Array_Albedo("Texture_Array_Albedo", 2DArray ) = "" {}
		_Texture_Array_Emission_AO("Texture_Array_Emission_AO", 2DArray ) = "" {}
		_Texture_Perlin_Normal_Index("Texture_Perlin_Normal_Index", Int) = -1
		_Texture_Snow_Index("Texture_Snow_Index", Int) = -1
		_Texture_Snow_Normal_Index("Texture_Snow_Normal_Index", Int) = -1
		_Snow_Ambient_Occlusion_Power("Snow_Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		_Ambient_Occlusion_Power("Ambient_Occlusion_Power", Range( 0 , 1)) = 1
		[Toggle]_Use_AO("Use_AO", Int) = 0
		_Texture_2_Triplanar("Texture_2_Triplanar", Range( 0 , 1)) = 0
		_Texture_16_Triplanar("Texture_16_Triplanar", Range( 0 , 1)) = 0
		_Texture_15_Triplanar("Texture_15_Triplanar", Range( 0 , 1)) = 0
		_Texture_10_Color("Texture_10_Color", Vector) = (1,1,1,1)
		_Texture_9_Color("Texture_9_Color", Vector) = (1,1,1,1)
		_Texture_12_Color("Texture_12_Color", Vector) = (1,1,1,1)
		_Texture_11_Color("Texture_11_Color", Vector) = (1,1,1,1)
		_Texture_13_Color("Texture_13_Color", Vector) = (1,1,1,1)
		_Texture_14_Color("Texture_14_Color", Vector) = (1,1,1,1)
		_Texture_16_Color("Texture_16_Color", Vector) = (1,1,1,1)
		_Texture_15_Color("Texture_15_Color", Vector) = (1,1,1,1)
		_Texture_8_Color("Texture_8_Color", Vector) = (1,1,1,1)
		_Texture_7_Color("Texture_7_Color", Vector) = (1,1,1,1)
		_Texture_4_Color("Texture_4_Color", Vector) = (1,1,1,1)
		_Texture_6_Color("Texture_6_Color", Vector) = (1,1,1,1)
		_Texture_5_Color("Texture_5_Color", Vector) = (1,1,1,1)
		_Texture_3_Color("Texture_3_Color", Vector) = (1,1,1,1)
		_Texture_1_Color("Texture_1_Color", Vector) = (1,1,1,1)
		_Texture_2_Color("Texture_2_Color", Vector) = (1,1,1,1)
		_Texture_12_Triplanar("Texture_12_Triplanar", Range( 0 , 1)) = 0
		_Texture_14_Triplanar("Texture_14_Triplanar", Range( 0 , 1)) = 0
		_Texture_13_Triplanar("Texture_13_Triplanar", Range( 0 , 1)) = 0
		_Texture_11_Triplanar("Texture_11_Triplanar", Range( 0 , 1)) = 0
		_Texture_9_Triplanar("Texture_9_Triplanar", Range( 0 , 1)) = 0
		_Texture_10_Triplanar("Texture_10_Triplanar", Range( 0 , 1)) = 0
		_Texture_8_Triplanar("Texture_8_Triplanar", Range( 0 , 1)) = 0
		[Toggle]_Use_AO_Texture("Use_AO_Texture", Int) = 0
		_Texture_7_Triplanar("Texture_7_Triplanar", Range( 0 , 1)) = 0
		_Texture_6_Triplanar("Texture_6_Triplanar", Range( 0 , 1)) = 0
		_Texture_5_Triplanar("Texture_5_Triplanar", Range( 0 , 1)) = 0
		_Texture_4_Triplanar("Texture_4_Triplanar", Range( 0 , 1)) = 0
		_Texture_3_Triplanar("Texture_3_Triplanar", Range( 0 , 1)) = 0
		_Texture_1_Triplanar("Texture_1_Triplanar", Range( 0 , 1)) = 0
		_Texture_1_Albedo_Index("Texture_1_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_2_Albedo_Index("Texture_2_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_3_Normal_Index("Texture_3_Normal_Index", Range( -1 , 100)) = -1
		_Texture_1_Emission_AO_Index("Texture_1_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_5_Normal_Index("Texture_5_Normal_Index", Range( -1 , 100)) = -1
		_Texture_6_Albedo_Index("Texture_6_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_6_Emission_AO_Index("Texture_6_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_8_Albedo_Index("Texture_8_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_16_Emission_AO_Index("Texture_16_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_16_Albedo_Index("Texture_16_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_14_Normal_Index("Texture_14_Normal_Index", Range( -1 , 100)) = -1
		_Texture_13_Albedo_Index("Texture_13_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_12_Albedo_Index("Texture_12_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_11_Emission_AO_Index("Texture_11_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_10_Albedo_Index("Texture_10_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_9_Albedo_Index("Texture_9_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_9_Normal_Index("Texture_9_Normal_Index", Range( -1 , 100)) = -1
		_Texture_11_Albedo_Index("Texture_11_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_9_Emission_AO_Index("Texture_9_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_10_Normal_Index("Texture_10_Normal_Index", Range( -1 , 100)) = -1
		_Texture_10_Emission_AO_Index("Texture_10_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_11_Normal_Index("Texture_11_Normal_Index", Range( -1 , 100)) = -1
		_Texture_13_Normal_Index("Texture_13_Normal_Index", Range( -1 , 100)) = -1
		_Texture_12_Normal_Index("Texture_12_Normal_Index", Range( -1 , 100)) = -1
		_Texture_12_Emission_AO_Index("Texture_12_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_13_Emission_AO_Index("Texture_13_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_14_Albedo_Index("Texture_14_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_14_Emission_AO_Index("Texture_14_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_15_Albedo_Index("Texture_15_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_15_Normal_Index("Texture_15_Normal_Index", Range( -1 , 100)) = -1
		_Texture_15_Emission_AO_Index("Texture_15_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_16_Normal_Index("Texture_16_Normal_Index", Range( -1 , 100)) = -1
		_Texture_7_Normal_Index("Texture_7_Normal_Index", Range( -1 , 100)) = -1
		_Texture_8_Emission_AO_Index("Texture_8_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_8_Normal_Index("Texture_8_Normal_Index", Range( -1 , 100)) = -1
		_Texture_7_Emission_AO_Index("Texture_7_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_7_Albedo_Index("Texture_7_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_6_Normal_Index("Texture_6_Normal_Index", Range( -1 , 100)) = -1
		_Texture_5_Emission_AO_Index("Texture_5_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_4_Emission_AO_Index("Texture_4_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_4_Normal_Index("Texture_4_Normal_Index", Range( -1 , 100)) = -1
		_Texture_5_Albedo_Index("Texture_5_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_4_Albedo_Index("Texture_4_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_3_Emission_AO_Index("Texture_3_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_3_Albedo_Index("Texture_3_Albedo_Index", Range( -1 , 100)) = -1
		_Texture_2_Emission_AO_Index("Texture_2_Emission_AO_Index", Range( -1 , 100)) = -1
		_Texture_2_Normal_Index("Texture_2_Normal_Index", Range( -1 , 100)) = -1
		_Texture_1_Normal_Index("Texture_1_Normal_Index", Range( -1 , 100)) = -1
		_Texture_1_Normal_Power("Texture_1_Normal_Power", Range( 0 , 2)) = 1
		_Texture_2_Normal_Power("Texture_2_Normal_Power", Range( 0 , 2)) = 1
		_Texture_3_Normal_Power("Texture_3_Normal_Power", Range( 0 , 2)) = 1
		_Texture_4_Normal_Power("Texture_4_Normal_Power", Range( 0 , 2)) = 1
		_Texture_5_Normal_Power("Texture_5_Normal_Power", Range( 0 , 2)) = 1
		_Texture_6_Normal_Power("Texture_6_Normal_Power", Range( 0 , 2)) = 1
		_Texture_7_Normal_Power("Texture_7_Normal_Power", Range( 0 , 2)) = 1
		_Texture_8_Normal_Power("Texture_8_Normal_Power", Range( 0 , 2)) = 1
		_Texture_9_Normal_Power("Texture_9_Normal_Power", Range( 0 , 2)) = 1
		_Texture_10_Normal_Power("Texture_10_Normal_Power", Range( 0 , 2)) = 1
		_Texture_11_Normal_Power("Texture_11_Normal_Power", Range( 0 , 2)) = 1
		_Texture_12_Normal_Power("Texture_12_Normal_Power", Range( 0 , 2)) = 1
		_Texture_13_Normal_Powerr("Texture_13_Normal_Powerr", Range( 0 , 2)) = 1
		_Texture_14_Normal_Power("Texture_14_Normal_Power", Range( 0 , 2)) = 1
		_Texture_15_Normal_Power("Texture_15_Normal_Power", Range( 0 , 2)) = 1
		_Texture_16_Normal_Power("Texture_16_Normal_Power", Range( 0 , 2)) = 1
		_Texture_Splat_1("Texture_Splat_1", 2D) = "black" {}
		_Texture_Splat_2("Texture_Splat_2", 2D) = "black" {}
		_Texture_Splat_3("Texture_Splat_3", 2D) = "black" {}
		_Texture_Splat_4("Texture_Splat_4", 2D) = "black" {}
		_Global_Normal_Map("Global_Normal_Map", 2D) = "bump" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+100" }
		Cull Back
		ZTest LEqual
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.5
		#pragma multi_compile __ _USE_AO_TEXTURE_ON
		#pragma multi_compile __ _USE_AO_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float2 texcoord_0;
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv_texcoord;
		};

		uniform fixed _Perlin_Normal_Tiling_Close;
		uniform int _Texture_Perlin_Normal_Index;
		uniform fixed _Perlin_Normal_Power_Close;
		uniform fixed _Perlin_Normal_Tiling_Far;
		uniform fixed _Perlin_Normal_Power;
		uniform fixed _UV_Mix_Start_Distance;
		uniform fixed _UV_Mix_Power;
		uniform fixed _Texture_16_Perlin_Power;
		uniform sampler2D _Texture_Splat_4;
		uniform fixed _Texture_15_Perlin_Power;
		uniform fixed _Texture_14_Perlin_Power;
		uniform fixed _Texture_13_Perlin_Power;
		uniform fixed _Texture_12_Perlin_Power;
		uniform sampler2D _Texture_Splat_3;
		uniform fixed _Texture_11_Perlin_Power;
		uniform fixed _Texture_10_Perlin_Power;
		uniform fixed _Texture_9_Perlin_Power;
		uniform fixed _Texture_8_Perlin_Power;
		uniform sampler2D _Texture_Splat_2;
		uniform fixed _Texture_7_Perlin_Power;
		uniform fixed _Texture_6_Perlin_Power;
		uniform fixed _Texture_5_Perlin_Power;
		uniform fixed _Texture_1_Perlin_Power;
		uniform sampler2D _Texture_Splat_1;
		uniform fixed _Texture_2_Perlin_Power;
		uniform fixed _Texture_4_Perlin_Power;
		uniform fixed _Texture_3_Perlin_Power;
		uniform fixed _Snow_Perlin_Power;
		uniform fixed _Texture_1_Normal_Index;
		uniform fixed _Texture_1_Triplanar;
		uniform fixed _Texture_1_Tiling;
		uniform fixed _Texture_1_Far_Multiplier;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Normal );
		uniform fixed _Texture_1_Height_Contrast;
		uniform fixed _Texture_1_Heightmap_Depth;
		uniform fixed _Texture_2_Heightmap_Depth;
		uniform fixed _Texture_2_Normal_Index;
		uniform fixed _Texture_2_Triplanar;
		uniform fixed _Texture_2_Tiling;
		uniform fixed _Texture_2_Far_Multiplier;
		uniform fixed _Texture_2_Height_Contrast;
		uniform fixed _Texture_3_Heightmap_Depth;
		uniform fixed _Texture_3_Normal_Index;
		uniform fixed _Texture_3_Triplanar;
		uniform fixed _Texture_3_Tiling;
		uniform fixed _Texture_3_Far_Multiplier;
		uniform fixed _Texture_3_Height_Contrast;
		uniform fixed _Texture_4_Heightmap_Depth;
		uniform fixed _Texture_4_Normal_Index;
		uniform fixed _Texture_4_Triplanar;
		uniform fixed _Texture_4_Tiling;
		uniform fixed _Texture_4_Far_Multiplier;
		uniform fixed _Texture_4_Height_Contrast;
		uniform fixed _Texture_5_Heightmap_Depth;
		uniform fixed _Texture_5_Normal_Index;
		uniform fixed _Texture_5_Triplanar;
		uniform fixed _Texture_5_Tiling;
		uniform fixed _Texture_5_Far_Multiplier;
		uniform fixed _Texture_5_Height_Contrast;
		uniform fixed _Texture_6_Heightmap_Depth;
		uniform fixed _Texture_6_Normal_Index;
		uniform fixed _Texture_6_Triplanar;
		uniform fixed _Texture_6_Tiling;
		uniform fixed _Texture_6_Far_Multiplier;
		uniform fixed _Texture_6_Height_Contrast;
		uniform fixed _Texture_7_Heightmap_Depth;
		uniform fixed _Texture_7_Normal_Index;
		uniform fixed _Texture_7_Triplanar;
		uniform fixed _Texture_7_Tiling;
		uniform fixed _Texture_7_Far_Multiplier;
		uniform fixed _Texture_7_Height_Contrast;
		uniform fixed _Texture_8_Tesselation_Depth;
		uniform fixed _Texture_8_Normal_Index;
		uniform fixed _Texture_8_Triplanar;
		uniform fixed _Texture_8_Tiling;
		uniform fixed _Texture_8_Far_Multiplier;
		uniform fixed _Texture_8_Height_Contrast;
		uniform fixed _Texture_9_Heightmap_Depth;
		uniform fixed _Texture_9_Normal_Index;
		uniform fixed _Texture_9_Triplanar;
		uniform fixed _Texture_9_Tiling;
		uniform fixed _Texture_9_Far_Multiplier;
		uniform fixed _Texture_9_Height_Contrast;
		uniform fixed _Texture_10_Heightmap_Depth;
		uniform fixed _Texture_10_Normal_Index;
		uniform fixed _Texture_10_Triplanar;
		uniform fixed _Texture_10_Tiling;
		uniform fixed _Texture_10_Far_Multiplier;
		uniform fixed _Texture_10_Height_Contrast;
		uniform fixed _Texture_11_Heightmap_Depth;
		uniform fixed _Texture_11_Normal_Index;
		uniform fixed _Texture_11_Triplanar;
		uniform fixed _Texture_11_Tiling;
		uniform fixed _Texture_11_Far_Multiplier;
		uniform fixed _Texture_11_Height_Contrast;
		uniform fixed _Texture_12_Heightmap_Depth;
		uniform fixed _Texture_12_Normal_Index;
		uniform fixed _Texture_12_Triplanar;
		uniform fixed _Texture_12_Tiling;
		uniform fixed _Texture_12_Far_Multiplier;
		uniform fixed _Texture_12_Height_Contrast;
		uniform fixed _Texture_12_Heightblend_Close;
		uniform fixed _Texture_12_Heightblend_Far;
		uniform fixed _Texture_13_Heightmap_Depth;
		uniform fixed _Texture_13_Normal_Index;
		uniform fixed _Texture_13_Triplanar;
		uniform fixed _Texture_13_Tiling;
		uniform fixed _Texture_13_Far_Multiplier;
		uniform fixed _Texture_13_Height_Contrast;
		uniform fixed _Texture_14_Heightmap_Depth;
		uniform fixed _Texture_14_Normal_Index;
		uniform fixed _Texture_14_Triplanar;
		uniform fixed _Texture_14_Tiling;
		uniform fixed _Texture_14_Far_Multiplier;
		uniform fixed _Texture_14_Height_Contrast;
		uniform fixed _Texture_15_Heightmap_Depth;
		uniform fixed _Texture_15_Normal_Index;
		uniform fixed _Texture_15_Triplanar;
		uniform fixed _Texture_15_Tiling;
		uniform fixed _Texture_15_Far_Multiplier;
		uniform fixed _Texture_15_Height_Contrast;
		uniform fixed _Texture_16_Heightmap_Depth;
		uniform fixed _Texture_16_Normal_Index;
		uniform fixed _Texture_16_Triplanar;
		uniform fixed _Texture_16_Tiling;
		uniform fixed _Texture_16_Far_Multiplier;
		uniform fixed _Texture_16_Height_Contrast;
		uniform fixed _Snow_Tiling;
		uniform int _Texture_Snow_Normal_Index;
		uniform fixed _Snow_Height_Contrast;
		uniform fixed _Snow_Heightmap_Depth;
		uniform fixed _Snow_Tiling_Far_Multiplier;
		uniform fixed _Snow_Amount;
		uniform fixed _Snow_Noise_Tiling;
		uniform int _Texture_Snow_Noise_Index;
		uniform fixed _Snow_Noise_Power;
		uniform fixed _Snow_Maximum_Angle_Hardness;
		uniform fixed _Snow_Maximum_Angle;
		uniform fixed _Snow_Min_Height;
		uniform fixed _Snow_Min_Height_Blending;
		uniform fixed _Texture_16_Snow_Reduction;
		uniform fixed _Texture_15_Snow_Reduction;
		uniform fixed _Texture_13_Snow_Reduction;
		uniform fixed _Texture_12_Snow_Reduction;
		uniform fixed _Texture_11_Snow_Reduction;
		uniform fixed _Texture_9_Snow_Reduction;
		uniform fixed _Texture_8_Snow_Reduction;
		uniform fixed _Texture_7_Snow_Reduction;
		uniform fixed _Texture_5_Snow_Reduction;
		uniform fixed _Texture_1_Snow_Reduction;
		uniform fixed _Texture_2_Snow_Reduction;
		uniform fixed _Texture_3_Snow_Reduction;
		uniform fixed _Texture_4_Snow_Reduction;
		uniform fixed _Texture_6_Snow_Reduction;
		uniform fixed _Texture_10_Snow_Reduction;
		uniform fixed _Texture_14_Snow_Reduction;
		uniform fixed _Snow_Heightblend_Close;
		uniform fixed _Snow_Heightblend_Far;
		uniform fixed _Texture_13_Heightblend_Close;
		uniform fixed _Texture_13_Heightblend_Far;
		uniform fixed _Texture_14_Heightblend_Close;
		uniform fixed _Texture_14_Heightblend_Far;
		uniform fixed _Texture_15_Heightblend_Close;
		uniform fixed _Texture_15_Heightblend_Far;
		uniform fixed _Texture_16_Heightblend_Close;
		uniform fixed _Texture_16_Heightblend_Far;
		uniform fixed _Texture_9_Heightblend_Close;
		uniform fixed _Texture_9_Heightblend_Far;
		uniform fixed _Texture_10_Heightblend_Close;
		uniform fixed _Texture_10_Heightblend_Far;
		uniform fixed _Texture_11_Heightblend_Close;
		uniform fixed _Texture_11_Heightblend_Far;
		uniform fixed _Texture_5_Heightblend_Close;
		uniform fixed _Texture_5_Heightblend_Far;
		uniform fixed _Texture_6_Heightblend_Close;
		uniform fixed _Texture_6_Heightblend_Far;
		uniform fixed _Texture_7_Heightblend_Close;
		uniform fixed _Texture_7_Heightblend_Far;
		uniform fixed _Texture_8_Heightblend_Close;
		uniform fixed _Texture_8_Heightblend_Far;
		uniform fixed _Texture_1_Heightblend_Close;
		uniform fixed _Texture_1_Heightblend_Far;
		uniform fixed _Texture_2_Heightblend_Close;
		uniform fixed _Texture_2_Heightblend_Far;
		uniform fixed _Texture_3_Heightblend_Close;
		uniform fixed _Texture_3_Heightblend_Far;
		uniform fixed _Texture_4_Heightblend_Close;
		uniform fixed _Texture_4_Heightblend_Far;
		uniform fixed _Texture_1_Normal_Power;
		uniform fixed _Texture_2_Normal_Power;
		uniform fixed _Texture_3_Normal_Power;
		uniform fixed _Texture_4_Normal_Power;
		uniform fixed _Texture_5_Normal_Power;
		uniform fixed _Texture_6_Normal_Power;
		uniform fixed _Texture_7_Normal_Power;
		uniform fixed _Texture_8_Normal_Power;
		uniform fixed _Texture_9_Normal_Power;
		uniform fixed _Texture_10_Normal_Power;
		uniform fixed _Texture_11_Normal_Power;
		uniform fixed _Texture_12_Normal_Power;
		uniform fixed _Texture_13_Normal_Powerr;
		uniform fixed _Texture_14_Normal_Power;
		uniform fixed _Texture_15_Normal_Power;
		uniform fixed _Texture_16_Normal_Power;
		uniform fixed _Snow_Normal_Scale;
		uniform fixed _Snow_Blend_Normal;
		uniform fixed _Global_Normalmap_Power;
		uniform sampler2D _Global_Normal_Map;
		uniform fixed _Texture_1_Albedo_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Albedo );
		uniform fixed4 _Texture_1_Color;
		uniform fixed _Texture_2_Albedo_Index;
		uniform fixed4 _Texture_2_Color;
		uniform fixed _Texture_3_Albedo_Index;
		uniform fixed4 _Texture_3_Color;
		uniform fixed _Texture_4_Albedo_Index;
		uniform fixed4 _Texture_4_Color;
		uniform fixed _Texture_5_Albedo_Index;
		uniform fixed4 _Texture_5_Color;
		uniform fixed _Texture_6_Albedo_Index;
		uniform fixed4 _Texture_6_Color;
		uniform fixed _Texture_7_Albedo_Index;
		uniform fixed4 _Texture_7_Color;
		uniform fixed _Texture_8_Albedo_Index;
		uniform fixed4 _Texture_8_Color;
		uniform fixed _Texture_9_Albedo_Index;
		uniform fixed4 _Texture_9_Color;
		uniform fixed _Texture_10_Albedo_Index;
		uniform fixed4 _Texture_10_Color;
		uniform fixed _Texture_11_Albedo_Index;
		uniform fixed4 _Texture_11_Color;
		uniform fixed _Texture_12_Albedo_Index;
		uniform fixed4 _Texture_12_Color;
		uniform fixed _Texture_13_Albedo_Index;
		uniform fixed4 _Texture_13_Color;
		uniform fixed _Texture_14_Albedo_Index;
		uniform fixed4 _Texture_14_Color;
		uniform fixed _Texture_15_Albedo_Index;
		uniform fixed4 _Texture_15_Color;
		uniform fixed _Texture_16_Albedo_Index;
		uniform fixed4 _Texture_16_Color;
		uniform sampler2D _Texture_Geological_Map;
		uniform fixed _Geological_Map_Offset_Close;
		uniform fixed _Geological_Tiling_Close;
		uniform fixed _Geological_Map_Close_Power;
		uniform fixed _Geological_Tiling_Far;
		uniform fixed _Geological_Map_Offset_Far;
		uniform fixed _Geological_Map_Far_Power;
		uniform fixed _Texture_16_Geological_Power;
		uniform fixed _Texture_15_Geological_Power;
		uniform fixed _Texture_14_Geological_Power;
		uniform fixed _Texture_13_Geological_Power;
		uniform fixed _Texture_12_Geological_Power;
		uniform fixed _Texture_11_Geological_Power;
		uniform fixed _Texture_10_Geological_Power;
		uniform fixed _Texture_9_Geological_Power;
		uniform fixed _Texture_8_Geological_Power;
		uniform fixed _Texture_7_Geological_Power;
		uniform fixed _Texture_6_Geological_Power;
		uniform fixed _Texture_5_Geological_Power;
		uniform fixed _Texture_1_Geological_Power;
		uniform fixed _Texture_2_Geological_Power;
		uniform fixed _Texture_4_Geological_Power;
		uniform fixed _Texture_3_Geological_Power;
		uniform int _Texture_Snow_Index;
		uniform fixed4 _Snow_Color;
		uniform fixed _Terrain_Specular;
		uniform fixed _Snow_Specular;
		uniform fixed _Terrain_Smoothness;
		uniform fixed _Texture_1_Emission_AO_Index;
		uniform UNITY_DECLARE_TEX2DARRAY( _Texture_Array_Emission_AO );
		uniform fixed _Texture_1_AO_Power;
		uniform fixed _Texture_2_Emission_AO_Index;
		uniform fixed _Texture_2_AO_Power;
		uniform fixed _Texture_3_Emission_AO_Index;
		uniform fixed _Texture_3_AO_Power;
		uniform fixed _Texture_4_Emission_AO_Index;
		uniform fixed _Texture_4_AO_Power;
		uniform fixed _Texture_5_Emission_AO_Index;
		uniform fixed _Texture_5_AO_Power;
		uniform fixed _Texture_6_Emission_AO_Index;
		uniform fixed _Texture_6_AO_Power;
		uniform fixed _Texture_7_Emission_AO_Index;
		uniform float4 _Texture_Array_Emission_AO_ST;
		uniform fixed _Texture_7_AO_Power;
		uniform fixed _Texture_8_Emission_AO_Index;
		uniform fixed _Texture_8_AO_Power;
		uniform fixed _Texture_9_Emission_AO_Index;
		uniform fixed _Texture_9_AO_Power;
		uniform fixed _Texture_10_Emission_AO_Index;
		uniform fixed _Texture_10_AO_Power;
		uniform fixed _Texture_11_Emission_AO_Index;
		uniform fixed _Texture_11_AO_Power;
		uniform fixed _Texture_12_Emission_AO_Index;
		uniform fixed _Texture_12_AO_Power;
		uniform fixed _Texture_13_Emission_AO_Index;
		uniform fixed _Texture_13_AO_Power;
		uniform fixed _Texture_14_Emission_AO_Index;
		uniform fixed _Texture_14_AO_Power;
		uniform fixed _Texture_15_Emission_AO_Index;
		uniform fixed _Texture_15_AO_Power;
		uniform fixed _Texture_16_Emission_AO_Index;
		uniform fixed _Texture_16_AO_Power;
		uniform fixed _Snow_Ambient_Occlusion_Power;
		uniform fixed _Ambient_Occlusion_Power;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			o.texcoord_0.xy = v.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
			float3 normalWorld = UnityObjectToWorldNormal( v.normal );
			v.vertex.xyz += 0.0;
			 v.tangent.xyz=cross( normalWorld , fixed3(0,0,1) );
			 v.tangent.w = -1;//;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 ase_worldPos = i.worldPos;
			float2 appendResult3897 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_0 = (( 1.0 / _Perlin_Normal_Tiling_Close )).xx;
			float4 texArray6256 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_0 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g578 = float2( texArray6256.x , texArray6256.y );
			float2 temp_cast_2 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_output_4_0_g578 = ( ( ( appendResult11_g578 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_2 );
			float2 temp_cast_3 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_cast_4 = (_Perlin_Normal_Power_Close).xx;
			float2 temp_cast_5 = (_Perlin_Normal_Power_Close).xx;
			float temp_output_9_0_g578 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g578 , temp_output_4_0_g578 ) ) ) );
			float3 appendResult10_g578 = float3( temp_output_4_0_g578.x , temp_output_4_0_g578.y , temp_output_9_0_g578 );
			fixed2 temp_cast_6 = (( 1.0 / _Perlin_Normal_Tiling_Far )).xx;
			float4 texArray4374 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3897 * temp_cast_6 ), (float)_Texture_Perlin_Normal_Index)  );
			float2 appendResult11_g579 = float2( texArray4374.x , texArray4374.y );
			float2 temp_cast_8 = (_Perlin_Normal_Power).xx;
			float2 temp_output_4_0_g579 = ( ( ( appendResult11_g579 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_8 );
			float2 temp_cast_9 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_10 = (_Perlin_Normal_Power).xx;
			float2 temp_cast_11 = (_Perlin_Normal_Power).xx;
			float temp_output_9_0_g579 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g579 , temp_output_4_0_g579 ) ) ) );
			float3 appendResult10_g579 = float3( temp_output_4_0_g579.x , temp_output_4_0_g579.y , temp_output_9_0_g579 );
			fixed UVmixDistance = clamp( pow( ( distance( ase_worldPos , _WorldSpaceCameraPos ) / _UV_Mix_Start_Distance ) , _UV_Mix_Power ) , 0.0 , 1.0 );
			fixed4 tex2DNode4371 = tex2D( _Texture_Splat_4, i.texcoord_0 );
			fixed Splat4_A = tex2DNode4371.a;
			fixed Splat4_B = tex2DNode4371.b;
			fixed Splat4_G = tex2DNode4371.g;
			fixed Splat4_R = tex2DNode4371.r;
			fixed4 tex2DNode4370 = tex2D( _Texture_Splat_3, i.texcoord_0 );
			fixed Splat3_A = tex2DNode4370.a;
			fixed Splat3_B = tex2DNode4370.b;
			fixed Splat3_G = tex2DNode4370.g;
			fixed Splat3_R = tex2DNode4370.r;
			fixed4 tex2DNode4369 = tex2D( _Texture_Splat_2, i.texcoord_0 );
			fixed Splat2_A = tex2DNode4369.a;
			fixed Splat2_B = tex2DNode4369.b;
			fixed Splat2_G = tex2DNode4369.g;
			fixed Splat2_R = tex2DNode4369.r;
			fixed4 tex2DNode4368 = tex2D( _Texture_Splat_1, i.texcoord_0 );
			fixed Splat1_R = tex2DNode4368.r;
			fixed Splat1_G = tex2DNode4368.g;
			fixed Splat1_A = tex2DNode4368.a;
			fixed Splat1_B = tex2DNode4368.b;
			float2 appendResult1998 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 Top_Bottom = appendResult1998;
			float2 appendResult3284 = float2( ( 1.0 / _Texture_1_Tiling ) , ( 1.0 / _Texture_1_Tiling ) );
			float4 texArray3300 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Normal_Index)  );
			fixed2 temp_cast_12 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5491 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_12 ), _Texture_1_Normal_Index)  );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			fixed3 BlendComponents = clamp( pow( ( ase_worldNormal * ase_worldNormal ) , 25.0 ) , float3( -1,-1,-1 ) , float3( 1,1,1 ) );
			float2 appendResult879 = float2( ase_worldPos.z , ase_worldPos.y );
			fixed2 Front_Back = appendResult879;
			float4 texArray3299 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3284 ), _Texture_1_Normal_Index)  );
			float2 appendResult2002 = float2( ase_worldPos.x , ase_worldPos.y );
			fixed2 Left_Right = appendResult2002;
			float4 texArray3301 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3284 ), _Texture_1_Normal_Index)  );
			float3 weightedBlendVar6391 = BlendComponents;
			float weightedAvg6391 = ( ( weightedBlendVar6391.x*texArray3299.w + weightedBlendVar6391.y*texArray3300.w + weightedBlendVar6391.z*texArray3301.w )/( weightedBlendVar6391.x + weightedBlendVar6391.y + weightedBlendVar6391.z ) );
			fixed2 temp_cast_13 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3284 ) / temp_cast_13 ), _Texture_1_Normal_Index)  );
			fixed2 temp_cast_14 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray5489 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3284 ) / temp_cast_14 ), _Texture_1_Normal_Index)  );
			float3 weightedBlendVar6394 = BlendComponents;
			float weightedAvg6394 = ( ( weightedBlendVar6394.x*texArray5486.w + weightedBlendVar6394.y*texArray5491.w + weightedBlendVar6394.z*texArray5489.w )/( weightedBlendVar6394.x + weightedBlendVar6394.y + weightedBlendVar6394.z ) );
			fixed ifLocalVar6609 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6609 = lerp( texArray3300.w , texArray5491.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6609 = lerp( weightedAvg6391 , weightedAvg6394 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6609 = lerp( texArray3300.w , texArray5491.w , UVmixDistance );
			fixed ifLocalVar7731 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar7731 = ifLocalVar6609;
			fixed Texture_1_H = ifLocalVar7731;
			float2 appendResult3349 = float2( ( 1.0 / _Texture_2_Tiling ) , ( 1.0 / _Texture_2_Tiling ) );
			float4 texArray3350 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Normal_Index)  );
			fixed2 temp_cast_15 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5533 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_15 ), _Texture_2_Normal_Index)  );
			float4 texArray3384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3349 ), _Texture_2_Normal_Index)  );
			float4 texArray3351 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3349 ), _Texture_2_Normal_Index)  );
			float3 weightedBlendVar6399 = BlendComponents;
			float weightedAvg6399 = ( ( weightedBlendVar6399.x*texArray3384.w + weightedBlendVar6399.y*texArray3350.w + weightedBlendVar6399.z*texArray3351.w )/( weightedBlendVar6399.x + weightedBlendVar6399.y + weightedBlendVar6399.z ) );
			fixed2 temp_cast_16 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5530 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3349 ) / temp_cast_16 ), _Texture_2_Normal_Index)  );
			fixed2 temp_cast_17 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray5532 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3349 ) / temp_cast_17 ), _Texture_2_Normal_Index)  );
			float3 weightedBlendVar6400 = BlendComponents;
			float weightedAvg6400 = ( ( weightedBlendVar6400.x*texArray5530.w + weightedBlendVar6400.y*texArray5533.w + weightedBlendVar6400.z*texArray5532.w )/( weightedBlendVar6400.x + weightedBlendVar6400.y + weightedBlendVar6400.z ) );
			fixed ifLocalVar6614 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6614 = lerp( texArray3350.w , texArray5533.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6614 = lerp( weightedAvg6399 , weightedAvg6400 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6614 = lerp( texArray3350.w , texArray5533.w , UVmixDistance );
			fixed ifLocalVar7734 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar7734 = ifLocalVar6614;
			fixed Texture_2_H = ifLocalVar7734;
			float2 appendResult3415 = float2( ( 1.0 / _Texture_3_Tiling ) , ( 1.0 / _Texture_3_Tiling ) );
			float4 texArray3416 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Normal_Index)  );
			fixed2 temp_cast_18 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_18 ), _Texture_3_Normal_Index)  );
			float4 texArray3445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3415 ), _Texture_3_Normal_Index)  );
			float4 texArray3417 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3415 ), _Texture_3_Normal_Index)  );
			float3 weightedBlendVar6406 = BlendComponents;
			float weightedAvg6406 = ( ( weightedBlendVar6406.x*texArray3445.w + weightedBlendVar6406.y*texArray3416.w + weightedBlendVar6406.z*texArray3417.w )/( weightedBlendVar6406.x + weightedBlendVar6406.y + weightedBlendVar6406.z ) );
			fixed2 temp_cast_19 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5560 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3415 ) / temp_cast_19 ), _Texture_3_Normal_Index)  );
			fixed2 temp_cast_20 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray5572 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3415 ) / temp_cast_20 ), _Texture_3_Normal_Index)  );
			float3 weightedBlendVar6407 = BlendComponents;
			float weightedAvg6407 = ( ( weightedBlendVar6407.x*texArray5560.w + weightedBlendVar6407.y*texArray5586.w + weightedBlendVar6407.z*texArray5572.w )/( weightedBlendVar6407.x + weightedBlendVar6407.y + weightedBlendVar6407.z ) );
			fixed ifLocalVar6620 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6620 = lerp( texArray3416.w , texArray5586.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6620 = lerp( weightedAvg6406 , weightedAvg6407 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6620 = lerp( texArray3416.w , texArray5586.w , UVmixDistance );
			fixed ifLocalVar7736 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Index > -1.0 )
				ifLocalVar7736 = ifLocalVar6620;
			fixed Texture_3_H = ifLocalVar7736;
			float2 appendResult3482 = float2( ( 1.0 / _Texture_4_Tiling ) , ( 1.0 / _Texture_4_Tiling ) );
			float4 texArray3483 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Normal_Index)  );
			fixed2 temp_cast_21 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5615 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_21 ), _Texture_4_Normal_Index)  );
			float4 texArray3512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult3482 ), _Texture_4_Normal_Index)  );
			float4 texArray3484 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult3482 ), _Texture_4_Normal_Index)  );
			float3 weightedBlendVar6413 = BlendComponents;
			float weightedAvg6413 = ( ( weightedBlendVar6413.x*texArray3512.w + weightedBlendVar6413.y*texArray3483.w + weightedBlendVar6413.z*texArray3484.w )/( weightedBlendVar6413.x + weightedBlendVar6413.y + weightedBlendVar6413.z ) );
			fixed2 temp_cast_22 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5596 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult3482 ) / temp_cast_22 ), _Texture_4_Normal_Index)  );
			fixed2 temp_cast_23 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray5604 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult3482 ) / temp_cast_23 ), _Texture_4_Normal_Index)  );
			float3 weightedBlendVar6414 = BlendComponents;
			float weightedAvg6414 = ( ( weightedBlendVar6414.x*texArray5596.w + weightedBlendVar6414.y*texArray5615.w + weightedBlendVar6414.z*texArray5604.w )/( weightedBlendVar6414.x + weightedBlendVar6414.y + weightedBlendVar6414.z ) );
			fixed ifLocalVar6626 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6626 = lerp( texArray3483.w , texArray5615.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6626 = lerp( weightedAvg6413 , weightedAvg6414 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6626 = lerp( texArray3483.w , texArray5615.w , UVmixDistance );
			fixed ifLocalVar7738 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Index > -1.0 )
				ifLocalVar7738 = ifLocalVar6626;
			fixed Texture_4_H = ifLocalVar7738;
			float4 layeredBlendVar7775 = tex2DNode4368;
			float layeredBlend7775 = ( lerp( lerp( lerp( lerp( 0.0 , ( pow( Texture_1_H , _Texture_1_Height_Contrast ) * _Texture_1_Heightmap_Depth ) , layeredBlendVar7775.x ) , ( _Texture_2_Heightmap_Depth * pow( Texture_2_H , _Texture_2_Height_Contrast ) ) , layeredBlendVar7775.y ) , ( _Texture_3_Heightmap_Depth * pow( Texture_3_H , _Texture_3_Height_Contrast ) ) , layeredBlendVar7775.z ) , ( _Texture_4_Heightmap_Depth * pow( Texture_4_H , _Texture_4_Height_Contrast ) ) , layeredBlendVar7775.w ) );
			float2 appendResult4399 = float2( ( 1.0 / _Texture_5_Tiling ) , ( 1.0 / _Texture_5_Tiling ) );
			float4 texArray4424 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Normal_Index)  );
			fixed2 temp_cast_24 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5655 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_24 ), _Texture_5_Normal_Index)  );
			float4 texArray4417 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4399 ), _Texture_5_Normal_Index)  );
			float4 texArray4422 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4399 ), _Texture_5_Normal_Index)  );
			float3 weightedBlendVar6420 = BlendComponents;
			float weightedAvg6420 = ( ( weightedBlendVar6420.x*texArray4417.w + weightedBlendVar6420.y*texArray4424.w + weightedBlendVar6420.z*texArray4422.w )/( weightedBlendVar6420.x + weightedBlendVar6420.y + weightedBlendVar6420.z ) );
			fixed2 temp_cast_25 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5636 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4399 ) / temp_cast_25 ), _Texture_5_Normal_Index)  );
			fixed2 temp_cast_26 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray5644 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4399 ) / temp_cast_26 ), _Texture_5_Normal_Index)  );
			float3 weightedBlendVar6421 = BlendComponents;
			float weightedAvg6421 = ( ( weightedBlendVar6421.x*texArray5636.w + weightedBlendVar6421.y*texArray5655.w + weightedBlendVar6421.z*texArray5644.w )/( weightedBlendVar6421.x + weightedBlendVar6421.y + weightedBlendVar6421.z ) );
			fixed ifLocalVar6632 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6632 = lerp( texArray4424.w , texArray5655.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6632 = lerp( weightedAvg6420 , weightedAvg6421 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6632 = lerp( texArray4424.w , texArray5655.w , UVmixDistance );
			fixed ifLocalVar7742 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Index > -1.0 )
				ifLocalVar7742 = ifLocalVar6632;
			fixed Texture_5_H = ifLocalVar7742;
			float2 appendResult4471 = float2( ( 1.0 / _Texture_6_Tiling ) , ( 1.0 / _Texture_6_Tiling ) );
			float4 texArray4493 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Normal_Index)  );
			fixed2 temp_cast_27 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5695 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_27 ), _Texture_6_Normal_Index)  );
			float4 texArray4486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4471 ), _Texture_6_Normal_Index)  );
			float4 texArray4491 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4471 ), _Texture_6_Normal_Index)  );
			float3 weightedBlendVar6427 = BlendComponents;
			float weightedAvg6427 = ( ( weightedBlendVar6427.x*texArray4486.w + weightedBlendVar6427.y*texArray4493.w + weightedBlendVar6427.z*texArray4491.w )/( weightedBlendVar6427.x + weightedBlendVar6427.y + weightedBlendVar6427.z ) );
			fixed2 temp_cast_28 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5676 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4471 ) / temp_cast_28 ), _Texture_6_Normal_Index)  );
			fixed2 temp_cast_29 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray5684 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4471 ) / temp_cast_29 ), _Texture_6_Normal_Index)  );
			float3 weightedBlendVar6428 = BlendComponents;
			float weightedAvg6428 = ( ( weightedBlendVar6428.x*texArray5676.w + weightedBlendVar6428.y*texArray5695.w + weightedBlendVar6428.z*texArray5684.w )/( weightedBlendVar6428.x + weightedBlendVar6428.y + weightedBlendVar6428.z ) );
			fixed ifLocalVar6638 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6638 = lerp( texArray4493.w , texArray5695.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6638 = lerp( weightedAvg6427 , weightedAvg6428 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6638 = lerp( texArray4493.w , texArray5695.w , UVmixDistance );
			fixed ifLocalVar7746 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Index > -1.0 )
				ifLocalVar7746 = ifLocalVar6638;
			fixed Texture_6_H = ifLocalVar7746;
			float2 appendResult4545 = float2( ( 1.0 / _Texture_7_Tiling ) , ( 1.0 / _Texture_7_Tiling ) );
			float4 texArray4567 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Normal_Index)  );
			fixed2 temp_cast_30 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5735 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_30 ), _Texture_7_Normal_Index)  );
			float4 texArray4560 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4545 ), _Texture_7_Normal_Index)  );
			float4 texArray4565 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4545 ), _Texture_7_Normal_Index)  );
			float3 weightedBlendVar6434 = BlendComponents;
			float weightedAvg6434 = ( ( weightedBlendVar6434.x*texArray4560.w + weightedBlendVar6434.y*texArray4567.w + weightedBlendVar6434.z*texArray4565.w )/( weightedBlendVar6434.x + weightedBlendVar6434.y + weightedBlendVar6434.z ) );
			fixed2 temp_cast_31 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5716 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4545 ) / temp_cast_31 ), _Texture_7_Normal_Index)  );
			fixed2 temp_cast_32 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray5724 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4545 ) / temp_cast_32 ), _Texture_7_Normal_Index)  );
			float3 weightedBlendVar6435 = BlendComponents;
			float weightedAvg6435 = ( ( weightedBlendVar6435.x*texArray5716.w + weightedBlendVar6435.y*texArray5735.w + weightedBlendVar6435.z*texArray5724.w )/( weightedBlendVar6435.x + weightedBlendVar6435.y + weightedBlendVar6435.z ) );
			fixed ifLocalVar6644 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6644 = lerp( texArray4567.w , texArray5735.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6644 = lerp( weightedAvg6434 , weightedAvg6435 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6644 = lerp( texArray4567.w , texArray5735.w , UVmixDistance );
			fixed ifLocalVar7748 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Index > -1.0 )
				ifLocalVar7748 = ifLocalVar6644;
			fixed Texture_7_H = ifLocalVar7748;
			float2 appendResult4619 = float2( ( 1.0 / _Texture_8_Tiling ) , ( 1.0 / _Texture_8_Tiling ) );
			float4 texArray4641 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Normal_Index)  );
			fixed2 temp_cast_33 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5775 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_33 ), _Texture_8_Normal_Index)  );
			float4 texArray4634 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4619 ), _Texture_8_Normal_Index)  );
			float4 texArray4639 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4619 ), _Texture_8_Normal_Index)  );
			float3 weightedBlendVar6441 = BlendComponents;
			float weightedAvg6441 = ( ( weightedBlendVar6441.x*texArray4634.w + weightedBlendVar6441.y*texArray4641.w + weightedBlendVar6441.z*texArray4639.w )/( weightedBlendVar6441.x + weightedBlendVar6441.y + weightedBlendVar6441.z ) );
			fixed2 temp_cast_34 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5756 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4619 ) / temp_cast_34 ), _Texture_8_Normal_Index)  );
			fixed2 temp_cast_35 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray5764 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4619 ) / temp_cast_35 ), _Texture_8_Normal_Index)  );
			float3 weightedBlendVar6442 = BlendComponents;
			float weightedAvg6442 = ( ( weightedBlendVar6442.x*texArray5756.w + weightedBlendVar6442.y*texArray5775.w + weightedBlendVar6442.z*texArray5764.w )/( weightedBlendVar6442.x + weightedBlendVar6442.y + weightedBlendVar6442.z ) );
			fixed ifLocalVar6650 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6650 = lerp( texArray4641.w , texArray5775.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6650 = lerp( weightedAvg6441 , weightedAvg6442 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6650 = lerp( texArray4641.w , texArray5775.w , UVmixDistance );
			fixed ifLocalVar7753 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Index > -1.0 )
				ifLocalVar7753 = ifLocalVar6650;
			fixed Texture_8_H = ifLocalVar7753;
			float4 layeredBlendVar7776 = tex2DNode4369;
			float layeredBlend7776 = ( lerp( lerp( lerp( lerp( layeredBlend7775 , ( _Texture_5_Heightmap_Depth * pow( Texture_5_H , _Texture_5_Height_Contrast ) ) , layeredBlendVar7776.x ) , ( _Texture_6_Heightmap_Depth * pow( Texture_6_H , _Texture_6_Height_Contrast ) ) , layeredBlendVar7776.y ) , ( _Texture_7_Heightmap_Depth * pow( Texture_7_H , _Texture_7_Height_Contrast ) ) , layeredBlendVar7776.z ) , ( _Texture_8_Tesselation_Depth * pow( Texture_8_H , _Texture_8_Height_Contrast ) ) , layeredBlendVar7776.w ) );
			float2 appendResult4736 = float2( ( 1.0 / _Texture_9_Tiling ) , ( 1.0 / _Texture_9_Tiling ) );
			float4 texArray4788 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Normal_Index)  );
			fixed2 temp_cast_36 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5811 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_36 ), _Texture_9_Normal_Index)  );
			float4 texArray5285 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4736 ), _Texture_9_Normal_Index)  );
			float4 texArray4783 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4736 ), _Texture_9_Normal_Index)  );
			float3 weightedBlendVar6448 = BlendComponents;
			float weightedAvg6448 = ( ( weightedBlendVar6448.x*texArray5285.w + weightedBlendVar6448.y*texArray4788.w + weightedBlendVar6448.z*texArray4783.w )/( weightedBlendVar6448.x + weightedBlendVar6448.y + weightedBlendVar6448.z ) );
			fixed2 temp_cast_37 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5796 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4736 ) / temp_cast_37 ), _Texture_9_Normal_Index)  );
			fixed2 temp_cast_38 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray5806 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4736 ) / temp_cast_38 ), _Texture_9_Normal_Index)  );
			float3 weightedBlendVar6449 = BlendComponents;
			float weightedAvg6449 = ( ( weightedBlendVar6449.x*texArray5796.w + weightedBlendVar6449.y*texArray5811.w + weightedBlendVar6449.z*texArray5806.w )/( weightedBlendVar6449.x + weightedBlendVar6449.y + weightedBlendVar6449.z ) );
			fixed ifLocalVar6668 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6668 = lerp( texArray4788.w , texArray5811.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar6668 = lerp( weightedAvg6448 , weightedAvg6449 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6668 = lerp( texArray4788.w , texArray5811.w , UVmixDistance );
			fixed ifLocalVar7771 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar7771 = ifLocalVar6668;
			fixed Texture_9_H = ifLocalVar7771;
			float2 appendResult4738 = float2( ( 1.0 / _Texture_10_Tiling ) , ( 1.0 / _Texture_10_Tiling ) );
			float4 texArray4822 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Normal_Index)  );
			fixed2 temp_cast_39 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5851 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_39 ), _Texture_10_Normal_Index)  );
			float4 texArray4798 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4738 ), _Texture_10_Normal_Index)  );
			float4 texArray4791 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4738 ), _Texture_10_Normal_Index)  );
			float3 weightedBlendVar6455 = BlendComponents;
			float weightedAvg6455 = ( ( weightedBlendVar6455.x*texArray4798.w + weightedBlendVar6455.y*texArray4822.w + weightedBlendVar6455.z*texArray4791.w )/( weightedBlendVar6455.x + weightedBlendVar6455.y + weightedBlendVar6455.z ) );
			fixed2 temp_cast_40 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5836 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4738 ) / temp_cast_40 ), _Texture_10_Normal_Index)  );
			fixed2 temp_cast_41 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray5846 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4738 ) / temp_cast_41 ), _Texture_10_Normal_Index)  );
			float3 weightedBlendVar6456 = BlendComponents;
			float weightedAvg6456 = ( ( weightedBlendVar6456.x*texArray5836.w + weightedBlendVar6456.y*texArray5851.w + weightedBlendVar6456.z*texArray5846.w )/( weightedBlendVar6456.x + weightedBlendVar6456.y + weightedBlendVar6456.z ) );
			fixed ifLocalVar6662 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6662 = lerp( texArray4822.w , texArray5851.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6662 = lerp( weightedAvg6455 , weightedAvg6456 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6662 = lerp( texArray4822.w , texArray5851.w , UVmixDistance );
			fixed ifLocalVar7769 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7769 = ifLocalVar6662;
			fixed Texture_10_H = ifLocalVar7769;
			float2 appendResult4741 = float2( ( 1.0 / _Texture_11_Tiling ) , ( 1.0 / _Texture_11_Tiling ) );
			float4 texArray4856 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Normal_Index)  );
			fixed2 temp_cast_42 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5891 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_42 ), _Texture_11_Normal_Index)  );
			float4 texArray4828 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4741 ), _Texture_11_Normal_Index)  );
			float4 texArray4811 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4741 ), _Texture_11_Normal_Index)  );
			float3 weightedBlendVar6462 = BlendComponents;
			float weightedAvg6462 = ( ( weightedBlendVar6462.x*texArray4828.w + weightedBlendVar6462.y*texArray4856.w + weightedBlendVar6462.z*texArray4811.w )/( weightedBlendVar6462.x + weightedBlendVar6462.y + weightedBlendVar6462.z ) );
			fixed2 temp_cast_43 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5876 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4741 ) / temp_cast_43 ), _Texture_11_Normal_Index)  );
			fixed2 temp_cast_44 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray5886 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4741 ) / temp_cast_44 ), _Texture_11_Normal_Index)  );
			float3 weightedBlendVar6463 = BlendComponents;
			float weightedAvg6463 = ( ( weightedBlendVar6463.x*texArray5876.w + weightedBlendVar6463.y*texArray5891.w + weightedBlendVar6463.z*texArray5886.w )/( weightedBlendVar6463.x + weightedBlendVar6463.y + weightedBlendVar6463.z ) );
			fixed ifLocalVar6656 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6656 = lerp( texArray4856.w , texArray5891.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6656 = lerp( weightedAvg6462 , weightedAvg6463 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6656 = lerp( texArray4856.w , texArray5891.w , UVmixDistance );
			fixed ifLocalVar7767 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Index > -1.0 )
				ifLocalVar7767 = ifLocalVar6656;
			fixed Texture_11_H = ifLocalVar7767;
			float2 appendResult4751 = float2( ( 1.0 / _Texture_12_Tiling ) , ( 1.0 / _Texture_12_Tiling ) );
			float4 texArray4870 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Normal_Index)  );
			fixed2 temp_cast_45 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5931 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_45 ), _Texture_12_Normal_Index)  );
			float4 texArray4850 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult4751 ), _Texture_12_Normal_Index)  );
			float4 texArray4852 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult4751 ), _Texture_12_Normal_Index)  );
			float3 weightedBlendVar6469 = BlendComponents;
			float weightedAvg6469 = ( ( weightedBlendVar6469.x*texArray4850.w + weightedBlendVar6469.y*texArray4870.w + weightedBlendVar6469.z*texArray4852.w )/( weightedBlendVar6469.x + weightedBlendVar6469.y + weightedBlendVar6469.z ) );
			fixed2 temp_cast_46 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5916 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult4751 ) / temp_cast_46 ), _Texture_12_Normal_Index)  );
			fixed2 temp_cast_47 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray5926 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult4751 ) / temp_cast_47 ), _Texture_12_Normal_Index)  );
			float3 weightedBlendVar6470 = BlendComponents;
			float weightedAvg6470 = ( ( weightedBlendVar6470.x*texArray5916.w + weightedBlendVar6470.y*texArray5931.w + weightedBlendVar6470.z*texArray5926.w )/( weightedBlendVar6470.x + weightedBlendVar6470.y + weightedBlendVar6470.z ) );
			fixed ifLocalVar6674 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6674 = lerp( texArray4870.w , texArray5931.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6674 = lerp( weightedAvg6469 , weightedAvg6470 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6674 = lerp( texArray4870.w , texArray5931.w , UVmixDistance );
			fixed ifLocalVar7765 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Index > -1.0 )
				ifLocalVar7765 = ifLocalVar6674;
			fixed Texture_12_H = ifLocalVar7765;
			float HeightMask6228 = saturate(pow(((( _Texture_12_Heightmap_Depth * pow( Texture_12_H , _Texture_12_Height_Contrast ) )*Splat3_A)*4)+(Splat3_A*2),lerp( _Texture_12_Heightblend_Close , _Texture_12_Heightblend_Far , UVmixDistance )));;
			float4 layeredBlendVar7777 = tex2DNode4370;
			float layeredBlend7777 = ( lerp( lerp( lerp( lerp( layeredBlend7776 , ( _Texture_9_Heightmap_Depth * pow( Texture_9_H , _Texture_9_Height_Contrast ) ) , layeredBlendVar7777.x ) , ( _Texture_10_Heightmap_Depth * pow( Texture_10_H , _Texture_10_Height_Contrast ) ) , layeredBlendVar7777.y ) , ( _Texture_11_Heightmap_Depth * pow( Texture_11_H , _Texture_11_Height_Contrast ) ) , layeredBlendVar7777.z ) , HeightMask6228 , layeredBlendVar7777.w ) );
			float2 appendResult5027 = float2( ( 1.0 / _Texture_13_Tiling ) , ( 1.0 / _Texture_13_Tiling ) );
			float4 texArray5120 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Normal_Index)  );
			fixed2 temp_cast_48 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5971 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_48 ), _Texture_13_Normal_Index)  );
			float4 texArray5127 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5027 ), _Texture_13_Normal_Index)  );
			float4 texArray5109 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5027 ), _Texture_13_Normal_Index)  );
			float3 weightedBlendVar6476 = BlendComponents;
			float weightedAvg6476 = ( ( weightedBlendVar6476.x*texArray5127.w + weightedBlendVar6476.y*texArray5120.w + weightedBlendVar6476.z*texArray5109.w )/( weightedBlendVar6476.x + weightedBlendVar6476.y + weightedBlendVar6476.z ) );
			fixed2 temp_cast_49 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5956 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5027 ) / temp_cast_49 ), _Texture_13_Normal_Index)  );
			fixed2 temp_cast_50 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5966 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5027 ) / temp_cast_50 ), _Texture_13_Normal_Index)  );
			float3 weightedBlendVar6477 = BlendComponents;
			float weightedAvg6477 = ( ( weightedBlendVar6477.x*texArray5956.w + weightedBlendVar6477.y*texArray5971.w + weightedBlendVar6477.z*texArray5966.w )/( weightedBlendVar6477.x + weightedBlendVar6477.y + weightedBlendVar6477.z ) );
			fixed ifLocalVar6680 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6680 = lerp( texArray5120.w , texArray5971.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6680 = lerp( weightedAvg6476 , weightedAvg6477 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6680 = lerp( texArray5120.w , texArray5971.w , UVmixDistance );
			fixed ifLocalVar7761 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Index > -1.0 )
				ifLocalVar7761 = ifLocalVar6680;
			fixed Texture_13_H = ifLocalVar7761;
			float2 appendResult5033 = float2( ( 1.0 / _Texture_14_Tiling ) , ( 1.0 / _Texture_14_Tiling ) );
			float4 texArray5178 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Normal_Index)  );
			fixed2 temp_cast_51 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6011 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_51 ), _Texture_14_Normal_Index)  );
			float4 texArray5017 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5033 ), _Texture_14_Normal_Index)  );
			float4 texArray5170 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5033 ), _Texture_14_Normal_Index)  );
			float3 weightedBlendVar6483 = BlendComponents;
			float weightedAvg6483 = ( ( weightedBlendVar6483.x*texArray5017.w + weightedBlendVar6483.y*texArray5178.w + weightedBlendVar6483.z*texArray5170.w )/( weightedBlendVar6483.x + weightedBlendVar6483.y + weightedBlendVar6483.z ) );
			fixed2 temp_cast_52 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5996 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5033 ) / temp_cast_52 ), _Texture_14_Normal_Index)  );
			fixed2 temp_cast_53 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray6006 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5033 ) / temp_cast_53 ), _Texture_14_Normal_Index)  );
			float3 weightedBlendVar6484 = BlendComponents;
			float weightedAvg6484 = ( ( weightedBlendVar6484.x*texArray5996.w + weightedBlendVar6484.y*texArray6011.w + weightedBlendVar6484.z*texArray6006.w )/( weightedBlendVar6484.x + weightedBlendVar6484.y + weightedBlendVar6484.z ) );
			fixed ifLocalVar6686 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6686 = lerp( texArray5178.w , texArray6011.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6686 = lerp( weightedAvg6483 , weightedAvg6484 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6686 = lerp( texArray5178.w , texArray6011.w , UVmixDistance );
			fixed ifLocalVar7759 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar7759 = ifLocalVar6686;
			fixed Texture_14_H = ifLocalVar7759;
			float2 appendResult5212 = float2( ( 1.0 / _Texture_15_Tiling ) , ( 1.0 / _Texture_15_Tiling ) );
			float4 texArray5246 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Normal_Index)  );
			fixed2 temp_cast_54 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6051 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_54 ), _Texture_15_Normal_Index)  );
			float4 texArray5227 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5212 ), _Texture_15_Normal_Index)  );
			float4 texArray5250 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5212 ), _Texture_15_Normal_Index)  );
			float3 weightedBlendVar6490 = BlendComponents;
			float weightedAvg6490 = ( ( weightedBlendVar6490.x*texArray5227.w + weightedBlendVar6490.y*texArray5246.w + weightedBlendVar6490.z*texArray5250.w )/( weightedBlendVar6490.x + weightedBlendVar6490.y + weightedBlendVar6490.z ) );
			fixed2 temp_cast_55 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6036 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5212 ) / temp_cast_55 ), _Texture_15_Normal_Index)  );
			fixed2 temp_cast_56 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray6046 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5212 ) / temp_cast_56 ), _Texture_15_Normal_Index)  );
			float3 weightedBlendVar6491 = BlendComponents;
			float weightedAvg6491 = ( ( weightedBlendVar6491.x*texArray6036.w + weightedBlendVar6491.y*texArray6051.w + weightedBlendVar6491.z*texArray6046.w )/( weightedBlendVar6491.x + weightedBlendVar6491.y + weightedBlendVar6491.z ) );
			fixed ifLocalVar6692 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6692 = lerp( texArray5246.w , texArray6051.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6692 = lerp( weightedAvg6490 , weightedAvg6491 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6692 = lerp( texArray5246.w , texArray6051.w , UVmixDistance );
			fixed ifLocalVar7757 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar7757 = ifLocalVar6692;
			fixed Texture_15_H = ifLocalVar7757;
			float2 appendResult5078 = float2( ( 1.0 / _Texture_16_Tiling ) , ( 1.0 / _Texture_16_Tiling ) );
			float4 texArray5099 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Normal_Index)  );
			fixed2 temp_cast_57 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6091 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_57 ), _Texture_16_Normal_Index)  );
			float4 texArray5082 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Front_Back * appendResult5078 ), _Texture_16_Normal_Index)  );
			float4 texArray4731 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( Left_Right * appendResult5078 ), _Texture_16_Normal_Index)  );
			float3 weightedBlendVar6497 = BlendComponents;
			float weightedAvg6497 = ( ( weightedBlendVar6497.x*texArray5082.w + weightedBlendVar6497.y*texArray5099.w + weightedBlendVar6497.z*texArray4731.w )/( weightedBlendVar6497.x + weightedBlendVar6497.y + weightedBlendVar6497.z ) );
			fixed2 temp_cast_58 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6076 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Front_Back * appendResult5078 ) / temp_cast_58 ), _Texture_16_Normal_Index)  );
			fixed2 temp_cast_59 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray6086 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( Left_Right * appendResult5078 ) / temp_cast_59 ), _Texture_16_Normal_Index)  );
			float3 weightedBlendVar6498 = BlendComponents;
			float weightedAvg6498 = ( ( weightedBlendVar6498.x*texArray6076.w + weightedBlendVar6498.y*texArray6091.w + weightedBlendVar6498.z*texArray6086.w )/( weightedBlendVar6498.x + weightedBlendVar6498.y + weightedBlendVar6498.z ) );
			fixed ifLocalVar6698 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6698 = lerp( texArray5099.w , texArray6091.w , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6698 = lerp( weightedAvg6497 , weightedAvg6498 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6698 = lerp( texArray5099.w , texArray6091.w , UVmixDistance );
			fixed ifLocalVar7755 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar7755 = ifLocalVar6698;
			fixed Texture_16_H = ifLocalVar7755;
			float4 layeredBlendVar7778 = tex2DNode4371;
			float layeredBlend7778 = ( lerp( lerp( lerp( lerp( layeredBlend7777 , ( _Texture_13_Heightmap_Depth * pow( Texture_13_H , _Texture_13_Height_Contrast ) ) , layeredBlendVar7778.x ) , ( _Texture_14_Heightmap_Depth * pow( Texture_14_H , _Texture_14_Height_Contrast ) ) , layeredBlendVar7778.y ) , ( _Texture_15_Heightmap_Depth * pow( Texture_15_H , _Texture_15_Height_Contrast ) ) , layeredBlendVar7778.z ) , ( _Texture_16_Heightmap_Depth * pow( Texture_16_H , _Texture_16_Height_Contrast ) ) , layeredBlendVar7778.w ) );
			float2 appendResult3679 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_60 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray6267 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3679 * temp_cast_60 ), (float)_Texture_Snow_Normal_Index)  );
			fixed2 temp_cast_62 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_63 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray6270 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3679 * temp_cast_62 ) / temp_cast_63 ), (float)_Texture_Snow_Normal_Index)  );
			float2 appendResult3750 = float2( ase_worldPos.x , ase_worldPos.z );
			fixed2 temp_cast_65 = (_Snow_Noise_Tiling).xx;
			float4 texArray4383 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3750 * temp_cast_65 ), (float)_Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_67 = (_Snow_Noise_Tiling).xx;
			float4 texArray4385 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_67 ) * float2( 0.23,0.23 ) ), (float)_Texture_Snow_Noise_Index)  );
			fixed2 temp_cast_69 = (_Snow_Noise_Tiling).xx;
			float4 texArray4384 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( ( appendResult3750 * temp_cast_69 ) * float2( 0.53,0.53 ) ), (float)_Texture_Snow_Noise_Index)  );
			fixed SnowSlope = ( 1.0 - ( clamp( ( clamp( ase_worldNormal.y , 0.0 , 0.9999 ) - ( 1.0 - ( _Snow_Maximum_Angle / 90.0 ) ) ) , 0.0 , 2.0 ) * ( 1.0 / ( _Snow_Maximum_Angle / 90.0 ) ) ) );
			float HeightMask6539 = saturate(pow(((( 1.0 - clamp( clamp( ( layeredBlend7778 + lerp( ( pow( texArray6267.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , ( pow( texArray6270.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , UVmixDistance ) ) , 0.0 , ( layeredBlend7778 + lerp( ( pow( texArray6267.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , ( pow( texArray6270.w , _Snow_Height_Contrast ) * _Snow_Heightmap_Depth ) , UVmixDistance ) ) ) , 0.0 , 1.0 ) )*( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) ))*4)+(( clamp( lerp( 0.0 , ( ( _Snow_Amount * lerp( 1.0 , ( ( texArray4383.w + texArray4385.w ) + texArray4384.w ) , ( _Snow_Noise_Power * 0.1 ) ) ) * clamp( pow( ( ( ( _Snow_Amount * 1.0 ) * ( 0.1 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.01 ) ) ) * ( 1.0 - SnowSlope ) ) , clamp( ( 1.0 - ( ( _Snow_Maximum_Angle_Hardness * 0.5 ) * 0.1 ) ) , 0.01 , 1.0 ) ) , 0.0 , 1.0 ) ) , clamp( ( clamp( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + 1.0 ) , 0.0 , 1.0 ) + clamp( ( ( 1.0 - ( ( ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) + _Snow_Min_Height_Blending ) / ( ( ( 1.0 - _Snow_Min_Height ) + 0.0 ) + ase_worldPos.y ) ) ) + -0.5 ) , 0.0 , 1.0 ) ) , 0.0 , 1.0 ) ) , 0.0 , 2.0 ) * ( 1.0 - ( ( _Texture_16_Snow_Reduction * Splat4_A ) + ( ( _Texture_15_Snow_Reduction * Splat4_B ) + ( ( ( _Texture_13_Snow_Reduction * Splat4_R ) + ( ( _Texture_12_Snow_Reduction * Splat3_A ) + ( ( _Texture_11_Snow_Reduction * Splat3_B ) + ( ( ( _Texture_9_Snow_Reduction * Splat3_R ) + ( ( _Texture_8_Snow_Reduction * Splat2_A ) + ( ( _Texture_7_Snow_Reduction * Splat2_B ) + ( ( ( _Texture_5_Snow_Reduction * Splat2_R ) + ( ( _Texture_1_Snow_Reduction * Splat1_R ) + ( ( _Texture_2_Snow_Reduction * Splat1_G ) + ( ( _Texture_3_Snow_Reduction * Splat1_B ) + ( _Texture_4_Snow_Reduction * Splat1_A ) ) ) ) ) + ( _Texture_6_Snow_Reduction * Splat2_G ) ) ) ) ) + ( _Texture_10_Snow_Reduction * Splat3_G ) ) ) ) ) + ( _Texture_14_Snow_Reduction * Splat4_G ) ) ) ) ) )*2),lerp( _Snow_Heightblend_Close , _Snow_Heightblend_Far , UVmixDistance )));;
			float HeightMask6231 = saturate(pow(((( _Texture_13_Heightmap_Depth * pow( Texture_13_H , _Texture_13_Height_Contrast ) )*Splat4_R)*4)+(Splat4_R*2),lerp( _Texture_13_Heightblend_Close , _Texture_13_Heightblend_Far , UVmixDistance )));;
			float HeightMask6234 = saturate(pow(((( _Texture_14_Heightmap_Depth * pow( Texture_14_H , _Texture_14_Height_Contrast ) )*Splat4_G)*4)+(Splat4_G*2),lerp( _Texture_14_Heightblend_Close , _Texture_14_Heightblend_Far , UVmixDistance )));;
			float HeightMask6237 = saturate(pow(((( _Texture_15_Heightmap_Depth * pow( Texture_15_H , _Texture_15_Height_Contrast ) )*Splat4_B)*4)+(Splat4_B*2),lerp( _Texture_15_Heightblend_Close , _Texture_15_Heightblend_Far , UVmixDistance )));;
			float HeightMask6240 = saturate(pow(((( _Texture_16_Heightmap_Depth * pow( Texture_16_H , _Texture_16_Height_Contrast ) )*Splat4_A)*4)+(Splat4_A*2),lerp( _Texture_16_Heightblend_Close , _Texture_16_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6533 = float4( HeightMask6231 , HeightMask6234 , HeightMask6237 , HeightMask6240 );
			float HeightMask6219 = saturate(pow(((( _Texture_9_Heightmap_Depth * pow( Texture_9_H , _Texture_9_Height_Contrast ) )*Splat3_R)*4)+(Splat3_R*2),lerp( _Texture_9_Heightblend_Close , _Texture_9_Heightblend_Far , UVmixDistance )));;
			float HeightMask6222 = saturate(pow(((( _Texture_10_Heightmap_Depth * pow( Texture_10_H , _Texture_10_Height_Contrast ) )*Splat3_G)*4)+(Splat3_G*2),lerp( _Texture_10_Heightblend_Close , _Texture_10_Heightblend_Far , UVmixDistance )));;
			float HeightMask6225 = saturate(pow(((( _Texture_11_Heightmap_Depth * pow( Texture_11_H , _Texture_11_Height_Contrast ) )*Splat3_B)*4)+(Splat3_B*2),lerp( _Texture_11_Heightblend_Close , _Texture_11_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6529 = float4( HeightMask6219 , HeightMask6222 , HeightMask6225 , HeightMask6228 );
			float HeightMask6205 = saturate(pow(((( _Texture_5_Heightmap_Depth * pow( Texture_5_H , _Texture_5_Height_Contrast ) )*Splat2_R)*4)+(Splat2_R*2),lerp( _Texture_5_Heightblend_Close , _Texture_5_Heightblend_Far , UVmixDistance )));;
			float HeightMask6208 = saturate(pow(((( _Texture_6_Heightmap_Depth * pow( Texture_6_H , _Texture_6_Height_Contrast ) )*Splat2_G)*4)+(Splat2_G*2),lerp( _Texture_6_Heightblend_Close , _Texture_6_Heightblend_Far , UVmixDistance )));;
			float HeightMask6211 = saturate(pow(((( _Texture_7_Heightmap_Depth * pow( Texture_7_H , _Texture_7_Height_Contrast ) )*Splat2_B)*4)+(Splat2_B*2),lerp( _Texture_7_Heightblend_Close , _Texture_7_Heightblend_Far , UVmixDistance )));;
			float HeightMask6214 = saturate(pow(((( _Texture_8_Tesselation_Depth * pow( Texture_8_H , _Texture_8_Height_Contrast ) )*Splat2_A)*4)+(Splat2_A*2),lerp( _Texture_8_Heightblend_Close , _Texture_8_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6524 = float4( HeightMask6205 , HeightMask6208 , HeightMask6211 , HeightMask6214 );
			float HeightMask6196 = saturate(pow(((( pow( Texture_1_H , _Texture_1_Height_Contrast ) * _Texture_1_Heightmap_Depth )*Splat1_R)*4)+(Splat1_R*2),lerp( _Texture_1_Heightblend_Close , _Texture_1_Heightblend_Far , UVmixDistance )));;
			float HeightMask6515 = saturate(pow(((( _Texture_2_Heightmap_Depth * pow( Texture_2_H , _Texture_2_Height_Contrast ) )*Splat1_G)*4)+(Splat1_G*2),lerp( _Texture_2_Heightblend_Close , _Texture_2_Heightblend_Far , UVmixDistance )));;
			float HeightMask6516 = saturate(pow(((( _Texture_3_Heightmap_Depth * pow( Texture_3_H , _Texture_3_Height_Contrast ) )*Splat1_B)*4)+(Splat1_B*2),lerp( _Texture_3_Heightblend_Close , _Texture_3_Heightblend_Far , UVmixDistance )));;
			float HeightMask6203 = saturate(pow(((( _Texture_4_Heightmap_Depth * pow( Texture_4_H , _Texture_4_Height_Contrast ) )*Splat1_A)*4)+(Splat1_A*2),lerp( _Texture_4_Heightblend_Close , _Texture_4_Heightblend_Far , UVmixDistance )));;
			float4 appendResult6517 = float4( HeightMask6196 , HeightMask6515 , HeightMask6516 , HeightMask6203 );
			float2 appendResult11_g538 = float2( texArray3300.x , texArray3300.y );
			float2 temp_cast_71 = (_Texture_1_Normal_Power).xx;
			float2 temp_output_4_0_g538 = ( ( ( appendResult11_g538 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_71 );
			float2 temp_cast_72 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_73 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_74 = (_Texture_1_Normal_Power).xx;
			float temp_output_9_0_g538 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g538 , temp_output_4_0_g538 ) ) ) );
			float3 appendResult10_g538 = float3( temp_output_4_0_g538.x , temp_output_4_0_g538.y , temp_output_9_0_g538 );
			float2 appendResult11_g190 = float2( texArray3299.x , texArray3299.y );
			float2 temp_cast_75 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g190 = ( ( ( appendResult11_g190 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_75 );
			float2 temp_cast_76 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_cast_77 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float2 temp_cast_78 = (( _Texture_1_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g190 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g190 , temp_output_4_0_g190 ) ) ) );
			float3 appendResult19_g190 = float3( temp_output_4_0_g190.y , temp_output_4_0_g190.x , temp_output_9_0_g190 );
			float3 appendResult6857 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g189 = float2( texArray3301.x , texArray3301.y );
			float2 temp_cast_79 = (_Texture_1_Normal_Power).xx;
			float2 temp_output_4_0_g189 = ( ( ( appendResult11_g189 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_79 );
			float2 temp_cast_80 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_81 = (_Texture_1_Normal_Power).xx;
			float2 temp_cast_82 = (_Texture_1_Normal_Power).xx;
			float temp_output_9_0_g189 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g189 , temp_output_4_0_g189 ) ) ) );
			float3 appendResult10_g189 = float3( temp_output_4_0_g189.x , temp_output_4_0_g189.y , temp_output_9_0_g189 );
			float3 appendResult6860 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6393 = BlendComponents;
			float3 weightedAvg6393 = ( ( weightedBlendVar6393.x*( appendResult19_g190 * appendResult6857 ) + weightedBlendVar6393.y*appendResult10_g538 + weightedBlendVar6393.z*( appendResult10_g189 * appendResult6860 ) )/( weightedBlendVar6393.x + weightedBlendVar6393.y + weightedBlendVar6393.z ) );
			fixed3 ifLocalVar6606 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6606 = appendResult10_g538;
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6606 = weightedAvg6393;
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6606 = appendResult10_g538;
			fixed3 ifLocalVar7594 = 0;
			UNITY_BRANCH if( _Texture_1_Normal_Index > -1.0 )
				ifLocalVar7594 = ifLocalVar6606;
			fixed3 Normal_1 = ifLocalVar7594;
			float2 appendResult11_g531 = float2( texArray3350.x , texArray3350.y );
			float2 temp_cast_83 = (_Texture_2_Normal_Power).xx;
			float2 temp_output_4_0_g531 = ( ( ( appendResult11_g531 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_83 );
			float2 temp_cast_84 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_85 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_86 = (_Texture_2_Normal_Power).xx;
			float temp_output_9_0_g531 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g531 , temp_output_4_0_g531 ) ) ) );
			float3 appendResult10_g531 = float3( temp_output_4_0_g531.x , temp_output_4_0_g531.y , temp_output_9_0_g531 );
			float2 appendResult11_g184 = float2( texArray3384.x , texArray3384.y );
			float2 temp_cast_87 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g184 = ( ( ( appendResult11_g184 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_87 );
			float2 temp_cast_88 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_cast_89 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float2 temp_cast_90 = (( _Texture_2_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g184 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g184 , temp_output_4_0_g184 ) ) ) );
			float3 appendResult19_g184 = float3( temp_output_4_0_g184.y , temp_output_4_0_g184.x , temp_output_9_0_g184 );
			float3 appendResult6864 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g187 = float2( texArray3351.x , texArray3351.y );
			float2 temp_cast_91 = (_Texture_2_Normal_Power).xx;
			float2 temp_output_4_0_g187 = ( ( ( appendResult11_g187 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_91 );
			float2 temp_cast_92 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_93 = (_Texture_2_Normal_Power).xx;
			float2 temp_cast_94 = (_Texture_2_Normal_Power).xx;
			float temp_output_9_0_g187 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g187 , temp_output_4_0_g187 ) ) ) );
			float3 appendResult10_g187 = float3( temp_output_4_0_g187.x , temp_output_4_0_g187.y , temp_output_9_0_g187 );
			float3 appendResult6867 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6401 = BlendComponents;
			float3 weightedAvg6401 = ( ( weightedBlendVar6401.x*( appendResult19_g184 * appendResult6864 ) + weightedBlendVar6401.y*appendResult10_g531 + weightedBlendVar6401.z*( appendResult10_g187 * appendResult6867 ) )/( weightedBlendVar6401.x + weightedBlendVar6401.y + weightedBlendVar6401.z ) );
			fixed3 ifLocalVar6613 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6613 = appendResult10_g531;
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6613 = weightedAvg6401;
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6613 = appendResult10_g531;
			fixed3 ifLocalVar7600 = 0;
			UNITY_BRANCH if( _Texture_2_Normal_Index > -1.0 )
				ifLocalVar7600 = ifLocalVar6613;
			fixed3 Normal_2 = ifLocalVar7600;
			float2 appendResult11_g524 = float2( texArray3416.x , texArray3416.y );
			float2 temp_cast_95 = (_Texture_3_Normal_Power).xx;
			float2 temp_output_4_0_g524 = ( ( ( appendResult11_g524 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_95 );
			float2 temp_cast_96 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_97 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_98 = (_Texture_3_Normal_Power).xx;
			float temp_output_9_0_g524 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g524 , temp_output_4_0_g524 ) ) ) );
			float3 appendResult10_g524 = float3( temp_output_4_0_g524.x , temp_output_4_0_g524.y , temp_output_9_0_g524 );
			float2 appendResult11_g191 = float2( texArray3445.x , texArray3445.y );
			float2 temp_cast_99 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g191 = ( ( ( appendResult11_g191 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_99 );
			float2 temp_cast_100 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_cast_101 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float2 temp_cast_102 = (( _Texture_3_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g191 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g191 , temp_output_4_0_g191 ) ) ) );
			float3 appendResult19_g191 = float3( temp_output_4_0_g191.y , temp_output_4_0_g191.x , temp_output_9_0_g191 );
			float3 appendResult6871 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g188 = float2( texArray3417.x , texArray3417.y );
			float2 temp_cast_103 = (_Texture_3_Normal_Power).xx;
			float2 temp_output_4_0_g188 = ( ( ( appendResult11_g188 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_103 );
			float2 temp_cast_104 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_105 = (_Texture_3_Normal_Power).xx;
			float2 temp_cast_106 = (_Texture_3_Normal_Power).xx;
			float temp_output_9_0_g188 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g188 , temp_output_4_0_g188 ) ) ) );
			float3 appendResult10_g188 = float3( temp_output_4_0_g188.x , temp_output_4_0_g188.y , temp_output_9_0_g188 );
			float3 appendResult6874 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6408 = BlendComponents;
			float3 weightedAvg6408 = ( ( weightedBlendVar6408.x*( appendResult19_g191 * appendResult6871 ) + weightedBlendVar6408.y*appendResult10_g524 + weightedBlendVar6408.z*( appendResult10_g188 * appendResult6874 ) )/( weightedBlendVar6408.x + weightedBlendVar6408.y + weightedBlendVar6408.z ) );
			fixed3 ifLocalVar6619 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6619 = appendResult10_g524;
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6619 = weightedAvg6408;
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6619 = appendResult10_g524;
			fixed3 ifLocalVar7604 = 0;
			UNITY_BRANCH if( _Texture_3_Normal_Power > -1.0 )
				ifLocalVar7604 = ifLocalVar6619;
			fixed3 Normal_3 = ifLocalVar7604;
			float2 appendResult11_g530 = float2( texArray3483.x , texArray3483.y );
			float2 temp_cast_107 = (_Texture_4_Normal_Power).xx;
			float2 temp_output_4_0_g530 = ( ( ( appendResult11_g530 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_107 );
			float2 temp_cast_108 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_109 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_110 = (_Texture_4_Normal_Power).xx;
			float temp_output_9_0_g530 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g530 , temp_output_4_0_g530 ) ) ) );
			float3 appendResult10_g530 = float3( temp_output_4_0_g530.x , temp_output_4_0_g530.y , temp_output_9_0_g530 );
			float2 appendResult11_g192 = float2( texArray3512.x , texArray3512.y );
			float2 temp_cast_111 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g192 = ( ( ( appendResult11_g192 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_111 );
			float2 temp_cast_112 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_cast_113 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float2 temp_cast_114 = (( _Texture_4_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g192 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g192 , temp_output_4_0_g192 ) ) ) );
			float3 appendResult19_g192 = float3( temp_output_4_0_g192.y , temp_output_4_0_g192.x , temp_output_9_0_g192 );
			float3 appendResult6878 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g193 = float2( texArray3484.x , texArray3484.y );
			float2 temp_cast_115 = (_Texture_4_Normal_Power).xx;
			float2 temp_output_4_0_g193 = ( ( ( appendResult11_g193 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_115 );
			float2 temp_cast_116 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_117 = (_Texture_4_Normal_Power).xx;
			float2 temp_cast_118 = (_Texture_4_Normal_Power).xx;
			float temp_output_9_0_g193 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g193 , temp_output_4_0_g193 ) ) ) );
			float3 appendResult10_g193 = float3( temp_output_4_0_g193.x , temp_output_4_0_g193.y , temp_output_9_0_g193 );
			float3 appendResult6881 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6415 = BlendComponents;
			float3 weightedAvg6415 = ( ( weightedBlendVar6415.x*( appendResult19_g192 * appendResult6878 ) + weightedBlendVar6415.y*appendResult10_g530 + weightedBlendVar6415.z*( appendResult10_g193 * appendResult6881 ) )/( weightedBlendVar6415.x + weightedBlendVar6415.y + weightedBlendVar6415.z ) );
			fixed3 ifLocalVar6625 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6625 = appendResult10_g530;
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6625 = weightedAvg6415;
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6625 = appendResult10_g530;
			fixed3 ifLocalVar7610 = 0;
			UNITY_BRANCH if( _Texture_4_Normal_Power > -1.0 )
				ifLocalVar7610 = ifLocalVar6625;
			fixed3 Normal_4 = ifLocalVar7610;
			float4 layeredBlendVar7722 = appendResult6517;
			float3 layeredBlend7722 = ( lerp( lerp( lerp( lerp( 0.0 , Normal_1 , layeredBlendVar7722.x ) , Normal_2 , layeredBlendVar7722.y ) , Normal_3 , layeredBlendVar7722.z ) , Normal_4 , layeredBlendVar7722.w ) );
			float2 appendResult11_g559 = float2( texArray4424.x , texArray4424.y );
			float2 temp_cast_119 = (_Texture_5_Normal_Power).xx;
			float2 temp_output_4_0_g559 = ( ( ( appendResult11_g559 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_119 );
			float2 temp_cast_120 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_121 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_122 = (_Texture_5_Normal_Power).xx;
			float temp_output_9_0_g559 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g559 , temp_output_4_0_g559 ) ) ) );
			float3 appendResult10_g559 = float3( temp_output_4_0_g559.x , temp_output_4_0_g559.y , temp_output_9_0_g559 );
			float2 appendResult11_g534 = float2( texArray4417.x , texArray4417.y );
			float2 temp_cast_123 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g534 = ( ( ( appendResult11_g534 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_123 );
			float2 temp_cast_124 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_cast_125 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float2 temp_cast_126 = (( _Texture_5_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g534 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g534 , temp_output_4_0_g534 ) ) ) );
			float3 appendResult19_g534 = float3( temp_output_4_0_g534.y , temp_output_4_0_g534.x , temp_output_9_0_g534 );
			float3 appendResult6885 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g528 = float2( texArray4422.x , texArray4422.y );
			float2 temp_cast_127 = (_Texture_5_Normal_Power).xx;
			float2 temp_output_4_0_g528 = ( ( ( appendResult11_g528 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_127 );
			float2 temp_cast_128 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_129 = (_Texture_5_Normal_Power).xx;
			float2 temp_cast_130 = (_Texture_5_Normal_Power).xx;
			float temp_output_9_0_g528 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g528 , temp_output_4_0_g528 ) ) ) );
			float3 appendResult10_g528 = float3( temp_output_4_0_g528.x , temp_output_4_0_g528.y , temp_output_9_0_g528 );
			float3 appendResult6888 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6422 = BlendComponents;
			float3 weightedAvg6422 = ( ( weightedBlendVar6422.x*( appendResult19_g534 * appendResult6885 ) + weightedBlendVar6422.y*appendResult10_g559 + weightedBlendVar6422.z*( appendResult10_g528 * appendResult6888 ) )/( weightedBlendVar6422.x + weightedBlendVar6422.y + weightedBlendVar6422.z ) );
			fixed3 ifLocalVar6631 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6631 = appendResult10_g559;
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6631 = weightedAvg6422;
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6631 = appendResult10_g559;
			fixed3 ifLocalVar7614 = 0;
			UNITY_BRANCH if( _Texture_5_Normal_Power > -1.0 )
				ifLocalVar7614 = ifLocalVar6631;
			fixed3 Normal_5 = ifLocalVar7614;
			float2 appendResult11_g553 = float2( texArray4493.x , texArray4493.y );
			float2 temp_cast_131 = (_Texture_6_Normal_Power).xx;
			float2 temp_output_4_0_g553 = ( ( ( appendResult11_g553 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_131 );
			float2 temp_cast_132 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_133 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_134 = (_Texture_6_Normal_Power).xx;
			float temp_output_9_0_g553 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g553 , temp_output_4_0_g553 ) ) ) );
			float3 appendResult10_g553 = float3( temp_output_4_0_g553.x , temp_output_4_0_g553.y , temp_output_9_0_g553 );
			float2 appendResult11_g536 = float2( texArray4486.x , texArray4486.y );
			float2 temp_cast_135 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g536 = ( ( ( appendResult11_g536 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_135 );
			float2 temp_cast_136 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_cast_137 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float2 temp_cast_138 = (( _Texture_6_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g536 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g536 , temp_output_4_0_g536 ) ) ) );
			float3 appendResult19_g536 = float3( temp_output_4_0_g536.y , temp_output_4_0_g536.x , temp_output_9_0_g536 );
			float3 appendResult6892 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g533 = float2( texArray4491.x , texArray4491.y );
			float2 temp_cast_139 = (_Texture_6_Normal_Power).xx;
			float2 temp_output_4_0_g533 = ( ( ( appendResult11_g533 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_139 );
			float2 temp_cast_140 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_141 = (_Texture_6_Normal_Power).xx;
			float2 temp_cast_142 = (_Texture_6_Normal_Power).xx;
			float temp_output_9_0_g533 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g533 , temp_output_4_0_g533 ) ) ) );
			float3 appendResult10_g533 = float3( temp_output_4_0_g533.x , temp_output_4_0_g533.y , temp_output_9_0_g533 );
			float3 appendResult6895 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6429 = BlendComponents;
			float3 weightedAvg6429 = ( ( weightedBlendVar6429.x*( appendResult19_g536 * appendResult6892 ) + weightedBlendVar6429.y*appendResult10_g553 + weightedBlendVar6429.z*( appendResult10_g533 * appendResult6895 ) )/( weightedBlendVar6429.x + weightedBlendVar6429.y + weightedBlendVar6429.z ) );
			fixed3 ifLocalVar6637 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6637 = appendResult10_g553;
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6637 = weightedAvg6429;
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6637 = appendResult10_g553;
			fixed3 ifLocalVar7618 = 0;
			UNITY_BRANCH if( _Texture_6_Normal_Power > -1.0 )
				ifLocalVar7618 = ifLocalVar6637;
			fixed3 Normal_6 = ifLocalVar7618;
			float2 appendResult11_g554 = float2( texArray4567.x , texArray4567.y );
			float2 temp_cast_143 = (_Texture_7_Normal_Power).xx;
			float2 temp_output_4_0_g554 = ( ( ( appendResult11_g554 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_143 );
			float2 temp_cast_144 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_145 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_146 = (_Texture_7_Normal_Power).xx;
			float temp_output_9_0_g554 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g554 , temp_output_4_0_g554 ) ) ) );
			float3 appendResult10_g554 = float3( temp_output_4_0_g554.x , temp_output_4_0_g554.y , temp_output_9_0_g554 );
			float2 appendResult11_g537 = float2( texArray4560.x , texArray4560.y );
			float2 temp_cast_147 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g537 = ( ( ( appendResult11_g537 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_147 );
			float2 temp_cast_148 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_cast_149 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float2 temp_cast_150 = (( _Texture_7_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g537 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g537 , temp_output_4_0_g537 ) ) ) );
			float3 appendResult19_g537 = float3( temp_output_4_0_g537.y , temp_output_4_0_g537.x , temp_output_9_0_g537 );
			float3 appendResult6899 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g529 = float2( texArray4565.x , texArray4565.y );
			float2 temp_cast_151 = (_Texture_7_Normal_Power).xx;
			float2 temp_output_4_0_g529 = ( ( ( appendResult11_g529 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_151 );
			float2 temp_cast_152 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_153 = (_Texture_7_Normal_Power).xx;
			float2 temp_cast_154 = (_Texture_7_Normal_Power).xx;
			float temp_output_9_0_g529 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g529 , temp_output_4_0_g529 ) ) ) );
			float3 appendResult10_g529 = float3( temp_output_4_0_g529.x , temp_output_4_0_g529.y , temp_output_9_0_g529 );
			float3 appendResult6902 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6436 = BlendComponents;
			float3 weightedAvg6436 = ( ( weightedBlendVar6436.x*( appendResult19_g537 * appendResult6899 ) + weightedBlendVar6436.y*appendResult10_g554 + weightedBlendVar6436.z*( appendResult10_g529 * appendResult6902 ) )/( weightedBlendVar6436.x + weightedBlendVar6436.y + weightedBlendVar6436.z ) );
			fixed3 ifLocalVar6643 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6643 = appendResult10_g554;
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6643 = weightedAvg6436;
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6643 = appendResult10_g554;
			fixed3 ifLocalVar7622 = 0;
			UNITY_BRANCH if( _Texture_7_Normal_Power > -1.0 )
				ifLocalVar7622 = ifLocalVar6643;
			fixed3 Normal_7 = ifLocalVar7622;
			float2 appendResult11_g561 = float2( texArray4641.x , texArray4641.y );
			float2 temp_cast_155 = (_Texture_8_Normal_Power).xx;
			float2 temp_output_4_0_g561 = ( ( ( appendResult11_g561 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_155 );
			float2 temp_cast_156 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_157 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_158 = (_Texture_8_Normal_Power).xx;
			float temp_output_9_0_g561 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g561 , temp_output_4_0_g561 ) ) ) );
			float3 appendResult10_g561 = float3( temp_output_4_0_g561.x , temp_output_4_0_g561.y , temp_output_9_0_g561 );
			float2 appendResult11_g539 = float2( texArray4634.x , texArray4634.y );
			float2 temp_cast_159 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g539 = ( ( ( appendResult11_g539 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_159 );
			float2 temp_cast_160 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_cast_161 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float2 temp_cast_162 = (( _Texture_8_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g539 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g539 , temp_output_4_0_g539 ) ) ) );
			float3 appendResult19_g539 = float3( temp_output_4_0_g539.y , temp_output_4_0_g539.x , temp_output_9_0_g539 );
			float3 appendResult6906 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g532 = float2( texArray4639.x , texArray4639.y );
			float2 temp_cast_163 = (_Texture_8_Normal_Power).xx;
			float2 temp_output_4_0_g532 = ( ( ( appendResult11_g532 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_163 );
			float2 temp_cast_164 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_165 = (_Texture_8_Normal_Power).xx;
			float2 temp_cast_166 = (_Texture_8_Normal_Power).xx;
			float temp_output_9_0_g532 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g532 , temp_output_4_0_g532 ) ) ) );
			float3 appendResult10_g532 = float3( temp_output_4_0_g532.x , temp_output_4_0_g532.y , temp_output_9_0_g532 );
			float3 appendResult6909 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6443 = BlendComponents;
			float3 weightedAvg6443 = ( ( weightedBlendVar6443.x*( appendResult19_g539 * appendResult6906 ) + weightedBlendVar6443.y*appendResult10_g561 + weightedBlendVar6443.z*( appendResult10_g532 * appendResult6909 ) )/( weightedBlendVar6443.x + weightedBlendVar6443.y + weightedBlendVar6443.z ) );
			fixed3 ifLocalVar6649 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6649 = appendResult10_g561;
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6649 = weightedAvg6443;
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6649 = appendResult10_g561;
			fixed3 ifLocalVar7626 = 0;
			UNITY_BRANCH if( _Texture_8_Normal_Power > -1.0 )
				ifLocalVar7626 = ifLocalVar6649;
			fixed3 Normal_8 = ifLocalVar7626;
			float4 layeredBlendVar7724 = appendResult6524;
			float3 layeredBlend7724 = ( lerp( lerp( lerp( lerp( layeredBlend7722 , Normal_5 , layeredBlendVar7724.x ) , Normal_6 , layeredBlendVar7724.y ) , Normal_7 , layeredBlendVar7724.z ) , Normal_8 , layeredBlendVar7724.w ) );
			float2 appendResult11_g562 = float2( texArray4788.x , texArray4788.y );
			float2 temp_cast_167 = (_Texture_9_Normal_Power).xx;
			float2 temp_output_4_0_g562 = ( ( ( appendResult11_g562 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_167 );
			float2 temp_cast_168 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_169 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_170 = (_Texture_9_Normal_Power).xx;
			float temp_output_9_0_g562 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g562 , temp_output_4_0_g562 ) ) ) );
			float3 appendResult10_g562 = float3( temp_output_4_0_g562.x , temp_output_4_0_g562.y , temp_output_9_0_g562 );
			float2 appendResult11_g552 = float2( texArray5285.x , texArray5285.y );
			float2 temp_cast_171 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g552 = ( ( ( appendResult11_g552 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_171 );
			float2 temp_cast_172 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_cast_173 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float2 temp_cast_174 = (( _Texture_9_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g552 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g552 , temp_output_4_0_g552 ) ) ) );
			float3 appendResult19_g552 = float3( temp_output_4_0_g552.y , temp_output_4_0_g552.x , temp_output_9_0_g552 );
			float3 appendResult6962 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g557 = float2( texArray4783.x , texArray4783.y );
			float2 temp_cast_175 = (_Texture_9_Normal_Power).xx;
			float2 temp_output_4_0_g557 = ( ( ( appendResult11_g557 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_175 );
			float2 temp_cast_176 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_177 = (_Texture_9_Normal_Power).xx;
			float2 temp_cast_178 = (_Texture_9_Normal_Power).xx;
			float temp_output_9_0_g557 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g557 , temp_output_4_0_g557 ) ) ) );
			float3 appendResult10_g557 = float3( temp_output_4_0_g557.x , temp_output_4_0_g557.y , temp_output_9_0_g557 );
			float3 appendResult6965 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6450 = BlendComponents;
			float3 weightedAvg6450 = ( ( weightedBlendVar6450.x*( appendResult19_g552 * appendResult6962 ) + weightedBlendVar6450.y*appendResult10_g562 + weightedBlendVar6450.z*( appendResult10_g557 * appendResult6965 ) )/( weightedBlendVar6450.x + weightedBlendVar6450.y + weightedBlendVar6450.z ) );
			fixed3 ifLocalVar6667 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6667 = appendResult10_g562;
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar6667 = weightedAvg6450;
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6667 = appendResult10_g562;
			fixed3 ifLocalVar7631 = 0;
			UNITY_BRANCH if( _Texture_9_Normal_Index > -1.0 )
				ifLocalVar7631 = ifLocalVar6667;
			fixed3 Normal_9 = ifLocalVar7631;
			float2 appendResult11_g560 = float2( texArray4822.x , texArray4822.y );
			float2 temp_cast_179 = (_Texture_10_Normal_Power).xx;
			float2 temp_output_4_0_g560 = ( ( ( appendResult11_g560 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_179 );
			float2 temp_cast_180 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_181 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_182 = (_Texture_10_Normal_Power).xx;
			float temp_output_9_0_g560 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g560 , temp_output_4_0_g560 ) ) ) );
			float3 appendResult10_g560 = float3( temp_output_4_0_g560.x , temp_output_4_0_g560.y , temp_output_9_0_g560 );
			float2 appendResult11_g540 = float2( texArray4798.x , texArray4798.y );
			float2 temp_cast_183 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g540 = ( ( ( appendResult11_g540 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_183 );
			float2 temp_cast_184 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_cast_185 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float2 temp_cast_186 = (( _Texture_10_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g540 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g540 , temp_output_4_0_g540 ) ) ) );
			float3 appendResult19_g540 = float3( temp_output_4_0_g540.y , temp_output_4_0_g540.x , temp_output_9_0_g540 );
			float3 appendResult6955 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g535 = float2( texArray4791.x , texArray4791.y );
			float2 temp_cast_187 = (_Texture_10_Normal_Power).xx;
			float2 temp_output_4_0_g535 = ( ( ( appendResult11_g535 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_187 );
			float2 temp_cast_188 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_189 = (_Texture_10_Normal_Power).xx;
			float2 temp_cast_190 = (_Texture_10_Normal_Power).xx;
			float temp_output_9_0_g535 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g535 , temp_output_4_0_g535 ) ) ) );
			float3 appendResult10_g535 = float3( temp_output_4_0_g535.x , temp_output_4_0_g535.y , temp_output_9_0_g535 );
			float3 appendResult6958 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6457 = BlendComponents;
			float3 weightedAvg6457 = ( ( weightedBlendVar6457.x*( appendResult19_g540 * appendResult6955 ) + weightedBlendVar6457.y*appendResult10_g560 + weightedBlendVar6457.z*( appendResult10_g535 * appendResult6958 ) )/( weightedBlendVar6457.x + weightedBlendVar6457.y + weightedBlendVar6457.z ) );
			fixed3 ifLocalVar6661 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6661 = appendResult10_g560;
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6661 = weightedAvg6457;
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6661 = appendResult10_g560;
			fixed3 ifLocalVar7636 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7636 = ifLocalVar6661;
			fixed3 ifLocalVar7637 = 0;
			UNITY_BRANCH if( _Texture_10_Normal_Index > -1.0 )
				ifLocalVar7637 = ifLocalVar7636;
			fixed3 Normal_10 = ifLocalVar7637;
			float2 appendResult11_g566 = float2( texArray4856.x , texArray4856.y );
			float2 temp_cast_191 = (_Texture_11_Normal_Power).xx;
			float2 temp_output_4_0_g566 = ( ( ( appendResult11_g566 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_191 );
			float2 temp_cast_192 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_193 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_194 = (_Texture_11_Normal_Power).xx;
			float temp_output_9_0_g566 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g566 , temp_output_4_0_g566 ) ) ) );
			float3 appendResult10_g566 = float3( temp_output_4_0_g566.x , temp_output_4_0_g566.y , temp_output_9_0_g566 );
			float2 appendResult11_g547 = float2( texArray4828.x , texArray4828.y );
			float2 temp_cast_195 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g547 = ( ( ( appendResult11_g547 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_195 );
			float2 temp_cast_196 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_cast_197 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float2 temp_cast_198 = (( _Texture_11_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g547 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g547 , temp_output_4_0_g547 ) ) ) );
			float3 appendResult19_g547 = float3( temp_output_4_0_g547.y , temp_output_4_0_g547.x , temp_output_9_0_g547 );
			float3 appendResult6948 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g556 = float2( texArray4811.x , texArray4811.y );
			float2 temp_cast_199 = (_Texture_11_Normal_Power).xx;
			float2 temp_output_4_0_g556 = ( ( ( appendResult11_g556 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_199 );
			float2 temp_cast_200 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_201 = (_Texture_11_Normal_Power).xx;
			float2 temp_cast_202 = (_Texture_11_Normal_Power).xx;
			float temp_output_9_0_g556 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g556 , temp_output_4_0_g556 ) ) ) );
			float3 appendResult10_g556 = float3( temp_output_4_0_g556.x , temp_output_4_0_g556.y , temp_output_9_0_g556 );
			float3 appendResult6951 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6464 = BlendComponents;
			float3 weightedAvg6464 = ( ( weightedBlendVar6464.x*( appendResult19_g547 * appendResult6948 ) + weightedBlendVar6464.y*appendResult10_g566 + weightedBlendVar6464.z*( appendResult10_g556 * appendResult6951 ) )/( weightedBlendVar6464.x + weightedBlendVar6464.y + weightedBlendVar6464.z ) );
			fixed3 ifLocalVar6655 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6655 = appendResult10_g566;
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6655 = weightedAvg6464;
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6655 = appendResult10_g566;
			fixed3 ifLocalVar7641 = 0;
			UNITY_BRANCH if( _Texture_11_Normal_Power > -1.0 )
				ifLocalVar7641 = ifLocalVar6655;
			fixed3 Normal_11 = ifLocalVar7641;
			float2 appendResult11_g565 = float2( texArray4870.x , texArray4870.y );
			float2 temp_cast_203 = (_Texture_12_Normal_Power).xx;
			float2 temp_output_4_0_g565 = ( ( ( appendResult11_g565 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_203 );
			float2 temp_cast_204 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_205 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_206 = (_Texture_12_Normal_Power).xx;
			float temp_output_9_0_g565 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g565 , temp_output_4_0_g565 ) ) ) );
			float3 appendResult10_g565 = float3( temp_output_4_0_g565.x , temp_output_4_0_g565.y , temp_output_9_0_g565 );
			float2 appendResult11_g558 = float2( texArray4850.x , texArray4850.y );
			float2 temp_cast_207 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g558 = ( ( ( appendResult11_g558 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_207 );
			float2 temp_cast_208 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_cast_209 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float2 temp_cast_210 = (( _Texture_12_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g558 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g558 , temp_output_4_0_g558 ) ) ) );
			float3 appendResult19_g558 = float3( temp_output_4_0_g558.y , temp_output_4_0_g558.x , temp_output_9_0_g558 );
			float3 appendResult6941 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g555 = float2( texArray4852.x , texArray4852.y );
			float2 temp_cast_211 = (_Texture_12_Normal_Power).xx;
			float2 temp_output_4_0_g555 = ( ( ( appendResult11_g555 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_211 );
			float2 temp_cast_212 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_213 = (_Texture_12_Normal_Power).xx;
			float2 temp_cast_214 = (_Texture_12_Normal_Power).xx;
			float temp_output_9_0_g555 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g555 , temp_output_4_0_g555 ) ) ) );
			float3 appendResult10_g555 = float3( temp_output_4_0_g555.x , temp_output_4_0_g555.y , temp_output_9_0_g555 );
			float3 appendResult6944 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6471 = BlendComponents;
			float3 weightedAvg6471 = ( ( weightedBlendVar6471.x*( appendResult19_g558 * appendResult6941 ) + weightedBlendVar6471.y*appendResult10_g565 + weightedBlendVar6471.z*( appendResult10_g555 * appendResult6944 ) )/( weightedBlendVar6471.x + weightedBlendVar6471.y + weightedBlendVar6471.z ) );
			fixed3 ifLocalVar6673 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6673 = appendResult10_g565;
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6673 = weightedAvg6471;
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6673 = appendResult10_g565;
			fixed3 ifLocalVar7645 = 0;
			UNITY_BRANCH if( _Texture_12_Normal_Power > -1.0 )
				ifLocalVar7645 = ifLocalVar6673;
			fixed3 Normal_12 = ifLocalVar7645;
			float4 layeredBlendVar7725 = appendResult6529;
			float3 layeredBlend7725 = ( lerp( lerp( lerp( lerp( layeredBlend7724 , Normal_9 , layeredBlendVar7725.x ) , Normal_10 , layeredBlendVar7725.y ) , Normal_11 , layeredBlendVar7725.z ) , Normal_12 , layeredBlendVar7725.w ) );
			float2 appendResult11_g575 = float2( texArray5120.x , texArray5120.y );
			float2 temp_cast_215 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_output_4_0_g575 = ( ( ( appendResult11_g575 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_215 );
			float2 temp_cast_216 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_217 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_218 = (_Texture_13_Normal_Powerr).xx;
			float temp_output_9_0_g575 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g575 , temp_output_4_0_g575 ) ) ) );
			float3 appendResult10_g575 = float3( temp_output_4_0_g575.x , temp_output_4_0_g575.y , temp_output_9_0_g575 );
			float2 appendResult11_g569 = float2( texArray5127.x , texArray5127.y );
			float2 temp_cast_219 = (( _Texture_13_Normal_Powerr * -1 )).xx;
			float2 temp_output_4_0_g569 = ( ( ( appendResult11_g569 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_219 );
			float2 temp_cast_220 = (( _Texture_13_Normal_Powerr * -1 )).xx;
			float2 temp_cast_221 = (( _Texture_13_Normal_Powerr * -1 )).xx;
			float2 temp_cast_222 = (( _Texture_13_Normal_Powerr * -1 )).xx;
			float temp_output_9_0_g569 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g569 , temp_output_4_0_g569 ) ) ) );
			float3 appendResult19_g569 = float3( temp_output_4_0_g569.y , temp_output_4_0_g569.x , temp_output_9_0_g569 );
			float3 appendResult6934 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g571 = float2( texArray5109.x , texArray5109.y );
			float2 temp_cast_223 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_output_4_0_g571 = ( ( ( appendResult11_g571 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_223 );
			float2 temp_cast_224 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_225 = (_Texture_13_Normal_Powerr).xx;
			float2 temp_cast_226 = (_Texture_13_Normal_Powerr).xx;
			float temp_output_9_0_g571 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g571 , temp_output_4_0_g571 ) ) ) );
			float3 appendResult10_g571 = float3( temp_output_4_0_g571.x , temp_output_4_0_g571.y , temp_output_9_0_g571 );
			float3 appendResult6937 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6478 = BlendComponents;
			float3 weightedAvg6478 = ( ( weightedBlendVar6478.x*( appendResult19_g569 * appendResult6934 ) + weightedBlendVar6478.y*appendResult10_g575 + weightedBlendVar6478.z*( appendResult10_g571 * appendResult6937 ) )/( weightedBlendVar6478.x + weightedBlendVar6478.y + weightedBlendVar6478.z ) );
			fixed3 ifLocalVar6679 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6679 = appendResult10_g575;
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6679 = weightedAvg6478;
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6679 = appendResult10_g575;
			fixed3 ifLocalVar7649 = 0;
			UNITY_BRANCH if( _Texture_13_Normal_Powerr > -1.0 )
				ifLocalVar7649 = ifLocalVar6679;
			fixed3 Normal_13 = ifLocalVar7649;
			float2 appendResult11_g573 = float2( texArray5178.x , texArray5178.y );
			float2 temp_cast_227 = (_Texture_14_Normal_Power).xx;
			float2 temp_output_4_0_g573 = ( ( ( appendResult11_g573 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_227 );
			float2 temp_cast_228 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_229 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_230 = (_Texture_14_Normal_Power).xx;
			float temp_output_9_0_g573 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g573 , temp_output_4_0_g573 ) ) ) );
			float3 appendResult10_g573 = float3( temp_output_4_0_g573.x , temp_output_4_0_g573.y , temp_output_9_0_g573 );
			float2 appendResult11_g567 = float2( texArray5017.x , texArray5017.y );
			float2 temp_cast_231 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g567 = ( ( ( appendResult11_g567 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_231 );
			float2 temp_cast_232 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_cast_233 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float2 temp_cast_234 = (( _Texture_14_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g567 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g567 , temp_output_4_0_g567 ) ) ) );
			float3 appendResult19_g567 = float3( temp_output_4_0_g567.y , temp_output_4_0_g567.x , temp_output_9_0_g567 );
			float3 appendResult6927 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g568 = float2( texArray5170.x , texArray5170.y );
			float2 temp_cast_235 = (_Texture_14_Normal_Power).xx;
			float2 temp_output_4_0_g568 = ( ( ( appendResult11_g568 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_235 );
			float2 temp_cast_236 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_237 = (_Texture_14_Normal_Power).xx;
			float2 temp_cast_238 = (_Texture_14_Normal_Power).xx;
			float temp_output_9_0_g568 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g568 , temp_output_4_0_g568 ) ) ) );
			float3 appendResult10_g568 = float3( temp_output_4_0_g568.x , temp_output_4_0_g568.y , temp_output_9_0_g568 );
			float3 appendResult6930 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6485 = BlendComponents;
			float3 weightedAvg6485 = ( ( weightedBlendVar6485.x*( appendResult19_g567 * appendResult6927 ) + weightedBlendVar6485.y*appendResult10_g573 + weightedBlendVar6485.z*( appendResult10_g568 * appendResult6930 ) )/( weightedBlendVar6485.x + weightedBlendVar6485.y + weightedBlendVar6485.z ) );
			fixed3 ifLocalVar6685 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6685 = appendResult10_g573;
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6685 = weightedAvg6485;
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6685 = appendResult10_g573;
			fixed3 ifLocalVar7653 = 0;
			UNITY_BRANCH if( _Texture_14_Normal_Index > -1.0 )
				ifLocalVar7653 = ifLocalVar6685;
			fixed3 Normal_14 = ifLocalVar7653;
			float2 appendResult11_g574 = float2( texArray5246.x , texArray5246.y );
			float2 temp_cast_239 = (_Texture_15_Normal_Power).xx;
			float2 temp_output_4_0_g574 = ( ( ( appendResult11_g574 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_239 );
			float2 temp_cast_240 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_241 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_242 = (_Texture_15_Normal_Power).xx;
			float temp_output_9_0_g574 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g574 , temp_output_4_0_g574 ) ) ) );
			float3 appendResult10_g574 = float3( temp_output_4_0_g574.x , temp_output_4_0_g574.y , temp_output_9_0_g574 );
			float2 appendResult11_g564 = float2( texArray5227.x , texArray5227.y );
			float2 temp_cast_243 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g564 = ( ( ( appendResult11_g564 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_243 );
			float2 temp_cast_244 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_cast_245 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float2 temp_cast_246 = (( _Texture_15_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g564 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g564 , temp_output_4_0_g564 ) ) ) );
			float3 appendResult19_g564 = float3( temp_output_4_0_g564.y , temp_output_4_0_g564.x , temp_output_9_0_g564 );
			float3 appendResult6920 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g563 = float2( texArray5250.x , texArray5250.y );
			float2 temp_cast_247 = (_Texture_15_Normal_Power).xx;
			float2 temp_output_4_0_g563 = ( ( ( appendResult11_g563 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_247 );
			float2 temp_cast_248 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_249 = (_Texture_15_Normal_Power).xx;
			float2 temp_cast_250 = (_Texture_15_Normal_Power).xx;
			float temp_output_9_0_g563 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g563 , temp_output_4_0_g563 ) ) ) );
			float3 appendResult10_g563 = float3( temp_output_4_0_g563.x , temp_output_4_0_g563.y , temp_output_9_0_g563 );
			float3 appendResult6923 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6492 = BlendComponents;
			float3 weightedAvg6492 = ( ( weightedBlendVar6492.x*( appendResult19_g564 * appendResult6920 ) + weightedBlendVar6492.y*appendResult10_g574 + weightedBlendVar6492.z*( appendResult10_g563 * appendResult6923 ) )/( weightedBlendVar6492.x + weightedBlendVar6492.y + weightedBlendVar6492.z ) );
			fixed3 ifLocalVar6691 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6691 = appendResult10_g574;
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6691 = weightedAvg6492;
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6691 = appendResult10_g574;
			fixed3 ifLocalVar7657 = 0;
			UNITY_BRANCH if( _Texture_15_Normal_Index > -1.0 )
				ifLocalVar7657 = ifLocalVar6691;
			fixed3 Normal_15 = ifLocalVar7657;
			float2 appendResult11_g576 = float2( texArray5099.x , texArray5099.y );
			float2 temp_cast_251 = (_Texture_16_Normal_Power).xx;
			float2 temp_output_4_0_g576 = ( ( ( appendResult11_g576 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_251 );
			float2 temp_cast_252 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_253 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_254 = (_Texture_16_Normal_Power).xx;
			float temp_output_9_0_g576 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g576 , temp_output_4_0_g576 ) ) ) );
			float3 appendResult10_g576 = float3( temp_output_4_0_g576.x , temp_output_4_0_g576.y , temp_output_9_0_g576 );
			float2 appendResult11_g570 = float2( texArray5082.x , texArray5082.y );
			float2 temp_cast_255 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_output_4_0_g570 = ( ( ( appendResult11_g570 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_255 );
			float2 temp_cast_256 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_cast_257 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float2 temp_cast_258 = (( _Texture_16_Normal_Power * -1.0 )).xx;
			float temp_output_9_0_g570 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g570 , temp_output_4_0_g570 ) ) ) );
			float3 appendResult19_g570 = float3( temp_output_4_0_g570.y , temp_output_4_0_g570.x , temp_output_9_0_g570 );
			float3 appendResult6913 = float3( ase_worldNormal.x , -1 , 1 );
			float2 appendResult11_g572 = float2( texArray4731.x , texArray4731.y );
			float2 temp_cast_259 = (_Texture_16_Normal_Power).xx;
			float2 temp_output_4_0_g572 = ( ( ( appendResult11_g572 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_259 );
			float2 temp_cast_260 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_261 = (_Texture_16_Normal_Power).xx;
			float2 temp_cast_262 = (_Texture_16_Normal_Power).xx;
			float temp_output_9_0_g572 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g572 , temp_output_4_0_g572 ) ) ) );
			float3 appendResult10_g572 = float3( temp_output_4_0_g572.x , temp_output_4_0_g572.y , temp_output_9_0_g572 );
			float3 appendResult6916 = float3( 1 , ( ase_worldNormal.z * -1.0 ) , 1 );
			float3 weightedBlendVar6499 = BlendComponents;
			float3 weightedAvg6499 = ( ( weightedBlendVar6499.x*( appendResult19_g570 * appendResult6913 ) + weightedBlendVar6499.y*appendResult10_g576 + weightedBlendVar6499.z*( appendResult10_g572 * appendResult6916 ) )/( weightedBlendVar6499.x + weightedBlendVar6499.y + weightedBlendVar6499.z ) );
			fixed3 ifLocalVar6697 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6697 = appendResult10_g576;
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6697 = weightedAvg6499;
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6697 = appendResult10_g576;
			fixed3 ifLocalVar7662 = 0;
			UNITY_BRANCH if( _Texture_16_Normal_Index > -1.0 )
				ifLocalVar7662 = ifLocalVar6697;
			fixed3 Normal_16 = ifLocalVar7662;
			float4 layeredBlendVar7726 = appendResult6533;
			float3 layeredBlend7726 = ( lerp( lerp( lerp( lerp( layeredBlend7725 , Normal_13 , layeredBlendVar7726.x ) , Normal_14 , layeredBlendVar7726.y ) , Normal_15 , layeredBlendVar7726.z ) , Normal_16 , layeredBlendVar7726.w ) );
			fixed2 temp_cast_263 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray4382 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Normal, float3(( appendResult3679 * temp_cast_263 ), (float)_Texture_Snow_Normal_Index)  );
			float2 appendResult11_g577 = float2( texArray4382.x , texArray4382.y );
			float2 temp_cast_265 = (_Snow_Normal_Scale).xx;
			float2 temp_output_4_0_g577 = ( ( ( appendResult11_g577 * float2( 2,2 ) ) + float2( -1,-1 ) ) * temp_cast_265 );
			float2 temp_cast_266 = (_Snow_Normal_Scale).xx;
			float2 temp_cast_267 = (_Snow_Normal_Scale).xx;
			float2 temp_cast_268 = (_Snow_Normal_Scale).xx;
			float temp_output_9_0_g577 = sqrt( ( 1.0 - saturate( dot( temp_output_4_0_g577 , temp_output_4_0_g577 ) ) ) );
			float3 appendResult10_g577 = float3( temp_output_4_0_g577.x , temp_output_4_0_g577.y , temp_output_9_0_g577 );
			o.Normal = BlendNormals( lerp( lerp( float3( 0,0,1 ) , lerp( appendResult10_g578 , appendResult10_g579 , UVmixDistance ) , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , lerp( appendResult10_g578 , appendResult10_g579 , UVmixDistance ) , ( _Snow_Perlin_Power * 0.5 ) ) , HeightMask6539 ) , normalize( lerp( lerp( normalize( layeredBlend7726 ) , lerp( normalize( layeredBlend7726 ) , appendResult10_g577 , _Snow_Blend_Normal ) , HeightMask6539 ) , UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) , UVmixDistance ) ) );
			float4 texArray3292 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Albedo_Index)  );
			fixed2 temp_cast_269 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3293 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3284 ) / temp_cast_269 ), _Texture_1_Albedo_Index)  );
			float4 texArray3287 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3284 ), _Texture_1_Albedo_Index)  );
			float4 texArray3294 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3284 ), _Texture_1_Albedo_Index)  );
			float3 weightedBlendVar6389 = BlendComponents;
			float4 weightedAvg6389 = ( ( weightedBlendVar6389.x*texArray3287 + weightedBlendVar6389.y*texArray3292 + weightedBlendVar6389.z*texArray3294 )/( weightedBlendVar6389.x + weightedBlendVar6389.y + weightedBlendVar6389.z ) );
			fixed2 temp_cast_270 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3291 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3284 ) / temp_cast_270 ), _Texture_1_Albedo_Index)  );
			fixed2 temp_cast_271 = (_Texture_1_Far_Multiplier).xx;
			float4 texArray3295 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3284 ) / temp_cast_271 ), _Texture_1_Albedo_Index)  );
			float3 weightedBlendVar6390 = BlendComponents;
			float4 weightedAvg6390 = ( ( weightedBlendVar6390.x*texArray3291 + weightedBlendVar6390.y*texArray3293 + weightedBlendVar6390.z*texArray3295 )/( weightedBlendVar6390.x + weightedBlendVar6390.y + weightedBlendVar6390.z ) );
			fixed4 ifLocalVar6607 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar6607 = lerp( texArray3292 , texArray3293 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar6607 = lerp( weightedAvg6389 , weightedAvg6390 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar6607 = lerp( texArray3292 , texArray3293 , UVmixDistance );
			fixed4 ifLocalVar7593 = 0;
			UNITY_BRANCH if( _Texture_1_Albedo_Index > -1.0 )
				ifLocalVar7593 = ( ifLocalVar6607 * _Texture_1_Color );
			fixed4 Texture_1_Final = ifLocalVar7593;
			float4 texArray3338 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Albedo_Index)  );
			fixed2 temp_cast_272 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3339 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3349 ) / temp_cast_272 ), _Texture_2_Albedo_Index)  );
			float4 texArray3355 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3349 ), _Texture_2_Albedo_Index)  );
			float4 texArray3341 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3349 ), _Texture_2_Albedo_Index)  );
			float3 weightedBlendVar6396 = BlendComponents;
			float4 weightedAvg6396 = ( ( weightedBlendVar6396.x*texArray3355 + weightedBlendVar6396.y*texArray3338 + weightedBlendVar6396.z*texArray3341 )/( weightedBlendVar6396.x + weightedBlendVar6396.y + weightedBlendVar6396.z ) );
			fixed2 temp_cast_273 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3356 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3349 ) / temp_cast_273 ), _Texture_2_Albedo_Index)  );
			fixed2 temp_cast_274 = (_Texture_2_Far_Multiplier).xx;
			float4 texArray3342 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3349 ) / temp_cast_274 ), _Texture_2_Albedo_Index)  );
			float3 weightedBlendVar6398 = BlendComponents;
			float4 weightedAvg6398 = ( ( weightedBlendVar6398.x*texArray3356 + weightedBlendVar6398.y*texArray3339 + weightedBlendVar6398.z*texArray3342 )/( weightedBlendVar6398.x + weightedBlendVar6398.y + weightedBlendVar6398.z ) );
			fixed4 ifLocalVar6612 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar6612 = lerp( texArray3338 , texArray3339 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar6612 = lerp( weightedAvg6396 , weightedAvg6398 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar6612 = lerp( texArray3338 , texArray3339 , UVmixDistance );
			fixed4 ifLocalVar7599 = 0;
			UNITY_BRANCH if( _Texture_2_Albedo_Index > -1.0 )
				ifLocalVar7599 = ( ifLocalVar6612 * _Texture_2_Color );
			fixed4 Texture_2_Final = ifLocalVar7599;
			float4 texArray3405 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Albedo_Index)  );
			fixed2 temp_cast_275 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3415 ) / temp_cast_275 ), _Texture_3_Albedo_Index)  );
			float4 texArray3419 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3415 ), _Texture_3_Albedo_Index)  );
			float4 texArray3408 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3415 ), _Texture_3_Albedo_Index)  );
			float3 weightedBlendVar6403 = BlendComponents;
			float4 weightedAvg6403 = ( ( weightedBlendVar6403.x*texArray3419 + weightedBlendVar6403.y*texArray3405 + weightedBlendVar6403.z*texArray3408 )/( weightedBlendVar6403.x + weightedBlendVar6403.y + weightedBlendVar6403.z ) );
			fixed2 temp_cast_276 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3420 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3415 ) / temp_cast_276 ), _Texture_3_Albedo_Index)  );
			fixed2 temp_cast_277 = (_Texture_3_Far_Multiplier).xx;
			float4 texArray3409 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3415 ) / temp_cast_277 ), _Texture_3_Albedo_Index)  );
			float3 weightedBlendVar6405 = BlendComponents;
			float4 weightedAvg6405 = ( ( weightedBlendVar6405.x*texArray3420 + weightedBlendVar6405.y*texArray3406 + weightedBlendVar6405.z*texArray3409 )/( weightedBlendVar6405.x + weightedBlendVar6405.y + weightedBlendVar6405.z ) );
			fixed4 ifLocalVar6618 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar6618 = lerp( texArray3405 , texArray3406 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar6618 = lerp( weightedAvg6403 , weightedAvg6405 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar6618 = lerp( texArray3405 , texArray3406 , UVmixDistance );
			fixed4 ifLocalVar7603 = 0;
			UNITY_BRANCH if( _Texture_3_Albedo_Index > -1.0 )
				ifLocalVar7603 = ( ifLocalVar6618 * _Texture_3_Color );
			fixed4 Texture_3_Final = ifLocalVar7603;
			float4 texArray3472 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_278 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3473 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult3482 ) / temp_cast_278 ), _Texture_4_Albedo_Index)  );
			float4 texArray3486 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult3482 ), _Texture_4_Albedo_Index)  );
			float4 texArray3475 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult3482 ), _Texture_4_Albedo_Index)  );
			float3 weightedBlendVar6410 = BlendComponents;
			float4 weightedAvg6410 = ( ( weightedBlendVar6410.x*texArray3486 + weightedBlendVar6410.y*texArray3472 + weightedBlendVar6410.z*texArray3475 )/( weightedBlendVar6410.x + weightedBlendVar6410.y + weightedBlendVar6410.z ) );
			fixed2 temp_cast_279 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3487 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult3482 ) / temp_cast_279 ), _Texture_4_Albedo_Index)  );
			fixed2 temp_cast_280 = (_Texture_4_Far_Multiplier).xx;
			float4 texArray3476 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult3482 ) / temp_cast_280 ), _Texture_4_Albedo_Index)  );
			float3 weightedBlendVar6412 = BlendComponents;
			float4 weightedAvg6412 = ( ( weightedBlendVar6412.x*texArray3487 + weightedBlendVar6412.y*texArray3473 + weightedBlendVar6412.z*texArray3476 )/( weightedBlendVar6412.x + weightedBlendVar6412.y + weightedBlendVar6412.z ) );
			fixed4 ifLocalVar6624 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar6624 = lerp( texArray3472 , texArray3473 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar6624 = lerp( weightedAvg6410 , weightedAvg6412 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar6624 = lerp( texArray3472 , texArray3473 , UVmixDistance );
			fixed4 ifLocalVar7608 = 0;
			UNITY_BRANCH if( _Texture_4_Albedo_Index > -1.0 )
				ifLocalVar7608 = ( ifLocalVar6624 * _Texture_4_Color );
			fixed4 Texture_4_Final = ifLocalVar7608;
			float4 layeredBlendVar6512 = appendResult6517;
			float4 layeredBlend6512 = ( lerp( lerp( lerp( lerp( float4( 0,0,0,0 ) , Texture_1_Final , layeredBlendVar6512.x ) , Texture_2_Final , layeredBlendVar6512.y ) , Texture_3_Final , layeredBlendVar6512.z ) , Texture_4_Final , layeredBlendVar6512.w ) );
			float4 texArray4450 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Albedo_Index)  );
			fixed2 temp_cast_281 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4445 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4399 ) / temp_cast_281 ), _Texture_5_Albedo_Index)  );
			float4 texArray4442 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4399 ), _Texture_5_Albedo_Index)  );
			float4 texArray4443 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4399 ), _Texture_5_Albedo_Index)  );
			float3 weightedBlendVar6417 = BlendComponents;
			float4 weightedAvg6417 = ( ( weightedBlendVar6417.x*texArray4442 + weightedBlendVar6417.y*texArray4450 + weightedBlendVar6417.z*texArray4443 )/( weightedBlendVar6417.x + weightedBlendVar6417.y + weightedBlendVar6417.z ) );
			fixed2 temp_cast_282 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4444 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4399 ) / temp_cast_282 ), _Texture_5_Albedo_Index)  );
			fixed2 temp_cast_283 = (_Texture_5_Far_Multiplier).xx;
			float4 texArray4439 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4399 ) / temp_cast_283 ), _Texture_5_Albedo_Index)  );
			float3 weightedBlendVar6419 = BlendComponents;
			float4 weightedAvg6419 = ( ( weightedBlendVar6419.x*texArray4444 + weightedBlendVar6419.y*texArray4445 + weightedBlendVar6419.z*texArray4439 )/( weightedBlendVar6419.x + weightedBlendVar6419.y + weightedBlendVar6419.z ) );
			fixed4 ifLocalVar6630 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar6630 = lerp( texArray4450 , texArray4445 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar6630 = lerp( weightedAvg6417 , weightedAvg6419 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar6630 = lerp( texArray4450 , texArray4445 , UVmixDistance );
			fixed4 ifLocalVar7613 = 0;
			UNITY_BRANCH if( _Texture_5_Albedo_Index > -1.0 )
				ifLocalVar7613 = ( ifLocalVar6630 * _Texture_5_Color );
			fixed4 Texture_5_Final = ifLocalVar7613;
			float4 texArray4517 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Albedo_Index)  );
			fixed2 temp_cast_284 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4512 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4471 ) / temp_cast_284 ), _Texture_6_Albedo_Index)  );
			float4 texArray4509 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4471 ), _Texture_6_Albedo_Index)  );
			float4 texArray4510 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4471 ), _Texture_6_Albedo_Index)  );
			float3 weightedBlendVar6424 = BlendComponents;
			float4 weightedAvg6424 = ( ( weightedBlendVar6424.x*texArray4509 + weightedBlendVar6424.y*texArray4517 + weightedBlendVar6424.z*texArray4510 )/( weightedBlendVar6424.x + weightedBlendVar6424.y + weightedBlendVar6424.z ) );
			fixed2 temp_cast_285 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4511 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4471 ) / temp_cast_285 ), _Texture_6_Albedo_Index)  );
			fixed2 temp_cast_286 = (_Texture_6_Far_Multiplier).xx;
			float4 texArray4506 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4471 ) / temp_cast_286 ), _Texture_6_Albedo_Index)  );
			float3 weightedBlendVar6426 = BlendComponents;
			float4 weightedAvg6426 = ( ( weightedBlendVar6426.x*texArray4511 + weightedBlendVar6426.y*texArray4512 + weightedBlendVar6426.z*texArray4506 )/( weightedBlendVar6426.x + weightedBlendVar6426.y + weightedBlendVar6426.z ) );
			fixed4 ifLocalVar6636 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar6636 = lerp( texArray4517 , texArray4512 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar6636 = lerp( weightedAvg6424 , weightedAvg6426 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar6636 = lerp( texArray4517 , texArray4512 , UVmixDistance );
			fixed4 ifLocalVar7617 = 0;
			UNITY_BRANCH if( _Texture_6_Albedo_Index > -1.0 )
				ifLocalVar7617 = ( ifLocalVar6636 * _Texture_6_Color );
			fixed4 Texture_6_Final = ifLocalVar7617;
			float4 texArray4591 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4545 ), _Texture_7_Albedo_Index)  );
			fixed2 temp_cast_287 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4586 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4545 ) / temp_cast_287 ), _Texture_7_Albedo_Index)  );
			float4 texArray4583 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4545 ), _Texture_7_Albedo_Index)  );
			float4 texArray4584 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4545 ), _Texture_7_Albedo_Index)  );
			float3 weightedBlendVar6431 = BlendComponents;
			float4 weightedAvg6431 = ( ( weightedBlendVar6431.x*texArray4583 + weightedBlendVar6431.y*texArray4591 + weightedBlendVar6431.z*texArray4584 )/( weightedBlendVar6431.x + weightedBlendVar6431.y + weightedBlendVar6431.z ) );
			fixed2 temp_cast_288 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4585 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4545 ) / temp_cast_288 ), _Texture_7_Albedo_Index)  );
			fixed2 temp_cast_289 = (_Texture_7_Far_Multiplier).xx;
			float4 texArray4580 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4545 ) / temp_cast_289 ), _Texture_7_Albedo_Index)  );
			float3 weightedBlendVar6433 = BlendComponents;
			float4 weightedAvg6433 = ( ( weightedBlendVar6433.x*texArray4585 + weightedBlendVar6433.y*texArray4586 + weightedBlendVar6433.z*texArray4580 )/( weightedBlendVar6433.x + weightedBlendVar6433.y + weightedBlendVar6433.z ) );
			fixed4 ifLocalVar6642 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar6642 = lerp( texArray4591 , texArray4586 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar6642 = lerp( weightedAvg6431 , weightedAvg6433 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar6642 = lerp( texArray4591 , texArray4586 , UVmixDistance );
			fixed4 ifLocalVar7621 = 0;
			UNITY_BRANCH if( _Texture_7_Albedo_Index > -1.0 )
				ifLocalVar7621 = ( ifLocalVar6642 * _Texture_7_Color );
			fixed4 Texture_7_Final = ifLocalVar7621;
			float4 texArray4665 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Albedo_Index)  );
			fixed2 temp_cast_290 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4660 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4619 ) / temp_cast_290 ), _Texture_8_Albedo_Index)  );
			float4 texArray4657 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4619 ), _Texture_8_Albedo_Index)  );
			float4 texArray4658 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4619 ), _Texture_8_Albedo_Index)  );
			float3 weightedBlendVar6438 = BlendComponents;
			float4 weightedAvg6438 = ( ( weightedBlendVar6438.x*texArray4657 + weightedBlendVar6438.y*texArray4665 + weightedBlendVar6438.z*texArray4658 )/( weightedBlendVar6438.x + weightedBlendVar6438.y + weightedBlendVar6438.z ) );
			fixed2 temp_cast_291 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4659 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4619 ) / temp_cast_291 ), _Texture_8_Albedo_Index)  );
			fixed2 temp_cast_292 = (_Texture_8_Far_Multiplier).xx;
			float4 texArray4654 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4619 ) / temp_cast_292 ), _Texture_8_Albedo_Index)  );
			float3 weightedBlendVar6440 = BlendComponents;
			float4 weightedAvg6440 = ( ( weightedBlendVar6440.x*texArray4659 + weightedBlendVar6440.y*texArray4660 + weightedBlendVar6440.z*texArray4654 )/( weightedBlendVar6440.x + weightedBlendVar6440.y + weightedBlendVar6440.z ) );
			fixed4 ifLocalVar6648 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar6648 = lerp( texArray4665 , texArray4660 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar6648 = lerp( weightedAvg6438 , weightedAvg6440 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar6648 = lerp( texArray4665 , texArray4660 , UVmixDistance );
			fixed4 ifLocalVar7625 = 0;
			UNITY_BRANCH if( _Texture_8_Albedo_Index > -1.0 )
				ifLocalVar7625 = ( ifLocalVar6648 * _Texture_8_Color );
			fixed4 Texture_8_Final = ifLocalVar7625;
			float4 layeredBlendVar6520 = appendResult6524;
			float4 layeredBlend6520 = ( lerp( lerp( lerp( lerp( layeredBlend6512 , Texture_5_Final , layeredBlendVar6520.x ) , Texture_6_Final , layeredBlendVar6520.y ) , Texture_7_Final , layeredBlendVar6520.z ) , Texture_8_Final , layeredBlendVar6520.w ) );
			float4 texArray4723 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Albedo_Index)  );
			fixed2 temp_cast_293 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4889 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4736 ) / temp_cast_293 ), _Texture_9_Albedo_Index)  );
			float4 texArray5286 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4736 ), _Texture_9_Albedo_Index)  );
			float4 texArray4858 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4736 ), _Texture_9_Albedo_Index)  );
			float3 weightedBlendVar6445 = BlendComponents;
			float4 weightedAvg6445 = ( ( weightedBlendVar6445.x*texArray5286 + weightedBlendVar6445.y*texArray4723 + weightedBlendVar6445.z*texArray4858 )/( weightedBlendVar6445.x + weightedBlendVar6445.y + weightedBlendVar6445.z ) );
			fixed2 temp_cast_294 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4719 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4736 ) / temp_cast_294 ), _Texture_9_Albedo_Index)  );
			fixed2 temp_cast_295 = (_Texture_9_Far_Multiplier).xx;
			float4 texArray4865 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4736 ) / temp_cast_295 ), _Texture_9_Albedo_Index)  );
			float3 weightedBlendVar6447 = BlendComponents;
			float4 weightedAvg6447 = ( ( weightedBlendVar6447.x*texArray4719 + weightedBlendVar6447.y*texArray4889 + weightedBlendVar6447.z*texArray4865 )/( weightedBlendVar6447.x + weightedBlendVar6447.y + weightedBlendVar6447.z ) );
			fixed4 ifLocalVar6666 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar6666 = lerp( texArray4723 , texArray4889 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar6666 = lerp( weightedAvg6445 , weightedAvg6447 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar6666 = lerp( texArray4723 , texArray4889 , UVmixDistance );
			fixed4 ifLocalVar7630 = 0;
			UNITY_BRANCH if( _Texture_9_Albedo_Index > -1.0 )
				ifLocalVar7630 = ( ifLocalVar6666 * _Texture_9_Color );
			fixed4 Texture_9_Final = ifLocalVar7630;
			float4 texArray4899 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Albedo_Index)  );
			fixed2 temp_cast_296 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4913 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4738 ) / temp_cast_296 ), _Texture_10_Albedo_Index)  );
			float4 texArray4886 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4738 ), _Texture_10_Albedo_Index)  );
			float4 texArray4877 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4738 ), _Texture_10_Albedo_Index)  );
			float3 weightedBlendVar6452 = BlendComponents;
			float4 weightedAvg6452 = ( ( weightedBlendVar6452.x*texArray4886 + weightedBlendVar6452.y*texArray4899 + weightedBlendVar6452.z*texArray4877 )/( weightedBlendVar6452.x + weightedBlendVar6452.y + weightedBlendVar6452.z ) );
			fixed2 temp_cast_297 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4894 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4738 ) / temp_cast_297 ), _Texture_10_Albedo_Index)  );
			fixed2 temp_cast_298 = (_Texture_10_Far_Multiplier).xx;
			float4 texArray4878 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4738 ) / temp_cast_298 ), _Texture_10_Albedo_Index)  );
			float3 weightedBlendVar6454 = BlendComponents;
			float4 weightedAvg6454 = ( ( weightedBlendVar6454.x*texArray4894 + weightedBlendVar6454.y*texArray4913 + weightedBlendVar6454.z*texArray4878 )/( weightedBlendVar6454.x + weightedBlendVar6454.y + weightedBlendVar6454.z ) );
			fixed4 ifLocalVar6660 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar6660 = lerp( texArray4899 , texArray4913 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar6660 = lerp( weightedAvg6452 , weightedAvg6454 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar6660 = lerp( texArray4899 , texArray4913 , UVmixDistance );
			fixed4 ifLocalVar7634 = 0;
			UNITY_BRANCH if( _Texture_10_Albedo_Index > -1.0 )
				ifLocalVar7634 = ( ifLocalVar6660 * _Texture_10_Color );
			fixed4 Texture_10_Final = ifLocalVar7634;
			float4 texArray4928 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Albedo_Index)  );
			fixed2 temp_cast_299 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4923 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4741 ) / temp_cast_299 ), _Texture_11_Albedo_Index)  );
			float4 texArray4917 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4741 ), _Texture_11_Albedo_Index)  );
			float4 texArray4911 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4741 ), _Texture_11_Albedo_Index)  );
			float3 weightedBlendVar6459 = BlendComponents;
			float4 weightedAvg6459 = ( ( weightedBlendVar6459.x*texArray4917 + weightedBlendVar6459.y*texArray4928 + weightedBlendVar6459.z*texArray4911 )/( weightedBlendVar6459.x + weightedBlendVar6459.y + weightedBlendVar6459.z ) );
			fixed2 temp_cast_300 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4898 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4741 ) / temp_cast_300 ), _Texture_11_Albedo_Index)  );
			fixed2 temp_cast_301 = (_Texture_11_Far_Multiplier).xx;
			float4 texArray4914 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4741 ) / temp_cast_301 ), _Texture_11_Albedo_Index)  );
			float3 weightedBlendVar6461 = BlendComponents;
			float4 weightedAvg6461 = ( ( weightedBlendVar6461.x*texArray4898 + weightedBlendVar6461.y*texArray4923 + weightedBlendVar6461.z*texArray4914 )/( weightedBlendVar6461.x + weightedBlendVar6461.y + weightedBlendVar6461.z ) );
			fixed4 ifLocalVar6654 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar6654 = lerp( texArray4928 , texArray4923 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar6654 = lerp( weightedAvg6459 , weightedAvg6461 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar6654 = lerp( texArray4928 , texArray4923 , UVmixDistance );
			fixed4 ifLocalVar7640 = 0;
			UNITY_BRANCH if( _Texture_11_Albedo_Index > -1.0 )
				ifLocalVar7640 = ( ifLocalVar6654 * _Texture_11_Color );
			fixed4 Texture_11_Final = ifLocalVar7640;
			float4 texArray4954 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Albedo_Index)  );
			fixed2 temp_cast_302 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4952 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult4751 ) / temp_cast_302 ), _Texture_12_Albedo_Index)  );
			float4 texArray4926 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult4751 ), _Texture_12_Albedo_Index)  );
			float4 texArray4927 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult4751 ), _Texture_12_Albedo_Index)  );
			float3 weightedBlendVar6466 = BlendComponents;
			float4 weightedAvg6466 = ( ( weightedBlendVar6466.x*texArray4926 + weightedBlendVar6466.y*texArray4954 + weightedBlendVar6466.z*texArray4927 )/( weightedBlendVar6466.x + weightedBlendVar6466.y + weightedBlendVar6466.z ) );
			fixed2 temp_cast_303 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4919 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult4751 ) / temp_cast_303 ), _Texture_12_Albedo_Index)  );
			fixed2 temp_cast_304 = (_Texture_12_Far_Multiplier).xx;
			float4 texArray4931 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult4751 ) / temp_cast_304 ), _Texture_12_Albedo_Index)  );
			float3 weightedBlendVar6468 = BlendComponents;
			float4 weightedAvg6468 = ( ( weightedBlendVar6468.x*texArray4919 + weightedBlendVar6468.y*texArray4952 + weightedBlendVar6468.z*texArray4931 )/( weightedBlendVar6468.x + weightedBlendVar6468.y + weightedBlendVar6468.z ) );
			fixed4 ifLocalVar6672 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar6672 = lerp( texArray4954 , texArray4952 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar6672 = lerp( weightedAvg6466 , weightedAvg6468 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar6672 = lerp( texArray4954 , texArray4952 , UVmixDistance );
			fixed4 ifLocalVar7644 = 0;
			UNITY_BRANCH if( _Texture_12_Albedo_Index > -1.0 )
				ifLocalVar7644 = ( ifLocalVar6672 * _Texture_12_Color );
			fixed4 Texture_12_Final = ifLocalVar7644;
			float4 layeredBlendVar6528 = appendResult6529;
			float4 layeredBlend6528 = ( lerp( lerp( lerp( lerp( layeredBlend6520 , Texture_9_Final , layeredBlendVar6528.x ) , Texture_10_Final , layeredBlendVar6528.y ) , Texture_11_Final , layeredBlendVar6528.z ) , Texture_12_Final , layeredBlendVar6528.w ) );
			float4 texArray5043 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Albedo_Index)  );
			fixed2 temp_cast_305 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5034 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5027 ) / temp_cast_305 ), _Texture_13_Albedo_Index)  );
			float4 texArray5128 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5027 ), _Texture_13_Albedo_Index)  );
			float4 texArray5129 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5027 ), _Texture_13_Albedo_Index)  );
			float3 weightedBlendVar6473 = BlendComponents;
			float4 weightedAvg6473 = ( ( weightedBlendVar6473.x*texArray5128 + weightedBlendVar6473.y*texArray5043 + weightedBlendVar6473.z*texArray5129 )/( weightedBlendVar6473.x + weightedBlendVar6473.y + weightedBlendVar6473.z ) );
			fixed2 temp_cast_306 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5130 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5027 ) / temp_cast_306 ), _Texture_13_Albedo_Index)  );
			fixed2 temp_cast_307 = (_Texture_13_Far_Multiplier).xx;
			float4 texArray5121 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5027 ) / temp_cast_307 ), _Texture_13_Albedo_Index)  );
			float3 weightedBlendVar6475 = BlendComponents;
			float4 weightedAvg6475 = ( ( weightedBlendVar6475.x*texArray5130 + weightedBlendVar6475.y*texArray5034 + weightedBlendVar6475.z*texArray5121 )/( weightedBlendVar6475.x + weightedBlendVar6475.y + weightedBlendVar6475.z ) );
			fixed4 ifLocalVar6678 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar6678 = lerp( texArray5043 , texArray5034 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar6678 = lerp( weightedAvg6473 , weightedAvg6475 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar6678 = lerp( texArray5043 , texArray5034 , UVmixDistance );
			fixed4 ifLocalVar7648 = 0;
			UNITY_BRANCH if( _Texture_13_Albedo_Index > -1.0 )
				ifLocalVar7648 = ( ifLocalVar6678 * _Texture_13_Color );
			fixed4 Texture_13_Final = ifLocalVar7648;
			float4 texArray5202 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Albedo_Index)  );
			fixed2 temp_cast_308 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5171 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5033 ) / temp_cast_308 ), _Texture_14_Albedo_Index)  );
			float4 texArray5168 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5033 ), _Texture_14_Albedo_Index)  );
			float4 texArray5239 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5033 ), _Texture_14_Albedo_Index)  );
			float3 weightedBlendVar6480 = BlendComponents;
			float4 weightedAvg6480 = ( ( weightedBlendVar6480.x*texArray5168 + weightedBlendVar6480.y*texArray5202 + weightedBlendVar6480.z*texArray5239 )/( weightedBlendVar6480.x + weightedBlendVar6480.y + weightedBlendVar6480.z ) );
			fixed2 temp_cast_309 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5205 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5033 ) / temp_cast_309 ), _Texture_14_Albedo_Index)  );
			fixed2 temp_cast_310 = (_Texture_14_Far_Multiplier).xx;
			float4 texArray5241 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5033 ) / temp_cast_310 ), _Texture_14_Albedo_Index)  );
			float3 weightedBlendVar6482 = BlendComponents;
			float4 weightedAvg6482 = ( ( weightedBlendVar6482.x*texArray5205 + weightedBlendVar6482.y*texArray5171 + weightedBlendVar6482.z*texArray5241 )/( weightedBlendVar6482.x + weightedBlendVar6482.y + weightedBlendVar6482.z ) );
			fixed4 ifLocalVar6684 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar6684 = lerp( texArray5202 , texArray5171 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar6684 = lerp( weightedAvg6480 , weightedAvg6482 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar6684 = lerp( texArray5202 , texArray5171 , UVmixDistance );
			fixed4 ifLocalVar7652 = 0;
			UNITY_BRANCH if( _Texture_14_Albedo_Index > -1.0 )
				ifLocalVar7652 = ( ifLocalVar6684 * _Texture_14_Color );
			fixed4 Texture_14_Final = ifLocalVar7652;
			float4 texArray5259 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Albedo_Index)  );
			fixed2 temp_cast_311 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5272 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5212 ) / temp_cast_311 ), _Texture_15_Albedo_Index)  );
			float4 texArray5182 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5212 ), _Texture_15_Albedo_Index)  );
			float4 texArray5189 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5212 ), _Texture_15_Albedo_Index)  );
			float3 weightedBlendVar6487 = BlendComponents;
			float4 weightedAvg6487 = ( ( weightedBlendVar6487.x*texArray5182 + weightedBlendVar6487.y*texArray5259 + weightedBlendVar6487.z*texArray5189 )/( weightedBlendVar6487.x + weightedBlendVar6487.y + weightedBlendVar6487.z ) );
			fixed2 temp_cast_312 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5188 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5212 ) / temp_cast_312 ), _Texture_15_Albedo_Index)  );
			fixed2 temp_cast_313 = (_Texture_15_Far_Multiplier).xx;
			float4 texArray5247 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5212 ) / temp_cast_313 ), _Texture_15_Albedo_Index)  );
			float3 weightedBlendVar6489 = BlendComponents;
			float4 weightedAvg6489 = ( ( weightedBlendVar6489.x*texArray5188 + weightedBlendVar6489.y*texArray5272 + weightedBlendVar6489.z*texArray5247 )/( weightedBlendVar6489.x + weightedBlendVar6489.y + weightedBlendVar6489.z ) );
			fixed4 ifLocalVar6690 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar6690 = lerp( texArray5259 , texArray5272 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar6690 = lerp( weightedAvg6487 , weightedAvg6489 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar6690 = lerp( texArray5259 , texArray5272 , UVmixDistance );
			fixed4 ifLocalVar7656 = 0;
			UNITY_BRANCH if( _Texture_15_Albedo_Index > -1.0 )
				ifLocalVar7656 = ( ifLocalVar6690 * _Texture_15_Color );
			fixed4 Texture_15_Final = ifLocalVar7656;
			float4 texArray5139 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Albedo_Index)  );
			fixed2 temp_cast_314 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5143 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Top_Bottom * appendResult5078 ) / temp_cast_314 ), _Texture_16_Albedo_Index)  );
			float4 texArray5150 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Front_Back * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float4 texArray5145 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( Left_Right * appendResult5078 ), _Texture_16_Albedo_Index)  );
			float3 weightedBlendVar6494 = BlendComponents;
			float4 weightedAvg6494 = ( ( weightedBlendVar6494.x*texArray5150 + weightedBlendVar6494.y*texArray5139 + weightedBlendVar6494.z*texArray5145 )/( weightedBlendVar6494.x + weightedBlendVar6494.y + weightedBlendVar6494.z ) );
			fixed2 temp_cast_315 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5144 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Front_Back * appendResult5078 ) / temp_cast_315 ), _Texture_16_Albedo_Index)  );
			fixed2 temp_cast_316 = (_Texture_16_Far_Multiplier).xx;
			float4 texArray5154 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( Left_Right * appendResult5078 ) / temp_cast_316 ), _Texture_16_Albedo_Index)  );
			float3 weightedBlendVar6496 = BlendComponents;
			float4 weightedAvg6496 = ( ( weightedBlendVar6496.x*texArray5144 + weightedBlendVar6496.y*texArray5143 + weightedBlendVar6496.z*texArray5154 )/( weightedBlendVar6496.x + weightedBlendVar6496.y + weightedBlendVar6496.z ) );
			fixed4 ifLocalVar6696 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar6696 = lerp( texArray5139 , texArray5143 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar6696 = lerp( weightedAvg6494 , weightedAvg6496 , UVmixDistance );
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar6696 = lerp( texArray5139 , texArray5143 , UVmixDistance );
			fixed4 ifLocalVar7661 = 0;
			UNITY_BRANCH if( _Texture_16_Albedo_Index > -1.0 )
				ifLocalVar7661 = ( ifLocalVar6696 * _Texture_16_Color );
			fixed4 Texture_16_Final = ifLocalVar7661;
			float4 layeredBlendVar6532 = appendResult6533;
			float4 layeredBlend6532 = ( lerp( lerp( lerp( lerp( layeredBlend6528 , Texture_13_Final , layeredBlendVar6532.x ) , Texture_14_Final , layeredBlendVar6532.y ) , Texture_15_Final , layeredBlendVar6532.z ) , Texture_16_Final , layeredBlendVar6532.w ) );
			float3 appendResult3857 = float3( layeredBlend6532.x , layeredBlend6532.y , layeredBlend6532.z );
			fixed2 temp_cast_318 = (( _Geological_Map_Offset_Close + ( ( 1.0 / _Geological_Tiling_Close ) * ase_worldPos.y ) )).xx;
			fixed4 tex2DNode6968 = tex2D( _Texture_Geological_Map, temp_cast_318 );
			float3 appendResult6970 = float3( tex2DNode6968.r , tex2DNode6968.g , tex2DNode6968.b );
			fixed3 temp_cast_319 = (_Geological_Map_Close_Power).xxx;
			fixed2 temp_cast_320 = (( ( ase_worldPos.y * ( 1.0 / _Geological_Tiling_Far ) ) + _Geological_Map_Offset_Far )).xx;
			fixed4 tex2DNode6969 = tex2D( _Texture_Geological_Map, temp_cast_320 );
			float3 appendResult6971 = float3( tex2DNode6969.r , tex2DNode6969.g , tex2DNode6969.b );
			fixed3 temp_cast_321 = (_Geological_Map_Far_Power).xxx;
			fixed3 temp_cast_322 = (( ( _Texture_16_Geological_Power * Splat4_A ) + ( ( _Texture_15_Geological_Power * Splat4_B ) + ( ( _Texture_14_Geological_Power * Splat4_G ) + ( ( _Texture_13_Geological_Power * Splat4_R ) + ( ( _Texture_12_Geological_Power * Splat3_A ) + ( ( _Texture_11_Geological_Power * Splat3_B ) + ( ( _Texture_10_Geological_Power * Splat3_G ) + ( ( _Texture_9_Geological_Power * Splat3_R ) + ( ( _Texture_8_Geological_Power * Splat2_A ) + ( ( _Texture_7_Geological_Power * Splat2_B ) + ( ( _Texture_6_Geological_Power * Splat2_G ) + ( ( _Texture_5_Geological_Power * Splat2_R ) + ( ( _Texture_1_Geological_Power * Splat1_R ) + ( ( _Texture_2_Geological_Power * Splat1_G ) + ( ( _Texture_4_Geological_Power * Splat1_A ) + ( _Texture_3_Geological_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) )).xxx;
			fixed2 temp_cast_324 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray4378 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( appendResult3679 * temp_cast_324 ), (float)_Texture_Snow_Index)  );
			fixed2 temp_cast_326 = (( 1.0 / _Snow_Tiling )).xx;
			fixed2 temp_cast_327 = (_Snow_Tiling_Far_Multiplier).xx;
			float4 texArray4379 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Albedo, float3(( ( appendResult3679 * temp_cast_326 ) / temp_cast_327 ), (float)_Texture_Snow_Index)  );
			float3 appendResult1410 = float3( ( lerp( texArray4378 , texArray4379 , UVmixDistance ) * _Snow_Color ).x , ( lerp( texArray4378 , texArray4379 , UVmixDistance ) * _Snow_Color ).y , ( lerp( texArray4378 , texArray4379 , UVmixDistance ) * _Snow_Color ).z );
			o.Albedo = lerp( ( saturate( ( fixed4( appendResult3857 , 0.0 ) + fixed4( ( lerp( ( ( appendResult6970 + float3( -0.3,-0.3,-0.3 ) ) * temp_cast_319 ) , ( ( appendResult6971 + float3( -0.3,-0.3,-0.3 ) ) * temp_cast_321 ) , UVmixDistance ) * temp_cast_322 ) , 0.0 ) ) )) , fixed4( appendResult1410 , 0.0 ) , HeightMask6539 ).rgb;
			fixed3 temp_cast_331 = (_Terrain_Specular).xxx;
			fixed3 temp_cast_332 = (_Snow_Specular).xxx;
			o.Specular = lerp( ( ( appendResult3857 * float3( 0.3,0.3,0.3 ) ) * temp_cast_331 ) , ( clamp( appendResult1410 , float3( 0,0,0 ) , float3( 0.5,0.5,0.5 ) ) * temp_cast_332 ) , HeightMask6539 );
			o.Smoothness = lerp( ( layeredBlend6532.w * _Terrain_Smoothness ) , ( lerp( texArray4378 , texArray4379 , UVmixDistance ) * _Snow_Color ).w , HeightMask6539 );
			float4 texArray7282 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3284 ), _Texture_1_Emission_AO_Index)  );
			float4 texArray7280 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3284 ), _Texture_1_Emission_AO_Index)  );
			float4 texArray7283 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3284 ), _Texture_1_Emission_AO_Index)  );
			float3 weightedBlendVar7286 = BlendComponents;
			float weightedAvg7286 = ( ( weightedBlendVar7286.x*texArray7280.w + weightedBlendVar7286.y*texArray7282.w + weightedBlendVar7286.z*texArray7283.w )/( weightedBlendVar7286.x + weightedBlendVar7286.y + weightedBlendVar7286.z ) );
			fixed ifLocalVar7287 = 0;
			UNITY_BRANCH if( _Texture_1_Triplanar > 1.0 )
				ifLocalVar7287 = texArray7282.w;
			else UNITY_BRANCH if( _Texture_1_Triplanar == 1.0 )
				ifLocalVar7287 = weightedAvg7286;
			else UNITY_BRANCH if( _Texture_1_Triplanar < 1.0 )
				ifLocalVar7287 = texArray7282.w;
			fixed ifLocalVar7676 = 0;
			UNITY_BRANCH if( _Texture_1_Emission_AO_Index > -1.0 )
				ifLocalVar7676 = lerp( ifLocalVar7287 , 1.0 , UVmixDistance );
			fixed Texture_1_E_AO = ifLocalVar7676;
			float4 texArray7293 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3349 ), _Texture_2_Emission_AO_Index)  );
			float4 texArray7304 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3349 ), _Texture_2_Emission_AO_Index)  );
			float4 texArray7294 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3349 ), _Texture_2_Emission_AO_Index)  );
			float3 weightedBlendVar7300 = BlendComponents;
			float weightedAvg7300 = ( ( weightedBlendVar7300.x*texArray7304.w + weightedBlendVar7300.y*texArray7293.w + weightedBlendVar7300.z*texArray7294.w )/( weightedBlendVar7300.x + weightedBlendVar7300.y + weightedBlendVar7300.z ) );
			fixed ifLocalVar7296 = 0;
			UNITY_BRANCH if( _Texture_2_Triplanar > 1.0 )
				ifLocalVar7296 = texArray7293.w;
			else UNITY_BRANCH if( _Texture_2_Triplanar == 1.0 )
				ifLocalVar7296 = weightedAvg7300;
			else UNITY_BRANCH if( _Texture_2_Triplanar < 1.0 )
				ifLocalVar7296 = texArray7293.w;
			fixed ifLocalVar7679 = 0;
			UNITY_BRANCH if( _Texture_2_Emission_AO_Index > -1.0 )
				ifLocalVar7679 = lerp( ifLocalVar7296 , 1.0 , UVmixDistance );
			fixed Texture_2_E_AO = ifLocalVar7679;
			float4 texArray7310 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3415 ), _Texture_3_Emission_AO_Index)  );
			float4 texArray7311 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3415 ), _Texture_3_Emission_AO_Index)  );
			float4 texArray7305 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3415 ), _Texture_3_Emission_AO_Index)  );
			float3 weightedBlendVar7308 = BlendComponents;
			float weightedAvg7308 = ( ( weightedBlendVar7308.x*texArray7311.w + weightedBlendVar7308.y*texArray7310.w + weightedBlendVar7308.z*texArray7305.w )/( weightedBlendVar7308.x + weightedBlendVar7308.y + weightedBlendVar7308.z ) );
			fixed ifLocalVar7315 = 0;
			UNITY_BRANCH if( _Texture_3_Triplanar > 1.0 )
				ifLocalVar7315 = texArray7310.w;
			else UNITY_BRANCH if( _Texture_3_Triplanar == 1.0 )
				ifLocalVar7315 = weightedAvg7308;
			else UNITY_BRANCH if( _Texture_3_Triplanar < 1.0 )
				ifLocalVar7315 = texArray7310.w;
			fixed ifLocalVar7681 = 0;
			UNITY_BRANCH if( _Texture_3_Emission_AO_Index > -1.0 )
				ifLocalVar7681 = lerp( ifLocalVar7315 , 1.0 , UVmixDistance );
			fixed Texture_3_E_AO = ifLocalVar7681;
			float4 texArray7322 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult3482 ), _Texture_4_Emission_AO_Index)  );
			float4 texArray7323 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult3482 ), _Texture_4_Emission_AO_Index)  );
			float4 texArray7317 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult3482 ), _Texture_4_Emission_AO_Index)  );
			float3 weightedBlendVar7320 = BlendComponents;
			float weightedAvg7320 = ( ( weightedBlendVar7320.x*texArray7323.w + weightedBlendVar7320.y*texArray7322.w + weightedBlendVar7320.z*texArray7317.w )/( weightedBlendVar7320.x + weightedBlendVar7320.y + weightedBlendVar7320.z ) );
			fixed ifLocalVar7327 = 0;
			UNITY_BRANCH if( _Texture_4_Triplanar > 1.0 )
				ifLocalVar7327 = texArray7322.w;
			else UNITY_BRANCH if( _Texture_4_Triplanar == 1.0 )
				ifLocalVar7327 = weightedAvg7320;
			else UNITY_BRANCH if( _Texture_4_Triplanar < 1.0 )
				ifLocalVar7327 = texArray7322.w;
			fixed ifLocalVar7683 = 0;
			UNITY_BRANCH if( _Texture_4_Emission_AO_Index > -1.0 )
				ifLocalVar7683 = lerp( ifLocalVar7327 , 1.0 , UVmixDistance );
			fixed Texture_4_E_AO = ifLocalVar7683;
			float4 layeredBlendVar7673 = appendResult6517;
			float layeredBlend7673 = ( lerp( lerp( lerp( lerp( 0.0 , clamp( Texture_1_E_AO , ( 1.0 - _Texture_1_AO_Power ) , 1.0 ) , layeredBlendVar7673.x ) , clamp( Texture_2_E_AO , ( 1.0 - _Texture_2_AO_Power ) , 1.0 ) , layeredBlendVar7673.y ) , clamp( Texture_3_E_AO , ( 1.0 - _Texture_3_AO_Power ) , 1.0 ) , layeredBlendVar7673.z ) , clamp( Texture_4_E_AO , ( 1.0 - _Texture_4_AO_Power ) , 1.0 ) , layeredBlendVar7673.w ) );
			float4 texArray7334 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4399 ), _Texture_5_Emission_AO_Index)  );
			float4 texArray7335 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4399 ), _Texture_5_Emission_AO_Index)  );
			float4 texArray7329 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4399 ), _Texture_5_Emission_AO_Index)  );
			float3 weightedBlendVar7332 = BlendComponents;
			float weightedAvg7332 = ( ( weightedBlendVar7332.x*texArray7335.w + weightedBlendVar7332.y*texArray7334.w + weightedBlendVar7332.z*texArray7329.w )/( weightedBlendVar7332.x + weightedBlendVar7332.y + weightedBlendVar7332.z ) );
			fixed ifLocalVar7339 = 0;
			UNITY_BRANCH if( _Texture_5_Triplanar > 1.0 )
				ifLocalVar7339 = texArray7334.w;
			else UNITY_BRANCH if( _Texture_5_Triplanar == 1.0 )
				ifLocalVar7339 = weightedAvg7332;
			else UNITY_BRANCH if( _Texture_5_Triplanar < 1.0 )
				ifLocalVar7339 = texArray7334.w;
			fixed ifLocalVar7744 = 0;
			UNITY_BRANCH if( _Texture_5_Emission_AO_Index > -1.0 )
				ifLocalVar7744 = lerp( ifLocalVar7339 , 1.0 , UVmixDistance );
			fixed Texture_5_E_AO = ifLocalVar7744;
			float4 texArray7346 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4471 ), _Texture_6_Emission_AO_Index)  );
			float4 texArray7347 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4471 ), _Texture_6_Emission_AO_Index)  );
			float4 texArray7341 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4471 ), _Texture_6_Emission_AO_Index)  );
			float3 weightedBlendVar7344 = BlendComponents;
			float weightedAvg7344 = ( ( weightedBlendVar7344.x*texArray7347.w + weightedBlendVar7344.y*texArray7346.w + weightedBlendVar7344.z*texArray7341.w )/( weightedBlendVar7344.x + weightedBlendVar7344.y + weightedBlendVar7344.z ) );
			fixed ifLocalVar7351 = 0;
			UNITY_BRANCH if( _Texture_6_Triplanar > 1.0 )
				ifLocalVar7351 = texArray7346.w;
			else UNITY_BRANCH if( _Texture_6_Triplanar == 1.0 )
				ifLocalVar7351 = weightedAvg7344;
			else UNITY_BRANCH if( _Texture_6_Triplanar < 1.0 )
				ifLocalVar7351 = texArray7346.w;
			fixed ifLocalVar7689 = 0;
			UNITY_BRANCH if( _Texture_6_Emission_AO_Index > -1.0 )
				ifLocalVar7689 = lerp( ifLocalVar7351 , 1.0 , UVmixDistance );
			fixed Texture_6_E_AO = ifLocalVar7689;
			float2 uv_Texture_Array_Emission_AO = i.uv_texcoord * _Texture_Array_Emission_AO_ST.xy + _Texture_Array_Emission_AO_ST.zw;
			float4 texArray7358 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(uv_Texture_Array_Emission_AO, _Texture_7_Emission_AO_Index)  );
			float4 texArray7359 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4545 ), _Texture_7_Emission_AO_Index)  );
			float4 texArray7353 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4545 ), _Texture_7_Emission_AO_Index)  );
			float3 weightedBlendVar7356 = BlendComponents;
			float weightedAvg7356 = ( ( weightedBlendVar7356.x*texArray7359.w + weightedBlendVar7356.y*texArray7358.w + weightedBlendVar7356.z*texArray7353.w )/( weightedBlendVar7356.x + weightedBlendVar7356.y + weightedBlendVar7356.z ) );
			fixed ifLocalVar7363 = 0;
			UNITY_BRANCH if( _Texture_7_Triplanar > 1.0 )
				ifLocalVar7363 = texArray7358.w;
			else UNITY_BRANCH if( _Texture_7_Triplanar == 1.0 )
				ifLocalVar7363 = weightedAvg7356;
			else UNITY_BRANCH if( _Texture_7_Triplanar < 1.0 )
				ifLocalVar7363 = texArray7358.w;
			fixed ifLocalVar7691 = 0;
			UNITY_BRANCH if( _Texture_7_Emission_AO_Index > -1.0 )
				ifLocalVar7691 = lerp( ifLocalVar7363 , 1.0 , UVmixDistance );
			fixed Texture_7_E_AO = ifLocalVar7691;
			float4 texArray7370 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4619 ), _Texture_8_Emission_AO_Index)  );
			float4 texArray7371 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4619 ), _Texture_8_Emission_AO_Index)  );
			float4 texArray7365 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4619 ), _Texture_8_Emission_AO_Index)  );
			float3 weightedBlendVar7368 = BlendComponents;
			float weightedAvg7368 = ( ( weightedBlendVar7368.x*texArray7371.w + weightedBlendVar7368.y*texArray7370.w + weightedBlendVar7368.z*texArray7365.w )/( weightedBlendVar7368.x + weightedBlendVar7368.y + weightedBlendVar7368.z ) );
			fixed ifLocalVar7375 = 0;
			UNITY_BRANCH if( _Texture_8_Triplanar > 1.0 )
				ifLocalVar7375 = texArray7370.w;
			else UNITY_BRANCH if( _Texture_8_Triplanar == 1.0 )
				ifLocalVar7375 = weightedAvg7368;
			else UNITY_BRANCH if( _Texture_8_Triplanar < 1.0 )
				ifLocalVar7375 = texArray7370.w;
			fixed ifLocalVar7693 = 0;
			UNITY_BRANCH if( _Texture_8_Emission_AO_Index > -1.0 )
				ifLocalVar7693 = lerp( ifLocalVar7375 , 1.0 , UVmixDistance );
			fixed Texture_8_E_AO = ifLocalVar7693;
			float4 layeredBlendVar7714 = appendResult6524;
			float layeredBlend7714 = ( lerp( lerp( lerp( lerp( layeredBlend7673 , clamp( Texture_5_E_AO , ( 1.0 - _Texture_5_AO_Power ) , 1.0 ) , layeredBlendVar7714.x ) , clamp( Texture_6_E_AO , ( 1.0 - _Texture_6_AO_Power ) , 1.0 ) , layeredBlendVar7714.y ) , clamp( Texture_7_E_AO , ( 1.0 - _Texture_7_AO_Power ) , 1.0 ) , layeredBlendVar7714.z ) , clamp( Texture_8_E_AO , ( 1.0 - _Texture_8_AO_Power ) , 1.0 ) , layeredBlendVar7714.w ) );
			float4 texArray7382 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4736 ), _Texture_9_Emission_AO_Index)  );
			float4 texArray7383 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4736 ), _Texture_9_Emission_AO_Index)  );
			float4 texArray7377 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4736 ), _Texture_9_Emission_AO_Index)  );
			float3 weightedBlendVar7380 = BlendComponents;
			float weightedAvg7380 = ( ( weightedBlendVar7380.x*texArray7383.w + weightedBlendVar7380.y*texArray7382.w + weightedBlendVar7380.z*texArray7377.w )/( weightedBlendVar7380.x + weightedBlendVar7380.y + weightedBlendVar7380.z ) );
			fixed ifLocalVar7387 = 0;
			UNITY_BRANCH if( _Texture_9_Triplanar > 1.0 )
				ifLocalVar7387 = texArray7382.w;
			else UNITY_BRANCH if( _Texture_9_Triplanar == 1.0 )
				ifLocalVar7387 = weightedAvg7380;
			else UNITY_BRANCH if( _Texture_9_Triplanar < 1.0 )
				ifLocalVar7387 = texArray7382.w;
			fixed ifLocalVar7709 = 0;
			UNITY_BRANCH if( _Texture_9_Emission_AO_Index > -1.0 )
				ifLocalVar7709 = lerp( ifLocalVar7387 , 1.0 , UVmixDistance );
			fixed Texture_9_E_AO = ifLocalVar7709;
			float4 texArray7394 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4738 ), _Texture_10_Emission_AO_Index)  );
			float4 texArray7395 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4738 ), _Texture_10_Emission_AO_Index)  );
			float4 texArray7389 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4738 ), _Texture_10_Emission_AO_Index)  );
			float3 weightedBlendVar7392 = BlendComponents;
			float weightedAvg7392 = ( ( weightedBlendVar7392.x*texArray7395.w + weightedBlendVar7392.y*texArray7394.w + weightedBlendVar7392.z*texArray7389.w )/( weightedBlendVar7392.x + weightedBlendVar7392.y + weightedBlendVar7392.z ) );
			fixed ifLocalVar7399 = 0;
			UNITY_BRANCH if( _Texture_10_Triplanar > 1.0 )
				ifLocalVar7399 = texArray7394.w;
			else UNITY_BRANCH if( _Texture_10_Triplanar == 1.0 )
				ifLocalVar7399 = weightedAvg7392;
			else UNITY_BRANCH if( _Texture_10_Triplanar < 1.0 )
				ifLocalVar7399 = texArray7394.w;
			fixed ifLocalVar7707 = 0;
			UNITY_BRANCH if( _Texture_10_Emission_AO_Index > -1.0 )
				ifLocalVar7707 = lerp( ifLocalVar7399 , 1.0 , UVmixDistance );
			fixed Texture_10_E_AO = ifLocalVar7707;
			float4 texArray7406 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4741 ), _Texture_11_Emission_AO_Index)  );
			float4 texArray7407 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4741 ), _Texture_11_Emission_AO_Index)  );
			float4 texArray7401 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4741 ), _Texture_11_Emission_AO_Index)  );
			float3 weightedBlendVar7404 = BlendComponents;
			float weightedAvg7404 = ( ( weightedBlendVar7404.x*texArray7407.w + weightedBlendVar7404.y*texArray7406.w + weightedBlendVar7404.z*texArray7401.w )/( weightedBlendVar7404.x + weightedBlendVar7404.y + weightedBlendVar7404.z ) );
			fixed ifLocalVar7411 = 0;
			UNITY_BRANCH if( _Texture_11_Triplanar > 1.0 )
				ifLocalVar7411 = texArray7406.w;
			else UNITY_BRANCH if( _Texture_11_Triplanar == 1.0 )
				ifLocalVar7411 = weightedAvg7404;
			else UNITY_BRANCH if( _Texture_11_Triplanar < 1.0 )
				ifLocalVar7411 = texArray7406.w;
			fixed ifLocalVar7705 = 0;
			UNITY_BRANCH if( _Texture_11_Emission_AO_Index > -1.0 )
				ifLocalVar7705 = lerp( ifLocalVar7411 , 1.0 , UVmixDistance );
			fixed Texture_11_E_AO = ifLocalVar7705;
			float4 texArray7418 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult4751 ), _Texture_12_Emission_AO_Index)  );
			float4 texArray7419 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult4751 ), _Texture_12_Emission_AO_Index)  );
			float4 texArray7413 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult4751 ), _Texture_12_Emission_AO_Index)  );
			float3 weightedBlendVar7416 = BlendComponents;
			float weightedAvg7416 = ( ( weightedBlendVar7416.x*texArray7419.w + weightedBlendVar7416.y*texArray7418.w + weightedBlendVar7416.z*texArray7413.w )/( weightedBlendVar7416.x + weightedBlendVar7416.y + weightedBlendVar7416.z ) );
			fixed ifLocalVar7423 = 0;
			UNITY_BRANCH if( _Texture_12_Triplanar > 1.0 )
				ifLocalVar7423 = texArray7418.w;
			else UNITY_BRANCH if( _Texture_12_Triplanar == 1.0 )
				ifLocalVar7423 = weightedAvg7416;
			else UNITY_BRANCH if( _Texture_12_Triplanar < 1.0 )
				ifLocalVar7423 = texArray7418.w;
			fixed ifLocalVar7703 = 0;
			UNITY_BRANCH if( _Texture_12_Emission_AO_Index > -1.0 )
				ifLocalVar7703 = lerp( ifLocalVar7423 , 1.0 , UVmixDistance );
			fixed Texture_12_E_AO = ifLocalVar7703;
			float4 layeredBlendVar7715 = appendResult6529;
			float layeredBlend7715 = ( lerp( lerp( lerp( lerp( layeredBlend7714 , clamp( Texture_9_E_AO , ( 1.0 - _Texture_9_AO_Power ) , 1.0 ) , layeredBlendVar7715.x ) , clamp( Texture_10_E_AO , ( 1.0 - _Texture_10_AO_Power ) , 1.0 ) , layeredBlendVar7715.y ) , clamp( Texture_11_E_AO , ( 1.0 - _Texture_11_AO_Power ) , 1.0 ) , layeredBlendVar7715.z ) , clamp( Texture_12_E_AO , ( 1.0 - _Texture_12_AO_Power ) , 1.0 ) , layeredBlendVar7715.w ) );
			float4 texArray7430 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5027 ), _Texture_13_Emission_AO_Index)  );
			float4 texArray7431 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5027 ), _Texture_13_Emission_AO_Index)  );
			float4 texArray7425 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5027 ), _Texture_13_Emission_AO_Index)  );
			float3 weightedBlendVar7428 = BlendComponents;
			float weightedAvg7428 = ( ( weightedBlendVar7428.x*texArray7431.w + weightedBlendVar7428.y*texArray7430.w + weightedBlendVar7428.z*texArray7425.w )/( weightedBlendVar7428.x + weightedBlendVar7428.y + weightedBlendVar7428.z ) );
			fixed ifLocalVar7435 = 0;
			UNITY_BRANCH if( _Texture_13_Triplanar > 1.0 )
				ifLocalVar7435 = texArray7430.w;
			else UNITY_BRANCH if( _Texture_13_Triplanar == 1.0 )
				ifLocalVar7435 = weightedAvg7428;
			else UNITY_BRANCH if( _Texture_13_Triplanar < 1.0 )
				ifLocalVar7435 = texArray7430.w;
			fixed ifLocalVar7701 = 0;
			UNITY_BRANCH if( _Texture_13_Emission_AO_Index > -1.0 )
				ifLocalVar7701 = lerp( ifLocalVar7435 , 1.0 , UVmixDistance );
			fixed Texture_13_E_AO = ifLocalVar7701;
			float4 texArray7442 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5033 ), _Texture_14_Emission_AO_Index)  );
			float4 texArray7443 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5033 ), _Texture_14_Emission_AO_Index)  );
			float4 texArray7437 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5033 ), _Texture_14_Emission_AO_Index)  );
			float3 weightedBlendVar7440 = BlendComponents;
			float weightedAvg7440 = ( ( weightedBlendVar7440.x*texArray7443.w + weightedBlendVar7440.y*texArray7442.w + weightedBlendVar7440.z*texArray7437.w )/( weightedBlendVar7440.x + weightedBlendVar7440.y + weightedBlendVar7440.z ) );
			fixed ifLocalVar7447 = 0;
			UNITY_BRANCH if( _Texture_14_Triplanar > 1.0 )
				ifLocalVar7447 = texArray7442.w;
			else UNITY_BRANCH if( _Texture_14_Triplanar == 1.0 )
				ifLocalVar7447 = weightedAvg7440;
			else UNITY_BRANCH if( _Texture_14_Triplanar < 1.0 )
				ifLocalVar7447 = texArray7442.w;
			fixed ifLocalVar7699 = 0;
			UNITY_BRANCH if( _Texture_14_Emission_AO_Index > -1.0 )
				ifLocalVar7699 = lerp( ifLocalVar7447 , 1.0 , UVmixDistance );
			fixed Texture_14_E_AO = ifLocalVar7699;
			float4 texArray7454 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5212 ), _Texture_15_Emission_AO_Index)  );
			float4 texArray7455 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5212 ), _Texture_15_Emission_AO_Index)  );
			float4 texArray7449 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5212 ), _Texture_15_Emission_AO_Index)  );
			float3 weightedBlendVar7452 = BlendComponents;
			float weightedAvg7452 = ( ( weightedBlendVar7452.x*texArray7455.w + weightedBlendVar7452.y*texArray7454.w + weightedBlendVar7452.z*texArray7449.w )/( weightedBlendVar7452.x + weightedBlendVar7452.y + weightedBlendVar7452.z ) );
			fixed ifLocalVar7459 = 0;
			UNITY_BRANCH if( _Texture_15_Triplanar > 1.0 )
				ifLocalVar7459 = texArray7454.w;
			else UNITY_BRANCH if( _Texture_15_Triplanar == 1.0 )
				ifLocalVar7459 = weightedAvg7452;
			else UNITY_BRANCH if( _Texture_15_Triplanar < 1.0 )
				ifLocalVar7459 = texArray7454.w;
			fixed ifLocalVar7697 = 0;
			UNITY_BRANCH if( _Texture_15_Emission_AO_Index > -1.0 )
				ifLocalVar7697 = lerp( ifLocalVar7459 , 1.0 , UVmixDistance );
			fixed Texture_15_E_AO = ifLocalVar7697;
			float4 texArray7466 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Top_Bottom * appendResult5078 ), _Texture_16_Emission_AO_Index)  );
			float4 texArray7467 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Front_Back * appendResult5078 ), _Texture_16_Emission_AO_Index)  );
			float4 texArray7461 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( Left_Right * appendResult5078 ), _Texture_16_Emission_AO_Index)  );
			float3 weightedBlendVar7464 = BlendComponents;
			float weightedAvg7464 = ( ( weightedBlendVar7464.x*texArray7467.w + weightedBlendVar7464.y*texArray7466.w + weightedBlendVar7464.z*texArray7461.w )/( weightedBlendVar7464.x + weightedBlendVar7464.y + weightedBlendVar7464.z ) );
			fixed ifLocalVar7471 = 0;
			UNITY_BRANCH if( _Texture_16_Triplanar > 1.0 )
				ifLocalVar7471 = texArray7466.w;
			else UNITY_BRANCH if( _Texture_16_Triplanar == 1.0 )
				ifLocalVar7471 = weightedAvg7464;
			else UNITY_BRANCH if( _Texture_16_Triplanar < 1.0 )
				ifLocalVar7471 = texArray7466.w;
			fixed ifLocalVar7695 = 0;
			UNITY_BRANCH if( _Texture_16_Emission_AO_Index > -1.0 )
				ifLocalVar7695 = lerp( ifLocalVar7471 , 1.0 , UVmixDistance );
			fixed Texture_16_E_AO = ifLocalVar7695;
			float4 layeredBlendVar7716 = appendResult6533;
			float layeredBlend7716 = ( lerp( lerp( lerp( lerp( layeredBlend7715 , clamp( Texture_13_E_AO , ( 1.0 - _Texture_13_AO_Power ) , 1.0 ) , layeredBlendVar7716.x ) , clamp( Texture_14_E_AO , ( 1.0 - _Texture_14_AO_Power ) , 1.0 ) , layeredBlendVar7716.y ) , clamp( Texture_15_E_AO , ( 1.0 - _Texture_15_AO_Power ) , 1.0 ) , layeredBlendVar7716.z ) , clamp( Texture_16_E_AO , ( 1.0 - _Texture_16_AO_Power ) , 1.0 ) , layeredBlendVar7716.w ) );
			fixed2 temp_cast_333 = (( 1.0 / _Snow_Tiling )).xx;
			float4 texArray7491 = UNITY_SAMPLE_TEX2DARRAY(_Texture_Array_Emission_AO, float3(( appendResult3679 * temp_cast_333 ), -1.0)  );
			fixed ifLocalVar7711 = 0;
			UNITY_BRANCH if( -1.0 > -1.0 )
				ifLocalVar7711 = clamp( lerp( texArray7491.w , 0.0 , UVmixDistance ) , ( 1.0 - _Snow_Ambient_Occlusion_Power ) , 1.0 );
			#ifdef _USE_AO_TEXTURE_ON
			float staticSwitch7665 = clamp( lerp( layeredBlend7716 , ifLocalVar7711 , HeightMask6539 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#else
			float staticSwitch7665 = clamp( ( ( 1.0 + BlendNormals( lerp( lerp( float3( 0,0,1 ) , lerp( appendResult10_g578 , appendResult10_g579 , UVmixDistance ) , clamp( ( ( _Texture_16_Perlin_Power * Splat4_A ) + ( ( _Texture_15_Perlin_Power * Splat4_B ) + ( ( _Texture_14_Perlin_Power * Splat4_G ) + ( ( _Texture_13_Perlin_Power * Splat4_R ) + ( ( _Texture_12_Perlin_Power * Splat3_A ) + ( ( _Texture_11_Perlin_Power * Splat3_B ) + ( ( _Texture_10_Perlin_Power * Splat3_G ) + ( ( _Texture_9_Perlin_Power * Splat3_R ) + ( ( _Texture_8_Perlin_Power * Splat2_A ) + ( ( _Texture_7_Perlin_Power * Splat2_B ) + ( ( _Texture_6_Perlin_Power * Splat2_G ) + ( ( _Texture_5_Perlin_Power * Splat2_R ) + ( ( _Texture_1_Perlin_Power * Splat1_R ) + ( ( _Texture_2_Perlin_Power * Splat1_G ) + ( ( _Texture_4_Perlin_Power * Splat1_A ) + ( _Texture_3_Perlin_Power * Splat1_B ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) ) , 0.0 , 1.0 ) ) , lerp( float3( 0,0,1 ) , lerp( appendResult10_g578 , appendResult10_g579 , UVmixDistance ) , ( _Snow_Perlin_Power * 0.5 ) ) , HeightMask6539 ) , normalize( lerp( lerp( normalize( layeredBlend7726 ) , lerp( normalize( layeredBlend7726 ) , appendResult10_g577 , _Snow_Blend_Normal ) , HeightMask6539 ) , UnpackScaleNormal( tex2D( _Global_Normal_Map, i.texcoord_0 ) ,_Global_Normalmap_Power ) , UVmixDistance ) ) ).y ) * 0.5 ) , ( 1.0 - _Ambient_Occlusion_Power ) , 1.0 );
			#endif
			#ifdef _USE_AO_ON
			float staticSwitch7666 = staticSwitch7665;
			#else
			float staticSwitch7666 = 1.0;
			#endif
			o.Occlusion = staticSwitch7666;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers d3d9 gles 
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows noforwardadd vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float3 worldPos : TEXCOORD6;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				float4 texcoords01 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.texcoords01 = float4( v.texcoord.xy, v.texcoord1.xy );
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord.xy = IN.texcoords01.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	Dependency "BaseMapShader" = "CTS Terrain Shader Advanced LOD"
}